#ifndef UTILS_H
#define UTILS_H

#include <stddef.h>

void u_arr_remove(size_t size, void** mem, size_t start, size_t amount);
void u_arr_append(size_t size, void** mem, size_t amount, void* data);
void fatal(const char* format, ...);

#endif
