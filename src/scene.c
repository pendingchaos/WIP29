#include "physics.h"
#include "scene.h"
#include "utils.h"

#include <assert.h>

static s_object_t* create_from_bin_obj(s_object_t** b_to_s, s_scene_t* res, b_scene_t* scene, b_object_t* obj, s_object_t* parent) {
    s_object_t* res_obj = s_obj_create(res, obj->name);
    switch (obj->type) {
    case B_OBJ_COMPONENTS: {
        if (obj->components.visible_mesh)
            s_obj_add_visible_mesh(res_obj, obj->components.visible_mesh);
        if (obj->components.has_sun_component)
            s_obj_add_sun(res_obj, obj->components.sun_color, obj->components.sun_intensity);
        if (obj->components.skybox)
            s_obj_add_sky(res_obj, obj->components.skybox);
        
        switch (obj->components.rigid_body_type) {
        case B_BODY_NONE:
            break;
        case B_BODY_STATIC:
            s_obj_add_rigid_body(res_obj, P_BODY_STATIC, obj->components.rigid_body_mass,
                                 obj->components.rigid_body_shape);
            break;
        case B_BODY_DYNAMIC:
            s_obj_add_rigid_body(res_obj, P_BODY_DYNAMIC, obj->components.rigid_body_mass,
                                 obj->components.rigid_body_shape);
            break;
        }
        break;
    }
    case B_OBJ_SCENE: {
        s_object_t** b_to_s2 = malloc(sizeof(s_object_t*)*obj->scene->object_count);
        for (size_t i = 0; i < obj->scene->object_count; i++)
            create_from_bin_obj(b_to_s2, res, obj->scene, &obj->scene->objects[i], res_obj);
        free(b_to_s2);
        break;
    }
    default:
        return NULL;
    }
    
    if (obj->parent && b_to_s[obj->parent-scene->objects])
        s_obj_set_parent(res_obj, b_to_s[obj->parent-scene->objects]);
    else if (obj->parent)
        s_obj_set_parent(res_obj, create_from_bin_obj(b_to_s, res, scene, obj->parent, parent));
    else
        s_obj_set_parent(res_obj, parent);
    
    res_obj->pos = obj->pos;
    res_obj->scale = obj->scale;
    res_obj->orientation = obj->orientation;
    
    b_to_s[obj-scene->objects] = res_obj;
    
    return res_obj;
}

s_scene_t* s_create(bin_t* bin, b_scene_t* scene) {
    s_scene_t* res = malloc(sizeof(s_scene_t));
    
    res->physics_world = p_world_create(m_vec3_create(0, -9.8, 0));
    
    res->obj_count = 0;
    res->objs = NULL;
    res->first_sun = NULL;
    res->first_sky = NULL;
    
    s_object_t** b_to_s = malloc(sizeof(s_object_t*)*scene->object_count);
    for (size_t i = 0; i < scene->object_count; i++)
        create_from_bin_obj(b_to_s, res, scene, &scene->objects[i], NULL);
    free(b_to_s);
    
    return res;
}

void s_del(s_scene_t* scene) {
    while (scene->obj_count) s_obj_del(scene->objs[0]);
    free(scene->objs);
    p_world_del(scene->physics_world);
    free(scene);
}

s_object_t* s_obj_create(s_scene_t* scene, const char* name) {
    s_object_t* obj = malloc(sizeof(s_object_t));
    obj->scene = scene;
    obj->name = malloc(strlen(name)+1);
    memcpy(obj->name, name, strlen(name)+1);
    obj->components = 0;
    obj->pos = m_vec3_create(0, 0, 0);
    obj->scale = m_vec3_create(1, 1, 1);
    obj->orientation = m_quat_create(0, 0, 0, 1);
    obj->parent = NULL;
    obj->child_count = 0;
    
    u_arr_append(scene->obj_count*sizeof(s_object_t*), (void**)&scene->objs, sizeof(s_object_t*), &obj);
    scene->obj_count++;
    
    return obj;
}

void s_obj_del(s_object_t* obj) {
    s_obj_remove_components(obj, obj->components);
    
    int index = -1;
    for (int i = 0; i < obj->scene->obj_count; i++) {
        if (obj->scene->objs[i] == obj) {
            index = i;
            break;
        }
    }
    assert(index >= 0);
    
    const size_t os = sizeof(s_object_t*);
    u_arr_remove(obj->scene->obj_count*os, (void**)&obj->scene->objs, index*os, os);
    obj->scene->obj_count--;
    
    for (size_t i = 0; i < obj->child_count; i++)
        s_obj_set_parent(obj->children[i], obj->parent);
    s_obj_set_parent(obj, NULL);
    
    free(obj->name);
    free(obj);
}

void s_obj_set_parent(s_object_t* obj, s_object_t* parent) {
    size_t os = sizeof(s_object_t*);
    
    if (obj->parent) {
        s_object_t* cp = obj->parent;
        
        int child_index = -1;
        for (int i = 0; i < cp->child_count; i++) {
            if (cp->children[i] == obj) {
                child_index = i;
                break;
            }
        }
        assert(child_index != -1);
        
        u_arr_remove(cp->child_count--*os, (void**)&cp->children, child_index*os, os);
    }
    
    obj->parent = parent;
    if (parent)
        u_arr_append(parent->child_count++*os, (void**)&parent->children, os, &obj);
}

void s_obj_add_visible_mesh(s_object_t* obj, b_mesh_t* mesh) {
    obj->components |= S_OBJ_VISIBLE_MESH;
    obj->visible_mesh = mesh;
}

static void get_transform(m_vec3_t* pos, m_quat_t* quat, void* userdata) {
    s_object_t* obj = userdata;
    *pos = obj->pos;
    *quat = obj->orientation;
}

static void set_transform(m_vec3_t pos, m_quat_t quat, void* userdata) {
    s_object_t* obj = userdata;
    obj->pos = pos;
    obj->orientation = quat;
}

void s_obj_add_rigid_body(s_object_t* obj, p_body_type_t type, float mass, b_physical_shape_t* shape) {
    obj->components |= S_OBJ_RIGID_BODY;
    p_body_info_t info;
    info.type = type;
    info.mass = mass;
    info.shape = b_get_p_shape(shape);
    info.get_transform = &get_transform;
    info.set_transform = &set_transform;
    obj->rigid_body = p_body_create(obj->scene->physics_world, info);
}

void s_obj_add_update(s_object_t* obj, s_update_component_t component) {
    obj->components |= S_OBJ_UPDATE;
    obj->update = component;
}

void s_obj_add_sun(s_object_t* obj, const float color[3], float intensity) {
    obj->components |= S_OBJ_SUN;
    for (uint32_t i = 0; i < 3; i++) obj->sun.color[i] = color[i];
    obj->sun.intensity = intensity;
    obj->sun.prev = NULL;
    obj->sun.next = obj->scene->first_sun;
    
    if (obj->scene->first_sun) obj->scene->first_sun->sun.prev = obj;
    obj->scene->first_sun = obj;
}

void s_obj_add_sky(s_object_t* obj, b_cubemap_t* skybox) {
    obj->components |= S_OBJ_SKY;
    obj->sky.skybox = skybox;
    obj->sky.prev = NULL;
    obj->sky.next = obj->scene->first_sky;
    
    if (obj->scene->first_sky) obj->scene->first_sky->sky.prev = obj;
    obj->scene->first_sky = obj;
}

void s_obj_remove_components(s_object_t* obj, uint32_t components) {
    if (components&S_OBJ_VISIBLE_MESH && obj->components&S_OBJ_VISIBLE_MESH)
        obj->components &= ~S_OBJ_VISIBLE_MESH;
    
    if (components&S_OBJ_RIGID_BODY && obj->components&S_OBJ_RIGID_BODY) {
        p_body_del(obj->rigid_body);
        obj->components &= ~S_OBJ_RIGID_BODY;
    }
    
    if (components&S_OBJ_UPDATE && obj->components&S_OBJ_UPDATE) {
        obj->update.on_destroy(obj);
        obj->components &= ~S_OBJ_UPDATE;
    }
    
    if (components&S_OBJ_SUN && obj->components&S_OBJ_SUN) {
        if (obj->sun.next) obj->sun.next->sun.prev = obj->sun.prev;
        if (obj->sun.prev) obj->sun.prev->sun.next = obj->sun.next;
        else obj->scene->first_sun = obj->sun.next;
    }
    
    if (components&S_OBJ_SKY && obj->components&S_OBJ_SKY) {
        if (obj->sky.next) obj->sky.next->sky.prev = obj->sky.prev;
        if (obj->sky.prev) obj->sky.prev->sky.next = obj->sky.next;
        else obj->scene->first_sky = obj->sky.next;
    }
}

static void calc_obj_mat_internal(m_mat4_t* restrict res, const s_object_t* restrict obj) {
    m_mat4_t t0, t1;
    m_mat4_create_translate(&t0, obj->pos.x, obj->pos.y, obj->pos.z);
    m_mat4_create_rotate(res, obj->orientation.x, obj->orientation.y, obj->orientation.z, obj->orientation.w);
    m_mat4_mul(&t1, &t0, res);
    m_mat4_create_scale(&t0, obj->scale.x, obj->scale.y, obj->scale.z);
    m_mat4_mul(res, &t1, &t0);
}

static void calc_obj_mat(m_mat4_t* restrict res, const s_object_t* restrict obj) {
    if (obj->parent) {
        m_mat4_t t0, t1;
        calc_obj_mat(&t0, obj->parent);
        calc_obj_mat_internal(&t1, obj);
        m_mat4_mul(res, &t0, &t1);
    } else {
        calc_obj_mat_internal(res, obj);
    }
}

void s_frame(s_scene_t* scene, m_vec3_t cam_pos, m_vec3_t cam_dir, m_vec3_t cam_up, float frametime) {
    if (!scene->first_sun) {
        fprintf(stderr, "No sun in scene\n");
        return;
    }
    
    if (!scene->first_sky) {
        fprintf(stderr, "No sky in scene\n");
        return;
    }
    
    p_world_step(scene->physics_world, frametime, 4, 1/60.0);
    
    for (size_t i = 0; i < scene->obj_count; i++) {
        s_object_t* obj = scene->objs[i];
        if (obj->components & S_OBJ_UPDATE)
            scene->objs[i]->update.on_update(scene->objs[i], frametime);
    }
    
    m_vec3_normalize(&cam_dir, &cam_dir);
    m_vec3_normalize(&cam_up, &cam_up);
    
    //p_world_debug_draw(scene->physics_world);
    
    r_obj_t* robjs = malloc(sizeof(r_obj_t)*scene->obj_count);
    uint32_t robj_count = 0;
    for (size_t i = 0; i < scene->obj_count; i++) {
        s_object_t* obj = scene->objs[i];
        if (obj->components & S_OBJ_VISIBLE_MESH) {
            r_obj_t* robj = &robjs[robj_count++];
            robj->mesh = b_get_r_mesh(obj->visible_mesh);
            calc_obj_mat(&robj->mat, obj);
        }
    }
    
    r_scene_t r_scene;
    r_scene.cam_pos = cam_pos;
    r_scene.cam_dir = cam_dir;
    r_scene.cam_up = cam_up;
    r_scene.sun = scene->first_sun;
    r_scene.skybox = b_get_r_cubemap(scene->first_sky->sky.skybox);
    r_scene.obj_count = robj_count;
    r_scene.objs = robjs;
    
    r_frame(&r_scene);
    
    free(robjs);
}
