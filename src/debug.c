#include "debug.h"

#include <stdlib.h>
#include <stddef.h>

size_t d_line_count = 0;
size_t d_line_capacity = 0;
d_line_t* d_lines = NULL;
size_t d_tri_count = 0;
size_t d_tri_capacity = 0;
d_tri_t* d_tris = NULL;

void d_line(m_vec3_t a, m_vec3_t b, float red, float green, float blue) {
    if (d_line_count == d_line_capacity) {
        d_line_capacity *= 2;
        if (d_line_capacity < 1) d_line_capacity = 1;
        d_lines = realloc(d_lines, d_line_capacity*sizeof(d_line_t));
    }
    
    d_lines[d_line_count].a = a;
    d_lines[d_line_count].b = b;
    d_lines[d_line_count].color[0] = red;
    d_lines[d_line_count].color[1] = green;
    d_lines[d_line_count++].color[2] = blue;
}

void d_line_clear() {
    free(d_lines);
    d_line_count = 0;
    d_line_capacity = 0;
    d_lines = NULL;
}

void d_tri(m_vec3_t a, m_vec3_t b, m_vec3_t c, float red, float green, float blue) {
    if (d_tri_count == d_tri_capacity) {
        d_tri_capacity *= 2;
        if (d_tri_capacity < 1) d_tri_capacity = 1;
        d_tris = realloc(d_tris, d_tri_capacity*sizeof(d_tri_t));
    }
    
    d_tris[d_tri_count].a = a;
    d_tris[d_tri_count].b = b;
    d_tris[d_tri_count].c = c;
    d_tris[d_tri_count].color[0] = red;
    d_tris[d_tri_count].color[1] = green;
    d_tris[d_tri_count++].color[2] = blue;
}

void d_tri_clear() {
    free(d_tris);
    d_tri_count = 0;
    d_tri_capacity = 0;
    d_tris = NULL;
}
