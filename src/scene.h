#ifndef SCENE_H
#define SCENE_H

#include <stddef.h>

#include "physics.h"
#include "render.h"
#include "bin.h"

typedef struct p_world_t p_world_t;
typedef struct p_body_t p_body_t;

typedef struct s_scene_t s_scene_t;
typedef struct s_object_t s_object_t;
typedef struct s_update_component_t s_update_component_t;
typedef struct s_sun_component_t s_sun_component_t;
typedef struct s_sky_component_t s_sky_component_t;
    
typedef enum s_obj_component_t {
    S_OBJ_VISIBLE_MESH = 1 << 0,
    S_OBJ_RIGID_BODY = 1 << 1,
    S_OBJ_UPDATE = 1 << 2,
    S_OBJ_SUN = 1 << 3,
    S_OBJ_SKY = 1 << 4
} s_obj_component_t;

struct s_scene_t {
    p_world_t* physics_world;
    
    size_t obj_count;
    s_object_t** objs;
    
    s_object_t* first_sun;
    s_object_t* first_sky;
};

struct s_update_component_t {
    void (*on_destroy)(s_object_t*);
    void (*on_update)(s_object_t*, float);
    void* data;
};

struct s_sun_component_t {
    s_object_t* prev;
    s_object_t* next;
    float color[3];
    float intensity;
};

struct s_sky_component_t {
    s_object_t* prev;
    s_object_t* next;
    b_cubemap_t* skybox;
};

struct s_object_t {
    s_scene_t* scene;
    char* name;
    uint32_t components;
    
    m_vec3_t pos;
    m_vec3_t scale;
    m_quat_t orientation;
    
    b_mesh_t* visible_mesh;
    p_body_t* rigid_body;
    s_update_component_t update;
    s_sun_component_t sun;
    s_sky_component_t sky;
    
    s_object_t* parent;
    size_t child_count;
    s_object_t** children;
};

s_scene_t* s_create(bin_t* bin, b_scene_t* scene);
void s_del(s_scene_t* scene);
void s_frame(s_scene_t* scene, m_vec3_t cam_pos, m_vec3_t cam_dir, m_vec3_t cam_up, float frametime);

s_object_t* s_obj_create(s_scene_t* scene, const char* name);
void s_obj_del(s_object_t* obj);
void s_obj_set_parent(s_object_t* obj, s_object_t* parent);
void s_obj_add_visible_mesh(s_object_t* obj, b_mesh_t* mesh);
void s_obj_add_rigid_body(s_object_t* obj, p_body_type_t type, float mass, b_physical_shape_t* shape);
void s_obj_add_update(s_object_t* obj, s_update_component_t component);
void s_obj_add_sun(s_object_t* obj, const float color[3], float intensity);
void s_obj_add_sky(s_object_t* obj, b_cubemap_t* skybox);
void s_obj_remove_components(s_object_t* obj, uint32_t components);
#endif
