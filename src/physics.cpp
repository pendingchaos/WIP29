#define restrict
extern "C" {
#include "physics.h"
#include "debug.h"
}

#include <btBulletCollisionCommon.h>
#include <btBulletDynamicsCommon.h>

struct p_world_t {
    btCollisionConfiguration *configuration;
    btCollisionDispatcher *dispatcher;
    btBroadphaseInterface *broadphase;
    btConstraintSolver *solver;
    btDynamicsWorld *world;
};

struct p_shape_t {
    btCollisionShape* shape;
};

struct p_body_t {
    p_world_t* world;
    btRigidBody* body;
    void (*get_transform)(m_vec3_t*, m_quat_t*, void*);
    void (*set_transform)(m_vec3_t, m_quat_t, void*);
    void* userdata;
};

class p_motion_state_t : public btMotionState {
    public:
    virtual void getWorldTransform(btTransform& transform) const {
        m_vec3_t pos;
        m_quat_t quat;
        body->get_transform(&pos, &quat, body->userdata);
        transform = btTransform(btQuaternion(quat.x, quat.y, quat.z, quat.w),
                                btVector3(pos.x, pos.y, pos.z));
    }
    
    virtual void setWorldTransform(const btTransform& transform) {
        body->set_transform(m_vec3_create(transform.getOrigin().getX(),
                                          transform.getOrigin().getY(),
                                          transform.getOrigin().getZ()),
                            m_quat_create(transform.getRotation().getX(),
                                          transform.getRotation().getY(),
                                          transform.getRotation().getZ(),
                                          transform.getRotation().getW()),
                            body->userdata);
    }
    
    p_body_t* body;
};

class p_debug_drawer_t : public btIDebugDraw {
    public:
    p_debug_drawer_t() : debug_mode(DBG_DrawWireframe) {}
    
    virtual void drawLine(const btVector3& from, const btVector3& to, const btVector3& color) {
        d_line(m_vec3_create(from.getX(), from.getY(), from.getZ()),
               m_vec3_create(to.getX(), to.getY(), to.getZ()),
               color.getX(), color.getY(), color.getZ());
    }
    
    virtual void drawContactPoint(const btVector3 &pointOnB,
                                  const btVector3 &normalOnB,
                                  btScalar distance,
                                  int lifeTime,
                                  const btVector3 &color) {
        drawLine(pointOnB, pointOnB+normalOnB*distance, color);
    }
    
    virtual void reportErrorWarning(const char *warningString) {
        printf("%s\n", warningString);
    }
    
    virtual void draw3dText(const btVector3 &location, const char *textString) {
        
    }
    
    virtual void setDebugMode(int debug_mode_) {
        debug_mode = debug_mode_;
    }
    
    virtual int getDebugMode() const {
        return debug_mode;
    }
    
    int debug_mode;
};

extern "C" {
p_world_t* p_world_create(m_vec3_t gravity) {
    p_world_t* world = new p_world_t;
    world->configuration = new btDefaultCollisionConfiguration;
    world->dispatcher = new btCollisionDispatcher(world->configuration);
    world->broadphase = new btDbvtBroadphase;
    world->solver = new btSequentialImpulseConstraintSolver;
    world->world = new btDiscreteDynamicsWorld(world->dispatcher, world->broadphase,
                                               world->solver, world->configuration);
    world->world->setGravity(btVector3(gravity.x, gravity.y, gravity.z));
    world->world->setDebugDrawer(new p_debug_drawer_t);
    return world;
}

void p_world_del(p_world_t* world) {
    delete world->world->getDebugDrawer();
    delete world->world;
    delete world->solver;
    delete world->broadphase;
    delete world->dispatcher;
    delete world->configuration;
    delete world;
}

void p_world_step(p_world_t* world, float timestep, unsigned int max_substeps, float fixed_timestep) {
    world->world->stepSimulation(timestep, max_substeps, fixed_timestep);
}

void p_world_debug_draw(p_world_t* world) {
    world->world->debugDrawWorld();
}

p_shape_t* p_shape_create_sphere(float radius) {
    return (p_shape_t*)new btSphereShape(radius);
}

p_shape_t* p_shape_create_box(m_vec3_t half_extents) {
    return (p_shape_t*)new btBoxShape(btVector3(half_extents.x, half_extents.y, half_extents.z));
}

p_shape_t* p_shape_create_cylinder_x(float radius, float half_height) {
    return (p_shape_t*)new btCylinderShapeX(btVector3(half_height, radius, radius));
}

p_shape_t* p_shape_create_cylinder_y(float radius, float half_height) {
    return (p_shape_t*)new btCylinderShape(btVector3(radius, half_height, radius));
}

p_shape_t* p_shape_create_cylinder_z(float radius, float half_height) {
    return (p_shape_t*)new btCylinderShapeZ(btVector3(radius, radius, half_height));
}

p_shape_t* p_shape_create_capsule_x(float radius, float half_height) {
    return (p_shape_t*)new btCapsuleShapeX(radius, half_height*2);
}

p_shape_t* p_shape_create_capsule_y(float radius, float half_height) {
    return (p_shape_t*)new btCapsuleShape(radius, half_height*2);
}

p_shape_t* p_shape_create_capsule_z(float radius, float half_height) {
    return (p_shape_t*)new btCapsuleShapeZ(radius, half_height*2);
}

p_shape_t* p_shape_create_cone_x(float radius, float half_height) {
    return (p_shape_t*)new btConeShapeX(radius, half_height*2);
}

p_shape_t* p_shape_create_cone_y(float radius, float half_height) {
    return (p_shape_t*)new btConeShape(radius, half_height*2);
}

p_shape_t* p_shape_create_cone_z(float radius, float half_height) {
    return (p_shape_t*)new btConeShapeZ(radius, half_height*2);
}

p_shape_t* p_shape_create_hull(uint32_t count, m_vec3_t* positions) {
    btScalar* points = new btScalar[count*3];
    for (uint32_t i = 0; i < count; i++) {
        points[i*3] = positions[i].x;
        points[i*3+1] = positions[i].y;
        points[i*3+2] = positions[i].z;
    }
    
    p_shape_t* res = (p_shape_t*)new btConvexHullShape(points, count, sizeof(btScalar)*3);
    
    free(points);
    
    return res;
}

static btStridingMeshInterface* make_mesh(uint32_t tri_count, uint32_t* indices, m_vec3_t* positions) {
    if ((int64_t)(int)tri_count != (int64_t)tri_count) return NULL; //TODO: Handle this
    
    int* bt_indices = new int[tri_count*3];
    int max_index = -1;
    for (uint32_t i = 0; i < tri_count*3; i++) {
        bt_indices[i] = indices[i];
        max_index = bt_indices[i] > max_index ? bt_indices[i] : max_index;
    }
    
    btScalar* bt_vertices = new btScalar[(max_index+1)*3];
    for (int i = 0; i < max_index+1; i++) {
        bt_vertices[i*3] = positions[i].x;
        bt_vertices[i*3+1] = positions[i].y;
        bt_vertices[i*3+2] = positions[i].z;
    }
    
    btStridingMeshInterface* mesh = new btTriangleIndexVertexArray(tri_count, bt_indices, sizeof(int)*3,
                                                                   max_index+1, bt_vertices,
                                                                   sizeof(btScalar)*3);
    
    (void)sizeof(btInfinityMask);
    
    return mesh;
}

p_shape_t* p_shape_create_triangle_mesh(uint32_t tri_count, uint32_t* indices, m_vec3_t* positions) {
    return (p_shape_t*)new btConvexTriangleMeshShape(make_mesh(tri_count, indices, positions));
}

p_shape_t* p_shape_create_bvh_triangle_mesh(uint32_t tri_count, uint32_t* indices, m_vec3_t* positions) {
    return (p_shape_t*)new btBvhTriangleMeshShape(make_mesh(tri_count, indices, positions), true);
}

p_shape_t* p_shape_create_plane(float dist, m_vec3_t normal) {
    return (p_shape_t*)new btStaticPlaneShape(btVector3(normal.x, normal.y, normal.z), dist);
}

p_shape_t* p_shape_create_compound(uint32_t count, const p_shape_t** shapes, const m_vec3_t* positions,
                                   const m_quat_t* orientations) {
    btCompoundShape* shape = new btCompoundShape();
    for (uint32_t i = 0; i < count; i++) {
        m_vec3_t pos = positions ? positions[i] : m_vec3_create(0, 0, 0);
        m_quat_t quat = orientations ? orientations[i] : m_quat_create(0, 0, 0, 1);
        btTransform transform(btQuaternion(quat.x, quat.y, quat.z, quat.w), btVector3(pos.x, pos.y, pos.z));
        shape->addChildShape(transform, (btCollisionShape*)shapes[i]);
    }
    return (p_shape_t*)shape;
}

void p_shape_del(p_shape_t* shape) {
    btCollisionShape* bt = (btCollisionShape*)shape;
    
    btTriangleIndexVertexArray* mesh = NULL;
    if (dynamic_cast<btTriangleMeshShape*>(bt))
        mesh = (btTriangleIndexVertexArray*)((btTriangleMeshShape*)shape)->getMeshInterface();
    else if (dynamic_cast<btConvexTriangleMeshShape*>(bt))
        mesh = (btTriangleIndexVertexArray*)((btConvexTriangleMeshShape*)shape)->getMeshInterface();
    
    if (mesh) {
        int* indices = (int*)mesh->getIndexedMeshArray()[0].m_triangleIndexBase;
        btScalar* vertices = (btScalar*)mesh->getIndexedMeshArray()[0].m_vertexBase;
        delete [] indices;
        delete [] vertices;
        delete mesh;
    }
    
    delete bt;
}

p_body_t* p_body_create(p_world_t* world, p_body_info_t info) {
    btCollisionShape* shape = (btCollisionShape*)info.shape;
    
    btVector3 inertia;
    shape->calculateLocalInertia(info.mass, inertia);
    
    btMotionState* motion_state;
    if (info.get_transform) motion_state = new p_motion_state_t;
    else motion_state = new btDefaultMotionState;
    
    btRigidBody::btRigidBodyConstructionInfo bt_info(info.mass, motion_state, shape, inertia);
    bt_info.m_mass = info.type == P_BODY_STATIC ? 0 : info.mass;
    
    p_body_t* res = new p_body_t;
    res->world = world;
    res->get_transform = info.get_transform;
    res->set_transform = info.set_transform;
    res->userdata = info.userdata;
    if (info.get_transform) ((p_motion_state_t*)motion_state)->body = res;
    
    res->body = new btRigidBody(bt_info);
    
    world->world->addRigidBody(res->body, -1, -1); //-1 == 0xFFFF
    
    return res;
}

void p_body_del(p_body_t* body) {
    body->world->world->removeRigidBody(body->body);
    delete body->body->getMotionState();
    delete body->body;
    delete body;
}

void p_body_set_transform(p_body_t* body, m_vec3_t position, m_quat_t orientation) {
    btTransform transform(btQuaternion(orientation.x, orientation.y, orientation.z, orientation.w),
                          btVector3(position.x, position.y, position.z));
    body->body->setCenterOfMassTransform(transform);
}

void p_body_get_transform(p_body_t* body, m_vec3_t* position, m_quat_t* orientation) {
    btTransform transform = body->body->getCenterOfMassTransform();
    position->x = transform.getOrigin().getX();
    position->y = transform.getOrigin().getY();
    position->z = transform.getOrigin().getZ();
    orientation->x = transform.getRotation().getX();
    orientation->y = transform.getRotation().getY();
    orientation->z = transform.getRotation().getZ();
    orientation->w = transform.getRotation().getW();
}
}
