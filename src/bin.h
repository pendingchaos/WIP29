#ifndef BIN_H
#define BIN_H

#include <stdint.h>

#include "mathy_stuff.h"

typedef enum b_object_type_t {
    B_OBJ_COMPONENTS,
    B_OBJ_SCENE
} b_object_type_t;

typedef enum b_texture_format_t {
    B_TEX_FMT_RGBA8_UNORM,
    B_TEX_FMT_RGB8_UNORM,
    B_TEX_FMT_SRGBA8_UNORM,
    B_TEX_FMT_SRGB8_UNORM
} b_texture_format_t;

typedef enum filter_t {
    B_FILTER_NEAREST,
    B_FILTER_LINEAR
} filter_t;

typedef enum b_shader_type_t {
    B_SHDR_TYPE_VERTEX,
    B_SHDR_TYPE_FRAGMENT,
    B_SHDR_TYPE_COMPUTE
} b_shader_type_t;

typedef enum b_physical_shape_type_t {
    B_SHAPE_SPHERE,
    B_SHAPE_BOX,
    B_SHAPE_CYLINDER_X,
    B_SHAPE_CYLINDER_Y,
    B_SHAPE_CYLINDER_Z,
    B_SHAPE_CAPSULE_X,
    B_SHAPE_CAPSULE_Y,
    B_SHAPE_CAPSULE_Z,
    B_SHAPE_CONE_X,
    B_SHAPE_CONE_Y,
    B_SHAPE_CONE_Z,
    B_SHAPE_HULL,
    B_SHAPE_TRIANGLE_MESH,
    B_SHAPE_BVH_TRIANGLE_MESH,
    B_SHAPE_PLANE,
    B_SHAPE_COMPOUND
} b_physical_shape_type_t;

typedef enum b_rigid_body_type_t {
    B_BODY_NONE,
    B_BODY_STATIC,
    B_BODY_DYNAMIC
} b_rigid_body_type_t;

typedef struct r_texture_t r_texture_t;
typedef struct r_cubemap_t r_cubemap_t;
typedef struct r_shader_t r_shader_t;
typedef struct r_material_t r_material_t;
typedef struct r_mesh_t r_mesh_t;
typedef struct p_shape_t p_shape_t;

typedef struct b_obj_components_t b_obj_components_t;
typedef struct b_texture_t b_texture_t;
typedef struct b_cubemap_t b_cubemap_t;
typedef struct b_shader_t b_shader_t;
typedef struct b_material_t b_material_t;
typedef struct b_mesh_t b_mesh_t;
typedef struct b_physical_shape_t b_physical_shape_t;
typedef struct b_sun_t b_sun_t;
typedef struct b_object_t b_object_t;
typedef struct b_scene_t b_scene_t;
typedef struct bin_t bin_t;

struct b_obj_components_t {
    b_mesh_t* visible_mesh;
    
    b_rigid_body_type_t rigid_body_type;
    b_physical_shape_t* rigid_body_shape;
    float rigid_body_mass;
    
    bool has_sun_component;
    float sun_color[3];
    float sun_intensity;
    
    b_cubemap_t* skybox;
    
    //TODO: Update component
};

struct b_texture_t {
    char* name;
    b_texture_format_t format;
    uint32_t width, height;
    uint32_t mipmaps;
    void** data;
    
    r_texture_t* _render_texture;
};

struct b_cubemap_t {
    char* name;
    uint32_t width, height;
    uint8_t* data[6]; //sRGB8 (no padding)
    
    r_cubemap_t* _render_cubemap;
};

struct b_shader_t {
    char* name;
    b_shader_type_t type;
    uint32_t spirv_count;
    char* glsl_source;
    uint32_t* spirv;
    
    r_shader_t* _render_shader;
};

struct b_material_t {
    char* name;
    b_shader_t* fragment;
    uint32_t frag_data_size;
    uint32_t tex_slot_count;
    void* frag_data;
    b_texture_t** tex_slots;
    filter_t* tex_filters;
    
    r_material_t* _render_material;
};

struct b_mesh_t {
    char* name;
    uint32_t vertex_count;
    uint32_t index_count;
    float* positions;
    float* normals;
    float* tangents;
    float* uvs;
    uint32_t* indices;
    b_material_t* material;
    
    r_mesh_t* _render_mesh;
};

struct b_physical_shape_t {
    char* name;
    b_physical_shape_type_t type;
    union {
        m_vec3_t half_extents; //box
        struct {float radius; float half_height;}; //sphere, cylinder, capsule and cone
        b_mesh_t* mesh; //hull, triangle mesh, bvh triangle mesh
        struct {float dist; m_vec3_t normal;}; //plane
        struct { //compound
            uint32_t count;
            b_physical_shape_t** shapes;
            m_vec3_t* positions;
            m_quat_t* orientations;
        };
    };
    
    p_shape_t* _physics_shape;
};

struct b_sun_t {
    float color[3];
    float intensity;
};

struct b_object_t {
    char* name;
    b_object_t* parent;
    m_vec3_t pos;
    m_vec3_t scale;
    m_quat_t orientation;
    b_object_type_t type;
    union {
        b_obj_components_t components;
        b_scene_t* scene;
    };
};

struct b_scene_t {
    char* name;
    uint32_t object_count;
    b_object_t* objects;
};

struct bin_t {
    uint32_t scene_count;
    b_scene_t* scenes;
    
    uint32_t mesh_count;
    b_mesh_t* meshes;
    
    uint32_t physical_shape_count;
    b_physical_shape_t* physical_shapes;
    
    uint32_t material_count;
    b_material_t* materials;
    
    uint32_t shader_count;
    b_shader_t* shaders;
    
    uint32_t cubemap_count;
    b_cubemap_t* cubemaps;
    
    uint32_t texture_count;
    b_texture_t* textures;
};

void b_del(bin_t* bin);
bin_t* b_read(const char* filename);
bool b_write(const char* filename, bin_t* bin);
b_texture_t* b_find_texture(bin_t* bin, const char* name);
b_cubemap_t* b_find_cubemap(bin_t* bin, const char* name);
b_shader_t* b_find_shader(bin_t* bin, const char* name);
b_material_t* b_find_material(bin_t* bin, const char* name);
b_physical_shape_t* b_find_physical_shape(bin_t* bin, const char* name);
b_mesh_t* b_find_mesh(bin_t* bin, const char* name);
b_scene_t* b_find_scene(bin_t* bin, const char* name);
r_texture_t* b_get_r_texture(b_texture_t* texture);
r_cubemap_t* b_get_r_cubemap(b_cubemap_t* cubemap);
r_shader_t* b_get_r_shader(b_shader_t* shader);
p_shape_t* b_get_p_shape(b_physical_shape_t* shape);
r_material_t* b_get_r_material(b_material_t* material);
r_mesh_t* b_get_r_mesh(b_mesh_t* mesh);
#endif
