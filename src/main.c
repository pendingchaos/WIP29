#define _DEFAULT_SOURCE
#include "render.h"
#include "bin.h"
#include "utils.h"
#include "scene.h"
#include "scripting.h"
#include "physics.h"

#include <sys/stat.h>
#include <SDL2/SDL.h>
#include <stdbool.h>
#include <stdint.h>
#include <endian.h>
#include <stdio.h>
#include <lauxlib.h>
#include <lualib.h>
#include <zlib.h>

static void print_usage(char** argv) {
    printf("Usage:\n");
    printf("%s runbin <bin> <scene> <options>\n", argv[0]);
    printf("Options:\n");
    printf("    -r <renderer>\n");
    printf("    --renderer <renderer> Specify the renderer (gl or vk)\n");
    printf("\n");
    printf("%s combinebin <output> <bin1> <bin2> ... <bin n>\n", argv[0]);
    printf("\n");
    printf("%s compressbin <output bin> <input bin>\n", argv[0]);
}

static int combinebin(int argc, char** argv) {
    if (argc < 3) {
        print_usage(argv);
        return EXIT_FAILURE;
    }
    
    size_t bin_count = argc - 3;
    bin_t** bins = malloc(bin_count*sizeof(bin_t*));
    for (size_t i = 0; i < bin_count; i++) {
        bins[i] = b_read(argv[i+3]);
        if (!bins[i])
            fatal("Failed to read %s\n", argv[i+3]);
    }
    
    bin_t result;
    result.scene_count = 0;
    result.mesh_count = 0;
    result.material_count = 0;
    result.physical_shape_count = 0;
    result.shader_count = 0;
    result.cubemap_count = 0;
    result.texture_count = 0;
    
    for (size_t i = 0; i < bin_count; i++) {
        result.scene_count += bins[i]->scene_count;
        result.mesh_count += bins[i]->mesh_count;
        result.material_count += bins[i]->material_count;
        result.physical_shape_count += bins[i]->physical_shape_count;
        result.shader_count += bins[i]->shader_count;
        result.cubemap_count += bins[i]->cubemap_count;
        result.texture_count += bins[i]->texture_count;
    }
    
    result.scenes = malloc(result.scene_count*sizeof(b_scene_t));
    result.meshes = malloc(result.mesh_count*sizeof(b_mesh_t));
    result.materials = malloc(result.material_count*sizeof(b_material_t));
    result.physical_shapes = malloc(result.physical_shape_count*sizeof(b_physical_shape_t));
    result.shaders = malloc(result.shader_count*sizeof(b_shader_t));
    result.cubemaps = malloc(result.cubemap_count*sizeof(b_cubemap_t));
    result.textures = malloc(result.texture_count*sizeof(b_texture_t));
    
    size_t texture_offset = 0;
    size_t cubemap_offset = 0;
    size_t shader_offset = 0;
    size_t material_offset = 0;
    size_t physical_shape_offset = 0;
    size_t mesh_offset = 0;
    size_t scene_offset = 0;
    for (size_t i = 0; i < bin_count; i++) {
        bin_t* bin = bins[i];
        memcpy(result.textures+texture_offset, bin->textures, bin->texture_count*sizeof(b_texture_t));
        memcpy(result.cubemaps+cubemap_offset, bin->cubemaps, bin->cubemap_count*sizeof(b_cubemap_t));
        memcpy(result.shaders+shader_offset, bin->shaders, bin->shader_count*sizeof(b_shader_t));
        memcpy(result.materials+material_offset, bin->materials, bin->material_count*sizeof(b_material_t));
        memcpy(result.physical_shapes+physical_shape_offset, bin->physical_shapes, bin->physical_shape_count*sizeof(b_physical_shape_t));
        memcpy(result.meshes+mesh_offset, bin->meshes, bin->mesh_count*sizeof(b_mesh_t));
        memcpy(result.scenes+scene_offset, bin->scenes, bin->scene_count*sizeof(b_scene_t));
        
        for (size_t j = 0; j < bin->material_count; j++) {
            b_material_t* mat = &result.materials[j+material_offset];
            mat->fragment = &result.shaders[mat->fragment-bin->shaders + shader_offset];
            for (uint32_t k = 0; k < mat->tex_slot_count; k++)
                mat->tex_slots[k] = &result.textures[mat->tex_slots[k]-bin->textures + texture_offset];
        }
        
        for (size_t j = 0; j < bin->physical_shape_count; j++) {
            b_physical_shape_t* shape = &result.physical_shapes[j+physical_shape_offset];
            switch (shape->type) {
            case B_SHAPE_COMPOUND: {
                for (uint32_t k = 0; k < shape->count; k++)
                    shape->shapes[k] = &result.physical_shapes[shape->shapes[k]-bin->physical_shapes + physical_shape_offset];
                break;
            }
            case B_SHAPE_HULL:
            case B_SHAPE_TRIANGLE_MESH:
            case B_SHAPE_BVH_TRIANGLE_MESH: {
                shape->mesh = &result.meshes[shape->mesh-bin->meshes + mesh_offset];
                break;
            }
            default:
                break;
            }
        }
        
        for (size_t j = 0; j < bin->mesh_count; j++) {
            b_mesh_t* mesh = &result.meshes[j+mesh_offset];
            mesh->material = &result.materials[mesh->material-bin->materials + material_offset];
        }
        
        for (size_t j = 0; j < bin->scene_count; j++) {
            b_scene_t* scene = &result.scenes[j+scene_offset];
            for (uint32_t k = 0; k < scene->object_count; k++) {
                b_object_t* obj = &scene->objects[k];
                switch (obj->type) {
                case B_OBJ_COMPONENTS: {
                    if (obj->components.visible_mesh) {
                        uint32_t mesh_index = obj->components.visible_mesh - bin->meshes;
                        obj->components.visible_mesh = &result.meshes[mesh_index + mesh_offset];
                    }
                    
                    if (obj->components.rigid_body_type != B_BODY_NONE) {
                        uint32_t shape_index = obj->components.rigid_body_shape - bin->physical_shapes;
                        obj->components.rigid_body_shape = &result.physical_shapes[shape_index + physical_shape_offset];
                    }
                    
                    if (obj->components.skybox) {
                        uint32_t skybox_index = obj->components.skybox - bin->cubemaps;
                        obj->components.skybox = &result.cubemaps[skybox_index + cubemap_offset];
                    }
                    break;
                }
                case B_OBJ_SCENE: {
                    obj->scene = &result.scenes[obj->scene - bin->scenes + scene_offset];
                    break;
                }
                }
            }
        }
        
        texture_offset += bin->texture_count;
        cubemap_offset += bin->cubemap_count;
        shader_offset += bin->shader_count;
        material_offset += bin->material_count;
        physical_shape_offset += bin->physical_shape_count;
        mesh_offset += bin->mesh_count;
        scene_offset += bin->scene_count;
    }
    
    b_write(argv[2], &result);
    
    free(result.textures);
    free(result.cubemaps);
    free(result.shaders);
    free(result.materials);
    free(result.physical_shapes);
    free(result.meshes);
    free(result.scenes);
    
    for (size_t i = 0; i < bin_count; i++)
        b_del(bins[i]);
    free(bins);
    
    return EXIT_SUCCESS;
}

//TODO: There is similar code in bin.c
static size_t min_size_t(size_t a, size_t b) {
    return a < b ? a : b;
}

static void* read_data(FILE* stream, uint32_t amount) {
    void* data = NULL;
    size_t cur_data_size = 0;
    uint8_t buf[4096];
    size_t buf_used;
    
    while ((buf_used=fread(buf, 1, min_size_t(amount-cur_data_size, 4096), stream))) {
        u_arr_append(cur_data_size, (void**)&data, buf_used, buf);
        cur_data_size += buf_used;
    }
    
    if (ferror(stream) || cur_data_size!=amount) {
        free(data);
        return NULL;
    }
    
    return data;
}

static void compress_obj(FILE* input, FILE* output) {
    uint8_t mode;
    if (!fread(&mode, 1, 1, input)) fatal("Failed to read from input\n");
    
    uint32_t size;
    if (!fread(&size, 4, 1, input)) fatal("Failed to read from input\n");
    size = le32toh(size);
    
    void* data = NULL;
    switch (mode) {
    case 0: {
        data = read_data(input, size);
        if (!data) fatal("Failed to read from iput");
        break;
    }
    case 1: {
        uint32_t uncompressed_size;
        if (!fread(&uncompressed_size, 4, 1, input)) fatal("Failed to read from input\n");
        uncompressed_size = le32toh(uncompressed_size);
        
        if (uncompressed_size > size) fatal("Invalid input\n");
        
        void* compressed = read_data(input, size);
        if (!compressed) fatal("Failed to read from iput");
        data = malloc(uncompressed_size);
        
        uLongf res_size = uncompressed_size;
        if ((uncompress(data, &res_size, compressed, size)!=Z_OK) ||
            (res_size!=uncompressed_size)) {
            free(compressed);
            free(data);
            fatal("Failed to decompress input\n");
        }
        
        free(compressed);
        size = uncompressed_size;
        break;
    }
    default: {
        fatal("Invalid input\n");
    }
    }
    
    void* compressed = malloc(size);
    uLongf dest_len = size;
    if (compress2(compressed, &dest_len, data, size, Z_BEST_COMPRESSION) != Z_OK) {
        mode = 0;
        fwrite(&mode, 1, 1, output);
        uint32_t size_le = htole32(size);
        fwrite(&size_le, 4, 1, output);
        fwrite(data, size, 1, output);
    } else {
        mode = 1;
        fwrite(&mode, 1, 1, output);
        uint32_t size_le = htole32(dest_len);
        fwrite(&size_le, 4, 1, output);
        size_le = htole32(size);
        fwrite(&size_le, 4, 1, output);
        fwrite(compressed, dest_len, 1, output);
    }
    
    free(compressed);
    free(data);
}

static int compressbin(int argc, char** argv) {
    struct stat out_stat;
    stat(argv[2], &out_stat);
    
    struct stat in_stat;
    if (stat(argv[3], &in_stat) < 0) {
        perror(argv[3]);
        return EXIT_FAILURE;
    }
    
    if (in_stat.st_ino == out_stat.st_ino)
        fatal("%s and %s use the same inodes (they are most likely the same file)\n", argv[2], argv[2]);
    
    FILE* output = fopen(argv[2], "wb");
    FILE* input = fopen(argv[3], "rb");
    
    uint32_t texture_count, cubemap_count, shader_count, material_count;
    uint32_t mesh_count, physical_shape_count, scene_count;
    if (!fread(&texture_count, 4, 1, input)) fatal("Failed to read from input");
    if (!fread(&cubemap_count, 4, 1, input)) fatal("Failed to read from input");
    if (!fread(&shader_count, 4, 1, input)) fatal("Failed to read from input");
    if (!fread(&material_count, 4, 1, input)) fatal("Failed to read from input");
    if (!fread(&mesh_count, 4, 1, input)) fatal("Failed to read from input");
    if (!fread(&physical_shape_count, 4, 1, input)) fatal("Failed to read from input");
    if (!fread(&scene_count, 4, 1, input)) fatal("Failed to read from input");
    
    fwrite(&texture_count, 4, 1, output);
    fwrite(&cubemap_count, 4, 1, output);
    fwrite(&shader_count, 4, 1, output);
    fwrite(&material_count, 4, 1, output);
    fwrite(&physical_shape_count, 4, 1, output);
    fwrite(&mesh_count, 4, 1, output);
    fwrite(&scene_count, 4, 1, output);
    
    printf("Compressing textures...\n");
    for (uint32_t i = 0; i < le32toh(texture_count); i++) compress_obj(input, output);
    
    printf("Compressing cubemaps...\n");
    for (uint32_t i = 0; i < le32toh(cubemap_count); i++) compress_obj(input, output);
    
    printf("Compressing shaders...\n");
    for (uint32_t i = 0; i < le32toh(shader_count); i++) compress_obj(input, output);
    
    printf("Compressing materials...\n");
    for (uint32_t i = 0; i < le32toh(material_count); i++) compress_obj(input, output);
    
    printf("Compressing meshes...\n");
    for (uint32_t i = 0; i < le32toh(mesh_count); i++) compress_obj(input, output);
    
    printf("Compressing physical shapes...\n");
    for (uint32_t i = 0; i < le32toh(physical_shape_count); i++) compress_obj(input, output);
    
    printf("Compressing scenes...\n");
    for (uint32_t i = 0; i < le32toh(scene_count); i++) compress_obj(input, output);
    
    fclose(input);
    fclose(output);
    
    return EXIT_SUCCESS;
}

static int hello(lua_State* state) {
    l_func_start(0, false);
    //printf("Hello from C!\n");
    return 0;
}

static l_metatable_t* class;

static int print_int(lua_State* state) {
    l_func_start(1, false);
    //printf("%d\n", *(int*)l_to_obj(1, class));
    return 0;
}

static int c_new_int(lua_State* state) {
    l_func_start(1, true);
    int res = l_to_int_range(1, INT_MIN, INT_MAX);
    l_push_obj(class, &res);
    return 1;
}

static int c_add_int(lua_State* state) {
    l_func_start(2, true);
    int res = *(int*)l_to_obj(1, class) + *(int*)l_to_obj(2, class);
    l_push_obj(class, &res);
    return 1;
}

static int valid_ptr(lua_State* state) {
    l_func_start(0, true);
    lua_pushinteger(state, 5);
    return 1;
}

static int invalid_ptr(lua_State* state) {
    l_func_start(0, true);
    lua_pushinteger(state, 4);
    return 1;
}

static int pass_ptr(lua_State* state) {
    l_func_start(1, true);
    lua_pushinteger(state, (intptr_t)l_validate_ptr((void*)(intptr_t)l_to_int32_t(1), class));
    return 1;
}

static int runbin(int argc, char** argv) {
    if (argc < 4) {
        print_usage(argv);
        return EXIT_FAILURE;
    }
    
    //TODO: Improve this
    int argi = 4;
    renderer_t renderer = RENDERER_GL;
    while (argi != argc) {
        if (!strcmp(argv[argi], "-r") ||
            !strcmp(argv[argi], "--renderer")) {
            if (!strcmp(argv[argi+1], "vk"))
                renderer = RENDERER_VK;
            else if (!strcmp(argv[argi+1], "gl"))
                renderer = RENDERER_GL;
            else
                fatal("Invalid renderer\n");
            
            argi += 2;
        }
    }
    
    SDL_Init(SDL_INIT_VIDEO|SDL_INIT_TIMER);
    
    int pos = SDL_WINDOWPOS_UNDEFINED;
    uint32_t flags = SDL_WINDOW_RESIZABLE;
    if (renderer == RENDERER_GL) flags |= SDL_WINDOW_OPENGL;
    SDL_Window* window = SDL_CreateWindow("WIP29", pos, pos, 640, 640, flags);
    
    l_init();
    class = l_register_metatable("int", sizeof(int));
    l_register_method(class, "new", &c_new_int);
    l_register_method(class, "__add", &c_add_int);
    l_register_func("hello", &hello);
    l_register_func("print_int", &print_int);
    l_register_func("valid_ptr", &valid_ptr);
    l_register_func("invalid_ptr", &invalid_ptr);
    l_register_func("pass_ptr", &pass_ptr);
    l_ptr_new((void*)(intptr_t)5, class);
    r_init(renderer, window);
    
    luaL_openlibs(lua_state);
    
    /*if (luaL_dostring(lua_state, "print(\"Hello world from Lua!\")") != LUA_OK) {
        const char* message = lua_tostring(lua_state, -1);
        fatal("Error: %s\n", message);
        lua_pop(lua_state, -1);
    }*/
    
    if (luaL_dostring(lua_state, "hello()") != LUA_OK) {
        const char* message = lua_tostring(lua_state, -1);
        fatal("Error: %s\n", message);
        lua_pop(lua_state, -1);
    }
    
    if (luaL_dostring(lua_state, "print_int(int.new(3)+int.new(5))") != LUA_OK) {
        const char* message = lua_tostring(lua_state, -1);
        fatal("Error: %s\n", message);
        lua_pop(lua_state, -1);
    }
    
    if (luaL_dostring(lua_state, "pass_ptr(valid_ptr())") != LUA_OK) {
        const char* message = lua_tostring(lua_state, -1);
        fatal("Error: %s\n", message);
        lua_pop(lua_state, -1);
    }
    
    if (luaL_dofile(lua_state, "test.lua") != LUA_OK) {
        const char* message = lua_tostring(lua_state, -1);
        fatal("Error: %s\n", message);
        lua_pop(lua_state, -1);
    }
    
    bin_t* bin = b_read(argv[2]);
    if (!bin)
        fatal("Failed to read bin\n");
    
    bin_t* skybox_bin = b_read("Yokohama.cubemap.bin");
    if (!skybox_bin)
        fatal("Failed to read Yokohama.cubemap.bin");
    
    b_scene_t* b_scene = b_find_scene(bin, argv[3]);
    if (!b_scene)
        fatal("No such scene '%s' in '%s'\n", argv[3], argv[2]);
    
    s_scene_t* scene = s_create(bin, b_scene);
    
    if (!scene->first_sun) {
        s_object_t* sun = s_obj_create(scene, "");
        sun->orientation = m_quat_create_euler_angles(45, 45, 0);
        static const float color[3] = {1, 1, 1};
        s_obj_add_sun(sun, color, 1);
    }
    
    if (!scene->first_sky) {
        s_object_t* sky = s_obj_create(scene, "");
        s_obj_add_sky(sky, &skybox_bin->cubemaps[0]);
    }
    
    p_body_info_t info;
    info.type = P_BODY_DYNAMIC;
    info.mass = 1;
    info.shape = p_shape_create_sphere(1);
    info.set_transform = NULL;
    info.get_transform = NULL;
    info.userdata = NULL;
    p_body_t* sphere = p_body_create(scene->physics_world, info);
    p_body_set_transform(sphere, m_vec3_create(-1.554084, 17.571688, 4.180062), m_quat_create(0, 0, 0, 1));
    
    bool running = true;
    float frametime = 1 / 60.0;
    m_vec3_t cam_pos = m_vec3_create(5, 5, 5);
    float cam_angle_x = 4;
    float cam_angle_y = -0.5;
    const float cam_speed = 5;
    const float cam_rotate_speed = 0.1;
    while (running) {
        uint64_t start = SDL_GetPerformanceCounter();
        
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
            case SDL_QUIT:
                running = false;
                break;
            }
        }
        
        const uint8_t* keys = SDL_GetKeyboardState(NULL);
        
        if (SDL_GetMouseState(NULL, NULL) & SDL_BUTTON_LMASK) {
            int w, h;
            SDL_GetWindowSize(window, &w, &h);
            
            int mx, my;
            SDL_GetMouseState(&mx, &my);
            SDL_WarpMouseInWindow(window, w/2, h/2);
            
            cam_angle_x += cam_rotate_speed * frametime * (w/2-mx);
            cam_angle_y += cam_rotate_speed * frametime * (h/2-my);
            
            SDL_ShowCursor(SDL_DISABLE);
        } else {
            SDL_ShowCursor(SDL_ENABLE);
        }
        
        m_vec3_t dir = m_vec3_create(cos(cam_angle_y) * sin(cam_angle_x),
                                 sin(cam_angle_y),
                                 cos(cam_angle_y) * cos(cam_angle_x));
        
        m_vec3_t right = m_vec3_create(sin(cam_angle_x - 3.14159/2),
                                   0,
                                   cos(cam_angle_x - 3.14159/2));
        
        m_vec3_t up;
        m_vec3_cross(&up, &right, &dir);
        
        float speed = frametime * cam_speed;
        m_vec3_t speed_vec3 = m_vec3_create(speed, speed, speed);
        
        if (keys[SDL_SCANCODE_W]) {
            m_vec3_t tmp;
            m_vec3_mul(&tmp, &dir, &speed_vec3);
            m_vec3_inc(&cam_pos, &tmp);
        }
        
        if (keys[SDL_SCANCODE_S]) {
            m_vec3_t tmp;
            m_vec3_mul(&tmp, &dir, &speed_vec3);
            m_vec3_dec(&cam_pos, &tmp);
        }
        
        if (keys[SDL_SCANCODE_D]) {
            m_vec3_t tmp;
            m_vec3_mul(&tmp, &right, &speed_vec3);
            m_vec3_inc(&cam_pos, &tmp);
        }
        
        if (keys[SDL_SCANCODE_A]) {
            m_vec3_t tmp;
            m_vec3_mul(&tmp, &right, &speed_vec3);
            m_vec3_dec(&cam_pos, &tmp);
        }
        
        s_frame(scene, cam_pos, dir, up, frametime);
        
        uint64_t end = SDL_GetPerformanceCounter();
        
        frametime = (end-start) / (double)SDL_GetPerformanceFrequency();
        double fps = SDL_GetPerformanceFrequency()/(double)(end-start);
        char title[256];
        memset(title, 0, sizeof(title));
        snprintf(title, sizeof(title), "WIP29 - %.0f fps", fps);
        
        SDL_SetWindowTitle(window, title);
        
        /*m_vec3_t pos;
        m_quat_t orientation;
        p_body_get_transform(sphere, &pos, &orientation);
        printf("%f %f %f\n", pos.x, pos.y, pos.z);*/
    }
    
    s_del(scene);
    b_del(skybox_bin);
    b_del(bin);
    r_deinit();
    l_deinit();
    
    SDL_DestroyWindow(window);
    SDL_Quit();
    
    return EXIT_SUCCESS;
}

int main(int argc, char** argv) {
    if (argc < 3) {
        print_usage(argv);
        return EXIT_FAILURE;
    }
    
    if (!strcmp(argv[1], "--help") || !strcmp(argv[1], "-h")) {
        print_usage(argv);
        return EXIT_SUCCESS;
    } else if (!strcmp(argv[1], "runbin")) {
        return runbin(argc, argv);
    } else if (!strcmp(argv[1], "combinebin")) {
        return combinebin(argc, argv);
    } else if (!strcmp(argv[1], "compressbin")) {
        return compressbin(argc, argv);
    } else {
        print_usage(argv);
        return EXIT_FAILURE;
    }
}
