#include "utils.h"

#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdarg.h>
#include <stdio.h>

void u_arr_remove(size_t size, void** mem, size_t start, size_t amount) {
    memmove((uint8_t*)*mem+start, (uint8_t*)*mem+start+amount, size-start-amount);
    *mem = realloc(*mem, size-amount);
}

void u_arr_append(size_t size, void** mem, size_t amount, void* data) {
    *mem = realloc(*mem, size+amount);
    memcpy((uint8_t*)*mem+size, data, amount);
}

void fatal(const char* format, ...) {
    va_list list;
    va_start(list, format);
    vfprintf(stderr, format, list);
    va_end(list);
    exit(EXIT_FAILURE);
}
