#ifndef VK_H
#define VK_H
#include <SDL2/SDL_video.h>
#define VK_USE_PLATFORM_XLIB_KHR
#include <vulkan/vulkan.h>
#include <stdbool.h>

#define VKR(f) do {handle_vk_result((f), __FILE__, __LINE__);} while (0)

typedef struct vk_init_info_t {
    SDL_Window* window;
    VkInstance* instance;
    VkPhysicalDevice* physical_device;
    VkDevice* device;
    VkSurfaceKHR* surface;
    VkQueue* queue;
    uint32_t* queue_family_index;
} vk_init_info_t;

typedef struct vk_deinit_info_t {
    VkInstance instance;
    VkDevice device;
    VkSurfaceKHR surface;
} vk_deinit_info_t;

typedef struct vku_buffer_copy_info_t {
    VkPhysicalDevice physical_device;
    VkDevice device;
    VkQueue queue;
    uint32_t queue_family_index;
    VkCommandPool cmd_pool;
    
    VkBuffer buffer; //Must have VK_BUFFER_USAGE_TRANSFER_DST_BIT
    size_t offset;
    size_t amount;
    const void* data;
} vku_buffer_copy_info_t;

typedef struct vku_buffer_create_info_t {
    VkPhysicalDevice physical_device;
    VkDevice device;
    VkQueue queue;
    uint32_t queue_family_index;
    VkCommandPool cmd_pool;
    
    VkBuffer* buffer;
    VkDeviceMemory* memory;
    
    VkBufferUsageFlags usage;
    size_t size;
    const void* data;
} vku_buffer_create_info_t;

typedef struct vku_image_blit_info_t {
    VkPhysicalDevice physical_device;
    VkDevice device;
    VkQueue queue;
    uint32_t queue_family_index;
    VkCommandPool cmd_pool;
    
    size_t width;
    size_t height;
    
    VkAccessFlags dst_img_access;
    VkImageLayout dst_img_layout;
    VkAccessFlags dst_img_new_access;
    VkImageLayout dst_img_new_layout;
    VkImage dst_image;
    size_t dst_mipmap;
    
    uint8_t* src_image;
    bool src_srgb;
} vku_image_blit_info_t;

typedef struct vku_image_create_info_t {
    VkPhysicalDevice physical_device;
    VkDevice device;
    uint32_t queue_family_index;
    
    size_t width;
    size_t height;
    size_t mipmap_count;
    float** data; //RGBA
    
    size_t format_count;
    VkFormat* formats;
    
    VkImageUsageFlags usage_flags;
    VkDeviceMemory* memory;
    VkImage* image;
} vku_image_create_info_t;

extern PFN_vkCreateDebugReportCallbackEXT fCreateDebugReportCallbackEXT;
extern PFN_vkCreateXlibSurfaceKHR fCreateXlibSurfaceKHR;
extern PFN_vkGetPhysicalDeviceSurfaceSupportKHR fGetPhysicalDeviceSurfaceSupportKHR;
extern PFN_vkGetPhysicalDeviceSurfacePresentModesKHR fGetPhysicalDeviceSurfacePresentModesKHR;
extern PFN_vkGetPhysicalDeviceSurfaceFormatsKHR fGetPhysicalDeviceSurfaceFormatsKHR;
extern PFN_vkCreateSwapchainKHR fCreateSwapchainKHR;
extern PFN_vkGetSwapchainImagesKHR fGetSwapchainImagesKHR;
extern PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR fGetPhysicalDeviceSurfaceCapabilitiesKHR;
extern PFN_vkDestroySwapchainKHR fDestroySwapchainKHR;
extern PFN_vkDestroySurfaceKHR fDestroySurfaceKHR;
extern PFN_vkDestroyDebugReportCallbackEXT fDestroyDebugReportCallbackEXT;
extern PFN_vkAcquireNextImageKHR fAcquireNextImageKHR;
extern PFN_vkQueuePresentKHR fQueuePresentKHR;

void vku_init(vk_init_info_t info);
void vku_deinit(vk_deinit_info_t info);
void handle_vk_result(VkResult res, char* file, unsigned int line);
void vku_image_barrier(VkImageAspectFlagBits aspect, VkCommandBuffer cmd_buf,
                       VkAccessFlags src_access, VkAccessFlags dst_access,
                       VkImageLayout src_layout, VkImageLayout dst_layout,
                       VkPipelineStageFlagBits prev_stage, VkPipelineStageFlagBits next_stage,
                       VkImage image);
void vku_begin_cmd_buf(VkCommandBuffer cmd_buf, VkCommandBufferUsageFlags flags);
VkFence vku_create_fence(VkDevice device);
VkSemaphore vku_create_semaphore(VkDevice device);
VkShaderModule vku_create_shader(VkDevice device, const char* filename);
VkDeviceMemory vku_create_memory(VkPhysicalDevice physical_device, VkDevice device, uint32_t type_bits, VkDeviceSize amount, bool host_visible);
void vku_copy_buffer(vku_buffer_copy_info_t info);
void vku_create_buffer(vku_buffer_create_info_t info);
void vku_create_image(vku_image_create_info_t info);
void vku_blit_image(vku_image_blit_info_t info);
#endif
