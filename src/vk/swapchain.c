#include "swapchain.h"
#include "utils.h"
#include "vk.h"

#include <stdlib.h>
#include <stdio.h>

typedef struct swapchain_buffer_t {
    VkImage image;
    VkImageView image_view;
    VkFramebuffer framebuffer;
} swapchain_buffer_t;

struct swapchain_t {
    VkPhysicalDevice physical_device;
    VkSurfaceKHR surface;
    VkRenderPass render_pass;
    VkCommandPool cmd_pool;
    VkFormat surface_format;
    VkColorSpaceKHR color_space;
    VkDevice device;
    VkQueue queue;
    VkSwapchainKHR swapchain;
    uint32_t queue_family_index;
    uint32_t buf_count;
    swapchain_buffer_t* bufs;
    
    VkFormat depthstencil_format;
    VkDeviceMemory depthstencil_mem;
    VkImage depthstencil_image;
    VkImageView depthstencil_view;
};

static void swapchain_init(swapchain_t* swapchain, size_t width, size_t height, VkSwapchainKHR old) {
    VkSurfaceCapabilitiesKHR surf_caps;
    VKR(fGetPhysicalDeviceSurfaceCapabilitiesKHR(swapchain->physical_device, swapchain->surface, &surf_caps));
    if (!(surf_caps.supportedUsageFlags & VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT))
        fatal("VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT not supported\n");
    
    VkSwapchainCreateInfoKHR swapchain_info;
    swapchain_info.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    swapchain_info.pNext = NULL;
    swapchain_info.flags = 0;
    swapchain_info.surface = swapchain->surface;
    swapchain_info.minImageCount = surf_caps.minImageCount;
    swapchain_info.imageFormat = swapchain->surface_format;
    swapchain_info.imageColorSpace = swapchain->color_space;
    swapchain_info.imageExtent = (VkExtent2D){width, height};
    swapchain_info.imageArrayLayers = 1;
    swapchain_info.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    swapchain_info.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    swapchain_info.queueFamilyIndexCount = 0;
    swapchain_info.pQueueFamilyIndices = NULL;
    swapchain_info.preTransform = surf_caps.currentTransform;
    
    if (surf_caps.supportedCompositeAlpha & VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR)
        swapchain_info.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    else if (surf_caps.supportedCompositeAlpha & VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR)
        swapchain_info.compositeAlpha = VK_COMPOSITE_ALPHA_INHERIT_BIT_KHR;
    else
        fatal("No supported alpha composition mode supported\n");
    
    swapchain_info.presentMode = VK_PRESENT_MODE_FIFO_KHR;
    swapchain_info.clipped = VK_FALSE; //TODO: This could be VK_TRUE
    swapchain_info.oldSwapchain = old;
    
    VKR(fCreateSwapchainKHR(swapchain->device, &swapchain_info, NULL, &swapchain->swapchain));
    
    swapchain->buf_count = 0;
    VKR(fGetSwapchainImagesKHR(swapchain->device, swapchain->swapchain, &swapchain->buf_count, NULL));
    VkImage images[swapchain->buf_count];
    VKR(fGetSwapchainImagesKHR(swapchain->device, swapchain->swapchain, &swapchain->buf_count, images));
    
    swapchain->bufs = malloc(sizeof(swapchain_buffer_t)*swapchain->buf_count);
    
    VkImageCreateInfo img_info;
    img_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    img_info.pNext = NULL;
    img_info.flags = 0;
    img_info.imageType = VK_IMAGE_TYPE_2D;
    img_info.format = swapchain->depthstencil_format;
    img_info.extent = (VkExtent3D){width, height, 1};
    img_info.mipLevels = 1;
    img_info.arrayLayers = 1;
    img_info.samples = VK_SAMPLE_COUNT_1_BIT;
    img_info.tiling = VK_IMAGE_TILING_OPTIMAL;
    img_info.usage = VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT;
    img_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    img_info.queueFamilyIndexCount = 1;
    img_info.pQueueFamilyIndices = &swapchain->queue_family_index;
    img_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    VKR(vkCreateImage(swapchain->device, &img_info, NULL, &swapchain->depthstencil_image));
    
    VkMemoryRequirements depthstencil_req;
    vkGetImageMemoryRequirements(swapchain->device, swapchain->depthstencil_image, &depthstencil_req);
    
    swapchain->depthstencil_mem = vku_create_memory(swapchain->physical_device, swapchain->device,
                                                    depthstencil_req.memoryTypeBits, depthstencil_req.size,
                                                    false);
    
    vkBindImageMemory(swapchain->device, swapchain->depthstencil_image, swapchain->depthstencil_mem, 0);
    
    VkImageViewCreateInfo img_view_info;
    img_view_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
    img_view_info.pNext = NULL;
    img_view_info.flags = 0;
    img_view_info.image = swapchain->depthstencil_image;
    img_view_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
    img_view_info.format = swapchain->depthstencil_format;
    img_view_info.components = (VkComponentMapping){VK_COMPONENT_SWIZZLE_R,
                                                    VK_COMPONENT_SWIZZLE_G,
                                                    VK_COMPONENT_SWIZZLE_B,
                                                    VK_COMPONENT_SWIZZLE_A};
    img_view_info.subresourceRange = (VkImageSubresourceRange){VK_IMAGE_ASPECT_DEPTH_BIT|VK_IMAGE_ASPECT_STENCIL_BIT, 0, 1, 0, 1};
    VKR(vkCreateImageView(swapchain->device, &img_view_info, NULL, &swapchain->depthstencil_view));
    
    for (uint32_t i = 0; i < swapchain->buf_count; i++) {
        swapchain_buffer_t* buf = &swapchain->bufs[i];
        buf->image = images[i];
        
        VkImageViewCreateInfo img_view_info;
        img_view_info.sType = VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        img_view_info.pNext = NULL;
        img_view_info.flags = 0;
        img_view_info.image = buf->image;
        img_view_info.viewType = VK_IMAGE_VIEW_TYPE_2D;
        img_view_info.format = swapchain_info.imageFormat;
        img_view_info.components = (VkComponentMapping){VK_COMPONENT_SWIZZLE_R,
                                                        VK_COMPONENT_SWIZZLE_G,
                                                        VK_COMPONENT_SWIZZLE_B,
                                                        VK_COMPONENT_SWIZZLE_A};
        img_view_info.subresourceRange = (VkImageSubresourceRange){VK_IMAGE_ASPECT_COLOR_BIT, 0, 1, 0, 1};
        VKR(vkCreateImageView(swapchain->device, &img_view_info, NULL, &buf->image_view));
        
        VkImageView views[] = {buf->image_view, swapchain->depthstencil_view};
        
        VkFramebufferCreateInfo fb_info;
        fb_info.sType = VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        fb_info.pNext = NULL;
        fb_info.flags = 0;
        fb_info.renderPass = swapchain->render_pass;
        fb_info.attachmentCount = 2;
        fb_info.pAttachments = views;
        fb_info.width = width;
        fb_info.height = height;
        fb_info.layers = 1;
        VKR(vkCreateFramebuffer(swapchain->device, &fb_info, NULL, &buf->framebuffer));
    }
    
    VkCommandBufferAllocateInfo alloc_info;
    alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    alloc_info.pNext = NULL;
    alloc_info.commandPool = swapchain->cmd_pool;
    alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    alloc_info.commandBufferCount = 1;
    VkCommandBuffer cmd_buf;
    VKR(vkAllocateCommandBuffers(swapchain->device, &alloc_info, &cmd_buf));
    
    vku_begin_cmd_buf(cmd_buf, VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
    
    for (size_t i = 0; i < swapchain->buf_count; i++)
        vku_image_barrier(VK_IMAGE_ASPECT_COLOR_BIT, cmd_buf, 0, VK_ACCESS_MEMORY_READ_BIT,
                          VK_IMAGE_LAYOUT_UNDEFINED, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
                          VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
                          swapchain->bufs[i].image);
    
    vku_image_barrier(VK_IMAGE_ASPECT_DEPTH_BIT|VK_IMAGE_ASPECT_STENCIL_BIT, cmd_buf, 0,
                      VK_ACCESS_MEMORY_READ_BIT, VK_IMAGE_LAYOUT_UNDEFINED,
                      VK_IMAGE_LAYOUT_PRESENT_SRC_KHR, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
                      VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, swapchain->depthstencil_image);
    
    vkEndCommandBuffer(cmd_buf);
    VkSubmitInfo submit_info;
    submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info.pNext = NULL;
    submit_info.waitSemaphoreCount = 0;
    submit_info.pWaitSemaphores = NULL;
    submit_info.pWaitDstStageMask = 0;
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &cmd_buf;
    submit_info.signalSemaphoreCount = 0;
    submit_info.pSignalSemaphores = 0;
    
    VkFence fence = vku_create_fence(swapchain->device);
    
    vkQueueSubmit(swapchain->queue, 1, &submit_info, fence);
    
    vkWaitForFences(swapchain->device, 1, &fence, VK_TRUE, UINT64_MAX);
    vkDestroyFence(swapchain->device, fence, NULL);
    
    vkFreeCommandBuffers(swapchain->device, swapchain->cmd_pool, 1, &cmd_buf);
}

swapchain_t* swapchain_create(swapchain_create_info_t info) {
    swapchain_t* res = malloc(sizeof(swapchain_t));
    res->physical_device = info.physical_device;
    res->surface = info.surface;
    res->render_pass = info.render_pass;
    res->cmd_pool = info.cmd_pool;
    res->queue = info.queue;
    res->device = info.device;
    res->queue_family_index = info.queue_family_index;
    res->depthstencil_format = info.depthstencil_format;
    
    uint32_t fmt_count = 0;
    VKR(fGetPhysicalDeviceSurfaceFormatsKHR(info.physical_device, info.surface, &fmt_count, NULL));
    VkSurfaceFormatKHR* fmts = malloc(sizeof(VkSurfaceFormatKHR)*fmt_count);
    VKR(fGetPhysicalDeviceSurfaceFormatsKHR(info.physical_device, info.surface, &fmt_count, fmts));
    
    uint32_t i = 0;
    for (; i < fmt_count; i++) {
        res->surface_format = fmts[i].format;
        res->color_space = fmts[i].colorSpace;
        if (res->color_space != VK_COLORSPACE_SRGB_NONLINEAR_KHR)
            continue;
        if (res->surface_format!=VK_FORMAT_R8G8B8A8_UNORM &&
            res->surface_format!=VK_FORMAT_B8G8R8A8_UNORM)
            continue;
        break;
    }
    free(fmts);
    if (i == fmt_count)
        fatal("Unable to find a good surface format\n");
    *info.surface_format = res->surface_format;
    
    swapchain_init(res, info.width, info.height, VK_NULL_HANDLE);
    
    return res;
}

void swapchain_deinit(swapchain_t* swapchain) {
    for (size_t i = 0; i < swapchain->buf_count; i++) {
        vkDestroyFramebuffer(swapchain->device, swapchain->bufs[i].framebuffer, NULL);
        vkDestroyImageView(swapchain->device, swapchain->bufs[i].image_view, NULL);
    }
    free(swapchain->bufs);
    
    vkDestroyImageView(swapchain->device, swapchain->depthstencil_view, NULL);
    vkDestroyImage(swapchain->device, swapchain->depthstencil_image, NULL);
    vkFreeMemory(swapchain->device, swapchain->depthstencil_mem, NULL);
    
    fDestroySwapchainKHR(swapchain->device, swapchain->swapchain, NULL);
}

void swapchain_del(swapchain_t* swapchain) {
    swapchain_deinit(swapchain);
    free(swapchain);
}

void swapchain_acquire(swapchain_t* swapchain, VkSemaphore done_sem, swapchain_acquire_t* res) {
    res->index = 0;
    fAcquireNextImageKHR(swapchain->device, swapchain->swapchain, UINT64_MAX,
                         done_sem, VK_NULL_HANDLE, &res->index);
    res->color_image = swapchain->bufs[res->index].image;
    res->depthstencil_image = swapchain->depthstencil_image;
    res->framebuffer = swapchain->bufs[res->index].framebuffer;
}

void swapchain_present(swapchain_t* swapchain, VkSemaphore wait_sem, swapchain_acquire_t aq) {
    VkPresentInfoKHR present_info;
    present_info.sType = VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
    present_info.pNext = NULL;
    if (wait_sem != VK_NULL_HANDLE) {
        present_info.waitSemaphoreCount = 1;
        present_info.pWaitSemaphores = &wait_sem;
    } else {
        present_info.waitSemaphoreCount = 0;
        present_info.pWaitSemaphores = NULL;
    }
    present_info.swapchainCount = 1;
    present_info.pSwapchains = &swapchain->swapchain;
    present_info.pImageIndices = &aq.index;
    present_info.pResults = NULL;
    
    VKR(fQueuePresentKHR(swapchain->queue, &present_info));
}

void swapchain_resize(swapchain_t* swapchain, uint32_t width, uint32_t height) {
    VKR(vkDeviceWaitIdle(swapchain->device));
    
    swapchain_t old = *swapchain;
    swapchain_init(swapchain, width, height, swapchain->swapchain);
    swapchain_deinit(&old);
}
