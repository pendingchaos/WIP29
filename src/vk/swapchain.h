#ifndef SWAPCHAIN_H
#define SWAPCHAIN_H

#define VK_USE_PLATFORM_XLIB_KHR
#include <vulkan/vulkan.h>
#include <stdint.h>

typedef struct swapchain_t swapchain_t;

typedef struct swapchain_create_info_t {
    VkPhysicalDevice physical_device;
    VkDevice device;
    VkSurfaceKHR surface;
    VkRenderPass render_pass;
    VkCommandPool cmd_pool;
    VkQueue queue;
    size_t width;
    size_t height;
    VkFormat* surface_format;
    VkFormat depthstencil_format;
    uint32_t queue_family_index;
} swapchain_create_info_t;

typedef struct swapchain_acquire_t {
    uint32_t index;
    VkImage color_image;
    VkImage depthstencil_image;
    VkFramebuffer framebuffer;
} swapchain_acquire_t;

swapchain_t* swapchain_create(swapchain_create_info_t info);
void swapchain_del(swapchain_t* swapchain);
void swapchain_acquire(swapchain_t* swapchain, VkSemaphore done_sem, swapchain_acquire_t* res);
void swapchain_present(swapchain_t* swapchain, VkSemaphore wait_sem, swapchain_acquire_t aq);
void swapchain_resize(swapchain_t* swapchain, uint32_t width, uint32_t height);
#endif
