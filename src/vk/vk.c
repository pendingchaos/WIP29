#ifdef __GNUC__
#define _GNU_SOURCE
#else
#define _DEFAULT_SOURCE
#endif
#include "vk.h"
#include "utils.h"

#include <SDL2/SDL_syswm.h>
#include <stdbool.h>
#include <endian.h>
#include <stdlib.h>
#ifdef __GNUC__
#include <execinfo.h>
#include <dlfcn.h>
#endif
#include <stdio.h>

static const char* layers[] = {"VK_LAYER_LUNARG_standard_validation"};
static const char* extensions[] = {"VK_KHR_xlib_surface", "VK_KHR_surface", "VK_EXT_debug_report"};
static const char* device_extensions[] = {"VK_KHR_swapchain"};

PFN_vkCreateDebugReportCallbackEXT fCreateDebugReportCallbackEXT;
PFN_vkCreateXlibSurfaceKHR fCreateXlibSurfaceKHR;
PFN_vkGetPhysicalDeviceSurfaceSupportKHR fGetPhysicalDeviceSurfaceSupportKHR;
PFN_vkGetPhysicalDeviceSurfacePresentModesKHR fGetPhysicalDeviceSurfacePresentModesKHR;
PFN_vkGetPhysicalDeviceSurfaceFormatsKHR fGetPhysicalDeviceSurfaceFormatsKHR;
PFN_vkCreateSwapchainKHR fCreateSwapchainKHR;
PFN_vkGetSwapchainImagesKHR fGetSwapchainImagesKHR;
PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR fGetPhysicalDeviceSurfaceCapabilitiesKHR;
PFN_vkDestroySwapchainKHR fDestroySwapchainKHR;
PFN_vkDestroySurfaceKHR fDestroySurfaceKHR;
PFN_vkDestroyDebugReportCallbackEXT fDestroyDebugReportCallbackEXT;
PFN_vkAcquireNextImageKHR fAcquireNextImageKHR;
PFN_vkQueuePresentKHR fQueuePresentKHR;

static FILE* log_file;
static VkDebugReportCallbackEXT dbg_callbacks[3];

static void print_backtrace(FILE* output) {
    #ifdef __GNUC__
    void* trace[256];
    int depth = backtrace(trace, 256);
    for (int i = 0; i < depth; i++) {
        Dl_info info;
        if (!dladdr(trace[i], &info)) {
            fprintf(output, "%p: <not found>\n", trace[i]);
            continue;
        }
        
        fprintf(output, "    %p: %s\n", trace[i], info.dli_sname);
    }
    #else
    fprintf(output, "    Backtrace not available\n");
    #endif
}

static void debug_callback(VkDebugReportFlagsEXT flags,
                           VkDebugReportObjectTypeEXT objecttype,
                           uint64_t object,
                           size_t location,
                           int32_t messagecode,
                           const char* layerprefix,
                           const char* message,
                           void* userdata) {
    const char* obj_types[] = {
        [VK_DEBUG_REPORT_OBJECT_TYPE_UNKNOWN_EXT]               = "            Unknown",
        [VK_DEBUG_REPORT_OBJECT_TYPE_INSTANCE_EXT]              = "           Instance",
        [VK_DEBUG_REPORT_OBJECT_TYPE_PHYSICAL_DEVICE_EXT]       = "     PhysicalDevice",
        [VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_EXT]                = "             Device",
        [VK_DEBUG_REPORT_OBJECT_TYPE_QUEUE_EXT]                 = "              Queue",
        [VK_DEBUG_REPORT_OBJECT_TYPE_SEMAPHORE_EXT]             = "          Semaphore",
        [VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_BUFFER_EXT]        = "      CommandBuffer",
        [VK_DEBUG_REPORT_OBJECT_TYPE_FENCE_EXT]                 = "              Fence",
        [VK_DEBUG_REPORT_OBJECT_TYPE_DEVICE_MEMORY_EXT]         = "       DeviceMemory",
        [VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_EXT]                = "             Buffer",
        [VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_EXT]                 = "              Image",
        [VK_DEBUG_REPORT_OBJECT_TYPE_EVENT_EXT]                 = "              Event",
        [VK_DEBUG_REPORT_OBJECT_TYPE_QUERY_POOL_EXT]            = "          QueryPool",
        [VK_DEBUG_REPORT_OBJECT_TYPE_BUFFER_VIEW_EXT]           = "         BufferView",
        [VK_DEBUG_REPORT_OBJECT_TYPE_IMAGE_VIEW_EXT]            = "          ImageView",
        [VK_DEBUG_REPORT_OBJECT_TYPE_SHADER_MODULE_EXT]         = "       ShaderModule",
        [VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_CACHE_EXT]        = "      PipelineCache",
        [VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_LAYOUT_EXT]       = "     PipelineLayout",
        [VK_DEBUG_REPORT_OBJECT_TYPE_RENDER_PASS_EXT]           = "         RenderPass",
        [VK_DEBUG_REPORT_OBJECT_TYPE_PIPELINE_EXT]              = "           Pipeline",
        [VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_LAYOUT_EXT] = "DescriptorSetLayout",
        [VK_DEBUG_REPORT_OBJECT_TYPE_SAMPLER_EXT]               = "            Sampler",
        [VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_POOL_EXT]       = "     DescriptorPool",
        [VK_DEBUG_REPORT_OBJECT_TYPE_DESCRIPTOR_SET_EXT]        = "      DescriptorSet",
        [VK_DEBUG_REPORT_OBJECT_TYPE_FRAMEBUFFER_EXT]           = "        Framebuffer",
        [VK_DEBUG_REPORT_OBJECT_TYPE_COMMAND_POOL_EXT]          = "        CommandPool",
        [VK_DEBUG_REPORT_OBJECT_TYPE_SURFACE_KHR_EXT]           = "            Surface",
        [VK_DEBUG_REPORT_OBJECT_TYPE_SWAPCHAIN_KHR_EXT]         = "          SwapChain",
        [VK_DEBUG_REPORT_OBJECT_TYPE_DEBUG_REPORT_EXT]          = "        DebugReport",
    };
    
    FILE* output = userdata;
    
    if (flags & VK_DEBUG_REPORT_INFORMATION_BIT_EXT)
        fprintf(output, "Info        ");
    else if (flags & VK_DEBUG_REPORT_WARNING_BIT_EXT)
        fprintf(output, "Warning     ");
    else if (flags & VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT)
        fprintf(output, "Perf warning");
    else if (flags & VK_DEBUG_REPORT_ERROR_BIT_EXT)
        fprintf(output, "Error       ");
    else if (flags & VK_DEBUG_REPORT_DEBUG_BIT_EXT)
        fprintf(output, "Debug       ");
    else
        fprintf(output, "Unknown     ");
    fprintf(output, ":%s@0x%lx:%s: %s\n", obj_types[objecttype], object, layerprefix, message);
    
    if (output!=stdout && output!=stderr && flags&VK_DEBUG_REPORT_ERROR_BIT_EXT)
        print_backtrace(output);
    
    fflush(output);
}

static void init_debug_callbacks(VkInstance instance) {
    VkDebugReportCallbackCreateInfoEXT dbg_info;
    dbg_info.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT;
    dbg_info.pNext = NULL;
    dbg_info.flags = VK_DEBUG_REPORT_WARNING_BIT_EXT |
                     VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT;
    dbg_info.pfnCallback = (PFN_vkDebugReportCallbackEXT)&debug_callback;
    dbg_info.pUserData = stdout;
    
    VKR(fCreateDebugReportCallbackEXT(instance, &dbg_info, NULL, &dbg_callbacks[0]));
    
    dbg_info.flags = VK_DEBUG_REPORT_ERROR_BIT_EXT;
    dbg_info.pUserData = stderr;
    VKR(fCreateDebugReportCallbackEXT(instance, &dbg_info, NULL, &dbg_callbacks[1]));
    
    dbg_info.flags = VK_DEBUG_REPORT_WARNING_BIT_EXT |
                     VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT |
                     VK_DEBUG_REPORT_ERROR_BIT_EXT |
                     VK_DEBUG_REPORT_DEBUG_BIT_EXT;
    dbg_info.pUserData = log_file = fopen("vku_log.txt", "w");
    VKR(fCreateDebugReportCallbackEXT(instance, &dbg_info, NULL, &dbg_callbacks[2]));
}

static void deinit_debug_callbacks(VkInstance instance) {
    for (size_t i = 0; i < sizeof(dbg_callbacks)/sizeof(dbg_callbacks[0]); i++)
        fDestroyDebugReportCallbackEXT(instance, dbg_callbacks[i], NULL);
    fclose(log_file);
}

static void get_inst_ext_funcs(VkInstance instance) {
    fCreateDebugReportCallbackEXT = (PFN_vkCreateDebugReportCallbackEXT)
        vkGetInstanceProcAddr(instance, "vkCreateDebugReportCallbackEXT");
    fCreateXlibSurfaceKHR = (PFN_vkCreateXlibSurfaceKHR)
        vkGetInstanceProcAddr(instance, "vkCreateXlibSurfaceKHR");
    fGetPhysicalDeviceSurfaceSupportKHR =  (PFN_vkGetPhysicalDeviceSurfaceSupportKHR)
        vkGetInstanceProcAddr(instance, "vkGetPhysicalDeviceSurfaceSupportKHR");
    fGetPhysicalDeviceSurfacePresentModesKHR = (PFN_vkGetPhysicalDeviceSurfacePresentModesKHR)
        vkGetInstanceProcAddr(instance, "vkGetPhysicalDeviceSurfacePresentModesKHR");
    fGetPhysicalDeviceSurfaceFormatsKHR = (PFN_vkGetPhysicalDeviceSurfaceFormatsKHR)
        vkGetInstanceProcAddr(instance, "vkGetPhysicalDeviceSurfaceFormatsKHR");
    fGetPhysicalDeviceSurfaceCapabilitiesKHR = (PFN_vkGetPhysicalDeviceSurfaceCapabilitiesKHR)
        vkGetInstanceProcAddr(instance, "vkGetPhysicalDeviceSurfaceCapabilitiesKHR");
    fDestroySurfaceKHR = (PFN_vkDestroySurfaceKHR)
        vkGetInstanceProcAddr(instance, "vkDestroySurfaceKHR");
    fDestroyDebugReportCallbackEXT = (PFN_vkDestroyDebugReportCallbackEXT)
        vkGetInstanceProcAddr(instance, "vkDestroyDebugReportCallbackEXT");
}

static void get_dev_ext_funcs(VkDevice device) {
    fCreateSwapchainKHR = (PFN_vkCreateSwapchainKHR)
        vkGetDeviceProcAddr(device, "vkCreateSwapchainKHR");
    fDestroySwapchainKHR = (PFN_vkDestroySwapchainKHR)
        vkGetDeviceProcAddr(device, "vkDestroySwapchainKHR");
    fGetSwapchainImagesKHR = (PFN_vkGetSwapchainImagesKHR)
        vkGetDeviceProcAddr(device, "vkGetSwapchainImagesKHR");
    fAcquireNextImageKHR = (PFN_vkAcquireNextImageKHR)
        vkGetDeviceProcAddr(device, "vkAcquireNextImageKHR");
    fQueuePresentKHR = (PFN_vkQueuePresentKHR)
        vkGetDeviceProcAddr(device, "vkQueuePresentKHR");
}

static void create_instance(vk_init_info_t info) {
    VkApplicationInfo app_info;
    app_info.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    app_info.pNext = NULL;
    app_info.pApplicationName = "";
    app_info.applicationVersion = 0;
    app_info.pEngineName = "";
    app_info.engineVersion = 0;
    app_info.apiVersion = VK_MAKE_VERSION(1, 0, 0);
    
    VkInstanceCreateInfo inst_info;
    inst_info.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    inst_info.pNext = NULL;
    inst_info.flags = 0;
    inst_info.pApplicationInfo = &app_info;
    inst_info.enabledLayerCount = sizeof(layers)/sizeof(layers[0]);
    inst_info.ppEnabledLayerNames = layers;
    inst_info.enabledExtensionCount = sizeof(extensions)/sizeof(extensions[0]);
    inst_info.ppEnabledExtensionNames = extensions;
    VKR(vkCreateInstance(&inst_info, NULL, info.instance));
    
    get_inst_ext_funcs(*info.instance);
}

static int32_t get_queue_family_index(VkPhysicalDevice dev, VkSurfaceKHR surface) {
    uint32_t queue_family_count = 0;
    vkGetPhysicalDeviceQueueFamilyProperties(dev, &queue_family_count, NULL);
    VkQueueFamilyProperties* queue_families = malloc(queue_family_count*sizeof(VkQueueFamilyProperties));
    vkGetPhysicalDeviceQueueFamilyProperties(dev, &queue_family_count, queue_families);
    int32_t index = -1;
    for (uint32_t i = 0; i < queue_family_count; i++) {
        VkBool32 surf_supported;
        VKR(fGetPhysicalDeviceSurfaceSupportKHR(dev, i, surface, &surf_supported));
        if (queue_families[i].queueFlags&VK_QUEUE_GRAPHICS_BIT && surf_supported) {
            index = i;
            break;
        }
    }
    free(queue_families);
    
    return index;
}

static void choose_physical_device(vk_init_info_t info) {
    uint32_t dev_count = 0;
    VKR(vkEnumeratePhysicalDevices(*info.instance, &dev_count, NULL));
    VkPhysicalDevice devs[dev_count];
    VKR(vkEnumeratePhysicalDevices(*info.instance, &dev_count, devs));
    
    for (uint32_t i = 0; i < dev_count; i++) {
        int32_t index = get_queue_family_index(devs[i], *info.surface);
        *info.physical_device = devs[i];
        if (index >= 0) {
            *info.queue_family_index = index;
            return;
        }
    }
    
    fatal("Unable to find a suitable physical device\n");
}

static void create_device(vk_init_info_t info) {
    VkDeviceQueueCreateInfo queue_info;
    queue_info.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
    queue_info.pNext = NULL;
    queue_info.flags = 0;
    queue_info.queueFamilyIndex = *info.queue_family_index;
    queue_info.queueCount = 1;
    static const float queue_priority = 1.0;
    queue_info.pQueuePriorities = &queue_priority;
    
    VkPhysicalDeviceFeatures dev_features;
    memset(&dev_features, 0, sizeof(VkPhysicalDeviceFeatures));
    dev_features.shaderClipDistance = VK_TRUE; //TODO: This is not actually used
    
    VkDeviceCreateInfo dev_info;
    dev_info.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    dev_info.pNext = NULL;
    dev_info.flags = 0;
    dev_info.queueCreateInfoCount = 1;
    dev_info.pQueueCreateInfos = &queue_info;
    dev_info.enabledLayerCount = sizeof(layers)/sizeof(layers[0]);
    dev_info.ppEnabledLayerNames = layers;
    dev_info.enabledExtensionCount = sizeof(device_extensions)/sizeof(device_extensions[0]);
    dev_info.ppEnabledExtensionNames = device_extensions;
    dev_info.pEnabledFeatures = &dev_features;
    
    VKR(vkCreateDevice(*info.physical_device, &dev_info, NULL, info.device));
    
    get_dev_ext_funcs(*info.device);
}

static void create_surface(vk_init_info_t info) {
    SDL_SysWMinfo wm_info;
    SDL_VERSION(&wm_info.version);
    
    if (!SDL_GetWindowWMInfo(info.window, &wm_info))
        fatal("Error: Unable to get WM info from window\n");
    
    if (wm_info.subsystem != SDL_SYSWM_X11)
        fatal("Error: Only X window system currently supported\n");
    
    VkXlibSurfaceCreateInfoKHR surf_info;
    surf_info.sType = VK_STRUCTURE_TYPE_XLIB_SURFACE_CREATE_INFO_KHR;
    surf_info.pNext = NULL;
    surf_info.flags = 0;
    surf_info.dpy = wm_info.info.x11.display;
    surf_info.window = wm_info.info.x11.window;
    
    //TODO
    /*if (fGetPhysicalDeviceXlibPresentationSupportKHR(physical_device, queue_family_index, wm_info.info.x11.display, ...)) {
        fprintf(stderr, "Error: No Xlib presentation support\n");
        exit(EXIT_FAILURE);
    }*/
    
    VKR(fCreateXlibSurfaceKHR(*info.instance, &surf_info, NULL, info.surface));
}

void vku_init(vk_init_info_t info) {
    create_instance(info);
    init_debug_callbacks(*info.instance);
    create_surface(info);
    choose_physical_device(info);
    create_device(info);
    vkGetDeviceQueue(*info.device, *info.queue_family_index, 0, info.queue);
}

void vku_deinit(vk_deinit_info_t info) {
    fDestroySurfaceKHR(info.instance, info.surface, NULL);
    vkDestroyDevice(info.device, NULL);
    deinit_debug_callbacks(info.instance);
    vkDestroyInstance(info.instance, NULL);
}

void handle_vk_result(VkResult res, char* file, unsigned int line) {
    if (res == VK_SUCCESS) return;
    
    const char* str = "Unknown";
    
    switch (res) {
    case VK_NOT_READY: str = "VK_NOT_READY"; break;
    case VK_TIMEOUT: str = "VK_TIMEOUT"; break;
    case VK_EVENT_SET: str = "VK_EVENT_SET"; break;
    case VK_EVENT_RESET: str = "VK_EVENT_RESET"; break;
    case VK_INCOMPLETE: str = "VK_ONCOMPLETE"; break;
    case VK_ERROR_OUT_OF_HOST_MEMORY: str = "VK_ERROR_OUT_OF_HOST_MEMORY"; break;
    case VK_ERROR_OUT_OF_DEVICE_MEMORY: str = "VK_ERROR_OUT_OF_DEVICE_MEMORY"; break;
    case VK_ERROR_INITIALIZATION_FAILED: str = "VK_ERROR_INITIALIZATION_FAILED"; break;
    case VK_ERROR_DEVICE_LOST: str = "VK_ERROR_DEVICE_LOST"; break;
    case VK_ERROR_MEMORY_MAP_FAILED: str = "VK_ERROR_MEMORY_MAP_FAILED"; break;
    case VK_ERROR_LAYER_NOT_PRESENT: str = "VK_ERROR_LAYER_NOT_PRESENT"; break;
    case VK_ERROR_EXTENSION_NOT_PRESENT: str = "VK_ERROR_EXTENSION_NOT_PRESENT"; break;
    case VK_ERROR_FEATURE_NOT_PRESENT: str = "VK_ERROR_FEATURE_NOT_PRESENT"; break;
    case VK_ERROR_INCOMPATIBLE_DRIVER: str = "VK_ERROR_INCOMPATIBLE_DRIVER"; break;
    case VK_ERROR_TOO_MANY_OBJECTS: str = "VK_ERROR_TOO_MANY_OBJECTS"; break;
    case VK_ERROR_FORMAT_NOT_SUPPORTED: str = "VK_ERROR_FORMAT_NOT_SUPPORTED"; break;
    case VK_ERROR_SURFACE_LOST_KHR: str = "VK_ERROR_SURFACE_LOST_KHR"; break;
    case VK_ERROR_NATIVE_WINDOW_IN_USE_KHR: str = "VK_ERROR_NATIVE_WINDOW_IN_USE_KHR"; break;
    case VK_SUBOPTIMAL_KHR: str = "VK_SUBOPTIMAL_KHR"; break;
    case VK_ERROR_OUT_OF_DATE_KHR: str = "VK_ERROR_OUT_OF_DATE_KHR"; break;
    case VK_ERROR_INCOMPATIBLE_DISPLAY_KHR: str = "VK_ERROR_INCOMPATIBLE_DISPLAY_KHR"; break;
    case VK_ERROR_VALIDATION_FAILED_EXT: str = "VK_ERROR_VALIDATION_FAILED_EXT"; break;
    case VK_ERROR_INVALID_SHADER_NV: str = "VK_ERROR_INVALID_SHADER_NV"; break;
    default: str = "Unknown"; break;
    }
    
    if (res < 0) {
        fprintf(stderr, "Error: %s:%u: %s\n", file, line, str);
        print_backtrace(stderr);
        exit(EXIT_FAILURE);
    } else {
        printf("Warning: %s:%u: %s\n", file, line, str);
    }
}

void vku_image_barrier(VkImageAspectFlagBits aspect, VkCommandBuffer cmd_buf,
                       VkAccessFlags src_access, VkAccessFlags dst_access,
                       VkImageLayout src_layout, VkImageLayout dst_layout,
                       VkPipelineStageFlagBits prev_stage, VkPipelineStageFlagBits next_stage,
                       VkImage image) {
    VkImageMemoryBarrier img_barrier;
    img_barrier.sType = VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER;
    img_barrier.pNext = NULL;
    img_barrier.srcAccessMask = src_access;
    img_barrier.dstAccessMask = dst_access;
    img_barrier.oldLayout = src_layout;
    img_barrier.newLayout = dst_layout;
    img_barrier.srcQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    img_barrier.dstQueueFamilyIndex = VK_QUEUE_FAMILY_IGNORED;
    img_barrier.image = image;
    img_barrier.subresourceRange = (VkImageSubresourceRange){aspect,
                                                             0, 1, 0, 1};
    vkCmdPipelineBarrier(cmd_buf, prev_stage, next_stage, 0, 0, NULL, 0, NULL, 1, &img_barrier);
}

void vku_begin_cmd_buf(VkCommandBuffer cmd_buf, VkCommandBufferUsageFlags flags) {
    VkCommandBufferBeginInfo info;
    info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO;
    info.pNext = NULL;
    info.flags = flags;
    info.pInheritanceInfo = NULL;
    VKR(vkBeginCommandBuffer(cmd_buf, &info));
}

VkFence vku_create_fence(VkDevice device) {
    VkFence fence;
    VkFenceCreateInfo fence_info;
    fence_info.sType = VK_STRUCTURE_TYPE_FENCE_CREATE_INFO;
    fence_info.pNext = NULL;
    fence_info.flags = 0;
    VKR(vkCreateFence(device, &fence_info, NULL, &fence));
    return fence;
}

VkSemaphore vku_create_semaphore(VkDevice device) {
    VkSemaphore semaphore;
    VkSemaphoreCreateInfo sem_info;
    sem_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    sem_info.pNext = NULL;
    sem_info.flags = 0;
    VKR(vkCreateSemaphore(device, &sem_info, NULL, &semaphore));
    return semaphore;
}

VkShaderModule vku_create_shader(VkDevice device, const char* filename) {
    FILE* file = fopen(filename, "rb");
    if (!file)
        fatal("Error: Failed to open '%s'\n", filename);
    
    uint32_t* data = NULL;
    size_t size = 0;
    size_t buf_size = 0;
    uint32_t buf[1024];
    while ((buf_size = fread(buf, 4, sizeof(buf)/sizeof(buf[0]), file))) {
        u_arr_append(size*4, (void**)&data, buf_size*4, buf);
        size += buf_size;
    }
    
    fclose(file);
    
    bool swap_endian = false;
    if (((uint8_t*)data)[0] == 0x03)
        swap_endian = BYTE_ORDER == BIG_ENDIAN;
    else
        swap_endian = BYTE_ORDER == LITTLE_ENDIAN;
    
    if (swap_endian) {
        for (size_t i = 0; i < size; i++)
            data[i] = le32toh(data[i]);
    }
    
    VkShaderModuleCreateInfo shader_info;
    shader_info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    shader_info.pNext = NULL;
    shader_info.flags = 0;
    shader_info.codeSize = size * 4;
    shader_info.pCode = data;
    
    VkShaderModule module;
    VKR(vkCreateShaderModule(device, &shader_info, NULL, &module));
    
    free(data);
    
    return module;
}

//TODO: This could be improved
VkDeviceMemory vku_create_memory(VkPhysicalDevice physical_device, VkDevice device, uint32_t type_bits, VkDeviceSize amount, bool host_visible) {
    amount = !amount ? 1 : amount;
    
    VkPhysicalDeviceMemoryProperties props;
    vkGetPhysicalDeviceMemoryProperties(physical_device, &props);
    
    VkMemoryPropertyFlags req_flags = host_visible ? VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT : 0;
    
    int32_t memory_type = -1;
    for (uint32_t i = 0; i < props.memoryTypeCount; i++) {
        VkMemoryPropertyFlags flags = props.memoryTypes[i].propertyFlags;
        if (type_bits&(1<<i) && (flags&req_flags)==req_flags) {
            memory_type = i;
            break;
        }
    }
    
    if (memory_type < 0)
        fatal("Failed to find a good memory type\n");
    
    VkMemoryAllocateInfo alloc_info;
    alloc_info.sType = VK_STRUCTURE_TYPE_MEMORY_ALLOCATE_INFO;
    alloc_info.pNext = NULL;
    alloc_info.allocationSize = amount;
    alloc_info.memoryTypeIndex = memory_type;
    VkDeviceMemory res;
    VKR(vkAllocateMemory(device, &alloc_info, NULL, &res));
    
    return res;
}

void vku_copy_buffer(vku_buffer_copy_info_t info) {
    if (!info.amount) return;
    
    VkBufferCreateInfo buf_info;
    buf_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    buf_info.pNext = NULL;
    buf_info.flags = 0;
    buf_info.size = info.amount;
    buf_info.usage = VK_BUFFER_USAGE_TRANSFER_SRC_BIT;
    buf_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    buf_info.queueFamilyIndexCount = 1;
    buf_info.pQueueFamilyIndices = &info.queue_family_index;
    VkBuffer src_buf;
    VKR(vkCreateBuffer(info.device, &buf_info, NULL, &src_buf));
    
    VkMemoryRequirements src_req;
    vkGetBufferMemoryRequirements(info.device, src_buf, &src_req);
    VkDeviceMemory src_mem = vku_create_memory(info.physical_device, info.device,
                                               src_req.memoryTypeBits, src_req.size,
                                               true);
    
    VKR(vkBindBufferMemory(info.device, src_buf, src_mem, 0));
    
    void* src_data = NULL;
    VKR(vkMapMemory(info.device, src_mem, 0, info.amount, 0, &src_data));
    memcpy(src_data, info.data, info.amount);
    VkMappedMemoryRange range;
    range.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
    range.pNext = NULL;
    range.memory = src_mem;
    range.offset = 0;
    range.size = info.amount;
    VKR(vkFlushMappedMemoryRanges(info.device, 1, &range));
    vkUnmapMemory(info.device, src_mem);
    
    VkCommandBufferAllocateInfo alloc_info;
    alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    alloc_info.pNext = NULL;
    alloc_info.commandPool = info.cmd_pool;
    alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    alloc_info.commandBufferCount = 1;
    VkCommandBuffer cmd_buf;
    VKR(vkAllocateCommandBuffers(info.device, &alloc_info, &cmd_buf));
    
    vku_begin_cmd_buf(cmd_buf, VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
    
    VkBufferCopy region;
    region.srcOffset = 0;
    region.dstOffset = info.offset;
    region.size = info.amount;
    vkCmdCopyBuffer(cmd_buf, src_buf, info.buffer, 1, &region);
    
    VKR(vkEndCommandBuffer(cmd_buf));
    VkSubmitInfo submit_info;
    submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info.pNext = NULL;
    submit_info.waitSemaphoreCount = 0;
    submit_info.pWaitSemaphores = NULL;
    submit_info.pWaitDstStageMask = 0;
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &cmd_buf;
    submit_info.signalSemaphoreCount = 0;
    submit_info.pSignalSemaphores = 0;
    
    VkFence fence = vku_create_fence(info.device);
    vkQueueSubmit(info.queue, 1, &submit_info, fence);
    vkWaitForFences(info.device, 1, &fence, VK_TRUE, UINT64_MAX);
    vkDestroyFence(info.device, fence, NULL);
    
    vkFreeCommandBuffers(info.device, info.cmd_pool, 1, &cmd_buf);
    vkDestroyBuffer(info.device, src_buf, NULL);
    vkFreeMemory(info.device, src_mem, NULL);
}

void vku_create_buffer(vku_buffer_create_info_t info) {
    VkBufferCreateInfo buf_info;
    buf_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    buf_info.pNext = NULL;
    buf_info.flags = 0;
    buf_info.size = info.size;
    buf_info.usage = info.usage | VK_BUFFER_USAGE_TRANSFER_DST_BIT;
    buf_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    buf_info.queueFamilyIndexCount = 1;
    buf_info.pQueueFamilyIndices = &info.queue_family_index;
    VKR(vkCreateBuffer(info.device, &buf_info, NULL, info.buffer));
    
    VkMemoryRequirements buf_req;
    vkGetBufferMemoryRequirements(info.device, *info.buffer, &buf_req);
    *info.memory = vku_create_memory(info.physical_device, info.device, buf_req.memoryTypeBits, buf_req.size, false);
    VKR(vkBindBufferMemory(info.device, *info.buffer, *info.memory, 0));
    
    vku_buffer_copy_info_t copy_info;
    copy_info.physical_device = info.physical_device;
    copy_info.device = info.device;
    copy_info.queue = info.queue;
    copy_info.queue_family_index = info.queue_family_index;
    copy_info.cmd_pool = info.cmd_pool;
    copy_info.buffer = *info.buffer;
    copy_info.offset = 0;
    copy_info.amount = info.size;
    copy_info.data = info.data;
    vku_copy_buffer(copy_info);
}

static void find_format(VkPhysicalDevice device, VkImageUsageFlags usage,
                        size_t mipmap_count, size_t format_count,
                        VkFormat* formats, VkFormat* format, int* max_mip_levels) {
    *format = VK_FORMAT_UNDEFINED;
    *max_mip_levels = -1;
    bool allow_less_mips = mipmap_count != 1;
    for (size_t _ = 0; _ < 2; _++) {
        for (size_t i = 0; i < format_count; i++) {
            VkImageFormatProperties props;
            VkResult res = vkGetPhysicalDeviceImageFormatProperties(device,  
                                                                    formats[i],
                                                                    VK_IMAGE_TYPE_2D,
                                                                    VK_IMAGE_TILING_LINEAR,
                                                                    usage,
                                                                    0,
                                                                    &props);
            if (res == VK_ERROR_FORMAT_NOT_SUPPORTED) continue;
            if (props.maxMipLevels<mipmap_count && !allow_less_mips) continue;
            
            *format = formats[i];
            *max_mip_levels = props.maxMipLevels;
            return;
        }
        
        allow_less_mips = true;
    }
}

static void write_image_data_row(VkFormat format, void* dest, float* src, size_t width) {
    switch (format) {
    case VK_FORMAT_R8G8B8A8_UNORM:
    case VK_FORMAT_R8G8B8A8_SRGB: {
        for (size_t x = 0; x < width; x++) {
            ((uint8_t*)dest)[x*4+0] = src[x*4+0] * 255;
            ((uint8_t*)dest)[x*4+1] = src[x*4+1] * 255;
            ((uint8_t*)dest)[x*4+2] = src[x*4+2] * 255;
            ((uint8_t*)dest)[x*4+3] = src[x*4+3] * 255;
        }
        break;
    }
    case VK_FORMAT_B8G8R8A8_UNORM:
    case VK_FORMAT_B8G8R8A8_SRGB: {
        for (size_t x = 0; x < width; x++) {
            ((uint8_t*)dest)[x*4+0] = src[x*4+0] * 255;
            ((uint8_t*)dest)[x*4+1] = src[x*4+1] * 255;
            ((uint8_t*)dest)[x*4+2] = src[x*4+2] * 255;
            ((uint8_t*)dest)[x*4+3] = src[x*4+3] * 255;
        }
        break;
    }
    case VK_FORMAT_R8G8B8_UNORM:
    case VK_FORMAT_R8G8B8_SRGB: {
        for (size_t x = 0; x < width; x++) {
            ((uint8_t*)dest)[x*3+0] = src[x*4+0] * 255;
            ((uint8_t*)dest)[x*3+1] = src[x*4+1] * 255;
            ((uint8_t*)dest)[x*3+2] = src[x*4+2] * 255;
        }
        break;
    }
    case VK_FORMAT_B8G8R8_UNORM:
    case VK_FORMAT_B8G8R8_SRGB: {
        for (size_t x = 0; x < width; x++) {
            ((uint8_t*)dest)[x*3+0] = src[x*4+2] * 255;
            ((uint8_t*)dest)[x*3+1] = src[x*4+1] * 255;
            ((uint8_t*)dest)[x*3+2] = src[x*4+0] * 255;
        }
        break;
    }
    default: {
        //TODO
        break;
    }
    }
}

void vku_create_image(vku_image_create_info_t info) {
    //Find a format
    VkFormat format = VK_FORMAT_UNDEFINED;
    int max_mip_levels = -1;
    find_format(info.physical_device, info.usage_flags, info.mipmap_count,
                info.format_count, info.formats, &format, &max_mip_levels);
    
    if (format == VK_FORMAT_UNDEFINED) {
        fprintf(stderr, "Failed to find a good image format\n");
        exit(EXIT_FAILURE);
    }
    
    switch (format) {
    case VK_FORMAT_R8G8B8A8_UNORM:
        printf("Using VK_FORMAT_R8G8B8A8_UNORM\n");
        break;
    case VK_FORMAT_R8G8B8A8_SRGB:
        printf("Using VK_FORMAT_R8G8B8A8_SRGB\n");
        break;
    case VK_FORMAT_B8G8R8A8_UNORM:
        printf("Using VK_FORMAT_B8G8R8A8_UNORM\n");
        break;
    case VK_FORMAT_B8G8R8A8_SRGB:
        printf("Using VK_FORMAT_B8G8R8A8_SRGB\n");
        break;
    case VK_FORMAT_R8G8B8_UNORM:
        printf("Using VK_FORMAT_R8G8B8_UNORM\n");
        break;
    case VK_FORMAT_R8G8B8_SRGB:
        printf("Using VK_FORMAT_R8G8B8_SRGB\n");
        break;
    case VK_FORMAT_B8G8R8_UNORM:
        printf("Using VK_FORMAT_B8G8R8_UNORM\n");
        break;
    case VK_FORMAT_B8G8R8_SRGB:
        printf("Using VK_FORMAT_B8G8R8_SRGB\n");
        break;
    default:
        printf("Using unhandled format\n");
        break;
    }
    
    VkImageCreateInfo img_info;
    img_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    img_info.pNext = NULL;
    img_info.flags = 0;
    img_info.imageType = VK_IMAGE_TYPE_2D;
    img_info.format = format;
    img_info.extent = (VkExtent3D){info.width, info.height, 1};
    img_info.mipLevels = info.mipmap_count;
    img_info.arrayLayers = 1;
    img_info.samples = VK_SAMPLE_COUNT_1_BIT;
    img_info.tiling = VK_IMAGE_TILING_LINEAR;
    img_info.usage = info.usage_flags;
    img_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    img_info.queueFamilyIndexCount = 1;
    img_info.pQueueFamilyIndices = &info.queue_family_index;
    img_info.initialLayout = VK_IMAGE_LAYOUT_PREINITIALIZED;
    VKR(vkCreateImage(info.device, &img_info, NULL, info.image));
    
    VkMemoryRequirements req;
    vkGetImageMemoryRequirements(info.device, *info.image, &req);
    *info.memory = vku_create_memory(info.physical_device, info.device,
                                     req.memoryTypeBits, req.size, true);
    
    VKR(vkBindImageMemory(info.device, *info.image, *info.memory, 0));
    
    VkImageSubresource subres;
    subres.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    size_t width = info.width;
    size_t height = info.height;
    for (size_t i = 0; i < max_mip_levels; i++) {
        subres.mipLevel = i;
        subres.arrayLayer = 0;
        VkSubresourceLayout layout;
        vkGetImageSubresourceLayout(info.device, *info.image, &subres, &layout);
        
        uint8_t* data = NULL;
        VKR(vkMapMemory(info.device, *info.memory, layout.offset, layout.rowPitch*info.height, 0, (void**)&data));
        
        for (size_t y = 0; y < info.height; y++)
            write_image_data_row(format, data+y*layout.rowPitch,
                                 &info.data[i][y*info.width], info.width);
        
        VkMappedMemoryRange range;
        range.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
        range.pNext = NULL;
        range.memory = *info.memory;
        range.offset = layout.offset;
        range.size = layout.rowPitch * info.height;
        VKR(vkFlushMappedMemoryRanges(info.device, 1, &range));
        vkUnmapMemory(info.device, *info.memory);
        
        width /= 2;
        height /= 2;
    }
}

void vku_blit_image(vku_image_blit_info_t info) {
    VkImageCreateInfo img_info;
    img_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    img_info.pNext = NULL;
    img_info.flags = 0;
    img_info.imageType = VK_IMAGE_TYPE_2D;
    img_info.format = info.src_srgb ? VK_FORMAT_R8G8B8A8_SRGB : VK_FORMAT_R8G8B8A8_UNORM; //TODO: Make sure that all required features are supported with linear tiling
    img_info.extent = (VkExtent3D){info.width, info.height, 1};
    img_info.mipLevels = 1;
    img_info.arrayLayers = 1;
    img_info.samples = VK_SAMPLE_COUNT_1_BIT;
    img_info.tiling = VK_IMAGE_TILING_LINEAR;
    img_info.usage = VK_IMAGE_USAGE_TRANSFER_SRC_BIT;
    img_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    img_info.queueFamilyIndexCount = 1;
    img_info.pQueueFamilyIndices = &info.queue_family_index;
    img_info.initialLayout = VK_IMAGE_LAYOUT_PREINITIALIZED;
    
    VkImage src_image;
    VKR(vkCreateImage(info.device, &img_info, NULL, &src_image));
    
    VkMemoryRequirements src_req;
    vkGetImageMemoryRequirements(info.device, src_image, &src_req);
    VkDeviceMemory src_mem = vku_create_memory(info.physical_device, info.device,
                                               src_req.memoryTypeBits, src_req.size,
                                               true);
    
    VKR(vkBindImageMemory(info.device, src_image, src_mem, 0));
    
    VkImageSubresource src_subres;
    src_subres.aspectMask = VK_IMAGE_ASPECT_COLOR_BIT;
    src_subres.mipLevel = 0;
    src_subres.arrayLayer = 0;
    
    VkSubresourceLayout src_subres_layout;
    vkGetImageSubresourceLayout(info.device, src_image, &src_subres, &src_subres_layout);
    
    uint8_t* src_data = NULL;
    VKR(vkMapMemory(info.device, src_mem, src_subres_layout.offset, src_subres_layout.rowPitch*info.height, 0, (void**)&src_data));
    
    for (size_t row = 0; row < info.height; row++)
        memcpy(src_data+row*src_subres_layout.rowPitch, info.src_image+row*info.width*4, src_subres_layout.rowPitch);
    
    VkMappedMemoryRange range;
    range.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
    range.pNext = NULL;
    range.memory = src_mem;
    range.offset = src_subres_layout.offset;
    range.size = src_subres_layout.rowPitch * info.height;
    VKR(vkFlushMappedMemoryRanges(info.device, 1, &range));
    vkUnmapMemory(info.device, src_mem);
    
    VkCommandBufferAllocateInfo alloc_info;
    alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    alloc_info.pNext = NULL;
    alloc_info.commandPool = info.cmd_pool;
    alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    alloc_info.commandBufferCount = 1;
    VkCommandBuffer cmd_buf;
    VKR(vkAllocateCommandBuffers(info.device, &alloc_info, &cmd_buf));
    
    vku_begin_cmd_buf(cmd_buf, VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
    
    //TODO: Mipmap levels
    vku_image_barrier(VK_IMAGE_ASPECT_COLOR_BIT, cmd_buf, VK_ACCESS_HOST_WRITE_BIT,
                  VK_ACCESS_TRANSFER_READ_BIT, VK_IMAGE_LAYOUT_PREINITIALIZED,
                  VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, VK_PIPELINE_STAGE_HOST_BIT,
                  VK_PIPELINE_STAGE_TRANSFER_BIT, src_image);
    
    vku_image_barrier(VK_IMAGE_ASPECT_COLOR_BIT, cmd_buf, info.dst_img_access,
                      VK_ACCESS_TRANSFER_WRITE_BIT, info.dst_img_layout,
                      VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
                      VK_PIPELINE_STAGE_TRANSFER_BIT, info.dst_image);
    
    VkImageBlit region;
    region.srcSubresource = (VkImageSubresourceLayers){VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1};
    region.srcOffsets[0] = (VkOffset3D){0, 0, 0};
    region.srcOffsets[1] = (VkOffset3D){info.width, info.height, 1};
    region.dstSubresource = (VkImageSubresourceLayers){VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1};
    region.dstOffsets[0] = (VkOffset3D){0, 0, 0};
    region.dstOffsets[1] = (VkOffset3D){info.width, info.height, 1};
    vkCmdBlitImage(cmd_buf, src_image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, info.dst_image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region, VK_FILTER_NEAREST);
    
    /*VkImageCopy region;
    region.srcSubresource = (VkImageSubresourceLayers){VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1};
    region.srcOffset = (VkOffset3D){0, 0, 0};
    region.dstSubresource = (VkImageSubresourceLayers){VK_IMAGE_ASPECT_COLOR_BIT, 0, 0, 1};
    region.dstOffset = (VkOffset3D){0, 0, 0};
    region.extent = (VkExtent3D){info.width, info.height, 1};
    vkCmdCopyImage(cmd_buf, src_image, VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL, info.dst_image, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, 1, &region);*/
    
    vku_image_barrier(VK_IMAGE_ASPECT_COLOR_BIT, cmd_buf, VK_ACCESS_TRANSFER_WRITE_BIT,
                  info.dst_img_new_access, VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                  info.dst_img_new_layout, VK_PIPELINE_STAGE_TRANSFER_BIT,
                  VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, info.dst_image);
    
    VKR(vkEndCommandBuffer(cmd_buf));
    VkSubmitInfo submit_info;
    submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info.pNext = NULL;
    submit_info.waitSemaphoreCount = 0;
    submit_info.pWaitSemaphores = NULL;
    submit_info.pWaitDstStageMask = 0;
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &cmd_buf;
    submit_info.signalSemaphoreCount = 0;
    submit_info.pSignalSemaphores = 0;
    
    VkFence fence = vku_create_fence(info.device);
    vkQueueSubmit(info.queue, 1, &submit_info, fence);
    vkWaitForFences(info.device, 1, &fence, VK_TRUE, UINT64_MAX);
    vkDestroyFence(info.device, fence, NULL);
    
    vkFreeCommandBuffers(info.device, info.cmd_pool, 1, &cmd_buf);
    vkDestroyImage(info.device, src_image, NULL);
    vkFreeMemory(info.device, src_mem, NULL);
}
