//TODO: Different queues for graphics and present
//TODO: Staging buffers
#include "vk.h"
#include "render.h"
#include "swapchain.h"
#include "mathy_stuff.h"
#include "utils.h"

#include <SDL2/SDL_syswm.h>
#include <SDL2/SDL.h>
#include <stdlib.h>

struct r_mesh_t {
    size_t index_count;
    size_t vertex_count;
    VkDeviceMemory vertex_mem;
    VkDeviceMemory index_mem;
    VkBuffer vertex_buffer;
    VkBuffer index_buffer;
    VkPipelineLayout pipeline_layout;
    VkPipeline pipeline;
};

struct r_texture_t {
    VkDeviceMemory memory;
    VkImage image;
};

struct r_shader_t {
    VkShaderModule module;
};

struct r_material_t {
    VkBuffer frag_data_buf;
    VkDeviceMemory frag_data_mem;
    VkShaderModule frag_shader;
};

static VkInstance instance;
static VkPhysicalDevice physical_device;
static uint32_t queue_family_index; 
static VkDevice device;
static VkSurfaceKHR surface;
static VkFormat surface_format;
static VkFormat depthstencil_format;
static swapchain_t* swapchain;
static VkCommandPool command_pool;
static VkCommandBuffer command_buffer;
static VkQueue queue;
static VkRenderPass renderpass;
static VkSemaphore acquire_image_sem;
static VkSemaphore render_done_sem;
static bin_t* shader_bin;
static b_shader_t* vertex_shader;
static uint32_t cur_width, cur_height;
static VkImage test_img;

static void create_command_pool() {
    VkCommandPoolCreateInfo pool_info;
    pool_info.sType = VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
    pool_info.pNext = NULL;
    pool_info.flags = VK_COMMAND_POOL_CREATE_TRANSIENT_BIT |
                      VK_COMMAND_POOL_CREATE_RESET_COMMAND_BUFFER_BIT;
    pool_info.queueFamilyIndex = queue_family_index;
    
    VKR(vkCreateCommandPool(device, &pool_info, NULL, &command_pool));
}

static void create_command_buffer() {
    VkCommandBufferAllocateInfo alloc_info;
    alloc_info.sType = VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO;
    alloc_info.pNext = NULL;
    alloc_info.commandPool = command_pool;
    alloc_info.level = VK_COMMAND_BUFFER_LEVEL_PRIMARY;
    alloc_info.commandBufferCount = 1;
    
    VKR(vkAllocateCommandBuffers(device, &alloc_info, &command_buffer));
}

static void create_renderpass() {
    VkAttachmentDescription attachments[2];
    attachments[0].flags = 0;
    attachments[0].format = surface_format;
    attachments[0].samples = VK_SAMPLE_COUNT_1_BIT;
    attachments[0].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachments[0].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    attachments[0].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_DONT_CARE;
    attachments[0].stencilStoreOp = VK_ATTACHMENT_STORE_OP_DONT_CARE;
    attachments[0].initialLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    attachments[0].finalLayout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    
    attachments[1].flags = 0;
    attachments[1].format = depthstencil_format;
    attachments[1].samples = VK_SAMPLE_COUNT_1_BIT;
    attachments[1].loadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachments[1].storeOp = VK_ATTACHMENT_STORE_OP_STORE;
    attachments[1].stencilLoadOp = VK_ATTACHMENT_LOAD_OP_CLEAR;
    attachments[1].stencilStoreOp = VK_ATTACHMENT_STORE_OP_STORE;
    attachments[1].initialLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    attachments[1].finalLayout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    
    VkAttachmentReference cl_attachment_ref;
    cl_attachment_ref.attachment = 0;
    cl_attachment_ref.layout = VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;
    
    VkAttachmentReference ds_attachment_ref;
    ds_attachment_ref.attachment = 1;
    ds_attachment_ref.layout = VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL;
    
    VkSubpassDescription subpass;
    subpass.flags = 0;
    subpass.pipelineBindPoint = VK_PIPELINE_BIND_POINT_GRAPHICS;
    subpass.inputAttachmentCount = 0;
    subpass.pInputAttachments = NULL;
    subpass.colorAttachmentCount = 1;
    subpass.pColorAttachments = &cl_attachment_ref;
    subpass.pResolveAttachments = NULL;
    subpass.pDepthStencilAttachment = &ds_attachment_ref;
    subpass.preserveAttachmentCount = 0;
    subpass.pPreserveAttachments = NULL;
    
    VkRenderPassCreateInfo rp_info;
    rp_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
    rp_info.pNext = NULL;
    rp_info.flags = 0;
    rp_info.attachmentCount = 2;
    rp_info.pAttachments = attachments;
    rp_info.subpassCount = 1;
    rp_info.pSubpasses = &subpass;
    rp_info.dependencyCount = 0;
    rp_info.pDependencies = NULL;
    VKR(vkCreateRenderPass(device, &rp_info, NULL, &renderpass));
}

void vk_init(SDL_Window* window) {
    vk_init_info_t init_info;
    init_info.window = window;
    init_info.instance = &instance;
    init_info.physical_device = &physical_device;
    init_info.device = &device;
    init_info.surface = &surface;
    init_info.queue = &queue;
    init_info.queue_family_index = &queue_family_index;
    vku_init(init_info);
    
    VkSurfaceCapabilitiesKHR surf_caps;
    VKR(fGetPhysicalDeviceSurfaceCapabilitiesKHR(physical_device, surface, &surf_caps));
    
    //TODO: Is this even possible? If so, handle it
    if (surf_caps.currentExtent.width == 0xFFFFFFFF)
        fatal("Error: Failed to get current extents of surface\n");
    
    cur_width = surf_caps.currentExtent.width;
    cur_height = surf_caps.currentExtent.height;
    
    VkFormatProperties d24_s8_props;
    vkGetPhysicalDeviceFormatProperties(physical_device, VK_FORMAT_D24_UNORM_S8_UINT, &d24_s8_props);
    depthstencil_format = d24_s8_props.optimalTilingFeatures&VK_FORMAT_FEATURE_DEPTH_STENCIL_ATTACHMENT_BIT ?
                          VK_FORMAT_D24_UNORM_S8_UINT : VK_FORMAT_D32_SFLOAT_S8_UINT;
    
    create_command_pool();
    create_command_buffer();
    create_renderpass();
    
    swapchain_create_info_t swapchain_info;
    swapchain_info.physical_device = physical_device;
    swapchain_info.device = device;
    swapchain_info.surface = surface;
    swapchain_info.render_pass = renderpass;
    swapchain_info.cmd_pool = command_pool;
    swapchain_info.queue = queue;
    swapchain_info.width = cur_width;
    swapchain_info.height = cur_height;
    swapchain_info.surface_format = &surface_format;
    swapchain_info.depthstencil_format = depthstencil_format;
    swapchain = swapchain_create(swapchain_info);
    
    acquire_image_sem = vku_create_semaphore(device);
    render_done_sem = vku_create_semaphore(device);
    
    shader_bin = b_read("shaders.bin");
    if (!shader_bin)
        fatal("Failed to read shaders.bin\n");
    
    vertex_shader = b_find_shader(shader_bin, "shaders/gbuffer.vs.glsl");
    if (!vertex_shader)
        fatal("shaders/gbuffer.vs.glsl not found in shaders.bin\n");
    
    /*VkImageCreateInfo img_info;
    img_info.sType = VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    img_info.pNext = NULL;
    img_info.flags = 0;
    img_info.imageType = VK_IMAGE_TYPE_2D;
    img_info.format = VK_FORMAT_R8G8B8A8_UNORM;
    img_info.extent = (VkExtent3D){4, 4, 1};
    img_info.mipLevels = 1;
    img_info.arrayLayers = 1;
    img_info.samples = VK_SAMPLE_COUNT_1_BIT;
    img_info.tiling = VK_IMAGE_TILING_OPTIMAL;
    img_info.usage = VK_IMAGE_USAGE_TRANSFER_DST_BIT;
    img_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    img_info.queueFamilyIndexCount = 0;
    img_info.pQueueFamilyIndices = NULL;
    img_info.initialLayout = VK_IMAGE_LAYOUT_UNDEFINED;
    VKR(vkCreateImage(device, &img_info, NULL, &test_img));
    
    vku_image_blit_info_t blit_info;
    blit_info.physical_device = physical_device;
    blit_info.device = device;
    blit_info.queue = queue;
    blit_info.queue_family_index = queue_family_index;
    blit_info.cmd_pool = command_pool;
    blit_info.width = 4;
    blit_info.height = 4;
    blit_info.dst_img_access = 0;
    blit_info.dst_img_layout = VK_IMAGE_LAYOUT_UNDEFINED;
    blit_info.dst_img_new_access = VK_ACCESS_TRANSFER_READ_BIT;
    blit_info.dst_img_new_layout = VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
    blit_info.dst_image = test_img;
    blit_info.dst_mipmap = 0;
    blit_info.src_image = (uint8_t*)("\xff\xff\xff\xff" "\x00\x00\x00\xff" "\xff\xff\xff\xff" "\x00\x00\x00\xff"
                                     "\x00\x00\x00\xff" "\xff\xff\xff\xff" "\x00\x00\x00\xff" "\xff\xff\xff\xff"
                                     "\xff\xff\xff\xff" "\x00\x00\x00\xff" "\xff\xff\xff\xff" "\x00\x00\x00\xff"
                                     "\x00\x00\x00\xff" "\xff\xff\xff\xff" "\x00\x00\x00\xff" "\xff\xff\xff\xff");
    blit_info.src_srgb = false;
    vku_blit_image(blit_info);*/
}

void vk_deinit() {
    //vkDestroyImage(device, test_img, NULL);
    
    b_del(shader_bin);
    vkDestroySemaphore(device, acquire_image_sem, NULL);
    vkDestroySemaphore(device, render_done_sem, NULL);
    swapchain_del(swapchain);
    vkDestroyRenderPass(device, renderpass, NULL);
    vkFreeCommandBuffers(device, command_pool, 1, &command_buffer);
    vkDestroyCommandPool(device, command_pool, NULL);
    vk_deinit_info_t deinit_info;
    deinit_info.instance = instance;
    deinit_info.device = device;
    deinit_info.surface = surface;
    vku_deinit(deinit_info);
}

r_texture_t* vk_create_texture(b_texture_t* texture) {
    r_texture_t* res = malloc(sizeof(r_texture_t));
    
    vku_image_create_info_t info;
    info.physical_device = physical_device;
    info.device = device;
    info.queue_family_index = queue_family_index;
    info.width = texture->width;
    info.height = texture->height;
    info.mipmap_count = 1;
    float* data = malloc(texture->width*texture->height*16);
    info.data = &data;
    
    switch (texture->format) {
    case B_TEX_FMT_SRGBA8_UNORM:
    case B_TEX_FMT_RGBA8_UNORM: {
        for (size_t y = 0; y < texture->height; y++) {
            for (size_t x = 0; x < texture->width; x++) {
                uint8_t* pixel = &((uint8_t*)texture->data)[(y*texture->width+x)*4];
                data[(y*texture->width+x)*4] = pixel[0] / 255.0;
                data[(y*texture->width+x)*4+1] = pixel[1] / 255.0;
                data[(y*texture->width+x)*4+2] = pixel[2] / 255.0;
                data[(y*texture->width+x)*4+3] = pixel[3] / 255.0;
            }
        }
        break;
    }
    case B_TEX_FMT_SRGB8_UNORM:
    case B_TEX_FMT_RGB8_UNORM: {
        for (size_t y = 0; y < texture->height; y++) {
            for (size_t x = 0; x < texture->width; x++) {
                uint8_t* pixel = &((uint8_t*)texture->data)[(y*texture->width+x)*3];
                data[(y*texture->width+x)*4] = pixel[0] / 255.0;
                data[(y*texture->width+x)*4+1] = pixel[1] / 255.0;
                data[(y*texture->width+x)*4+2] = pixel[2] / 255.0;
                data[(y*texture->width+x)*4+3] = 1;
            }
        }
        break;
    }
    }
    
    switch (texture->format) {
    case B_TEX_FMT_SRGBA8_UNORM: {
        static VkFormat formats[] = {VK_FORMAT_B8G8R8A8_SRGB, VK_FORMAT_R8G8B8A8_SRGB};
        info.format_count = sizeof(formats)/sizeof(formats[0]);
        info.formats = formats;
        break;
    }
    case B_TEX_FMT_RGBA8_UNORM: {
        static VkFormat formats[] = {VK_FORMAT_B8G8R8A8_UNORM, VK_FORMAT_R8G8B8A8_UNORM}; //TODO: Add more
        info.format_count = sizeof(formats)/sizeof(formats[0]);
        info.formats = formats;
        break;
    }
    case B_TEX_FMT_SRGB8_UNORM: {
        static VkFormat formats[] = {VK_FORMAT_B8G8R8_SRGB, VK_FORMAT_R8G8B8_SRGB,
                                     VK_FORMAT_B8G8R8A8_SRGB, VK_FORMAT_R8G8B8A8_SRGB};
        info.format_count = sizeof(formats)/sizeof(formats[0]);
        info.formats = formats;
        break;
    }
    case B_TEX_FMT_RGB8_UNORM: {
        static VkFormat formats[] = {VK_FORMAT_B8G8R8_UNORM, VK_FORMAT_R8G8B8_UNORM,
                                     VK_FORMAT_B8G8R8A8_UNORM, VK_FORMAT_R8G8B8A8_UNORM}; //TODO: Add more
        info.format_count = sizeof(formats)/sizeof(formats[0]);
        info.formats = formats;
        break;
    }
    }
    
    info.usage_flags = VK_IMAGE_USAGE_SAMPLED_BIT;
    info.memory = &res->memory;
    info.image = &res->image;
    
    vku_create_image(info);
    
    free(data);
    
    return res;
}

void vk_del_texture(r_texture_t* texture) { //TODO: Ensure that it is not in use
    vkDestroyImage(device, texture->image, NULL);
    vkFreeMemory(device, texture->memory, NULL);
    free(texture);
}

r_cubemap_t* vk_create_cubemap(b_cubemap_t* cubemap) {
    
}

void vk_del_cubemap(r_cubemap_t* cubemap) {
    
}

r_shader_t* vk_create_shader(b_shader_t* shader) {
    r_shader_t* res = malloc(sizeof(r_shader_t));
    
    VkShaderModuleCreateInfo shader_info;
    shader_info.sType = VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO;
    shader_info.pNext = NULL;
    shader_info.flags = 0;
    shader_info.codeSize = shader->spirv_count * 4;
    shader_info.pCode = shader->spirv;
    VKR(vkCreateShaderModule(device, &shader_info, NULL, &res->module));
    
    return res;
}

void vk_del_shader(r_shader_t* shader) { //TODO: Ensure that it is not in use
    vkDestroyShaderModule(device, shader->module, NULL);
    free(shader);
}

r_material_t* vk_create_material(b_material_t* material) {
    for (size_t i = 0; i < material->tex_slot_count; i++)
        b_get_r_texture(material->tex_slots[i]);
    
    r_material_t* res = malloc(sizeof(r_material_t));
    
    vku_buffer_create_info_t buf_info;
    buf_info.physical_device = physical_device;
    buf_info.device = device;
    buf_info.queue = queue;
    buf_info.queue_family_index = queue_family_index;
    buf_info.cmd_pool = command_pool;
    buf_info.buffer = &res->frag_data_buf;
    buf_info.memory = &res->frag_data_mem;
    buf_info.usage = VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT;
    buf_info.size = material->frag_data_size;
    buf_info.data = material->frag_data;
    vku_create_buffer(buf_info);
    
    res->frag_shader = b_get_r_shader(material->fragment)->module;
    return res;
}

void vk_del_material(r_material_t* material) {
    vkDestroyBuffer(device, material->frag_data_buf, NULL);
    vkFreeMemory(device, material->frag_data_mem, NULL);
    free(material);
}

r_mesh_t* vk_create_mesh(b_mesh_t* mesh) {
    r_mesh_t* res = malloc(sizeof(r_mesh_t));
    
    res->index_count = mesh->index_count;
    res->vertex_count = mesh->vertex_count;
    
    VkBufferCreateInfo buf_info;
    buf_info.sType = VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
    buf_info.pNext = NULL;
    buf_info.flags = 0;
    buf_info.size = mesh->vertex_count * 32;
    buf_info.usage = VK_BUFFER_USAGE_VERTEX_BUFFER_BIT;
    buf_info.sharingMode = VK_SHARING_MODE_EXCLUSIVE;
    buf_info.queueFamilyIndexCount = 1;
    buf_info.pQueueFamilyIndices = &queue_family_index;
    VKR(vkCreateBuffer(device, &buf_info, NULL, &res->vertex_buffer));
    
    buf_info.size = mesh->index_count * 4;
    buf_info.usage = VK_BUFFER_USAGE_INDEX_BUFFER_BIT;
    VKR(vkCreateBuffer(device, &buf_info, NULL, &res->index_buffer));
    
    VkMemoryRequirements vert_req;
    vkGetBufferMemoryRequirements(device, res->vertex_buffer, &vert_req);
    
    VkMemoryRequirements idx_req;
    vkGetBufferMemoryRequirements(device, res->index_buffer, &idx_req);
    
    res->vertex_mem = vku_create_memory(physical_device, device, vert_req.memoryTypeBits, vert_req.size, true);
    res->index_mem = vku_create_memory(physical_device, device, idx_req.memoryTypeBits, idx_req.size, true);
    
    VKR(vkBindBufferMemory(device, res->vertex_buffer, res->vertex_mem, 0));
    VKR(vkBindBufferMemory(device, res->index_buffer, res->index_mem, 0));
    
    float* vertex_data = NULL;
    VKR(vkMapMemory(device, res->vertex_mem, 0, mesh->vertex_count*32, 0, (void**)&vertex_data));
    for (size_t i = 0; i < mesh->vertex_count; i++) {
        vertex_data[i*8] = mesh->positions[i*3];
        vertex_data[i*8+1] = mesh->positions[i*3+1];
        vertex_data[i*8+2] = mesh->positions[i*3+2];
        vertex_data[i*8+3] = mesh->normals[i*3];
        vertex_data[i*8+4] = mesh->normals[i*3+1];
        vertex_data[i*8+5] = mesh->normals[i*3+2];
        vertex_data[i*8+6] = mesh->uvs[i*2];
        vertex_data[i*8+7] = mesh->uvs[i*2+1];
    }
    VkMappedMemoryRange range;
    range.sType = VK_STRUCTURE_TYPE_MAPPED_MEMORY_RANGE;
    range.pNext = NULL;
    range.memory = res->vertex_mem;
    range.offset = 0;
    range.size = mesh->vertex_count * 32;
    VKR(vkFlushMappedMemoryRanges(device, 1, &range));
    vkUnmapMemory(device, res->vertex_mem);
    
    void* index_data = NULL;
    VKR(vkMapMemory(device, res->index_mem, 0, mesh->index_count*4, 0, &index_data));
    memcpy(index_data, mesh->indices, mesh->index_count*4);
    range.memory = res->index_mem;
    range.size = mesh->index_count * 4;
    VKR(vkFlushMappedMemoryRanges(device, 1, &range));
    
    vkUnmapMemory(device, res->index_mem);
    
    //Note: The minimum amount of space for push constants is 128 bytes
    VkPushConstantRange push_constant_range;
    push_constant_range.stageFlags = VK_SHADER_STAGE_VERTEX_BIT;
    push_constant_range.offset = 0;
    push_constant_range.size = 128;
    
    VkPipelineLayoutCreateInfo pipeline_layout_info;
    pipeline_layout_info.sType = VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO;
    pipeline_layout_info.pNext = NULL;
    pipeline_layout_info.flags = 0;
    pipeline_layout_info.setLayoutCount = 0;
    pipeline_layout_info.pSetLayouts = NULL;
    pipeline_layout_info.pushConstantRangeCount = 1;
    pipeline_layout_info.pPushConstantRanges = &push_constant_range;
    VKR(vkCreatePipelineLayout(device, &pipeline_layout_info, NULL, &res->pipeline_layout));
    
    VkPipelineShaderStageCreateInfo pipeline_stages[2];
    pipeline_stages[0].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    pipeline_stages[0].pNext = NULL;
    pipeline_stages[0].flags = 0;
    pipeline_stages[0].stage = VK_SHADER_STAGE_VERTEX_BIT;
    pipeline_stages[0].module = b_get_r_shader(vertex_shader)->module;
    pipeline_stages[0].pName = "main";
    pipeline_stages[0].pSpecializationInfo = NULL;
    pipeline_stages[1].sType = VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO;
    pipeline_stages[1].pNext = NULL;
    pipeline_stages[1].flags = 0;
    pipeline_stages[1].stage = VK_SHADER_STAGE_FRAGMENT_BIT;
    pipeline_stages[1].module = b_get_r_material(mesh->material)->frag_shader;
    pipeline_stages[1].pName = "main";
    pipeline_stages[1].pSpecializationInfo = NULL;
    
    VkVertexInputBindingDescription binding_desc;
    binding_desc.binding = 0;
    binding_desc.stride = 32;
    binding_desc.inputRate = VK_VERTEX_INPUT_RATE_VERTEX;
    
    VkVertexInputAttributeDescription attr_desc[3];
    attr_desc[0].location = 0;
    attr_desc[0].binding = 0;
    attr_desc[0].format = VK_FORMAT_R32G32B32_SFLOAT;
    attr_desc[0].offset = 0;
    
    attr_desc[1].location = 1;
    attr_desc[1].binding = 0;
    attr_desc[1].format = VK_FORMAT_R32G32B32_SFLOAT;
    attr_desc[1].offset = 12;
    
    attr_desc[2].location = 2;
    attr_desc[2].binding = 0;
    attr_desc[2].format = VK_FORMAT_R32G32_SFLOAT;
    attr_desc[2].offset = 24;
    
    VkPipelineVertexInputStateCreateInfo vertex_input;
    vertex_input.sType = VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO;
    vertex_input.pNext = NULL;
    vertex_input.flags = 0;
    vertex_input.vertexBindingDescriptionCount = 1;
    vertex_input.pVertexBindingDescriptions = &binding_desc;
    vertex_input.vertexAttributeDescriptionCount = 3;
    vertex_input.pVertexAttributeDescriptions = attr_desc;
    
    VkPipelineInputAssemblyStateCreateInfo input_asm_state;
    input_asm_state.sType = VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO;
    input_asm_state.pNext = NULL;
    input_asm_state.flags = 0;
    input_asm_state.topology = VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST;
    input_asm_state.primitiveRestartEnable = VK_FALSE;
    
    VkPipelineViewportStateCreateInfo viewport_state;
    viewport_state.sType = VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO;
    viewport_state.pNext = NULL;
    viewport_state.flags = 0;
    viewport_state.viewportCount = 1;
    viewport_state.pViewports = NULL;
    viewport_state.scissorCount = 1;
    viewport_state.pScissors = NULL;
    
    VkPipelineRasterizationStateCreateInfo raster_state;
    raster_state.sType = VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO;
    raster_state.pNext = NULL;
    raster_state.flags = 0;
    raster_state.depthClampEnable = VK_FALSE;
    raster_state.rasterizerDiscardEnable = VK_FALSE;
    raster_state.polygonMode = VK_POLYGON_MODE_FILL;
    raster_state.cullMode = VK_CULL_MODE_NONE;
    raster_state.frontFace = VK_FRONT_FACE_COUNTER_CLOCKWISE;
    raster_state.depthBiasEnable = VK_FALSE;
    raster_state.depthBiasConstantFactor = 0;
    raster_state.depthBiasClamp = 0;
    raster_state.depthBiasSlopeFactor = 0;
    raster_state.lineWidth = 1;
    
    VkPipelineMultisampleStateCreateInfo msaa_state;
    msaa_state.sType = VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO;
    msaa_state.pNext = NULL;
    msaa_state.flags = 0;
    msaa_state.rasterizationSamples = 1;
    msaa_state.sampleShadingEnable = VK_FALSE;
    msaa_state.minSampleShading = 0;
    msaa_state.pSampleMask = NULL;
    msaa_state.alphaToCoverageEnable = VK_FALSE;
    msaa_state.alphaToOneEnable = VK_FALSE;
    
    VkPipelineDepthStencilStateCreateInfo depth_stencil_state;
    depth_stencil_state.sType = VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO;
    depth_stencil_state.pNext = NULL;
    depth_stencil_state.flags = 0;
    depth_stencil_state.depthTestEnable = VK_TRUE;
    depth_stencil_state.depthWriteEnable = VK_TRUE;
    depth_stencil_state.depthCompareOp = VK_COMPARE_OP_LESS;
    depth_stencil_state.depthBoundsTestEnable = VK_FALSE;
    depth_stencil_state.stencilTestEnable = VK_FALSE;
    depth_stencil_state.front.failOp = VK_STENCIL_OP_KEEP;
    depth_stencil_state.front.passOp = VK_STENCIL_OP_KEEP;
    depth_stencil_state.front.depthFailOp = VK_STENCIL_OP_KEEP;
    depth_stencil_state.front.compareOp = VK_COMPARE_OP_ALWAYS;
    depth_stencil_state.front.compareMask = 0xFFFFFFFF;
    depth_stencil_state.front.writeMask = 0xFFFFFFFF;
    depth_stencil_state.front.reference = 0;
    depth_stencil_state.back.failOp = VK_STENCIL_OP_KEEP;
    depth_stencil_state.back.passOp = VK_STENCIL_OP_KEEP;
    depth_stencil_state.back.depthFailOp = VK_STENCIL_OP_KEEP;
    depth_stencil_state.back.compareOp = VK_COMPARE_OP_ALWAYS;
    depth_stencil_state.back.compareMask = 0xFFFFFFFF;
    depth_stencil_state.back.writeMask = 0xFFFFFFFF;
    depth_stencil_state.back.reference = 0;
    depth_stencil_state.minDepthBounds = 0;
    depth_stencil_state.maxDepthBounds = 1;
    
    VkPipelineColorBlendAttachmentState blend_attachment;
    blend_attachment.blendEnable = VK_FALSE;
    blend_attachment.srcColorBlendFactor = VK_BLEND_FACTOR_ZERO;
    blend_attachment.dstColorBlendFactor = VK_BLEND_FACTOR_ONE;
    blend_attachment.colorBlendOp = VK_BLEND_OP_ADD;
    blend_attachment.srcAlphaBlendFactor = VK_BLEND_FACTOR_ZERO;
    blend_attachment.dstAlphaBlendFactor = VK_BLEND_FACTOR_ONE;
    blend_attachment.alphaBlendOp = VK_BLEND_OP_ADD;
    blend_attachment.colorWriteMask = VK_COLOR_COMPONENT_R_BIT | 
                                      VK_COLOR_COMPONENT_G_BIT | 
                                      VK_COLOR_COMPONENT_B_BIT | 
                                      VK_COLOR_COMPONENT_A_BIT;
    
    VkPipelineColorBlendStateCreateInfo blend_state;
    blend_state.sType = VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO;
    blend_state.pNext = NULL;
    blend_state.flags = 0;
    blend_state.logicOpEnable = VK_FALSE;
    blend_state.logicOp = VK_LOGIC_OP_NO_OP;
    blend_state.attachmentCount = 1;
    blend_state.pAttachments = &blend_attachment;
    blend_state.blendConstants[0] = 0;
    blend_state.blendConstants[1] = 0;
    blend_state.blendConstants[2] = 0;
    blend_state.blendConstants[3] = 0;
    
    VkPipelineDynamicStateCreateInfo dynamic_state;
    dynamic_state.sType = VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO;
    dynamic_state.pNext = NULL;
    dynamic_state.flags = 0;
    dynamic_state.dynamicStateCount = 2;
    VkDynamicState dynamic_states[] = {VK_DYNAMIC_STATE_VIEWPORT, VK_DYNAMIC_STATE_SCISSOR};
    dynamic_state.pDynamicStates = dynamic_states;
    
    VkGraphicsPipelineCreateInfo pipeline_info;
    pipeline_info.sType = VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO;
    pipeline_info.pNext = NULL;
    pipeline_info.flags = 0;
    pipeline_info.stageCount = 2;
    pipeline_info.pStages = pipeline_stages;
    pipeline_info.pVertexInputState = &vertex_input;
    pipeline_info.pInputAssemblyState = &input_asm_state;
    pipeline_info.pTessellationState = NULL;
    pipeline_info.pViewportState = &viewport_state;
    pipeline_info.pRasterizationState = &raster_state;
    pipeline_info.pMultisampleState = &msaa_state;
    pipeline_info.pDepthStencilState = &depth_stencil_state;
    pipeline_info.pColorBlendState = &blend_state;
    pipeline_info.pDynamicState = &dynamic_state;
    pipeline_info.layout = res->pipeline_layout;
    pipeline_info.renderPass = renderpass;
    pipeline_info.subpass = 0;
    pipeline_info.basePipelineHandle = VK_NULL_HANDLE;
    pipeline_info.basePipelineIndex = 0;
    VKR(vkCreateGraphicsPipelines(device, VK_NULL_HANDLE, 1, &pipeline_info, NULL, &res->pipeline));
    
    return res;
}

void vk_del_mesh(r_mesh_t* mesh) { //TODO: Ensure that it is not in use
    vkDestroyPipeline(device, mesh->pipeline, NULL);
    vkDestroyPipelineLayout(device, mesh->pipeline_layout, NULL);
    vkDestroyBuffer(device, mesh->vertex_buffer, NULL);
    vkDestroyBuffer(device, mesh->index_buffer, NULL);
    vkFreeMemory(device, mesh->index_mem, NULL);
    vkFreeMemory(device, mesh->vertex_mem, NULL);
    free(mesh);
}

void vk_frame(cam_t cam, s_sun_obj_t* sun, size_t obj_count, const r_obj_t* objs) {
    m_vec3_normalize(&cam.dir, &cam.dir);
    
    m_mat4_t mvp1, mvp2, model1, model2, view, proj, vp;
    m_mat4_create_perspective(&proj, 45, cur_width/(float)cur_height, 0.1, 100);
    m_mat4_create_lookat(&view, &cam.pos, &cam.dir, &cam.up);
    m_mat4_create_identity(&model1);
    m_mat4_create_translate(&model2, -2, -2, -1);
    m_mat4_mul(&vp, &proj, &view);
    m_mat4_mul(&mvp1, &vp, &model1);
    m_mat4_mul(&mvp2, &vp, &model2);
    
    VkSurfaceCapabilitiesKHR surf_caps;
    VKR(fGetPhysicalDeviceSurfaceCapabilitiesKHR(physical_device, surface, &surf_caps));
    uint32_t width = surf_caps.currentExtent.width;
    uint32_t height = surf_caps.currentExtent.height;
    
    if (width!=cur_width || height!=cur_height) {
        swapchain_resize(swapchain, width, height);
        cur_width = width;
        cur_height = height;
    }
    
    swapchain_acquire_t buf;
    swapchain_acquire(swapchain, acquire_image_sem, &buf);
    
    vku_begin_cmd_buf(command_buffer, VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT);
    
    vku_image_barrier(VK_IMAGE_ASPECT_COLOR_BIT, command_buffer, VK_ACCESS_MEMORY_READ_BIT,
                  VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
                  VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
                  VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, buf.color_image);
    
    vku_image_barrier(VK_IMAGE_ASPECT_DEPTH_BIT|VK_IMAGE_ASPECT_STENCIL_BIT, command_buffer,
                  VK_ACCESS_MEMORY_READ_BIT, VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT,
                  VK_IMAGE_LAYOUT_PRESENT_SRC_KHR, VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
                  VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT,
                  buf.depthstencil_image);
    
    VkViewport viewport = (VkViewport){0, 0, cur_width, cur_height, 0.0, 1.0};
    VkRect2D scissor = (VkRect2D){(VkOffset2D){0, 0}, (VkExtent2D){cur_width, cur_height}};
    vkCmdSetViewport(command_buffer, 0, 1, &viewport);
    vkCmdSetScissor(command_buffer, 0, 1, &scissor);
    
    VkClearValue clear_values[2];
    clear_values[0].color.float32[0] = 1;
    clear_values[0].color.float32[1] = 0.5;
    clear_values[0].color.float32[2] = 0.5;
    clear_values[0].color.float32[3] = 1;
    clear_values[1].depthStencil.depth = 1;
    clear_values[1].depthStencil.stencil = 0;
    
    VkRenderPassBeginInfo begin_rp_info;
    begin_rp_info.sType = VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO;
    begin_rp_info.pNext = NULL;
    begin_rp_info.renderPass = renderpass;
    begin_rp_info.framebuffer = buf.framebuffer;
    begin_rp_info.renderArea = (VkRect2D){(VkOffset2D){0, 0}, (VkExtent2D){cur_width, cur_height}};
    begin_rp_info.clearValueCount = 2;
    begin_rp_info.pClearValues = clear_values;
    vkCmdBeginRenderPass(command_buffer, &begin_rp_info, VK_SUBPASS_CONTENTS_INLINE);
    
    for (size_t i = 0; i < obj_count; i++) {
        const r_obj_t* obj = &objs[i];
        const r_mesh_t* mesh = obj->mesh;
        
        vkCmdBindPipeline(command_buffer, VK_PIPELINE_BIND_POINT_GRAPHICS, mesh->pipeline);
        VkDeviceSize vert_offset = 0;
        vkCmdBindVertexBuffers(command_buffer, 0, 1, &mesh->vertex_buffer, &vert_offset);
        vkCmdBindIndexBuffer(command_buffer, mesh->index_buffer, 0, VK_INDEX_TYPE_UINT32);
        
        m_mat4_t push_constants[2];
        m_mat4_mul(&push_constants[0], &vp, &obj->mat);
        
        m_mat4_t temp_mat;
        m_mat4_inverse(&temp_mat, &obj->mat);
        m_mat4_transpose(&push_constants[1], &temp_mat);
        
        vkCmdPushConstants(command_buffer, mesh->pipeline_layout, VK_SHADER_STAGE_VERTEX_BIT, 0, 128, push_constants);
        
        vkCmdDrawIndexed(command_buffer, mesh->index_count, 1, 0, 0, 0);
    }
    
    vkCmdEndRenderPass(command_buffer);
    
    vku_image_barrier(VK_IMAGE_ASPECT_COLOR_BIT, command_buffer, VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,
                  VK_ACCESS_MEMORY_READ_BIT, VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                  VK_IMAGE_LAYOUT_PRESENT_SRC_KHR, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT,
                  VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT, buf.color_image);
    
    vku_image_barrier(VK_IMAGE_ASPECT_DEPTH_BIT|VK_IMAGE_ASPECT_STENCIL_BIT, command_buffer,
                  VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT, VK_ACCESS_MEMORY_READ_BIT, 
                  VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL, VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
                  VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT,
                  buf.depthstencil_image);
    
    vkEndCommandBuffer(command_buffer);
    
    VkSubmitInfo submit_info;
    submit_info.sType = VK_STRUCTURE_TYPE_SUBMIT_INFO;
    submit_info.pNext = NULL;
    submit_info.waitSemaphoreCount = 1;
    submit_info.pWaitSemaphores = &acquire_image_sem;
    VkPipelineStageFlags wait_stage_mask = VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT;
    submit_info.pWaitDstStageMask = &wait_stage_mask;
    submit_info.commandBufferCount = 1;
    submit_info.pCommandBuffers = &command_buffer;
    submit_info.signalSemaphoreCount = 1;
    submit_info.pSignalSemaphores = &render_done_sem;
    vkQueueSubmit(queue, 1, &submit_info, VK_NULL_HANDLE);
    
    swapchain_present(swapchain, render_done_sem, buf);
    
    vkQueueWaitIdle(queue);
}
