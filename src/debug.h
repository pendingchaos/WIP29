#ifndef DEBUG_H
#define DEBUG_H

#include "mathy_stuff.h"

typedef struct d_line_t {
    m_vec3_t a;
    m_vec3_t b;
    float color[3];
} d_line_t;

typedef struct d_tri_t {
    m_vec3_t a;
    m_vec3_t b;
    m_vec3_t c;
    float color[3];
} d_tri_t;

void d_line(m_vec3_t a, m_vec3_t b, float red, float green, float blue);
void d_tri(m_vec3_t a, m_vec3_t b, m_vec3_t c, float red, float green, float blue);

#endif
