#include "rapi.h"

void gl_init_api(SDL_Window* window);
void gl_deinit_api();

void gl_init_swapchain(uint32_t width, uint32_t height, uint32_t flags);
r_image_t* gl_get_swapchain_color_image();
r_image_t* gl_swapchain_get_depth_image();

r_image_t* gl_create_image(r_image_create_info_t info);
void gl_del_image(r_image_t* image);

r_shader_t* gl_create_shader(const b_shader_t* shader);
void gl_del_shader(r_shader_t* shader);

r_sampler_t* gl_sampler_create();
void gl_del_sampler(r_sampler_t* sampler);
void gl_sampler_min_filter(r_sampler_t* sampler, r_sampler_filter_t filter);
void gl_sampler_mag_filter(r_sampler_t* sampler, r_sampler_filter_t filter);
void gl_sampler_mipmap_filter(r_sampler_t* sampler, r_sampler_filter_t filter);
void gl_sampler_address_modes(r_sampler_t* sampler, const r_sampler_address_mode_t* addr_modes);
void gl_sampler_anisotropy(r_sampler_t* sampler, bool enabled, float max_anisotropy);
void gl_sampler_min_max_lod(r_sampler_t* sampler, float min_lod, float max_lod);
void gl_sampler_finalize(r_sampler_t* sampler);

r_sampler_t* gl_sampler_create();
void gl_del_sampler(r_sampler_t* sampler);
void gl_sampler_min_filter(r_sampler_t* sampler, r_sampler_filter_t filter);
void gl_sampler_mag_filter(r_sampler_t* sampler, r_sampler_filter_t filter);
void gl_sampler_mipmap_filter(r_sampler_t* sampler, r_sampler_filter_t filter);
void gl_sampler_address_modes(r_sampler_t* sampler, const r_sampler_address_mode_t* addr_mode);
void gl_sampler_anisotropy(r_sampler_t* sampler, bool enabled, float max_anisotropy);
void gl_sampler_min_max_lod(r_sampler_t* sampler, float min_lod, float max_lod);
void gl_sampler_finalize(r_sampler_t* sampler);

r_pass_t* gl_pass_create();
void gl_pass_del(r_pass_t* pass);
void gl_pass_add_attachment(r_pass_t* pass, uint32_t flags);
void gl_pass_add_subpass(r_pass_t* pass, const r_subpass_t* subpass);
void gl_pass_finalize(r_pass_t* pass);

r_framebuffer_t* gl_framebuffer_create(r_pass_t* pass, r_image_t*const* images);
void gl_framebuffer_del(r_framebuffer_t* framebuffer);

r_buffer_t* gl_buffer_create(uint32_t size, const void* data, r_buffer_flags_t flags);
void gl_buffer_del(r_buffer_t* buffer);

r_desc_set_layout_t* gl_desc_set_layout_create(uint32_t binding_count, r_desc_set_layout_binding_t* bindings);
void gl_desc_set_layout_del(r_desc_set_layout_t* layout);

r_desc_set_t* gl_desc_set_create(r_desc_set_layout_t* layout, const r_desc_t* descs);
void gl_desc_set_del(r_desc_set_t* set);

r_timestamp_t* gl_timestamp_create();
void gl_timestamp_del(r_timestamp_t* timestamp);
bool gl_timestamp_available(r_timestamp_t* timestamp);
uint64_t gl_timestamp_get(r_timestamp_t* timestamp);

r_pipeline_t* gl_pipeline_create(r_pipeline_type_t type);
void gl_pipeline_del(r_pipeline_t* pipeline);
void gl_pipeline_push_constant_info(r_pipeline_t* pipeline, uint32_t start, uint32_t size);
void gl_pipeline_vertex_shader(r_pipeline_t* pipeline, r_shader_t* vertex);
void gl_pipeline_fragment_shader(r_pipeline_t* pipeline, r_shader_t* fragment);
void gl_pipeline_compute_shader(r_pipeline_t* pipeline, r_shader_t* compute);
void gl_pipeline_vertex_attribute(r_pipeline_t* pipeline, uint32_t index, uint32_t stride, uint32_t offset, r_vertex_format_t format);
void gl_pipeline_topology(r_pipeline_t* pipeline, r_topology_t topology);
void gl_pipeline_cull(r_pipeline_t* pipeline, r_cull_mode_t mode, r_winding_t winding);
void gl_pipeline_depth_stencil(r_pipeline_t* pipeline, uint32_t flags, r_compare_func_t compare, r_stencil_state_face_t* faces);
void gl_pipeline_pass(r_pipeline_t* pipeline, r_pass_t* pass, uint32_t subpass_index);
void gl_pipeline_desc_set_layout(r_pipeline_t* pipeline, r_stage_t stage, r_desc_set_layout_t* layout);
void gl_pipeline_finalize(r_pipeline_t* pipeline);

r_cmd_buf_t* gl_cmd_buf_create(r_cmd_buf_type_t type);
void gl_cmd_buf_del(r_cmd_buf_t* cmd_buf);
void gl_cmd_buf_begin(r_cmd_buf_t* cmd_buf);
void gl_cmd_buf_end(r_cmd_buf_t* cmd_buf);
void gl_cmd_buf_set_viewport(r_cmd_buf_t* cmd_buf, uint32_t left, uint32_t bottom, uint32_t width, uint32_t height);
void gl_cmd_buf_pass_begin(r_cmd_buf_t* cmd_buf, r_pass_t* pass, r_framebuffer_t* framebuffer, const r_clear_value_t* clear_values);
void gl_cmd_buf_pass_end(r_cmd_buf_t* cmd_buf);
void gl_cmd_buf_next_subpass(r_cmd_buf_t* cmd_buf);
void gl_cmd_buf_set_timestamp(r_cmd_buf_t* cmd_buf, r_timestamp_t* timestamp);
void gl_cmd_buf_bind_pipeline(r_cmd_buf_t* cmd_buf, const r_pipeline_t* pipeline);
void gl_cmd_buf_bind_vertex_buffers(r_cmd_buf_t* cmd_buf, uint32_t count, r_buffer_t*const* buffers, const uint32_t* offsets);
void gl_cmd_buf_bind_index_buffer(r_cmd_buf_t* cmd_buf, const r_buffer_t* buffer, uint32_t offset, r_index_type_t type);
void gl_cmd_buf_set_push_constants(r_cmd_buf_t* cmd_buf, uint32_t start, uint32_t size, const void* data);
void gl_cmd_buf_set_desc_set(r_cmd_buf_t* cmd_buf, r_stage_t stage, r_desc_set_t* set);
void gl_cmd_buf_image_barriers(r_cmd_buf_t* cmd_buf, uint32_t count, r_image_t*const* image, const r_image_access_t** access, const r_image_layout_t** layout);
void gl_cmd_buf_set_buffer_data(r_cmd_buf_t* cmd_buf, r_buffer_t* buffer, uint32_t offset, uint32_t size, const void* data);
void gl_cmd_buf_draw_indexed(r_cmd_buf_t* cmd_buf, uint32_t index_count, uint32_t instance_count, uint32_t first_index, int32_t vertex_offset, uint32_t first_instance);
void gl_cmd_buf_draw(r_cmd_buf_t* cmd_buf, uint32_t vertex_count, uint32_t instance_count, uint32_t first_vertex, uint32_t first_instance);
void gl_cmd_buf_dispatch_compute(r_cmd_buf_t* cmd_buf, const uint32_t* dim);
void gl_cmd_buf_submit(uint32_t count, r_cmd_buf_t*const* cmd_buf);

void gl_frame_submit();

static r_api_t api;

void r_init_api(r_api_t api_, SDL_Window* window) {
    api = api_;
    switch (api) {
    case R_API_OPENGL: gl_init_api(window);
    }
}

void r_deinit_api() {
    switch (api) {
    case R_API_OPENGL: gl_deinit_api();
    }
}

void r_init_swapchain(uint32_t width, uint32_t height, uint32_t flags) {
    switch (api) {
    case R_API_OPENGL: gl_init_swapchain(width, height, flags);
    }
}

r_image_t* r_swapchain_get_color_image() {
    switch (api) {
    case R_API_OPENGL: return gl_get_swapchain_color_image();
    }
    return NULL;
}

r_image_t* r_swapchain_get_depth_image() {
    switch (api) {
    case R_API_OPENGL: return gl_swapchain_get_depth_image();
    }
    return NULL;
}

r_image_t* r_image_create(r_image_create_info_t info) {
    switch (api) {
    case R_API_OPENGL: return gl_create_image(info);
    }
    return NULL;
}

void r_image_del(r_image_t* image) {
    switch (api) {
    case R_API_OPENGL: gl_del_image(image);
    }
}

r_shader_t* r_shader_create(const b_shader_t* shader) {
    switch (api) {
    case R_API_OPENGL: return gl_create_shader(shader);
    }
    return NULL;
}

void r_shader_del(r_shader_t* shader) {
    switch (api) {
    case R_API_OPENGL: gl_del_shader(shader);
    }
}

r_sampler_t* r_sampler_create() {
    switch (api) {
    case R_API_OPENGL: return gl_sampler_create();
    }
    return NULL;
}

void r_sampler_del(r_sampler_t* sampler) {
    switch (api) {
    case R_API_OPENGL: gl_del_sampler(sampler);
    }
}

void r_sampler_min_filter(r_sampler_t* sampler, r_sampler_filter_t filter) {
    switch (api) {
    case R_API_OPENGL: gl_sampler_min_filter(sampler, filter);
    }
}

void r_sampler_mag_filter(r_sampler_t* sampler, r_sampler_filter_t filter) {
    switch (api) {
    case R_API_OPENGL: gl_sampler_mag_filter(sampler, filter);
    }
}

void r_sampler_mipmap_filter(r_sampler_t* sampler, r_sampler_filter_t filter) {
    switch (api) {
    case R_API_OPENGL: gl_sampler_mipmap_filter(sampler, filter);
    }
}

void r_sampler_address_modes(r_sampler_t* sampler, const r_sampler_address_mode_t* addr_modes) {
    switch (api) {
    case R_API_OPENGL: gl_sampler_address_modes(sampler, addr_modes);
    }
}

void r_sampler_anisotropy(r_sampler_t* sampler, bool enabled, float max_anisotropy) {
    switch (api) {
    case R_API_OPENGL: gl_sampler_anisotropy(sampler, enabled, max_anisotropy);
    }
}

void r_sampler_min_max_lod(r_sampler_t* sampler, float min_lod, float max_lod) {
    switch (api) {
    case R_API_OPENGL: gl_sampler_min_max_lod(sampler, min_lod, max_lod);
    }
}

void r_sampler_finalize(r_sampler_t* sampler) {
    switch (api) {
    case R_API_OPENGL: gl_sampler_finalize(sampler);
    }
}

r_pass_t* r_pass_create() {
    switch (api) {
    case R_API_OPENGL: return gl_pass_create();
    }
    return NULL;
}

void r_pass_del(r_pass_t* pass) {
    switch (api) {
    case R_API_OPENGL: gl_pass_del(pass);
    }
}

void r_pass_add_attachment(r_pass_t* pass, uint32_t flags) {
    switch (api) {
    case R_API_OPENGL: gl_pass_add_attachment(pass, flags);
    }
}

void r_pass_add_subpass(r_pass_t* pass, const r_subpass_t* subpass) {
    switch (api) {
    case R_API_OPENGL: gl_pass_add_subpass(pass, subpass);
    }
}

void r_pass_finalize(r_pass_t* pass) {
    switch (api) {
    case R_API_OPENGL: gl_pass_finalize(pass);
    }
}

r_framebuffer_t* r_framebuffer_create(r_pass_t* pass, r_image_t*const* images) {
    switch (api) {
    case R_API_OPENGL: return gl_framebuffer_create(pass, images);
    }
    return NULL;
}

void r_framebuffer_del(r_framebuffer_t* framebuffer) {
    switch (api) {
    case R_API_OPENGL: gl_framebuffer_del(framebuffer);
    }
}

r_buffer_t* r_buffer_create(uint32_t size, const void* data, r_buffer_flags_t flags) {
    switch (api) {
    case R_API_OPENGL: return gl_buffer_create(size, data, flags);
    }
    return NULL;
}

void r_buffer_del(r_buffer_t* buffer) {
    switch (api) {
    case R_API_OPENGL: gl_buffer_del(buffer);
    }
}

r_desc_set_layout_t* r_desc_set_layout_create(uint32_t binding_count, r_desc_set_layout_binding_t* bindings) {
    switch (api) {
    case R_API_OPENGL: return gl_desc_set_layout_create(binding_count, bindings);
    }
    return NULL;
}

void r_desc_set_layout_del(r_desc_set_layout_t* layout) {
    switch (api) {
    case R_API_OPENGL: gl_desc_set_layout_del(layout);
    }
}

r_desc_set_t* r_desc_set_create(r_desc_set_layout_t* layout, const r_desc_t* descs) {
    switch (api) {
    case R_API_OPENGL: return gl_desc_set_create(layout, descs);
    }
    return NULL;
}

void r_desc_set_del(r_desc_set_t* set) {
    switch (api) {
    case R_API_OPENGL: gl_desc_set_del(set);
    }
}

r_timestamp_t* r_timestamp_create() {
    switch (api) {
    case R_API_OPENGL: return gl_timestamp_create();
    }
    return NULL;
}

void r_timestamp_del(r_timestamp_t* timestamp) {
    switch (api) {
    case R_API_OPENGL: gl_timestamp_del(timestamp);
    }
}

bool r_timestamp_available(r_timestamp_t* timestamp) {
    switch (api) {
    case R_API_OPENGL: return gl_timestamp_available(timestamp);
    }
    return false;
}

uint64_t r_timestamp_get(r_timestamp_t* timestamp) {
    switch (api) {
    case R_API_OPENGL: return gl_timestamp_get(timestamp);
    }
    return 0;
}

r_pipeline_t* r_pipeline_create(r_pipeline_type_t type) {
    switch (api) {
    case R_API_OPENGL: return gl_pipeline_create(type);
    }
    return NULL;
}

void r_pipeline_del(r_pipeline_t* pipeline) {
    switch (api) {
    case R_API_OPENGL: gl_pipeline_del(pipeline);
    }
}

void r_pipeline_push_constant_info(r_pipeline_t* pipeline, uint32_t start, uint32_t size) {
    switch (api) {
    case R_API_OPENGL: gl_pipeline_push_constant_info(pipeline, start, size);
    }
}

void r_pipeline_vertex_shader(r_pipeline_t* pipeline, r_shader_t* vertex) {
    switch (api) {
    case R_API_OPENGL: gl_pipeline_vertex_shader(pipeline, vertex);
    }
}

void r_pipeline_fragment_shader(r_pipeline_t* pipeline, r_shader_t* fragment) {
    switch (api) {
    case R_API_OPENGL: gl_pipeline_fragment_shader(pipeline, fragment);
    }
}

void r_pipeline_vertex_attribute(r_pipeline_t* pipeline, uint32_t index, uint32_t stride, uint32_t offset, r_vertex_format_t format) {
    switch (api) {
    case R_API_OPENGL: gl_pipeline_vertex_attribute(pipeline, index, stride, offset, format);
    }
}

void r_pipeline_topology(r_pipeline_t* pipeline, r_topology_t topology) {
    switch (api) {
    case R_API_OPENGL: gl_pipeline_topology(pipeline, topology);
    }
}

void r_pipeline_cull(r_pipeline_t* pipeline, r_cull_mode_t mode, r_winding_t winding) {
    switch (api) {
    case R_API_OPENGL: gl_pipeline_cull(pipeline, mode, winding);
    }
}

void r_pipeline_depth_stencil(r_pipeline_t* pipeline, uint32_t flags, r_compare_func_t compare, r_stencil_state_face_t* faces) {
    switch (api) {
    case R_API_OPENGL: gl_pipeline_depth_stencil(pipeline, flags, compare, faces);
    }
}

void r_pipeline_pass(r_pipeline_t* pipeline, r_pass_t* pass, uint32_t subpass_index) {
    switch (api) {
    case R_API_OPENGL: gl_pipeline_pass(pipeline, pass, subpass_index);
    }
}

void r_pipeline_desc_set_layout(r_pipeline_t* pipeline, r_stage_t stage, r_desc_set_layout_t* layout) {
    switch (api) {
    case R_API_OPENGL: gl_pipeline_desc_set_layout(pipeline, stage, layout);
    }
}

void r_pipeline_finalize(r_pipeline_t* pipeline) {
    switch (api) {
    case R_API_OPENGL: gl_pipeline_finalize(pipeline);
    }
}

r_cmd_buf_t* r_cmd_buf_create(r_cmd_buf_type_t type) {
    switch (api) {
    case R_API_OPENGL: return gl_cmd_buf_create(type);
    }
    return NULL;
}

void r_cmd_buf_del(r_cmd_buf_t* cmd_buf) {
    switch (api) {
    case R_API_OPENGL: gl_cmd_buf_del(cmd_buf);
    }
}

void r_cmd_buf_begin(r_cmd_buf_t* cmd_buf) {
    switch (api) {
    case R_API_OPENGL: gl_cmd_buf_begin(cmd_buf);
    }
}

void r_cmd_buf_end(r_cmd_buf_t* cmd_buf) {
    switch (api) {
    case R_API_OPENGL: gl_cmd_buf_end(cmd_buf);
    }
}

void r_cmd_buf_set_viewport(r_cmd_buf_t* cmd_buf, uint32_t left, uint32_t bottom, uint32_t width, uint32_t height) {
    switch (api) {
    case R_API_OPENGL: gl_cmd_buf_set_viewport(cmd_buf, left, bottom, width, height);
    }
}

void r_cmd_buf_pass_begin(r_cmd_buf_t* cmd_buf, r_pass_t* pass, r_framebuffer_t* framebuffer, const r_clear_value_t* clear_values) {
    switch (api) {
    case R_API_OPENGL: gl_cmd_buf_pass_begin(cmd_buf, pass, framebuffer, clear_values);
    }
}

void r_cmd_buf_pass_end(r_cmd_buf_t* cmd_buf) {
    switch (api) {
    case R_API_OPENGL: gl_cmd_buf_pass_end(cmd_buf);
    }
}

void r_cmd_buf_next_subpass(r_cmd_buf_t* cmd_buf) {
    switch (api) {
    case R_API_OPENGL: gl_cmd_buf_next_subpass(cmd_buf);
    }
}

void r_cmd_buf_set_timestamp(r_cmd_buf_t* cmd_buf, r_timestamp_t* timestamp) {
    switch (api) {
    case R_API_OPENGL: gl_cmd_buf_set_timestamp(cmd_buf, timestamp);
    }
}

void r_cmd_buf_bind_pipeline(r_cmd_buf_t* cmd_buf, const r_pipeline_t* pipeline) {
    switch (api) {
    case R_API_OPENGL: gl_cmd_buf_bind_pipeline(cmd_buf, pipeline);
    }
}

void r_cmd_buf_bind_vertex_buffers(r_cmd_buf_t* cmd_buf, uint32_t count, r_buffer_t*const* buffers, const uint32_t* offsets) {
    switch (api) {
    case R_API_OPENGL: gl_cmd_buf_bind_vertex_buffers(cmd_buf, count, buffers, offsets);
    }
}

void r_cmd_buf_bind_index_buffer(r_cmd_buf_t* cmd_buf, const r_buffer_t* buffer, uint32_t offset, r_index_type_t type) {
    switch (api) {
    case R_API_OPENGL: gl_cmd_buf_bind_index_buffer(cmd_buf, buffer, offset, type);
    }
}

void r_cmd_buf_set_push_constants(r_cmd_buf_t* cmd_buf, uint32_t start, uint32_t size, const void* data) {
    switch (api) {
    case R_API_OPENGL: gl_cmd_buf_set_push_constants(cmd_buf, start, size, data);
    }
}

void r_cmd_buf_bind_desc_set(r_cmd_buf_t* cmd_buf, r_stage_t stage, r_desc_set_t* set) {
    switch (api) {
    case R_API_OPENGL: gl_cmd_buf_set_desc_set(cmd_buf, stage, set);
    }
}

void r_cmd_buf_image_barriers(r_cmd_buf_t* cmd_buf, uint32_t count, r_image_t*const* image, const r_image_access_t** access, const r_image_layout_t** layout) {
    switch (api) {
    case R_API_OPENGL: gl_cmd_buf_image_barriers(cmd_buf, count, image, access, layout);
    }
}

void r_cmd_buf_set_buffer_data(r_cmd_buf_t* cmd_buf, r_buffer_t* buffer, uint32_t offset, uint32_t size, const void* data) {
    switch (api) {
    case R_API_OPENGL: gl_cmd_buf_set_buffer_data(cmd_buf, buffer, offset, size, data);
    }
}

void r_cmd_buf_draw_indexed(r_cmd_buf_t* cmd_buf, uint32_t index_count, uint32_t instance_count, uint32_t first_index, int32_t vertex_offset, uint32_t first_instance) {
    switch (api) {
    case R_API_OPENGL: gl_cmd_buf_draw_indexed(cmd_buf, index_count, instance_count, first_index, vertex_offset, first_instance);
    }
}

void r_cmd_buf_draw(r_cmd_buf_t* cmd_buf, uint32_t vertex_count, uint32_t instance_count, uint32_t first_index, uint32_t first_instance) {
    switch (api) {
    case R_API_OPENGL: gl_cmd_buf_draw(cmd_buf, vertex_count, instance_count, first_index, first_instance);
    }
}

void r_cmd_buf_dispatch_compute(r_cmd_buf_t* cmd_buf, const uint32_t* dim) {
    switch (api) {
    case R_API_OPENGL: gl_cmd_buf_dispatch_compute(cmd_buf, dim);
    }
}

void r_cmd_buf_submit(uint32_t count, r_cmd_buf_t*const* cmd_buf) {
    switch (api) {
    case R_API_OPENGL: gl_cmd_buf_submit(count, cmd_buf);
    }
}

void r_frame_submit() {
    switch (api) {
    case R_API_OPENGL: gl_frame_submit();
    }
}
