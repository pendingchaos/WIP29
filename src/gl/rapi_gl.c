#include "rapi.h"
#include "glfl.h"
#include "utils.h"

#include <assert.h>
#include <SDL2/SDL_video.h>

#define DEBUG_GROUP_ID 0

typedef struct program_t {
    union {
        struct {GLuint first; GLuint second;};
        uint64_t shader_combination;
    };
    GLuint program;
} program_t;

static SDL_Window* window;
static SDL_GLContext context;
static uint32_t width, height;
static r_image_t* color_image;
static r_image_t* depth_image;
static GLuint blit_vao;
static GLuint blit_program;
static GLuint vao;
static size_t program_count;
static program_t* programs;

typedef struct r_image_t {
    r_image_type_t type;
    GLuint id;
} r_image_t;

typedef struct r_shader_t {
    GLuint shader;
} r_shader_t;

typedef struct gl_subpass_t {
    GLuint framebuffer;
} gl_subpass_t;

typedef struct r_pass_t {
    uint32_t attachment_count;
    uint32_t attachment_flags[8];
    uint32_t subpass_count;
    r_subpass_t* subpasses;
} r_pass_t;

typedef struct r_framebuffer_t {
    r_pass_t* pass;
    uint32_t image_count;
    r_image_t** images;
    GLuint* framebuffers;
    gl_subpass_t* subpasses;
} r_framebuffer_t;

typedef struct r_buffer_t {
    GLuint id;
} r_buffer_t;

typedef struct r_desc_set_layout_t {
    uint32_t binding_count;
    r_desc_set_layout_binding_t* bindings;
} r_desc_set_layout_t;

typedef struct gl_desc_t {
    r_desc_type_t type;
    uint32_t binding;
    GLuint vals[3];
} gl_desc_t;

typedef struct r_desc_set_t {
    uint32_t desc_count;
    gl_desc_t* descs;
} r_desc_set_t;

typedef struct gl_vertex_attrib_t {
    uint32_t index;
    uint32_t stride;
    GLuint offset;
    uint32_t components;
    GLenum type;
} gl_vertex_attrib_t;

typedef struct r_pipeline_t {
    r_pipeline_type_t type;
    
    GLuint vertex_shader;
    GLuint fragment_shader;
    GLuint compute_shader;
    GLuint program;
    
    uint32_t vertex_attrib_count;
    gl_vertex_attrib_t vertex_attribs[16];
    
    GLenum primitive;
    
    GLboolean enable_culling;
    GLenum cull_face;
    GLenum winding;
    
    GLboolean enable_depth_test;
    GLboolean depth_mask;
    GLenum depth_compare_func;
    
    GLboolean enable_stencil_test;
    GLenum front_compare_func;
    GLint front_compare_ref;
    GLuint front_compare_mask;
    GLuint front_write_mask;
    GLenum front_op_sfail;
    GLenum front_op_dpfail;
    GLenum front_op_dppass;
    GLenum back_compare_func;
    GLint back_compare_ref;
    GLuint back_compare_mask;
    GLuint back_write_mask;
    GLenum back_op_sfail;
    GLenum back_op_dpfail;
    GLenum back_op_dppass;
    
    r_pass_t* pass;
    uint32_t subpass_index;
} r_pipeline_t;

typedef struct r_timestamp_t {
    GLuint query;
} r_timestamp_t;

typedef enum gl_cmd_type_t {
    GL_CMD_SET_VIEWPORT,
    GL_CMD_BEGIN_PASS,
    GL_CMD_END_PASS,
    GL_CMD_NEXT_SUBPASS,
    GL_CMD_SET_TIMESTAMP,
    GL_CMD_BIND_PIPELINE,
    GL_CMD_BIND_VERTEX_BUFFERS,
    GL_CMD_BIND_INDEX_BUFFER,
    GL_CMD_SET_PUSH_CONSTANTS,
    GL_CMD_SET_DESC_SET,
    GL_CMD_SET_BUFFER_DATA,
    GL_CMD_DRAW_INDEXED,
    GL_CMD_DRAW,
    GL_CMD_DISPATCH_COMPUTE
} gl_cmd_type_t;

typedef struct gl_cmd_t {
    gl_cmd_type_t type;
    union {
        uint32_t viewport[4];
        struct {
            r_pass_t* pass;
            r_framebuffer_t* framebuffer;
            r_clear_value_t clear_values[8];
        } begin_pass;
        r_timestamp_t* timestamp;
        const r_pipeline_t* pipeline;
        struct {
            const r_buffer_t* buffers[16];
            uint32_t offsets[16];
        } bind_vertex_bufs;
        struct {
            const r_buffer_t* buffer;
            uint32_t offset;
            GLenum type;
        } bind_index_buf;
        struct {
            uint32_t start, amount;
            uint8_t data[128];
        } set_push_constants;
        struct {
            r_stage_t stage;
            r_desc_set_t* set;
        } desc_set;
        struct {
            r_buffer_t* buffer;
            uint32_t offset;
            uint32_t size;
            void* data;
        } buf_data;
        struct {
            uint32_t index_count;
            uint32_t instance_count;
            uint32_t first_index;
            uint32_t first_instance;
            uint32_t vertex_offset;
        } draw_indexed;
        struct {
            uint32_t vertex_count;
            uint32_t instance_count;
            uint32_t first_vertex;
            uint32_t first_instance;
        } draw;
        struct {
            uint32_t dim[3];
        } dispatch_compute;
    };
} gl_cmd_t;

typedef struct r_cmd_buf_t {
    char* label;
    uint32_t cmd_count;
    gl_cmd_t* cmds;
} r_cmd_buf_t;

typedef struct r_sampler_t {
    char* label;
    GLuint id;
    r_sampler_filter_t min_filter, mipmap_filter;
} r_sampler_t;

static GLuint get_program(GLuint first, GLuint second) {
    union {
        struct {GLuint f, s;};
        uint64_t comb;
    } u;
    u.f = first;
    u.s = second;
    
    for (size_t i = 0; i < program_count; i++)
        if (programs[i].shader_combination == u.comb)
            return programs[i].program;
    
    GLuint program = glCreateProgram();
    glAttachShader(program, first);
    if (second) glAttachShader(program, second);
    
    GLchar info_log[1024];
    
    glLinkProgram(program);
    glGetProgramInfoLog(program, sizeof(info_log), NULL, info_log);
    if (strlen(info_log))
        printf("program link info log: %s\n", info_log);
    
    glValidateProgram(program);
    glGetProgramInfoLog(program, sizeof(info_log), NULL, info_log);
    if (strlen(info_log))
        printf("program validate info log: %s\n", info_log);
    
    program_t new_program;
    new_program.shader_combination = u.comb;
    new_program.program = program;
    programs = realloc(programs, (program_count+1)*sizeof(program_t));
    programs[program_count++] = new_program;
    
    return program;
}

static void debug_group_start(const char* format, ...) {
    char message[1024];
    
    va_list list;
    va_start(list, format);
    vsnprintf(message, sizeof(message)-1, format, list);
    va_end(list);
    
    glPushDebugGroup(GL_DEBUG_SOURCE_APPLICATION, DEBUG_GROUP_ID, -1, message);
}

static void debug_group_end() {
    glPopDebugGroup();
}

static void debug_callback(GLenum _1, GLenum _2, GLuint id, GLenum severity,
                           GLsizei _5, const char* msg, const void* _6) {
    if (id == 131169 || id == 131185 || id == 131218 || id == 131204 || id == 131186 || id == DEBUG_GROUP_ID)
        return;
    
    if (severity == GL_DEBUG_SEVERITY_HIGH_ARB) {
        fprintf(stderr, "OpenGL Error: %s\n", msg);
        exit(0);
    } else if (severity == GL_DEBUG_SEVERITY_MEDIUM_ARB) {
        fprintf(stderr, "OpenGL Warning: %s\n", msg);
    } else {
        printf("OpenGL Info: %s\n", msg);
    }
}

void gl_init_swapchain(uint32_t width_, uint32_t height_, uint32_t flags);

void gl_init_api(SDL_Window* window_) {
    window = window_;
    
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 4);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 3);
    SDL_GL_SetAttribute(SDL_GL_FRAMEBUFFER_SRGB_CAPABLE, 1);
    SDL_GL_SetAttribute(SDL_GL_CONTEXT_FLAGS, SDL_GL_CONTEXT_DEBUG_FLAG);
    
    context = SDL_GL_CreateContext(window);
    if (!context)
        fatal("Failed to create an OpenGL context\n");
    
    if (!glflInit())
        fatal("Failed to initialize GLFL\n");
    
    glDebugMessageCallback((GLDEBUGPROC)&debug_callback, NULL);
    
    glEnable(GL_FRAMEBUFFER_SRGB);
    glEnable( GL_TEXTURE_CUBE_MAP_SEAMLESS);
    
    glGenVertexArrays(1, &blit_vao);
    
    GLuint blit_vertex = glCreateShader(GL_VERTEX_SHADER);
    static const char* blit_vertex_glsl = "#version 430 core\n"
                                          "out vec2 vfUv;\n"
                                          "void main(){gl_Position=vec4(vec2[](vec2(-1.0,1.0),\n"
                                                                              "vec2(-1.0,-1.0),\n"
                                                                              "vec2(1.0,1.0),\n"
                                                                              "vec2(1.0,-1.0))[gl_VertexID],0.0,1.0);\n"
                                                       "vfUv = gl_Position.xy * 0.5 + 0.5;}";
    glShaderSource(blit_vertex, 1, &blit_vertex_glsl, NULL);
    glCompileShader(blit_vertex);
    
    GLuint blit_fragment = glCreateShader(GL_FRAGMENT_SHADER);
    static const char* blit_fragment_glsl = "#version 430 core\n"
                                            "layout(location=0) out vec4 oColor;\n"
                                            "in vec2 vfUv;\n"
                                            "layout(binding=0) uniform sampler2D uTex;\n"
                                            "void main(){oColor=texture(uTex, vfUv);}";
    glShaderSource(blit_fragment, 1, &blit_fragment_glsl, NULL);
    glCompileShader(blit_fragment);
    
    blit_program = glCreateProgram();
    glAttachShader(blit_program, blit_vertex);
    glAttachShader(blit_program, blit_fragment);
    glLinkProgram(blit_program);
    glDetachShader(blit_program, blit_vertex);
    glDetachShader(blit_program, blit_fragment);
    glDeleteShader(blit_vertex);
    glDeleteShader(blit_fragment);
    
    glGenVertexArrays(1, &vao);
    
    program_count = 0;
    programs = NULL;
    
    gl_init_swapchain(1, 1, 0);
}

void gl_deinit_api() {
    for (size_t i = 0; i < program_count; i++)
        glDeleteProgram(programs[i].program);
    free(programs);
    glDeleteVertexArrays(1, &vao);
    glDeleteProgram(blit_program);
    glDeleteVertexArrays(1, &blit_vao);
    SDL_GL_DeleteContext(context);
}

void gl_init_swapchain(uint32_t width_, uint32_t height_, uint32_t flags) {
    width = width_;
    height = height_;
    
    if (color_image) glDeleteTextures(1, &color_image->id);
    if (depth_image) glDeleteTextures(1, &depth_image->id);
    free(color_image);
    free(depth_image);
    
    color_image = malloc(sizeof(r_image_t));
    glGenTextures(1, &color_image->id);
    glBindTexture(GL_TEXTURE_2D, color_image->id);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_SRGB8, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    
    if (flags & R_SWAPCHAIN_DEPTH) {
        depth_image = malloc(sizeof(r_image_t));
        glGenTextures(1, &depth_image->id);
        glBindTexture(GL_TEXTURE_2D, depth_image->id);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_STENCIL, width, height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, NULL);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    } else {
        depth_image = NULL;
    }
}

r_image_t* gl_get_swapchain_color_image() {
    return color_image;
}

r_image_t* gl_swapchain_get_depth_image() {
    return depth_image;
}

void get_format_info(r_image_format_t format, GLenum* dest_internal_format, GLenum* dest_format, GLenum* dest_type) {
    switch (format) {
    case R_IMAGE_DEPTH_U16_NORM: {
        *dest_internal_format = GL_DEPTH_COMPONENT16;
        *dest_format = GL_DEPTH_COMPONENT;
        *dest_type = GL_UNSIGNED_BYTE;
        return;
    }
    case R_IMAGE_DEPTH_U32_NORM: {
        *dest_internal_format = GL_DEPTH_COMPONENT24;
        *dest_format = GL_DEPTH_COMPONENT;
        *dest_type = GL_UNSIGNED_INT;
        return;
    }
    case R_IMAGE_DEPTH_F32: {
        *dest_internal_format = GL_DEPTH_COMPONENT32F;
        *dest_format = GL_DEPTH_COMPONENT;
        *dest_type = GL_FLOAT;
        return;
    }
    case R_IMAGE_DEPTH_STENCIL_U24_NORM_U8: {
        *dest_internal_format = GL_DEPTH24_STENCIL8;
        *dest_format = GL_DEPTH_STENCIL;
        *dest_type = GL_UNSIGNED_INT_24_8;
        return;
    }
    }
    
    if (R_FORMAT_NORMALIZED(format)) {
        static const GLuint v[] = {GL_RED, GL_RG, GL_RGB, GL_RGBA};
        *dest_format = v[R_FORMAT_COMPONENTS(format)-1];
    } else {
        static const GLuint v[] = {GL_RED_INTEGER, GL_RG_INTEGER, GL_RGB_INTEGER, GL_RGBA_INTEGER};
        *dest_format = v[R_FORMAT_COMPONENTS(format)-1];
    }
    
    switch (R_FORMAT_TYPE(format)) {
    case R_IMAGE_U8:
        *dest_type = GL_UNSIGNED_BYTE;
        if (R_FORMAT_NORMALIZED(format)) {
            static const GLuint v[] = {GL_R8, GL_RG8, GL_RGB8, GL_RGBA8};
            *dest_internal_format = v[R_FORMAT_COMPONENTS(format)-1];
        } else {
            static const GLuint v[] = {GL_R8UI, GL_RG8UI, GL_RGB8UI, GL_RGBA8UI};
            *dest_internal_format = v[R_FORMAT_COMPONENTS(format)-1];
        }
        break;
    case R_IMAGE_S8:
        *dest_type = GL_BYTE;
        if (R_FORMAT_NORMALIZED(format)) {
            static const GLuint v[] = {GL_R8_SNORM, GL_RG8_SNORM, GL_RGB8_SNORM, GL_RGBA8_SNORM};
            *dest_internal_format = v[R_FORMAT_COMPONENTS(format)-1];
        } else {
            static const GLuint v[] = {GL_R8I, GL_RG8I, GL_RGB8I, GL_RGBA8I};
            *dest_internal_format = v[R_FORMAT_COMPONENTS(format)-1];
        }
        break;
    case R_IMAGE_U16:
        *dest_type = GL_UNSIGNED_SHORT;
        if (R_FORMAT_NORMALIZED(format)) {
            static const GLuint v[] = {GL_R16, GL_RG16, GL_RGB16, GL_RGBA16};
            *dest_internal_format = v[R_FORMAT_COMPONENTS(format)-1];
        } else {
            static const GLuint v[] = {GL_R16UI, GL_RG16UI, GL_RGB16UI, GL_RGBA16UI};
            *dest_internal_format = v[R_FORMAT_COMPONENTS(format)-1];
        }
        break;
    case R_IMAGE_S16:
        *dest_type = GL_SHORT;
        if (R_FORMAT_NORMALIZED(format)) {
            static const GLuint v[] = {GL_R16_SNORM, GL_RG16_SNORM, GL_RGB16_SNORM, GL_RGBA16_SNORM};
            *dest_internal_format = v[R_FORMAT_COMPONENTS(format)-1];
        } else {
            static const GLuint v[] = {GL_R16I, GL_RG16I, GL_RGB16I, GL_RGBA16I};
            *dest_internal_format = v[R_FORMAT_COMPONENTS(format)-1];
        }
        break;
    case R_IMAGE_U32:
        *dest_type = GL_UNSIGNED_INT;
        if (R_FORMAT_NORMALIZED(format)) {
            static const GLuint v[] = {GL_R16, GL_RG16, GL_RGB16, GL_RGBA16};
            *dest_internal_format = v[R_FORMAT_COMPONENTS(format)-1];
        } else {
            static const GLuint v[] = {GL_R32UI, GL_RG32UI, GL_RGB32UI, GL_RGBA32UI};
            *dest_internal_format = v[R_FORMAT_COMPONENTS(format)-1];
        }
        break;
    case R_IMAGE_S32:
        *dest_type = GL_INT;
        if (R_FORMAT_NORMALIZED(format)) {
            static const GLuint v[] = {GL_R16_SNORM, GL_RG16_SNORM, GL_RGB16_SNORM, GL_RGBA16_SNORM};
            *dest_internal_format = v[R_FORMAT_COMPONENTS(format)-1];
        } else {
            static const GLuint v[] = {GL_R32I, GL_RG32I, GL_RGB32I, GL_RGBA32I};
            *dest_internal_format = v[R_FORMAT_COMPONENTS(format)-1];
        }
        break;
    case R_IMAGE_F16: //TODO: Handle this type when specifying data
        *dest_type = GL_FLOAT;
        {
            static const GLuint v[] = {GL_R16F, GL_RG16F, GL_RGB16F, GL_RGBA16F};
            *dest_internal_format = v[R_FORMAT_COMPONENTS(format)-1];
        }
        break;
    case R_IMAGE_F32:
        *dest_type = GL_FLOAT;
        {
            static const GLuint v[] = {GL_R32F, GL_RG32F, GL_RGB32F, GL_RGBA32F};
            *dest_internal_format = v[R_FORMAT_COMPONENTS(format)-1];
        }
        break;
    }
    
    if (R_FORMAT_SRGB(format)) {
        if (R_FORMAT_COMPONENTS(format) > 3) *dest_internal_format = GL_SRGB8_ALPHA8;
        else *dest_internal_format = GL_SRGB8;
    }
}

r_image_t* gl_create_image(r_image_create_info_t info) {
    r_image_t* image = malloc(sizeof(r_image_t));
    image->type = info.type;
    
    GLenum target;
    switch (info.type) {
    case R_IMAGE_2D: target = GL_TEXTURE_2D; break;
    case R_IMAGE_CUBE_MAP: target = GL_TEXTURE_CUBE_MAP; break;
    default: assert(false);
    }
    
    glGenTextures(1, &image->id);
    glBindTexture(target, image->id);
    GLenum gl_internal_format, gl_format, gl_type;
    get_format_info(info.format, &gl_internal_format, &gl_format, &gl_type);
    glTexStorage2D(target, info.mipmaps, gl_internal_format, info.width, info.height);
    glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
    for (uint32_t i = 0; i < (info.type==R_IMAGE_CUBE_MAP?6:1); i++) {
        uint32_t w = info.width;
        uint32_t h = info.height;
        for (uint32_t j = 0; j < info.mipmaps; j++) {
            const void* data;
            GLenum target;
            if (info.type == R_IMAGE_2D) {
                if (!info.data2d || !info.data2d[j]) continue;
                data = info.data2d[j];
                target = GL_TEXTURE_2D;
            } else {
                if (!info.data_cube[i] || !info.data_cube[i][j]) continue;
                data = info.data_cube[i][j];
                target = GL_TEXTURE_CUBE_MAP_POSITIVE_X + i;
            }
            
            glTexSubImage2D(target, j, 0, 0, w, h, gl_format, gl_type, data);
            
            w /= 2;
            h /= 2;
            if (!w) w = 1;
            if (!h) h = 1;
        }
    }
    return image;
}

void gl_del_image(r_image_t* image) {
    glDeleteTextures(1, &image->id);
    free(image);
}

r_shader_t* gl_create_shader(const b_shader_t* shader) {
    r_shader_t* res = malloc(sizeof(r_shader_t));
    
    GLuint type;
    switch (shader->type) {
    case B_SHDR_TYPE_VERTEX:
        type = GL_VERTEX_SHADER;
        break;
    case B_SHDR_TYPE_FRAGMENT:
        type = GL_FRAGMENT_SHADER;
        break;
    case B_SHDR_TYPE_COMPUTE:
        type = GL_COMPUTE_SHADER;
        break;
    default:
        assert(false);
    }
    
    res->shader = glCreateShader(type);
    
    glShaderSource(res->shader, 1, (const GLchar*const*)&shader->glsl_source, NULL);
    glCompileShader(res->shader);
    
    GLchar info_log[1024];
    glGetShaderInfoLog(res->shader, sizeof(info_log), NULL, info_log);
    if (strlen(info_log))
        printf("%s shader info log: %s\n", shader->name, info_log);
    
    return res;
}

void gl_del_shader(r_shader_t* shader) {
    glDeleteShader(shader->shader);
    free(shader);
}

r_sampler_t* gl_sampler_create() {
    r_sampler_t* sampler = malloc(sizeof(r_sampler_t));
    glGenSamplers(1, &sampler->id);
    sampler->min_filter = R_SAMPLER_LINEAR;
    sampler->mipmap_filter = R_SAMPLER_LINEAR;
    return sampler;
}

void gl_del_sampler(r_sampler_t* sampler) {
    glDeleteSamplers(1, &sampler->id);
    free(sampler);
}

static void set_min_filter(r_sampler_t* sampler) {
    switch (sampler->mipmap_filter) {
    case R_SAMPLER_LINEAR: {
        switch (sampler->min_filter) {
        case R_SAMPLER_LINEAR: {
            glSamplerParameteri(sampler->id, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
            break;
        }
        case R_SAMPLER_NEAREST: {
            glSamplerParameteri(sampler->id, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_LINEAR);
            break;
        }
        }
        break;
    }
    case R_SAMPLER_NEAREST: {
        switch (sampler->min_filter) {
        case R_SAMPLER_LINEAR: {
            glSamplerParameteri(sampler->id, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_NEAREST);
            break;
        }
        case R_SAMPLER_NEAREST: {
            glSamplerParameteri(sampler->id, GL_TEXTURE_MIN_FILTER, GL_NEAREST_MIPMAP_NEAREST);
            break;
        }
        }
        break;
    }
    }
}

void gl_sampler_min_filter(r_sampler_t* sampler, r_sampler_filter_t filter) {
    sampler->min_filter = filter;
    set_min_filter(sampler);
}

void gl_sampler_mag_filter(r_sampler_t* sampler, r_sampler_filter_t filter) {
    switch (filter) {
    case R_SAMPLER_LINEAR:
        glSamplerParameteri(sampler->id, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
        break;
    case R_SAMPLER_NEAREST:
        glSamplerParameteri(sampler->id, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        break;
    }
}

void gl_sampler_mipmap_filter(r_sampler_t* sampler, r_sampler_filter_t filter) {
    sampler->mipmap_filter = filter;
    set_min_filter(sampler);
}

void gl_sampler_address_modes(r_sampler_t* sampler, const r_sampler_address_mode_t* addr_modes) {
    for (uint32_t i = 0; i < 3; i++) {
        static GLuint params[] = {GL_TEXTURE_WRAP_S, GL_TEXTURE_WRAP_T, GL_TEXTURE_WRAP_R};
        switch (addr_modes[i]) {
        case R_SAMPLER_REPEAT:
            glSamplerParameteri(sampler->id, params[i], GL_REPEAT);
            break;
        case R_SAMPLER_MIRRORED_REPEAT:
            glSamplerParameteri(sampler->id, params[i], GL_MIRRORED_REPEAT);
            break;
        case R_SAMPLER_CLAMP_TO_EDGE:
            glSamplerParameteri(sampler->id, params[i], GL_CLAMP_TO_EDGE);
            break;
        }
    }
}

void gl_sampler_anisotropy(r_sampler_t* sampler, bool enabled, float max_anisotropy) {
    if (enabled && GL_EXT_texture_filter_anisotropic) {
        float max;
        glGetFloatv(GL_MAX_TEXTURE_MAX_ANISOTROPY_EXT, &max);
        if (max_anisotropy > max) max_anisotropy = max;
        glSamplerParameteri(sampler->id, GL_TEXTURE_MAX_ANISOTROPY_EXT, max_anisotropy);
    }
}

void gl_sampler_min_max_lod(r_sampler_t* sampler, float min_lod, float max_lod) {
    glSamplerParameteri(sampler->id, GL_TEXTURE_MIN_LOD, min_lod);
    glSamplerParameteri(sampler->id, GL_TEXTURE_MAX_LOD, max_lod);
}

void gl_sampler_finalize(r_sampler_t* sampler) {
    
}

r_pass_t* gl_pass_create() {
    r_pass_t* res = malloc(sizeof(r_pass_t));
    res->attachment_count = 0;
    res->subpass_count = 0;
    res->subpasses = NULL;
    return res;
}

void gl_pass_del(r_pass_t* pass) {
    free(pass->subpasses);
    free(pass);
}

void gl_pass_add_attachment(r_pass_t* pass, uint32_t flags) {
    pass->attachment_flags[pass->attachment_count++] = flags;
}

void gl_pass_add_subpass(r_pass_t* pass, const r_subpass_t* subpass) {
    pass->subpasses = realloc(pass->subpasses, (pass->subpass_count+1)*sizeof(r_subpass_t));
    pass->subpasses[pass->subpass_count++] = *subpass;
}

void gl_pass_finalize(r_pass_t* pass) {
    
}

r_framebuffer_t* gl_framebuffer_create(r_pass_t* pass, r_image_t*const* images) {
    r_framebuffer_t* res = malloc(sizeof(r_framebuffer_t));
    res->pass = pass;
    res->image_count = pass->attachment_count;
    res->images = malloc(res->image_count*sizeof(r_image_t*));
    memcpy(res->images, images, res->image_count*sizeof(r_image_t*));
    
    res->framebuffers = malloc(res->image_count*sizeof(GLuint));
    glGenFramebuffers(res->image_count, res->framebuffers);
    
    for (uint32_t i = 0; i < res->image_count; i++) {
        glBindFramebuffer(GL_FRAMEBUFFER, res->framebuffers[i]);
        if (pass->attachment_flags[i] & R_PASS_ATTACHMENT_DEPTHSTENCIL)
            glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, images[i]->id, 0);
        else
            glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, images[i]->id, 0);
    }
    
    res->subpasses = malloc(pass->subpass_count*sizeof(gl_subpass_t));
    
    for (uint32_t i = 0; i < pass->subpass_count; i++) {
        r_subpass_t* subpass = &pass->subpasses[i];
        
        gl_subpass_t* res_subpass = &res->subpasses[i];
        glGenFramebuffers(1, &res_subpass->framebuffer);
        glBindFramebuffer(GL_FRAMEBUFFER, res_subpass->framebuffer);
        
        if (subpass->depthstencil_index >= 0) {
            r_image_t* image = res->images[subpass->depthstencil_index];
            glFramebufferTexture(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, image->id, 0);
        }
        
        GLuint drawbufs[8];
        for (uint32_t i = 0; i < 8; i++) {
            int32_t index = subpass->color_indices[i];
            if (index < 0) {
                drawbufs[i] = GL_NONE;
                continue;
            }
            r_image_t* image = res->images[index];
            glFramebufferTexture(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0+i, image->id, 0);
            drawbufs[i] = GL_COLOR_ATTACHMENT0 + i;
        }
        
        glDrawBuffers(8, drawbufs);
    }
    
    return res;
}

void gl_framebuffer_del(r_framebuffer_t* framebuffer) {
    for (uint32_t i = 0; i < framebuffer->pass->subpass_count; i++)
        glDeleteFramebuffers(1, &framebuffer->subpasses[i].framebuffer);
    glDeleteFramebuffers(framebuffer->image_count, framebuffer->framebuffers);
    free(framebuffer->subpasses);
    free(framebuffer->framebuffers);
    free(framebuffer->images);
    free(framebuffer);
}

r_buffer_t* gl_buffer_create(uint32_t size, const void* data, r_buffer_flags_t flags) {
    r_buffer_t* res = malloc(sizeof(r_buffer_t));
    glGenBuffers(1, &res->id);
    glBindBuffer(GL_ARRAY_BUFFER, res->id);
    glBufferData(GL_ARRAY_BUFFER, size, data, GL_STATIC_DRAW);
    return res;
}

void gl_buffer_del(r_buffer_t* buffer) {
    glDeleteBuffers(1, &buffer->id);
    free(buffer);
}

r_desc_set_layout_t* gl_desc_set_layout_create(uint32_t binding_count, r_desc_set_layout_binding_t* bindings) {
    r_desc_set_layout_t* layout = malloc(sizeof(r_desc_set_layout_t));
    layout->binding_count = binding_count;
    layout->bindings = malloc(sizeof(r_desc_set_layout_binding_t)*binding_count);
    memcpy(layout->bindings, bindings, sizeof(r_desc_set_layout_binding_t)*binding_count);
    return layout;
}

void gl_desc_set_layout_del(r_desc_set_layout_t* layout) {
    free(layout->bindings);
    free(layout);
}

r_desc_set_t* gl_desc_set_create(r_desc_set_layout_t* layout, const r_desc_t* descs) {
    r_desc_set_t* res = malloc(sizeof(r_desc_set_t));
    res->desc_count = layout->binding_count;
    res->descs = malloc(sizeof(gl_desc_t)*layout->binding_count);
    for (uint32_t i = 0; i < layout->binding_count; i++) {
        gl_desc_t* desc = &res->descs[i];
        const r_desc_set_layout_binding_t* binding = &layout->bindings[i];
        desc->type = binding->type;
        desc->binding = binding->binding;
        switch (binding->type) {
        case R_DESC_IMAGE_AND_SAMPLER: {
            desc->vals[0] = descs[i].image_and_sampler.image->id;
            desc->vals[1] = descs[i].image_and_sampler.sampler->id;
            desc->vals[2] = descs[i].image_and_sampler.image->type == R_IMAGE_2D ?
                            GL_TEXTURE_2D : GL_TEXTURE_CUBE_MAP;
            break;
        }
        case R_DESC_UNIFORM_BUFFER: {
            desc->vals[0] = descs[i].uniform_buffer->id;
            desc->vals[1] = 0;
            break;
        }
        }
    }
    return res;
}

void gl_desc_set_del(r_desc_set_t* set) {
    free(set->descs);
    free(set);
}

r_timestamp_t* gl_timestamp_create() {
    r_timestamp_t* timestamp = malloc(sizeof(r_timestamp_t));
    glGenQueries(1, &timestamp->query);
    glBeginQuery(GL_TIME_ELAPSED, timestamp->query); //TODO: Why is this needed?
    glEndQuery(GL_TIME_ELAPSED);
    return timestamp;
}

void gl_timestamp_del(r_timestamp_t* timestamp) {
    glDeleteQueries(1, &timestamp->query);
    free(timestamp);
}

bool gl_timestamp_available(r_timestamp_t* timestamp) {
    GLint available;
    glGetQueryObjectiv(timestamp->query, GL_QUERY_RESULT_AVAILABLE, &available);
    return available;
}

uint64_t gl_timestamp_get(r_timestamp_t* timestamp) {
    GLuint64 result;
    glGetQueryObjectui64v(timestamp->query, GL_QUERY_RESULT/*_NO_WAIT*/, &result); //GL_QUERY_RESULT_NO_WAIT is only for OpenGL 4.4+
    return result;
}

r_pipeline_t* gl_pipeline_create(r_pipeline_type_t type) {
    r_pipeline_t* res = malloc(sizeof(r_pipeline_t));
    res->type = type;
    res->vertex_attrib_count = 0;
    res->vertex_shader = 0;
    res->fragment_shader = 0;
    res->compute_shader = 0;
    return res;
}

void gl_pipeline_del(r_pipeline_t* pipeline) {
    free(pipeline);
}

void gl_pipeline_push_constant_info(r_pipeline_t* pipeline, uint32_t start, uint32_t size) {
    
}

void gl_pipeline_vertex_shader(r_pipeline_t* pipeline, r_shader_t* vertex) {
    pipeline->vertex_shader = vertex ? vertex->shader : 0;
}

void gl_pipeline_fragment_shader(r_pipeline_t* pipeline, r_shader_t* fragment) {
    pipeline->fragment_shader = fragment ? fragment->shader : 0;
}

void gl_pipeline_compute_shader(r_pipeline_t* pipeline, r_shader_t* compute) {
    pipeline->compute_shader = compute ? compute->shader : 0;
}

void gl_pipeline_vertex_attribute(r_pipeline_t* pipeline, uint32_t index, uint32_t stride, uint32_t offset, r_vertex_format_t format) {
    gl_vertex_attrib_t attrib;
    attrib.index = index;
    attrib.stride = stride;
    attrib.offset = offset;
    switch (format) {
    case R_VERTEX_FORMAT_X32F: {
        attrib.components = 1;
        attrib.type = GL_FLOAT;
        break;
    }
    case R_VERTEX_FORMAT_XY32F: {
        attrib.components = 2;
        attrib.type = GL_FLOAT;
        break;
    }
    case R_VERTEX_FORMAT_XYZ32F: {
        attrib.components = 3;
        attrib.type = GL_FLOAT;
        break;
    }
    case R_VERTEX_FORMAT_XYZW32F: {
        attrib.components = 4;
        attrib.type = GL_FLOAT;
        break;
    }
    }
    
    pipeline->vertex_attribs[pipeline->vertex_attrib_count++] = attrib;
}

void gl_pipeline_topology(r_pipeline_t* pipeline, r_topology_t topology) {
    switch (topology) {
    case R_TOPOLOGY_TRIANGLES:
        pipeline->primitive = GL_TRIANGLES;
        break;
    case R_TOPOLOGY_TRIANGLE_STRIP:
        pipeline->primitive = GL_TRIANGLE_STRIP;
        break;
    case R_TOPOLOGY_LINES:
        pipeline->primitive = GL_LINES;
        break;
    }
}

void gl_pipeline_cull(r_pipeline_t* pipeline, r_cull_mode_t mode, r_winding_t winding) {
    switch (mode) {
    case R_CULL_BACK: {
        pipeline->enable_culling = true;
        pipeline->cull_face = GL_BACK;
        break;
    }
    case R_CULL_FRONT: {
        pipeline->enable_culling = true;
        pipeline->cull_face = GL_FRONT;
        break;
    }
    case R_CULL_NONE: {
        pipeline->enable_culling = false;
        pipeline->cull_face = GL_BACK;
        break;
    }
    }
    
    switch (winding) {
    case R_WINDING_CCW:
        pipeline->winding = GL_CCW;
        break;
    case R_WINDING_CW:
        pipeline->winding = GL_CW;
        break;
    }
}

static GLenum conv_compare_func(r_compare_func_t func) {
    switch (func) {
    case R_COMPARE_LESS: return GL_LESS;
    case R_COMPARE_GREATER: return GL_GREATER;
    case R_COMPARE_LESS_EQUAL: return GL_LEQUAL;
    case R_COMPARE_GREATER_EQUAL: return GL_GEQUAL;
    case R_COMPARE_NOT_EQUAL: return GL_NOTEQUAL;
    case R_COMPARE_EQUAL: return GL_EQUAL;
    case R_COMPARE_TRUE: return GL_ALWAYS;
    case R_COMPARE_FALSE: return GL_NEVER;
    }
    assert(false);
}

static GLenum conv_stencil_func(r_stencil_func_t func) {
    switch (func) {
    case R_STENCIL_KEEP: return GL_KEEP;
    case R_STENCIL_ZERO: return GL_ZERO;
    case R_STENCIL_REPLACE: return GL_REPLACE;
    case R_STENCIL_INCREMENT_AND_CLAMP: return GL_INCR;
    case R_STENCIL_DECREMENT_AND_CLAMP: return GL_DECR;
    case R_STENCIL_INVERT: return GL_INVERT;
    case R_STENCIL_INCREMENT_AND_WRAP: return GL_INCR_WRAP;
    case R_STENCIL_DECREMENT_AND_WRAP: return GL_DECR_WRAP;
    }
    assert(false);
}

void gl_pipeline_depth_stencil(r_pipeline_t* pipeline, uint32_t flags, r_compare_func_t compare, r_stencil_state_face_t* faces) {
    pipeline->depth_compare_func = conv_compare_func(compare);
    
    if (flags & R_PIPELINE_DEPTH_TEST) {
        pipeline->enable_depth_test = true;
    } else {
        if (flags & R_PIPELINE_DEPTH_WRITE) {
            pipeline->enable_depth_test = true;
            pipeline->depth_compare_func = GL_ALWAYS;
        } else {
            pipeline->enable_depth_test = false;
        }
    }
    
    pipeline->depth_mask = flags & R_PIPELINE_DEPTH_WRITE ? GL_TRUE : GL_FALSE;
    
    pipeline->front_compare_func = conv_compare_func(faces[0].compare_func);
    pipeline->front_compare_ref = faces[0].reference;
    pipeline->front_compare_mask = faces[0].compare_mask;
    pipeline->front_write_mask = faces[0].write_mask;
    pipeline->front_op_sfail = conv_stencil_func(faces[0].stencil_fail_func);
    pipeline->front_op_dpfail = conv_stencil_func(faces[0].stencil_pass_depth_fail_func);
    pipeline->front_op_dppass = conv_stencil_func(faces[0].stencil_pass_depth_pass_func);
    pipeline->back_compare_func = conv_compare_func(faces[1].compare_func);
    pipeline->back_compare_ref = faces[1].reference;
    pipeline->back_compare_mask = faces[1].compare_mask;
    pipeline->back_write_mask = faces[1].write_mask;
    pipeline->back_op_sfail = conv_stencil_func(faces[1].stencil_fail_func);
    pipeline->back_op_dpfail = conv_stencil_func(faces[1].stencil_pass_depth_fail_func);
    pipeline->back_op_dppass = conv_stencil_func(faces[1].stencil_pass_depth_pass_func);
    
    if (flags & R_PIPELINE_STENCIL_TEST) {
        pipeline->enable_stencil_test = true;
    } else {
        if (flags & R_PIPELINE_STENCIL_WRITE) {
            pipeline->enable_stencil_test = true;
            pipeline->front_compare_func = GL_ALWAYS;
            pipeline->back_compare_func = GL_ALWAYS;
        } else {
            pipeline->enable_stencil_test = false;
        }
    }
}

void gl_pipeline_pass(r_pipeline_t* pipeline, r_pass_t* pass, uint32_t subpass_index) {
    pipeline->pass = pass;
    pipeline->subpass_index = subpass_index;
}

void gl_pipeline_desc_set_layout(r_pipeline_t* pipeline, r_stage_t stage, r_desc_set_layout_t* layout) {
    
}

void gl_pipeline_finalize(r_pipeline_t* pipeline) {
    if (pipeline->type == R_PIPELINE_COMPUTE) pipeline->program = get_program(pipeline->compute_shader, 0);
    else pipeline->program = get_program(pipeline->vertex_shader, pipeline->fragment_shader);
}

r_cmd_buf_t* gl_cmd_buf_create(r_cmd_buf_type_t type) {
    r_cmd_buf_t* res = malloc(sizeof(r_cmd_buf_t));
    res->cmd_count = 0;
    res->cmds = NULL;
    return res;
}

static void free_cmd_buf_cmds(r_cmd_buf_t* cmd_buf) {
    for (uint32_t i = 0; i < cmd_buf->cmd_count; i++) {
        switch (cmd_buf->cmds[i].type) {
        case GL_CMD_SET_BUFFER_DATA:
            free(cmd_buf->cmds[i].buf_data.data);
            break;
        default:
            break;
        }
    }
    free(cmd_buf->cmds);
}

void gl_cmd_buf_del(r_cmd_buf_t* cmd_buf) {
    free_cmd_buf_cmds(cmd_buf);
    free(cmd_buf);
}

void gl_cmd_buf_begin(r_cmd_buf_t* cmd_buf) {
    free_cmd_buf_cmds(cmd_buf);
    cmd_buf->cmd_count = 0;
    cmd_buf->cmds = NULL;
}

void gl_cmd_buf_end(r_cmd_buf_t* cmd_buf) {
    
}

void add_command(r_cmd_buf_t* cmd_buf, const gl_cmd_t* cmd) {
    cmd_buf->cmds = realloc(cmd_buf->cmds, (cmd_buf->cmd_count+1)*sizeof(gl_cmd_t));
    cmd_buf->cmds[cmd_buf->cmd_count++] = *cmd;
}

void gl_cmd_buf_set_viewport(r_cmd_buf_t* cmd_buf, uint32_t left, uint32_t bottom, uint32_t width, uint32_t height) {
    gl_cmd_t cmd;
    cmd.type = GL_CMD_SET_VIEWPORT;
    cmd.viewport[0] = left;
    cmd.viewport[1] = bottom;
    cmd.viewport[2] = width;
    cmd.viewport[3] = height;
    add_command(cmd_buf, &cmd);
}

void gl_cmd_buf_pass_begin(r_cmd_buf_t* cmd_buf, r_pass_t* pass, r_framebuffer_t* framebuffer, const r_clear_value_t* clear_values) {
    gl_cmd_t cmd;
    cmd.type = GL_CMD_BEGIN_PASS;
    cmd.begin_pass.pass = pass;
    cmd.begin_pass.framebuffer = framebuffer;
    for (uint32_t i = 0; i < 8; i++) cmd.begin_pass.clear_values[i] = clear_values[i];
    add_command(cmd_buf, &cmd);
}

void gl_cmd_buf_pass_end(r_cmd_buf_t* cmd_buf) {
    gl_cmd_t cmd;
    cmd.type = GL_CMD_END_PASS;
    add_command(cmd_buf, &cmd);
}

void gl_cmd_buf_next_subpass(r_cmd_buf_t* cmd_buf) {
    gl_cmd_t cmd;
    cmd.type = GL_CMD_NEXT_SUBPASS;
    add_command(cmd_buf, &cmd);
}

void gl_cmd_buf_set_timestamp(r_cmd_buf_t* cmd_buf, r_timestamp_t* timestamp) {
    gl_cmd_t cmd;
    cmd.type = GL_CMD_SET_TIMESTAMP;
    cmd.timestamp = timestamp;
    add_command(cmd_buf, &cmd);
}

void gl_cmd_buf_bind_pipeline(r_cmd_buf_t* cmd_buf, const r_pipeline_t* pipeline) {
    gl_cmd_t cmd;
    cmd.type = GL_CMD_BIND_PIPELINE;
    cmd.pipeline = pipeline;
    add_command(cmd_buf, &cmd);
}

void gl_cmd_buf_bind_vertex_buffers(r_cmd_buf_t* cmd_buf, uint32_t count, r_buffer_t*const* buffers, const uint32_t* offsets) {
    gl_cmd_t cmd;
    cmd.type = GL_CMD_BIND_VERTEX_BUFFERS;
    for (uint32_t i = 0; i < count; i++) {
        cmd.bind_vertex_bufs.buffers[i] = buffers[i];
        cmd.bind_vertex_bufs.offsets[i] = offsets[i];
    }
    for (uint32_t i = count; i < 16; i++) {
        cmd.bind_vertex_bufs.buffers[i] = NULL;
        cmd.bind_vertex_bufs.offsets[i] = 0;
    }
    add_command(cmd_buf, &cmd);
}

void gl_cmd_buf_bind_index_buffer(r_cmd_buf_t* cmd_buf, const r_buffer_t* buffer, uint32_t offset, r_index_type_t type) {
    gl_cmd_t cmd;
    cmd.type = GL_CMD_BIND_INDEX_BUFFER;
    cmd.bind_index_buf.buffer = buffer;
    cmd.bind_index_buf.offset = offset;
    switch (type) {
    case R_INDEX_TYPE_UINT16: cmd.bind_index_buf.type = GL_UNSIGNED_SHORT; break;
    case R_INDEX_TYPE_UINT32: cmd.bind_index_buf.type = GL_UNSIGNED_INT; break;
    }
    add_command(cmd_buf, &cmd);
}

void gl_cmd_buf_set_push_constants(r_cmd_buf_t* cmd_buf, uint32_t start, uint32_t size, const void* data) {
    gl_cmd_t cmd;
    cmd.type = GL_CMD_SET_PUSH_CONSTANTS;
    cmd.set_push_constants.start = start;
    cmd.set_push_constants.amount = size;
    memcpy(cmd.set_push_constants.data, data, size);
    add_command(cmd_buf, &cmd);
}

void gl_cmd_buf_set_desc_set(r_cmd_buf_t* cmd_buf, r_stage_t stage, r_desc_set_t* set) {
    gl_cmd_t cmd;
    cmd.type = GL_CMD_SET_DESC_SET;
    cmd.desc_set.stage = stage;
    cmd.desc_set.set = set;
    add_command(cmd_buf, &cmd);
}

void gl_cmd_buf_image_barriers(r_cmd_buf_t* cmd_buf, uint32_t count, r_image_t*const* image, const r_image_access_t** access, const r_image_layout_t** layout) {
    
}

void gl_cmd_buf_set_buffer_data(r_cmd_buf_t* cmd_buf, r_buffer_t* buffer, uint32_t offset, uint32_t size, const void* data) {
    gl_cmd_t cmd;
    cmd.type = GL_CMD_SET_BUFFER_DATA;
    cmd.buf_data.buffer = buffer;
    cmd.buf_data.offset = offset;
    cmd.buf_data.size = size;
    cmd.buf_data.data = malloc(size);
    memcpy(cmd.buf_data.data, data, size);
    add_command(cmd_buf, &cmd);
}

void gl_cmd_buf_draw_indexed(r_cmd_buf_t* cmd_buf, uint32_t index_count, uint32_t instance_count, uint32_t first_index, int32_t vertex_offset, uint32_t first_instance) {
    gl_cmd_t cmd;
    cmd.type = GL_CMD_DRAW_INDEXED;
    cmd.draw_indexed.index_count = index_count;
    cmd.draw_indexed.instance_count = instance_count;
    cmd.draw_indexed.first_index = first_index;
    cmd.draw_indexed.first_instance = first_instance;
    cmd.draw_indexed.vertex_offset = vertex_offset;
    add_command(cmd_buf, &cmd);
}

void gl_cmd_buf_draw(r_cmd_buf_t* cmd_buf, uint32_t vertex_count, uint32_t instance_count, uint32_t first_vertex, uint32_t first_instance) {
    gl_cmd_t cmd;
    cmd.type = GL_CMD_DRAW;
    cmd.draw.vertex_count = vertex_count;
    cmd.draw.instance_count = instance_count;
    cmd.draw.first_vertex = first_vertex;
    cmd.draw.first_instance = first_instance;
    add_command(cmd_buf, &cmd);
}

void gl_cmd_buf_dispatch_compute(r_cmd_buf_t* cmd_buf, const uint32_t* dim) {
    gl_cmd_t cmd;
    cmd.type = GL_CMD_DISPATCH_COMPUTE;
    cmd.dispatch_compute.dim[0] = dim[0];
    cmd.dispatch_compute.dim[1] = dim[1];
    cmd.dispatch_compute.dim[2] = dim[2];
    add_command(cmd_buf, &cmd);
}

void _cmd_buf_submit(r_cmd_buf_t* cmd_buf) {
    bool pipeline_dirty = true;
    const r_pipeline_t* pipeline = NULL;
    
    bool vertex_bufs_dirty = true;
    const r_buffer_t* vertex_bufs[16];
    uint32_t vertex_buf_offsets[16];
    
    uint32_t index_buf_offset = 0;
    GLenum index_buf_type = 0;
    
    bool push_constants_dirty = true;
    uint8_t push_constants[128];
    memset(push_constants, 0, sizeof(push_constants));
    
    glBindVertexArray(vao);
    
    r_pass_t* pass = NULL;
    uint32_t cur_subpass = 0;
    r_framebuffer_t* framebuffer = NULL;
    
    for (uint32_t i = 0; i < cmd_buf->cmd_count; i++) {
        gl_cmd_t* cmd = &cmd_buf->cmds[i];
        switch (cmd->type) {
        case GL_CMD_SET_VIEWPORT: {
            glViewport(cmd->viewport[0], cmd->viewport[1], cmd->viewport[2], cmd->viewport[3]);
            break;
        }
        case GL_CMD_BEGIN_PASS: {
            debug_group_start("Render pass sub pass 0");
            debug_group_start("Begin render pass");
            
            pass = cmd->begin_pass.pass;
            framebuffer = cmd->begin_pass.framebuffer;
            r_clear_value_t* clear_vals = cmd->begin_pass.clear_values;
            
            for (uint32_t i = 0; i < pass->attachment_count; i++) {
                if (!framebuffer->images[i]) continue;
                if (pass->attachment_flags[i] & R_PASS_ATTACHMENT_CLEAR) {
                    glBindFramebuffer(GL_FRAMEBUFFER, framebuffer->framebuffers[i]);
                    if (pass->attachment_flags[i] & R_PASS_ATTACHMENT_DEPTHSTENCIL) {
                        glClearDepth(clear_vals[i].depthstencil.depth);
                        glClearStencil(clear_vals[i].depthstencil.stencil);
                        glDepthMask(GL_TRUE);
                        glStencilMask(0xFFFFFFFF);
                        pipeline_dirty = true;
                        glClear(GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);
                    } else {
                        glColorMask(GL_TRUE, GL_TRUE, GL_TRUE, GL_TRUE);
                        glClearColor(clear_vals[i].color[0], clear_vals[i].color[1],
                                     clear_vals[i].color[2], clear_vals[i].color[3]);
                        glClear(GL_COLOR_BUFFER_BIT);
                    }
                }
            }
            
            cur_subpass = 0;
            glBindFramebuffer(GL_FRAMEBUFFER,  framebuffer->subpasses[0].framebuffer);
            
            debug_group_end();
            break;
        }
        case GL_CMD_END_PASS: {
            debug_group_end();
            break;
        }
        case GL_CMD_NEXT_SUBPASS: {
            cur_subpass++;
            glBindFramebuffer(GL_FRAMEBUFFER,  framebuffer->subpasses[cur_subpass].framebuffer);
            
            debug_group_end();
            debug_group_start("Render pass sub pass %u", cur_subpass);
            break;
        }
        case GL_CMD_SET_TIMESTAMP: {
            glQueryCounter(cmd->timestamp->query, GL_TIMESTAMP);
            break;
        }
        case GL_CMD_BIND_PIPELINE: {
            pipeline = cmd->pipeline;
            pipeline_dirty = true;
            break;
        }
        case GL_CMD_BIND_VERTEX_BUFFERS: {
            vertex_bufs_dirty = true;
            memcpy(vertex_bufs, cmd->bind_vertex_bufs.buffers, sizeof(vertex_bufs));
            memcpy(vertex_buf_offsets, cmd->bind_vertex_bufs.offsets, sizeof(vertex_buf_offsets));
            break;
        }
        case GL_CMD_BIND_INDEX_BUFFER: {
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, cmd->bind_index_buf.buffer->id);
            index_buf_offset = cmd->bind_index_buf.offset;
            index_buf_type = cmd->bind_index_buf.type;
            break;
        }
        case GL_CMD_SET_PUSH_CONSTANTS: {
            memcpy(push_constants+cmd->set_push_constants.start,
                   cmd->set_push_constants.data,
                   cmd->set_push_constants.amount);
            push_constants_dirty = true;
            break;
        }
        case GL_CMD_SET_DESC_SET: {
            debug_group_start("Set descriptor set");
            
            uint32_t offset = cmd->desc_set.stage == R_STAGE_FRAGMENT ? 16 : 0;
            r_desc_set_t* set = cmd->desc_set.set;
            for (uint32_t i = 0; i < set->desc_count; i++) {
                const gl_desc_t* desc = &set->descs[i];
                switch (desc->type) {
                case R_DESC_IMAGE_AND_SAMPLER: {
                    glActiveTexture(GL_TEXTURE0+desc->binding+offset);
                    glBindTexture(desc->vals[2], desc->vals[0]);
                    glBindSampler(desc->binding+offset, desc->vals[1]);
                    break;
                }
                case R_DESC_UNIFORM_BUFFER: {
                    glBindBufferBase(GL_UNIFORM_BUFFER, desc->binding+offset, desc->vals[0]);
                    break;
                }
                }
            }
            
            debug_group_end();
            break;
        }
        case GL_CMD_SET_BUFFER_DATA: {
            glBindBuffer(GL_ARRAY_BUFFER, cmd->buf_data.buffer->id);
            glBufferSubData(GL_ARRAY_BUFFER, cmd->buf_data.offset, cmd->buf_data.size, cmd->buf_data.data);
            break;
        }
        case GL_CMD_DRAW_INDEXED:
        case GL_CMD_DRAW: {
            debug_group_start("Draw (possibly indexed)");
            
            if (pipeline_dirty) {
                debug_group_start("Pipeline update");
                
                glUseProgram(pipeline->program);
                
                if (pipeline->enable_culling) glEnable(GL_CULL_FACE);
                else glDisable(GL_CULL_FACE);
                glCullFace(pipeline->cull_face);
                glFrontFace(pipeline->winding);
                
                if (pipeline->enable_depth_test) glEnable(GL_DEPTH_TEST);
                else glDisable(GL_DEPTH_TEST);
                glDepthMask(pipeline->depth_mask);
                glDepthFunc(pipeline->depth_compare_func);
                
                if (pipeline->enable_stencil_test) glEnable(GL_STENCIL_TEST);
                else glDisable(GL_STENCIL_TEST);
                glStencilFuncSeparate(GL_FRONT, pipeline->front_compare_func, pipeline->front_compare_ref, pipeline->front_compare_mask);
                glStencilMaskSeparate(GL_FRONT, pipeline->front_write_mask);
                glStencilOpSeparate(GL_FRONT, pipeline->front_op_sfail, pipeline->front_op_dpfail, pipeline->front_op_dppass);
                glStencilFuncSeparate(GL_BACK, pipeline->back_compare_func, pipeline->back_compare_ref, pipeline->back_compare_mask);
                glStencilMaskSeparate(GL_BACK, pipeline->back_write_mask);
                glStencilOpSeparate(GL_BACK, pipeline->back_op_sfail, pipeline->back_op_dpfail, pipeline->back_op_dppass);
                
                debug_group_end();
            }
            
            if (vertex_bufs_dirty || pipeline_dirty) {
                debug_group_start("VAO update");
                
                for (uint32_t i = 0; i < pipeline->vertex_attrib_count; i++) {
                    const gl_vertex_attrib_t* attrib = &pipeline->vertex_attribs[i];
                    glBindBuffer(GL_ARRAY_BUFFER, vertex_bufs[i]->id);
                    glVertexAttribPointer(attrib->index, attrib->components, attrib->type, GL_FALSE, attrib->stride, (const GLvoid*)(uintptr_t)(vertex_buf_offsets[i]+attrib->offset));
                    glEnableVertexAttribArray(attrib->index);
                }
                
                debug_group_end();
            }
            
            if (push_constants_dirty || pipeline_dirty) {
                glUniform1fv(0, 32, (const GLfloat*)push_constants);
                push_constants_dirty = false;
            }
            
            pipeline_dirty = false;
            vertex_bufs_dirty = false;
            push_constants_dirty = false;
            
            if (cmd->type == GL_CMD_DRAW_INDEXED) {
                uint32_t index_size = index_buf_type == GL_UNSIGNED_SHORT ? 2 : 4;
                uint32_t offset = cmd->draw_indexed.first_index*index_size+index_buf_offset;
                
                glDrawElementsInstancedBaseVertexBaseInstance(pipeline->primitive,
                                                              cmd->draw_indexed.index_count,
                                                              index_buf_type,
                                                              (const GLvoid*)(uintptr_t)offset,
                                                              cmd->draw_indexed.instance_count,
                                                              cmd->draw_indexed.vertex_offset,
                                                              cmd->draw_indexed.first_instance);
            } else {
                glDrawArraysInstancedBaseInstance(pipeline->primitive, cmd->draw.first_vertex,
                                                  cmd->draw.vertex_count, cmd->draw.instance_count,
                                                  cmd->draw.first_instance);
            }
            
            debug_group_end();
            break;
        }
        case GL_CMD_DISPATCH_COMPUTE: {
            debug_group_start("Dispatch compute");
            
            if (pipeline_dirty) {
                debug_group_start("Pipeline update");
                
                glUseProgram(pipeline->program);
                
                debug_group_end();
            }
            
            glDispatchCompute(cmd->dispatch_compute.dim[0], cmd->dispatch_compute.dim[1], cmd->dispatch_compute.dim[2]);
            
            debug_group_end();
            break;
        }
        }
    }
}

void gl_cmd_buf_submit(uint32_t count, r_cmd_buf_t*const* cmd_buf) {
    for (uint32_t i = 0; i < count; i++) _cmd_buf_submit(cmd_buf[i]);
}

void gl_frame_submit() {
    debug_group_start("Blit resulting image");
    glDisable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    glBindFramebuffer(GL_FRAMEBUFFER, 0);
    glUseProgram(blit_program);
    glBindVertexArray(blit_vao);
    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, color_image->id);
    glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);
    debug_group_end();
    
    SDL_GL_SwapWindow(window);
}
