#ifndef MATHY_STUFF_H
#define MATHY_STUFF_H

#include "pi.h"

#include <stdbool.h>

typedef struct m_vec3_t m_vec3_t;
typedef struct m_mat4_t m_mat4_t;
typedef struct m_plane_t m_plane_t;
typedef struct m_frustum_t m_frustum_t;
typedef struct m_aabb_t m_aabb_t;
typedef struct m_sphere_t m_sphere_t;
typedef struct m_quat_t m_quat_t;

struct m_vec3_t {
    float x, y, z;
};

struct m_mat4_t {
    float rows[4][4];
};

struct m_plane_t {
    m_vec3_t normal;
    float dist;
};

struct m_frustum_t {
    m_plane_t planes[6];
    m_vec3_t origin;
    m_vec3_t corners[8];
};

struct m_aabb_t {
    m_vec3_t min, max;
};

struct m_sphere_t {
    m_vec3_t pos;
    float radius;
};

struct m_quat_t {
    float x, y, z, w;
};

m_vec3_t m_vec3_create(float x, float y, float z);
void m_vec3_normalize(m_vec3_t* dest, const m_vec3_t* src);
void m_vec3_cross(m_vec3_t* restrict dest, const m_vec3_t* restrict a, const m_vec3_t* restrict b);
void m_vec3_inc(m_vec3_t* restrict val, const m_vec3_t* restrict by);
void m_vec3_dec(m_vec3_t* restrict val, const m_vec3_t* restrict by);
void m_vec3_sub(m_vec3_t* restrict dest, const m_vec3_t* restrict a, const m_vec3_t* restrict b);
void m_vec3_mul(m_vec3_t* restrict dest, const m_vec3_t* restrict a, const m_vec3_t* restrict b);
float m_vec3_dot(const m_vec3_t* a, const m_vec3_t* b);
float m_vec3_length(const m_vec3_t* v);

void m_mat4_zero(m_mat4_t* mat);
void m_mat4_create_identity(m_mat4_t* mat);
void m_mat4_create_scale(m_mat4_t* mat, float sx, float sy, float sz);
void m_mat4_create_translate(m_mat4_t* mat, float tx, float ty, float tz);
void m_mat4_create_rotate(m_mat4_t* mat, float ox, float oy, float oz, float ow);
void m_mat4_create_perspective(m_mat4_t* dest, float fov, float aspect, float near, float far);
void m_mat4_create_ortho(m_mat4_t* dest, const float *bx, const float* by, const float* bz);
void m_mat4_create_lookat(m_mat4_t* dest, const m_vec3_t* eye, const m_vec3_t* dir, const m_vec3_t* up);
void m_mat4_mul(m_mat4_t* restrict dest, const m_mat4_t* restrict a, const m_mat4_t* restrict b);
void m_mat4_transpose(m_mat4_t* restrict dest, const m_mat4_t* restrict mat);
float m_mat4_determinant(const m_mat4_t* mat);
void m_mat4_inverse(m_mat4_t* restrict dest, const m_mat4_t* restrict mat);
void m_mat4_mul_vec3(m_vec3_t* restrict res, const m_mat4_t* restrict mat, const m_vec3_t* restrict p);

m_plane_t m_plane_create(m_vec3_t normal, float dist);
m_plane_t m_plane_create_4f(float nx, float ny, float nz, float d);
m_plane_t m_plane_create_3v(m_vec3_t a, m_vec3_t b, m_vec3_t c);
float m_plane_dist_to_point(const m_plane_t* plane, const m_vec3_t* p);

m_frustum_t m_frustum_create_view(const m_mat4_t* mat, float left, float right, float bottom, float top, float near, float far);
m_frustum_t m_frustum_create_perspective(const m_mat4_t* mat, float fov, float aspect, float near, float far);
m_frustum_t m_frustum_create_mats(const m_mat4_t* view, const m_mat4_t* proj);
bool m_frustum_cull_aabb(const m_frustum_t* restrict frustum, const m_aabb_t* restrict aabb);
bool m_frustum_cull_sphere(const m_frustum_t* restrict frustum, const m_sphere_t* restrict sphere);

m_aabb_t m_aabb_create(const m_vec3_t* min, const m_vec3_t* max);
m_aabb_t m_aabb_create_empty();
void m_aabb_extend_vec3(m_aabb_t* restrict aabb, const m_vec3_t* restrict other);
void m_aabb_extent_aabb(m_aabb_t* restrict aabb, const m_aabb_t* restrict other);
void m_aabb_transform(m_aabb_t* restrict dest, const m_aabb_t* restrict aabb, const m_mat4_t* mat);

m_quat_t m_quat_create(float x, float y, float z, float w);
m_quat_t m_quat_create_axis_angle(const m_vec3_t* axis, float angle);
m_quat_t m_quat_create_euler_angles(float x, float y, float z);
float m_quat_get_angle(const m_quat_t* quat);
float m_quat_get_x_axis(const m_quat_t* quat);
float m_quat_get_y_axis(const m_quat_t* quat);
float m_quat_get_z_axis(const m_quat_t* quat);
void m_quat_set_angle(m_quat_t* quat, float angle);
void m_quat_set_x_axis(m_quat_t* quat, float xaxis);
void m_quat_set_y_axis(m_quat_t* quat, float yaxis);
void m_quat_set_z_axis(m_quat_t* quat, float zaxis);
void m_quat_get_eulur_angles(const m_quat_t* restrict quat, m_vec3_t* restrict angles);
#endif
