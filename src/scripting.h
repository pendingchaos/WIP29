#ifndef SCRIPTING_H
#define SCRIPTING_H

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>
#include <lua.h>

typedef struct l_metatable_t l_metatable_t;
typedef struct l_script_t l_script_t;

extern lua_State* lua_state;

void l_init();
void l_deinit();
void l_register_func(const char* name, lua_CFunction func);
l_metatable_t* l_register_metatable(const char* name, size_t size);
void l_register_method(l_metatable_t* class, const char* name, lua_CFunction func);
void l_register_metatable_static_property(l_metatable_t* class, const char* name, int index);
l_script_t* l_script_read(const char* filename);
void l_script_del(l_script_t* script);
void l_ptr_new(void* ptr, l_metatable_t* class);
void l_ptr_del(void* ptr);
void l_push_obj(l_metatable_t* class, void* data);
void l_func_start(size_t arg_count, bool returns);
int8_t l_to_int8_t(int index);
uint8_t l_to_uint8_t(int index);
int16_t l_to_int16_t(int index);
uint16_t l_to_uint16_t(int index);
int32_t l_to_int32_t(int index);
uint32_t l_to_uint32_t(int index);
int64_t l_to_int64_t(int index);
uint64_t l_to_uint64_t(int index);
size_t l_to_size_t(int index);
ptrdiff_t l_to_ptrdiff_t(int index);
int64_t l_to_int_range(int index, int64_t min, int64_t max);
uint64_t l_to_uint_range(int index, uint64_t min, uint64_t max);
const char* l_to_str(int index);
double l_to_double(int index);
void* l_to_obj(int index, l_metatable_t* class);
void* l_validate_ptr(void* ptr, l_metatable_t* class);
#endif
