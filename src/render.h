#ifndef RENDER_H
#define RENDER_H

#include "mathy_stuff.h"
#include "rapi.h"
#include "bin.h"

#include <SDL2/SDL_video.h>

typedef enum renderer_t {
    RENDERER_VK,
    RENDERER_GL
} renderer_t;

typedef struct s_object_t s_object_t;

typedef struct r_texture_t r_texture_t;
typedef struct r_cubemap_t r_cubemap_t;
typedef struct r_shader_t r_shader_t;
typedef struct r_material_t r_material_t;
typedef struct r_mesh_t r_mesh_t;

typedef struct r_obj_t {
    r_mesh_t* mesh;
    m_mat4_t mat;
} r_obj_t;

typedef struct r_scene_t {
    m_vec3_t cam_pos;
    m_vec3_t cam_dir;
    m_vec3_t cam_up;
    const s_object_t* sun;
    const r_cubemap_t* skybox;
    size_t obj_count;
    const r_obj_t* objs;
} r_scene_t;

void r_init(renderer_t renderer, SDL_Window* window);
void r_deinit();
r_texture_t* r_texture_create(const b_texture_t* texture);
void r_texture_del(r_texture_t* texture);
r_cubemap_t* r_cubemap_create(const b_cubemap_t* cubemap);
void r_cubemap_del(r_cubemap_t* cubemap);
r_material_t* r_material_create(const b_material_t* material);
void r_material_del(r_material_t* material);
r_mesh_t* r_mesh_create(const b_mesh_t* mesh);
void r_mesh_del(r_mesh_t* mesh);
void r_frame(const r_scene_t* scene);
#endif
