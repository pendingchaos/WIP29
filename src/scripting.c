#include "scripting.h"
#include "utils.h"

#include <lauxlib.h>
#include <stdint.h>
#include <stdarg.h>
#include <stdlib.h>
#include <string.h>
#include <lua.h>

#define NAME_MAX 64

struct l_metatable_t {
    l_metatable_t* next;
    char name[NAME_MAX];
    size_t size;
    int metatable_ref;
};

struct l_script_t {
    int res_ref;
};

lua_State* lua_state = NULL;
static l_metatable_t* classes = NULL;
static int ptrs_ref = LUA_NOREF;

void l_bindings_init();

void l_init() {
    lua_state = luaL_newstate();
    classes = NULL;
    
    lua_newtable(lua_state);
    ptrs_ref = luaL_ref(lua_state, LUA_REGISTRYINDEX);
    
    l_bindings_init();
}

void l_deinit() {
    luaL_unref(lua_state, LUA_REGISTRYINDEX, ptrs_ref);
    
    l_metatable_t* cur = classes;
    while (cur) {
        l_metatable_t* next = cur->next;
        luaL_unref(lua_state, LUA_REGISTRYINDEX, cur->metatable_ref);
        free(cur);
        cur = next;
    }
    
    lua_close(lua_state);
}

void l_register_func(const char* name, lua_CFunction func) {
    lua_pushcfunction(lua_state, func);
    lua_setglobal(lua_state, name);
}

l_metatable_t* l_register_metatable(const char* name, size_t size) {
    l_metatable_t* class = malloc(sizeof(l_metatable_t));
    
    class->next = NULL;
    
    memset(class->name, 0, NAME_MAX);
    strncpy(class->name, name, NAME_MAX-1);
    
    class->size = size;
    
    luaL_newmetatable(lua_state, name);
    
    lua_pushnil(lua_state);
    lua_copy(lua_state, -2, -1);
    lua_pushnil(lua_state);
    lua_copy(lua_state, -2, -1);
    lua_setglobal(lua_state, name);
    
    class->metatable_ref = luaL_ref(lua_state, LUA_REGISTRYINDEX);
    
    lua_pushstring(lua_state, "__index");
    lua_pushnil(lua_state);
    lua_copy(lua_state, -3, -1);
    lua_rawset(lua_state, -3);
    
    if (classes) classes->next = class;
    classes = class;
    
    return class;
}

void l_register_method(l_metatable_t* class, const char* name, lua_CFunction func) {
    lua_rawgeti(lua_state, LUA_REGISTRYINDEX, class->metatable_ref);
    lua_pushcfunction(lua_state, func);
    lua_setfield(lua_state, -2, name);
    lua_pop(lua_state, -1);
}

void l_register_metatable_static_property(l_metatable_t* class, const char* name, int index) {
    lua_rawgeti(lua_state, LUA_REGISTRYINDEX, class->metatable_ref);
    lua_pushnil(lua_state);
    lua_copy(lua_state, index-1, -1);
    lua_setfield(lua_state, -2, name);
    lua_pop(lua_state, -1);
}

l_script_t* l_script_read(const char* filename) {
    if (luaL_loadfile(lua_state, filename) != LUA_OK) {
        const char* message = lua_tostring(lua_state, -1);
        fatal("Error loading %s: %s\n", filename, message);
        //lua_pop(lua_state, -1);
    }
    
    if (lua_pcall(lua_state, 0, 1, 0) != LUA_OK) {
        const char* message = lua_tostring(lua_state, -1);
        fatal("Error Running %s: %s\n", filename, message);
        //lua_pop(lua_state, -1);
    }
    
    l_script_t* script = malloc(sizeof(l_script_t));
    script->res_ref = luaL_ref(lua_state, LUA_REGISTRYINDEX);
    
    return script;
}

void l_script_del(l_script_t* script) {
    luaL_unref(lua_state, LUA_REGISTRYINDEX, script->res_ref);
    free(script);
}

void l_ptr_new(void* ptr, l_metatable_t* class) {
    lua_rawgeti(lua_state, LUA_REGISTRYINDEX, ptrs_ref);
    lua_rawgeti(lua_state, LUA_REGISTRYINDEX, class->metatable_ref);
    lua_rawsetp(lua_state, -2, ptr);
    lua_pop(lua_state, 1);
}

void l_ptr_del(void* ptr) {
    lua_rawgeti(lua_state, LUA_REGISTRYINDEX, ptrs_ref);
    lua_pushnil(lua_state);
    lua_rawsetp(lua_state, -2, ptr);
    lua_pop(lua_state, 1);
}

void l_push_obj(l_metatable_t* class, void* data) {
    lua_newuserdata(lua_state, class->size);
    memcpy(lua_touserdata(lua_state, -1), data, class->size);
    lua_rawgeti(lua_state, LUA_REGISTRYINDEX, class->metatable_ref);
    lua_setmetatable(lua_state, -2);
}

void l_func_start(size_t arg_count, bool returns) {
    //if (returns) luaL_checkstack(lua_state, 1, NULL); //TODO: Is this correct?
    int count = lua_gettop(lua_state);
    if (count != arg_count)
        luaL_error(lua_state, "Function expect %d arguments. Got %d.\n", (int)arg_count, count);
}

int8_t l_to_int8_t(int index) {
    return l_to_int_range(index, INT8_MIN, INT8_MAX);
}

uint8_t l_to_uint8_t(int index) {
    return l_to_int_range(index, 0, UINT8_MAX);
}

int16_t l_to_int16_t(int index) {
    return l_to_int_range(index, INT16_MIN, INT16_MAX);
}

uint16_t l_to_uint16_t(int index) {
    return l_to_int_range(index, 0, UINT16_MAX);
}

int32_t l_to_int32_t(int index) {
    return l_to_int_range(index, INT32_MIN, INT32_MAX);
}

uint32_t l_to_uint32_t(int index) {
    return l_to_int_range(index, 0, UINT32_MAX);
}

int64_t l_to_int64_t(int index) {
    return l_to_int_range(index, INT64_MIN, INT64_MAX);
}

uint64_t l_to_uint64_t(int index) {
    int success;
    lua_Integer i = lua_tointegerx(lua_state, index, &success);
    if (!success) luaL_error(lua_state, "Failed to convert value to integer\n");
    return i;
}

size_t l_to_size_t(int index) {
    return l_to_uint_range(index, 0, SIZE_MAX);
}

ptrdiff_t l_to_ptrdiff_t(int index) {
    return l_to_int_range(index, PTRDIFF_MIN, PTRDIFF_MAX);
}

int64_t l_to_int_range(int index, int64_t min, int64_t max) {
    int success;
    lua_Integer i = lua_tointegerx(lua_state, index, &success);
    if (!success) luaL_error(lua_state, "Failed to convert value to integer\n");
    if ((int64_t)i < min || (int64_t)i > max)
        luaL_error(lua_state, "Integer value outside range (%U to %U)", (long int)min, (long int)max);
    return i;
}

uint64_t l_to_uint_range(int index, uint64_t min, uint64_t max) {
    int success;
    lua_Integer i = lua_tointegerx(lua_state, index, &success);
    if (!success) luaL_error(lua_state, "Failed to convert value to integer\n");
    if (i < min || i > max)
        luaL_error(lua_state, "Integer value outside range (%U to %U)", (long int)min, (long int)max);
    return i;
}

const char* l_to_str(int index) {
    if (lua_type(lua_state, index) != LUA_TSTRING)
        luaL_error(lua_state, "Failed to convert value to string\n");
    size_t len;
    const char* res = lua_tolstring(lua_state, index, &len);
    for (size_t i = 0; i < len; i++)
        if (!res[i]) luaL_error(lua_state, "Strings must not contain NULL characters\n");
    return res;
}

double l_to_double(int index) {
    int success;
    lua_Number n = lua_tonumberx(lua_state, index, &success);
    if (!success) luaL_error(lua_state, "Failed to convert value to double\n");
    return n;
}

void* l_to_obj(int index, l_metatable_t* class) {
    return luaL_checkudata(lua_state, index, class->name);
}

void* l_validate_ptr(void* ptr, l_metatable_t* class) {
    lua_rawgeti(lua_state, LUA_REGISTRYINDEX, ptrs_ref);
    lua_rawgetp(lua_state, -1, ptr);
    if (lua_isnil(lua_state, -1)) {
        luaL_error(lua_state, "Invalid pointer\n");
        lua_pop(lua_state, 1);
    }
    
    if (class) {
        lua_rawgeti(lua_state, LUA_REGISTRYINDEX, class->metatable_ref);
        if (!lua_rawequal(lua_state, -1, -2)) {
            lua_pop(lua_state, 2);
            luaL_error(lua_state, "Invalid pointer\n");
        }
        lua_pop(lua_state, 2);
    } else {
        lua_pop(lua_state, 1);
    }
    
    return ptr;
}
