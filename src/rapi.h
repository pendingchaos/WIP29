#ifndef RAPI_H
#define RAPI_H

#include "bin.h"

#include <SDL2/SDL_video.h>
#include <stdbool.h>
#include <stdint.h>

#define R_MAKE_FORMAT(type, components, normalized, srgb) (((components)-1) | ((type)<<4) | (((normalized)?1:0)<<3) | (((srgb)?1:0)<<2))
#define R_FORMAT_SRGB(fmt) (((fmt)&0x4)>>2)
#define R_FORMAT_NORMALIZED(fmt) (((fmt)&0x8)>>3)
#define R_FORMAT_TYPE(fmt) (((fmt)&~0xe)>>4)
#define R_FORMAT_COMPONENTS(fmt) (((fmt)&0x3)+1)

#define R_IMAGE_DEPTH_U16_NORM 128
#define R_IMAGE_DEPTH_U32_NORM 129
#define R_IMAGE_DEPTH_F32 130
#define R_IMAGE_DEPTH_STENCIL_U24_NORM_U8 131

typedef enum r_api_t {
    R_API_OPENGL
} r_api_t;

typedef enum r_image_format_type_t {
    R_IMAGE_U8,
    R_IMAGE_S8,
    R_IMAGE_U16,
    R_IMAGE_S16,
    R_IMAGE_U32,
    R_IMAGE_S32,
    R_IMAGE_F16,
    R_IMAGE_F32
} r_image_format_type_t;

typedef uint32_t r_image_format_t;

typedef enum r_image_layout_t {
    R_IMAGE_GENERAL,
    R_IMAGE_COLOR_ATTACHMENT_OPTIMAL,
    R_IMAGE_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
    R_IMAGE_SHADER_SAMPLE_OPTIMAL,
    R_IMAGE_PRESENT
} r_image_layout_t;

typedef enum r_image_access_t {
    R_IMAGE_COLOR_ATTACHMENT_READ = 1 << 0,
    R_IMAGE_COLOR_ATTACHMENT_WRITE = 1 << 1,
    R_IMAGE_DEPTH_STENCIL_ATTACHMENT_READ = 1 << 2,
    R_IMAGE_DEPTH_STENCIL_ATTACHMENT_WRITE = 1 << 3,
    R_IMAGE_SHADER_READ = 1 << 4,
    R_IMAGE_MEMORY_READ = 1 << 5
} r_image_access_t;

typedef enum r_stage_t {
    R_STAGE_VERTEX,
    R_STAGE_FRAGMENT,
    R_STAGE_COMPUTE
} r_stage_t;

typedef enum r_sampler_address_mode_t {
    R_SAMPLER_REPEAT,
    R_SAMPLER_MIRRORED_REPEAT,
    R_SAMPLER_CLAMP_TO_EDGE
} r_sampler_address_mode_t;

typedef enum r_sampler_filter_t {
    R_SAMPLER_NEAREST,
    R_SAMPLER_LINEAR
} r_sampler_filter_t;

typedef enum r_swapchain_flags_t {
    R_SWAPCHAIN_DEPTH = 1 << 0
} r_swapchain_flags_t;

typedef enum r_pass_attachment_flags_t {
    R_PASS_ATTACHMENT_DEPTHSTENCIL = 1 << 0,
    R_PASS_ATTACHMENT_CLEAR = 1 << 1
} r_pass_attachment_flags_t;

typedef enum r_buffer_flags_t {
    R_BUFFER_VERTEX = 1 << 0,
    R_BUFFER_INDEX = 1 << 1,
    R_BUFFER_UNIFORM = 1 << 2
} r_buffer_flags_t;

typedef enum r_image_flags_t {
    R_IMAGE_SAMPLE = 1 << 0,
    R_IMAGE_COLOR_ATTACHMENT = 1 << 1,
    R_IMAGE_DEPTH_STENCIL_ATTACHMENT = 1 << 2
} r_image_flags_t;

typedef enum r_image_type_t {
    R_IMAGE_2D,
    R_IMAGE_CUBE_MAP
} r_image_type_t;

typedef enum r_pipeline_type_t {
    R_PIPELINE_GRAPHICS,
    R_PIPELINE_COMPUTE
} r_pipeline_type_t;

typedef enum r_vertex_format_t {
    R_VERTEX_FORMAT_X32F,
    R_VERTEX_FORMAT_XY32F,
    R_VERTEX_FORMAT_XYZ32F,
    R_VERTEX_FORMAT_XYZW32F
} r_vertex_format_t;

typedef enum r_topology_t {
    R_TOPOLOGY_TRIANGLES,
    R_TOPOLOGY_TRIANGLE_STRIP,
    R_TOPOLOGY_LINES
} r_topology_t;

typedef enum r_cull_mode_t {
    R_CULL_BACK,
    R_CULL_FRONT,
    R_CULL_NONE
} r_cull_mode_t;

typedef enum r_winding_t {
    R_WINDING_CCW,
    R_WINDING_CW
} r_winding_t;

typedef enum r_pipeline_depth_stencil_flags_t {
    R_PIPELINE_DEPTH_TEST = 1 << 0,
    R_PIPELINE_DEPTH_WRITE = 1 << 1,
    R_PIPELINE_STENCIL_TEST = 1 << 2,
    R_PIPELINE_STENCIL_WRITE = 1 << 3
} r_pipeline_depth_stencil_flags_t;

typedef enum r_compare_func_t {
    R_COMPARE_LESS,
    R_COMPARE_GREATER,
    R_COMPARE_LESS_EQUAL,
    R_COMPARE_GREATER_EQUAL,
    R_COMPARE_NOT_EQUAL,
    R_COMPARE_EQUAL,
    R_COMPARE_TRUE,
    R_COMPARE_FALSE
} r_compare_func_t;

typedef enum r_stencil_func_t {
    R_STENCIL_KEEP,
    R_STENCIL_ZERO,
    R_STENCIL_REPLACE,
    R_STENCIL_INCREMENT_AND_CLAMP,
    R_STENCIL_DECREMENT_AND_CLAMP,
    R_STENCIL_INVERT,
    R_STENCIL_INCREMENT_AND_WRAP,
    R_STENCIL_DECREMENT_AND_WRAP
} r_stencil_func_t;

typedef enum r_index_type_t {
    R_INDEX_TYPE_UINT16,
    R_INDEX_TYPE_UINT32
} r_index_type_t;

typedef enum r_desc_type_t  {
    R_DESC_IMAGE_AND_SAMPLER,
    R_DESC_UNIFORM_BUFFER
} r_desc_type_t ;

typedef enum r_cmd_buf_type_t {
    R_CMD_BUF_GRAPHICS,
    R_CMD_BUF_COMPUTE
} r_cmd_buf_type_t;

typedef struct r_shader_t r_shader_t;
typedef struct r_sampler_t r_sampler_t;
typedef struct r_image_t r_image_t;
typedef struct r_pass_t r_pass_t;
typedef struct r_framebuffer_t r_framebuffer_t;
typedef struct r_buffer_t r_buffer_t;
typedef struct r_pipeline_t r_pipeline_t;
typedef struct r_cmd_buf_t r_cmd_buf_t;
typedef struct r_desc_set_layout_t r_desc_set_layout_t;
typedef struct r_desc_set_t r_desc_set_t;
typedef struct r_timestamp_t r_timestamp_t;

typedef struct r_stencil_state_face_t {
    r_stencil_func_t stencil_fail_func;
    r_stencil_func_t stencil_pass_depth_pass_func;
    r_stencil_func_t stencil_pass_depth_fail_func;
    r_compare_func_t compare_func;
    uint32_t compare_mask;
    uint32_t write_mask;
    uint32_t reference;
} r_stencil_state_face_t;

typedef struct r_subpass_attachment_t { //Get rid of this
    uint32_t buffer;
    uint32_t flags;
} r_subpass_attachment_t;

typedef struct r_subpass_t {
    int32_t color_indices[8];
    int32_t depthstencil_index;
} r_subpass_t;

typedef union r_clear_value_t {
    float color[4];
    struct {float depth; uint32_t stencil;} depthstencil;
} r_clear_value_t;

typedef struct r_desc_set_layout_binding_t {
    uint32_t binding;
    r_desc_type_t type;
} r_desc_set_layout_binding_t;

typedef union r_desc_t {
    struct {
        r_image_t* image;
        r_sampler_t* sampler;
    } image_and_sampler;
    r_buffer_t* uniform_buffer;
} r_desc_t;

typedef struct r_image_create_info_t {
    r_image_type_t type;
    r_image_format_t format;
    uint32_t width;
    uint32_t height;
    uint32_t mipmaps;
    union {
        const void** data2d;
        const void** data_cube[6];
    };
    r_image_flags_t flags;
    r_image_access_t initial_access;
    r_image_layout_t initial_layout;
} r_image_create_info_t;

void r_init_api(r_api_t api, SDL_Window* window);
void r_deinit_api();

void r_init_swapchain(uint32_t width, uint32_t height, uint32_t flags);
//Initial layout: R_IMAGE_PRESENT
//Initial access: R_IMAGE_MEMORY_READ
r_image_t* r_swapchain_get_color_image();
//Initial layout: R_IMAGE_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
//Initial access: R_IMAGE_DEPTH_STENCIL_ATTACHMENT_READ | R_IMAGE_DEPTH_STENCIL_ATTACHMENT_WRITE
r_image_t* r_swapchain_get_depth_image();

r_image_t* r_image_create(r_image_create_info_t info);
void r_image_del(r_image_t* image);

r_shader_t* r_shader_create(const b_shader_t* shader);
void r_shader_del(r_shader_t* shader);

r_sampler_t* r_sampler_create();
void r_sampler_del(r_sampler_t* sampler);
void r_sampler_min_filter(r_sampler_t* sampler, r_sampler_filter_t filter);
void r_sampler_mag_filter(r_sampler_t* sampler, r_sampler_filter_t filter);
void r_sampler_mipmap_filter(r_sampler_t* sampler, r_sampler_filter_t filter);
void r_sampler_address_modes(r_sampler_t* sampler, const r_sampler_address_mode_t* addr_modes);
void r_sampler_anisotropy(r_sampler_t* sampler, bool enabled, float max_anisotropy);
void r_sampler_min_max_lod(r_sampler_t* sampler, float min_lod, float max_lod);
void r_sampler_finalize(r_sampler_t* sampler);

r_pass_t* r_pass_create();
void r_pass_del(r_pass_t* pass);
void r_pass_add_attachment(r_pass_t* pass, uint32_t flags); //Attachment count max: 8
void r_pass_add_subpass(r_pass_t* pass, const r_subpass_t* subpass);
void r_pass_finalize(r_pass_t* pass);

r_framebuffer_t* r_framebuffer_create(r_pass_t* pass, r_image_t*const* images);
void r_framebuffer_del(r_framebuffer_t* framebuffer);

r_buffer_t* r_buffer_create(uint32_t size, const void* data, r_buffer_flags_t flags);
void r_buffer_del(r_buffer_t* buffer);

//Image+sampler binding count max: 16 for fragment and compute
//                                 8 for vertex, geometry, tesselation control and evaluation
//Uniform buffer binding count max: 12
r_desc_set_layout_t* r_desc_set_layout_create(uint32_t binding_count, r_desc_set_layout_binding_t* bindings);
void r_desc_set_layout_del(r_desc_set_layout_t* layout);

r_desc_set_t* r_desc_set_create(r_desc_set_layout_t* layout, const r_desc_t* descs);
void r_desc_set_del(r_desc_set_t* set);

r_timestamp_t* r_timestamp_create();
void r_timestamp_del(r_timestamp_t* timestamp);
bool r_timestamp_available(r_timestamp_t* timestamp); //TODO: This may not make sense withm multiple r_cmd_set_timestamp
uint64_t r_timestamp_get(r_timestamp_t* timestamp);

r_pipeline_t* r_pipeline_create(r_pipeline_type_t type);
void r_pipeline_del(r_pipeline_t* pipeline);
void r_pipeline_push_constant_info(r_pipeline_t* pipeline, uint32_t start, uint32_t size);
void r_pipeline_vertex_shader(r_pipeline_t* pipeline, r_shader_t* vertex);
void r_pipeline_fragment_shader(r_pipeline_t* pipeline, r_shader_t* fragment);
void r_pipeline_compute_shader(r_pipeline_t* pipeline, r_shader_t* compute);
void r_pipeline_vertex_attribute(r_pipeline_t* pipeline, uint32_t index, uint32_t stride, uint32_t offset, r_vertex_format_t format); //Vertex attribute max: 16
void r_pipeline_topology(r_pipeline_t* pipeline, r_topology_t topology);
void r_pipeline_cull(r_pipeline_t* pipeline, r_cull_mode_t mode, r_winding_t winding);
void r_pipeline_depth_stencil(r_pipeline_t* pipeline, uint32_t flags, r_compare_func_t compare, r_stencil_state_face_t* faces); //0 = front 1 = back
void r_pipeline_pass(r_pipeline_t* pipeline, r_pass_t* pass, uint32_t subpass_index);
void r_pipeline_desc_set_layout(r_pipeline_t* pipeline, r_stage_t stage, r_desc_set_layout_t* layout);
void r_pipeline_finalize(r_pipeline_t* pipeline);

r_cmd_buf_t* r_cmd_buf_create(r_cmd_buf_type_t type);
void r_cmd_buf_del(r_cmd_buf_t* cmd_buf);
void r_cmd_buf_begin(r_cmd_buf_t* cmd_buf);
void r_cmd_buf_end(r_cmd_buf_t* cmd_buf);
void r_cmd_buf_set_viewport(r_cmd_buf_t* cmd_buf, uint32_t left, uint32_t bottom, uint32_t width, uint32_t height);
void r_cmd_buf_pass_begin(r_cmd_buf_t* cmd_buf, r_pass_t* pass, r_framebuffer_t* framebuffer, const r_clear_value_t* clear_values);
void r_cmd_buf_pass_end(r_cmd_buf_t* cmd_buf);
void r_cmd_buf_next_subpass(r_cmd_buf_t* cmd_buf);
void r_cmd_buf_set_timestamp(r_cmd_buf_t* cmd_buf, r_timestamp_t* timestamp);
void r_cmd_buf_bind_pipeline(r_cmd_buf_t* cmd_buf, const r_pipeline_t* pipeline);
void r_cmd_buf_bind_vertex_buffers(r_cmd_buf_t* cmd_buf, uint32_t count, r_buffer_t*const* buffers, const uint32_t* offsets);
void r_cmd_buf_bind_index_buffer(r_cmd_buf_t* cmd_buf, const r_buffer_t* buffer, uint32_t offset, r_index_type_t type);
void r_cmd_buf_set_push_constants(r_cmd_buf_t* cmd_buf, uint32_t start, uint32_t size, const void* data);
void r_cmd_buf_bind_desc_set(r_cmd_buf_t* cmd_buf, r_stage_t stage, r_desc_set_t* set);
void r_cmd_buf_image_barriers(r_cmd_buf_t* cmd_buf, uint32_t count, r_image_t*const* image, const r_image_access_t** access, const r_image_layout_t** layout);
void r_cmd_buf_set_buffer_data(r_cmd_buf_t* cmd_buf, r_buffer_t* buffer, uint32_t offset, uint32_t size, const void* data);
void r_cmd_buf_draw_indexed(r_cmd_buf_t* cmd_buf, uint32_t index_count, uint32_t instance_count, uint32_t first_index, int32_t vertex_offset, uint32_t first_instance);
void r_cmd_buf_draw(r_cmd_buf_t* cmd_buf, uint32_t vertex_count, uint32_t instance_count, uint32_t first_vertex, uint32_t first_instance);
void r_cmd_buf_dispatch_compute(r_cmd_buf_t* cmd_buf, const uint32_t* dim);
void r_cmd_buf_submit(uint32_t count, r_cmd_buf_t*const* cmd_buf); //They are executed in order. There are no command buffer submission ordering guarentees

void r_frame_submit();
#endif
