#ifndef PHYSICS_H
#define PHYSICS_H

#include "mathy_stuff.h"

#include <stdint.h>

typedef struct p_world_t p_world_t;
typedef struct p_shape_t p_shape_t;
typedef struct p_body_t p_body_t;

typedef enum p_body_type_t {
    P_BODY_STATIC,
    P_BODY_DYNAMIC
} p_body_type_t;

typedef struct p_body_info_t {
    p_body_type_t type;
    float mass;
    p_shape_t* shape;
    void (*get_transform)(m_vec3_t*, m_quat_t*, void*);
    void (*set_transform)(m_vec3_t, m_quat_t, void*);
    void* userdata;
} p_body_info_t;

p_world_t* p_world_create(m_vec3_t gravity);
void p_world_del(p_world_t* world);
void p_world_step(p_world_t* world, float timestep, unsigned int max_substeps, float fixed_timestep);
void p_world_debug_draw(p_world_t* world);

p_shape_t* p_shape_create_sphere(float radius);
p_shape_t* p_shape_create_box(m_vec3_t half_extents);
p_shape_t* p_shape_create_cylinder_x(float radius, float half_height);
p_shape_t* p_shape_create_cylinder_y(float radius, float half_height);
p_shape_t* p_shape_create_cylinder_z(float radius, float half_height);
p_shape_t* p_shape_create_capsule_x(float radius, float half_height);
p_shape_t* p_shape_create_capsule_y(float radius, float half_height);
p_shape_t* p_shape_create_capsule_z(float radius, float half_height);
p_shape_t* p_shape_create_cone_x(float radius, float half_height);
p_shape_t* p_shape_create_cone_y(float radius, float half_height);
p_shape_t* p_shape_create_cone_z(float radius, float half_height);
p_shape_t* p_shape_create_hull(uint32_t count, m_vec3_t* positions);
p_shape_t* p_shape_create_triangle_mesh(uint32_t tri_count, uint32_t* indices, m_vec3_t* positions);
p_shape_t* p_shape_create_bvh_triangle_mesh(uint32_t tri_count, uint32_t* indices, m_vec3_t* positions);
p_shape_t* p_shape_create_plane(float dist, m_vec3_t normal);
p_shape_t* p_shape_create_compound(uint32_t count, const p_shape_t** shapes, const m_vec3_t* positions,
                                   const m_quat_t* orientations);
void p_shape_del(p_shape_t* shape);

p_body_t* p_body_create(p_world_t* world, p_body_info_t info);
void p_body_del(p_body_t* body);
void p_body_set_transform(p_body_t* body, m_vec3_t position, m_quat_t orientation);
void p_body_get_transform(p_body_t* body, m_vec3_t* position, m_quat_t* orientation);
#endif
