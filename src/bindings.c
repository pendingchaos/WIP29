#include "scripting.h"
#include "scene.h"
#include "bin.h"

#include <lauxlib.h>
#include <string.h>

#if 0
static l_metatable_t* vec3_class;
static l_metatable_t* mat4_class;
static l_metatable_t* plane_class;
static l_metatable_t* frustum_class;
static l_metatable_t* aabb_class;
static l_metatable_t* sphere_class;
static l_metatable_t* quat_class;
static l_metatable_t* bin_class;
static l_metatable_t* bin_texture_map_class;
static l_metatable_t* bin_cubemap_map_class;
static l_metatable_t* bin_shader_map_class;
static l_metatable_t* bin_material_map_class;
static l_metatable_t* bin_mesh_map_class;
static l_metatable_t* bin_scene_map_class;
static l_metatable_t* bin_texture_class;
static l_metatable_t* bin_cubemap_class;
static l_metatable_t* bin_shader_class;
static l_metatable_t* bin_material_class;
static l_metatable_t* bin_mesh_class;
static l_metatable_t* bin_scene_class;
static l_metatable_t* scene_class;
static l_metatable_t* scene_object_class;
static l_metatable_t* scene_mesh_obj_list_class;

static int l_b_read(lua_State* state) {
    l_func_start(1, true);
    bin_t* bin = b_read(l_to_str(1));
    if (bin) {
        l_push_obj(bin_class, &bin);
        l_ptr_new(bin, bin_class);
    } else {
        lua_pushnil(state);
    }
    return 1;
}

static int l_b_write(lua_State* state) {
    l_func_start(2, true);
    bool res = b_write(l_to_str(2), l_validate_ptr(*(void**)l_to_obj(1, bin_class), NULL));
    lua_pushboolean(state, res);
    return 1;
}

static int l_b_del(lua_State* state) {
    l_func_start(1, false);
    bin_t* bin = l_validate_ptr(*(void**)l_to_obj(1, bin_class), NULL);
    b_del(bin);
    l_ptr_del(bin);
    return 0;
}

static int l_b_index(lua_State* state) {
    l_func_start(2, true);
    
    bin_t* bin = l_validate_ptr(*(void**)l_to_obj(1, bin_class), NULL);
    
    const char* name = l_to_str(2);
    if (!strcmp(name, "textures"))
        l_push_obj(bin_texture_map_class, &bin);
    else if (!strcmp(name, "cubemaps"))
        l_push_obj(bin_cubemap_map_class, &bin);
    else if (!strcmp(name, "shaders"))
        l_push_obj(bin_shader_map_class, &bin);
    else if (!strcmp(name, "materials"))
        l_push_obj(bin_material_map_class, &bin);
    else if (!strcmp(name, "meshes"))
        l_push_obj(bin_mesh_map_class, &bin);
    else if (!strcmp(name, "scenes"))
        l_push_obj(bin_scene_map_class, &bin);
    else if (!strcmp(name, "write"))
        lua_pushcfunction(state, &l_b_write);
    else if (!strcmp(name, "del"))
        lua_pushcfunction(state, &l_b_del);
    else
        lua_pushnil(state);
    
    return 1;
}

static int l_b_texture_map_index(lua_State* state) {
    l_func_start(2, true);
    bin_t* bin = l_validate_ptr(l_to_obj(1, bin_class), NULL);
    const char* name = l_to_str(2);
    b_texture_t* res = b_find_texture(bin, name);
    if (res) l_push_obj(bin_texture_class, &res);
    else lua_pushnil(state);
    return 1;
}

static int l_b_cubemap_map_index(lua_State* state) {
    l_func_start(2, true);
    bin_t* bin = l_validate_ptr(*(void**)l_to_obj(1, bin_class), NULL);
    const char* name = l_to_str(2);
    b_cubemap_t* res = b_find_cubemap(bin, name);
    if (res) l_push_obj(bin_cubemap_class, &res);
    else lua_pushnil(state);
    return 1;
}

static int l_b_shader_map_index(lua_State* state) {
    l_func_start(2, true);
    bin_t* bin = l_validate_ptr(*(void**)l_to_obj(1, bin_class), NULL);
    const char* name = l_to_str(2);
    b_shader_t* res = b_find_shader(bin, name);
    if (res) l_push_obj(bin_shader_class, &res);
    else lua_pushnil(state);
    return 1;
}

static int l_b_material_map_index(lua_State* state) {
    l_func_start(2, true);
    bin_t* bin = l_validate_ptr(*(void**)l_to_obj(1, bin_class), NULL);
    const char* name = l_to_str(2);
    b_material_t* res = b_find_material(bin, name);
    if (res) l_push_obj(bin_material_class, &res);
    else lua_pushnil(state);
    return 1;
}

static int l_b_mesh_map_index(lua_State* state) {
    l_func_start(2, true);
    bin_t* bin = l_validate_ptr(*(void**)l_to_obj(1, bin_class), NULL);
    const char* name = l_to_str(2);
    b_mesh_t* res = b_find_mesh(bin, name);
    if (res) l_push_obj(bin_mesh_class, &res);
    else lua_pushnil(state);
    return 1;
}

static int l_b_scene_map_index(lua_State* state) {
    l_func_start(2, true);
    bin_t* bin = l_validate_ptr(*(void**)l_to_obj(1, bin_class), NULL);
    const char* name = l_to_str(2);
    b_scene_t* res = b_find_scene(bin, name);
    if (res) l_push_obj(bin_scene_class, &res);
    else lua_pushnil(state);
    return 1;
}

static int l_s_create(lua_State* state) {
    l_func_start(2, true);
    bin_t* bin = l_validate_ptr(*(void**)l_to_obj(1, bin_class), NULL);
    b_scene_t* scene;
    if (lua_isnil(state, 2))
        scene = bin->scene_count ? &bin->scenes[0] : NULL;
    else
        scene = l_validate_ptr(*(void**)l_to_obj(2, bin_scene_class), NULL);
    
    if (scene) {
        s_scene_t* res = s_create(bin, scene);
        for (size_t i = 0; i < res->mesh_obj_count; i++)
            l_ptr_new(res->mesh_objs[i], scene_object_class);
        if (res->sun) l_ptr_new(res->sun, scene_object_class);
        l_push_obj(scene_class, &res);
        l_ptr_new(res, scene_class);
    } else {
        lua_pushnil(state);
    }
    
    return 1;
}

static int l_s_del(lua_State* state) {
    l_func_start(1, false);
    s_scene_t* scene = l_validate_ptr(*(void**)l_to_obj(1, scene_class), NULL);
    s_del(scene);
    l_ptr_del(scene);
    return 0;
}

static int l_s_create_mesh_obj(lua_State* state) {
    l_func_start(2, true);
    s_scene_t* scene = l_validate_ptr(*(void**)l_to_obj(1, scene_class), NULL);
    const char* name = l_to_str(2);
    b_mesh_t* mesh = l_validate_ptr(*(void**)l_to_obj(3, bin_mesh_class), NULL);
    //TODO
    //s_mesh_obj_t* obj = s_create_mesh_obj(scene, name, mesh);
    //l_push_obj(scene_object_class, &obj);
    //l_ptr_new(obj, scene_object_class);
    return 1;
}

static int l_s_create_sun_obj(lua_State* state) {
    l_func_start(1, true);
    s_scene_t* scene = l_validate_ptr(*(void**)l_to_obj(1, scene_class), NULL);
    const char* name = l_to_str(2);
    s_sun_obj_t* obj = s_create_sun_obj(scene, name);
    l_push_obj(scene_object_class, &obj);
    l_ptr_new(obj, scene_object_class);
    return 1;
}

static int l_s_del_obj(lua_State* state) {
    l_func_start(2, false);
    s_scene_t* scene = l_validate_ptr(*(void**)l_to_obj(1, scene_class), NULL);
    s_object_t* obj = l_validate_ptr(*(void**)l_to_obj(1, scene_object_class), NULL);
    s_del_obj(scene, obj);
    l_ptr_del(obj);
    return 0;
}

static int l_s_do_frame(lua_State* state) {
    l_func_start(1, false);
    s_scene_t* scene = l_validate_ptr(*(void**)l_to_obj(1, scene_class), NULL);
    m_vec3_t cam_pos = *(m_vec3_t*)l_to_obj(2, vec3_class);
    m_vec3_t cam_dir = *(m_vec3_t*)l_to_obj(3, vec3_class);
    m_vec3_t cam_up = *(m_vec3_t*)l_to_obj(4, vec3_class);
    float frametime = l_to_double(5);
    m_vec3_normalize(&cam_dir, &cam_dir);
    m_vec3_normalize(&cam_up, &cam_up);
    s_frame(scene, cam_pos, cam_dir, cam_up, frametime);
    return 0;
}

static int l_s_index(lua_State* state) {
    l_func_start(2, true);
    
    s_scene_t* scene = l_validate_ptr(*(void**)l_to_obj(1, scene_class), NULL);
    
    const char* name = l_to_str(2);
    if (!strcmp(name, "mesh_objs")) {
        l_push_obj(scene_mesh_obj_list_class, &scene);
    } else if (!strcmp(name, "sun")) {
        if (scene->sun) l_push_obj(scene_object_class, scene->sun);
        else lua_pushnil(state);
    } else if (!strcmp(name, "create_mesh_obj")) {
        lua_pushcfunction(state, &l_s_create_mesh_obj);
    } else if (!strcmp(name, "create_sun_obj")) {
        lua_pushcfunction(state, &l_s_create_sun_obj);
    } else if (!strcmp(name, "del_obj")) {
        lua_pushcfunction(state, &l_s_del_obj);
    } else if (!strcmp(name, "do_frame")) {
        lua_pushcfunction(state, &l_s_do_frame);
    } else if (!strcmp(name, "del")) {
        lua_pushcfunction(state, &l_s_del);
    } else {
        lua_pushnil(state);
    }
    
    return 1;
}

static int l_s_scene_obj_index(lua_State* state) {
    l_func_start(2, true);
    
    s_object_t* obj = l_validate_ptr(*(void**)l_to_obj(1, scene_object_class), NULL);
    
    const char* name = l_to_str(2);
    if (!strcmp(name, "name")) {
        lua_pushstring(state, obj->name);
    } else if (!strcmp(name, "type")) {
        switch (obj->type) {
        case S_OBJ_MESH: lua_pushstring(state, "mesh"); break;
        case S_OBJ_SUN: lua_pushstring(state, "sun"); break;
        case S_OBJ_SKY: lua_pushstring(state, "sky"); break;
        case S_OBJ_ACTOR: lua_pushstring(state, "actor"); break;
        }
    } else if (!strcmp(name, "pos")) {
        l_push_obj(vec3_class, &obj->pos);
    } else if (!strcmp(name, "scale")) {
        l_push_obj(vec3_class, &obj->scale);
    } else if (!strcmp(name, "orientation")) {
        l_push_obj(quat_class, &obj->orientation);
    } else if (!strcmp(name, "parent")) {
        if (obj->parent) l_push_obj(scene_object_class, &obj->parent);
        else lua_pushnil(state);
    } else if (!strcmp(name, "children")) {
        //TODO
    } else if (!strcmp(name, "visible_mesh") && obj->type==S_OBJ_MESH) {
        l_push_obj(bin_mesh_class, &((s_mesh_obj_t*)obj)->visible_mesh);
    } else if (!strcmp(name, "color") && obj->type==S_OBJ_SUN) {
        //TODO
    } else if (!strcmp(name, "intensity") && obj->type==S_OBJ_SUN) {
        lua_pushnumber(state, ((s_sun_obj_t*)state)->intensity);
    } else if (!strcmp(name, "skybox") && obj->type==S_OBJ_SKY) {
        //TODO
    } else {
        lua_pushnil(state);
    }
    
    return 1;
}

static int l_s_scene_obj_newindex(lua_State* state) {
    l_func_start(3, false);
    
    s_object_t* obj = l_validate_ptr(*(void**)l_to_obj(1, scene_object_class), NULL);
    
    const char* name = l_to_str(2);
    if (!strcmp(name, "name")) {
        const char* new = l_to_str(3);
        free(obj->name);
        obj->name = calloc(1, strlen(new)+1);
        strcpy(obj->name, new);
    } else if (!strcmp(name, "pos")) {
        obj->pos = *(m_vec3_t*)l_to_obj(3, vec3_class);
    } else if (!strcmp(name, "scale")) {
        obj->scale = *(m_vec3_t*)l_to_obj(3, vec3_class);
    } else if (!strcmp(name, "orientation")) {
        obj->orientation = *(m_quat_t*)l_to_obj(3, quat_class);
    } else if (!strcmp(name, "parent")) {
        if (lua_isnil(state, 3)) {
            s_set_parent(obj, NULL);
        } else {
            s_object_t* p = l_validate_ptr(*(void**)l_to_obj(3, scene_object_class), NULL);
            s_set_parent(obj, p);
        }
    } else if (!strcmp(name, "visible_mesh") && obj->type==S_OBJ_MESH) {
        ((s_mesh_obj_t*)obj)->visible_mesh = l_validate_ptr(*(void**)l_to_obj(3, bin_mesh_class), NULL);
    } else if (!strcmp(name, "color") && obj->type==S_OBJ_SUN) {
        //TODO
    } else if (!strcmp(name, "intensity") && obj->type==S_OBJ_SUN) {
        ((s_sun_obj_t*)state)->intensity = l_to_double(3);
    } else {
        luaL_error(state, "Attempted to set nonexistent field\n");
    }
    
    return 0;
}

static int l_s_mesh_obj_list_index(lua_State* state) {
    l_func_start(2, true);
    
    s_scene_t* scene = l_validate_ptr(*(void**)l_to_obj(1, scene_mesh_obj_list_class), NULL);
    
    size_t index = l_to_size_t(2);
    
    if (index<1 || index>scene->mesh_obj_count) lua_pushnil(state);
    else l_push_obj(scene_object_class, &scene->mesh_objs[index-1]);
    
    return 1;
}

static int l_s_mesh_obj_list_length(lua_State* state) {
    l_func_start(2, true);
    
    s_scene_t* scene = l_validate_ptr(*(void**)l_to_obj(1, scene_mesh_obj_list_class), NULL);
    
    lua_pushinteger(state, scene->mesh_obj_count);
    
    return 1;
}

void l_bindings_init() {
    vec3_class = l_register_metatable("Vec3", sizeof(m_vec3_t));
    //TODO
    
    mat4_class = l_register_metatable("Mat4", sizeof(m_mat4_t));
    //TODO
    
    plane_class = l_register_metatable("Plane", sizeof(m_plane_t));
    //TODO
    
    frustum_class = l_register_metatable("Frustum", sizeof(m_frustum_t));
    //TODO
    
    aabb_class = l_register_metatable("AABB", sizeof(m_aabb_t));
    //TODO
    
    sphere_class = l_register_metatable("Sphere", sizeof(m_sphere_t));
    //TODO
    
    quat_class = l_register_metatable("Quat", sizeof(m_quat_t));
    //TODO
    
    bin_class = l_register_metatable("Bin", sizeof(bin_t*));
    l_register_method(bin_class, "new", &l_b_read);
    l_register_method(bin_class, "__index", &l_b_index);
    
    bin_texture_map_class = l_register_metatable("BinTextureMap", sizeof(bin_t*));
    l_register_method(bin_texture_map_class, "__index", &l_b_texture_map_index);
    bin_cubemap_map_class = l_register_metatable("BinCubemapMap", sizeof(bin_t*));
    l_register_method(bin_cubemap_map_class, "__index", &l_b_cubemap_map_index);
    bin_shader_map_class = l_register_metatable("BinShaderMap", sizeof(bin_t*));
    l_register_method(bin_shader_map_class, "__index", &l_b_shader_map_index);
    bin_material_map_class = l_register_metatable("BinMaterialMap", sizeof(bin_t*));
    l_register_method(bin_material_map_class, "__index", &l_b_material_map_index);
    bin_mesh_map_class = l_register_metatable("BinMeshMap", sizeof(bin_t*));
    l_register_method(bin_mesh_map_class, "__index", &l_b_mesh_map_index);
    bin_scene_map_class = l_register_metatable("BinSceneMap", sizeof(bin_t*));
    l_register_method(bin_scene_map_class, "__index", &l_b_scene_map_index);
    
    bin_texture_class = l_register_metatable("BinTexture", sizeof(bin_t*));
    bin_cubemap_class = l_register_metatable("BinCubemap", sizeof(bin_t*));
    bin_shader_class = l_register_metatable("BinShader", sizeof(bin_t*));
    bin_material_class = l_register_metatable("BinMaterial", sizeof(bin_t*));
    bin_mesh_class = l_register_metatable("BinMesh", sizeof(bin_t*));
    bin_scene_class = l_register_metatable("BinScene", sizeof(bin_t*));
    
    scene_class = l_register_metatable("Scene", sizeof(s_scene_t*));
    l_register_method(scene_class, "new", &l_s_create);
    l_register_method(scene_class, "__index", &l_s_index);
    scene_object_class = l_register_metatable("SceneObject", sizeof(s_object_t*));
    l_register_method(scene_object_class, "__index", &l_s_scene_obj_index);
    l_register_method(scene_object_class, "__newindex", &l_s_scene_obj_newindex);
    scene_mesh_obj_list_class = l_register_metatable("SceneMeshObjectList", sizeof(s_scene_t*));
    l_register_method(scene_mesh_obj_list_class, "__index", &l_s_mesh_obj_list_index);
    l_register_method(scene_mesh_obj_list_class, "__len", &l_s_mesh_obj_list_length);
}
#else
void l_bindings_init() {}
#endif
