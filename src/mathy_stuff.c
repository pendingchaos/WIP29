#include "mathy_stuff.h"

#include <math.h>
#include <xmmintrin.h>

m_vec3_t m_vec3_create(float x, float y, float z) {
    m_vec3_t v;
    v.x = x;
    v.y = y;
    v.z = z;
    return v;
}

void m_vec3_normalize(m_vec3_t* dest, const m_vec3_t* src) {
    float len = sqrt(src->x*src->x + src->y*src->y + src->z*src->z);
    if (len < 0.0001) return;
    dest->x = src->x / len;
    dest->y = src->y / len;
    dest->z = src->z / len;
}

void m_vec3_cross(m_vec3_t* restrict dest, const m_vec3_t* restrict a, const m_vec3_t* restrict b) {
    dest->x = a->y*b->z - a->z*b->y;
    dest->y = a->z*b->x - a->x*b->z;
    dest->z = a->x*b->y - a->y*b->x;
}

void m_vec3_inc(m_vec3_t* restrict val, const m_vec3_t* restrict by) {
    val->x += by->x;
    val->y += by->y;
    val->z += by->z;
}

void m_vec3_dec(m_vec3_t* restrict val, const m_vec3_t* restrict by) {
    val->x -= by->x;
    val->y -= by->y;
    val->z -= by->z;
}

void m_vec3_sub(m_vec3_t* restrict dest, const m_vec3_t* restrict a, const m_vec3_t* restrict b) {
    dest->x = a->x - b->x;
    dest->y = a->y - b->y;
    dest->z = a->z - b->z;
}

void m_vec3_mul(m_vec3_t* restrict dest, const m_vec3_t* restrict a, const m_vec3_t* restrict b) {
    dest->x = a->x * b->x;
    dest->y = a->y * b->y;
    dest->z = a->z * b->z;
}

float m_vec3_dot(const m_vec3_t* a, const m_vec3_t* b) {
    return a->x*b->x + a->y*b->y + a->z*b->z;
}

float m_vec3_length(const m_vec3_t* v) {
    return sqrtf(v->x*v->x + v->y*v->y + v->z*v->z);
}

void m_mat4_zero(m_mat4_t* mat) {
    for (int i = 0; i < 4; i++)
        for (int j = 0; j < 4; j++)
            mat->rows[i][j] = 0;
}

void m_mat4_create_identity(m_mat4_t* mat) {
    m_mat4_create_scale(mat, 1, 1, 1);
}

void m_mat4_create_scale(m_mat4_t* mat, float sx, float sy, float sz) {
    m_mat4_zero(mat);
    mat->rows[0][0] = sx;
    mat->rows[1][1] = sy;
    mat->rows[2][2] = sz;
    mat->rows[3][3] = 1;
}

void m_mat4_create_translate(m_mat4_t* mat, float tx, float ty, float tz) {
    m_mat4_create_identity(mat);
    mat->rows[0][3] = tx;
    mat->rows[1][3] = ty;
    mat->rows[2][3] = tz;
}

void m_mat4_create_rotate(m_mat4_t* mat, float ox, float oy, float oz, float ow) {
    float xx = ox * ox;
    float xy = ox * oy;
    float xz = ox * oz;
    float xw = ox * ow;
    float yy = oy * oy;
    float yz = oy * oz;
    float yw = oy * ow;
    float zz = oz * oz;
    float zw = oz * ow;
    
    m_mat4_zero(mat);
    mat->rows[0][0] = 1 - 2*yy - 2*zz;
    mat->rows[0][1] = 2*xy - 2*zw;
    mat->rows[0][2] = 2*xz - 2*yw;
    
    mat->rows[1][0] = 2*xy + 2*zw;
    mat->rows[1][1] = 1 - 2*xx - 2*zz;
    mat->rows[1][2] = 2*yz - 2*xw;
    
    mat->rows[2][0] = 2*xz - 2*yw;
    mat->rows[2][1] = 2*yz + 2*xw;
    mat->rows[2][2] = 1 - 2*xx - 2*yy;
    
    mat->rows[3][3] = 1;
}

void m_mat4_create_perspective(m_mat4_t* dest, float fov, float aspect, float near, float far) {
    fov = fov / 180 * pi;
    float e = tan(pi/2 - fov/2);
    float fpn = far + near;
    float fmn = far - near;
    
    m_mat4_zero(dest);
    dest->rows[0][0] = e / aspect;
    dest->rows[1][1] = e;
    dest->rows[2][2] = -(fpn / fmn);
    dest->rows[2][3] = -((2*far*near) / fmn);
    dest->rows[3][2] = -1;
}

void m_mat4_create_ortho(m_mat4_t* dest, const float *bx, const float* by, const float* bz) {
    float tx = (bx[1]+bx[0]) / (bx[1]-bx[0]);
    float ty = (by[1]+by[0]) / (by[1]-by[0]);
    float tz = (bz[1]+bz[0]) / (bz[1]-bz[0]);
    m_mat4_create_translate(dest, tx, ty, tz);
    
    dest->rows[0][0] = 2 / (bx[1]-bx[0]);
    dest->rows[1][1] = 2 / (by[1]-by[0]);
    dest->rows[2][2] = -2 / (bz[1]-bz[0]);
}

void m_mat4_create_lookat(m_mat4_t* dest, const m_vec3_t* eye, const m_vec3_t* dir, const m_vec3_t* up) {
    m_vec3_t up_, f, s, u;
    m_vec3_normalize(&up_, up);
    m_vec3_normalize(&f, dir);
    m_vec3_cross(&s, &f, &up_);
    m_vec3_normalize(&s, &s);
    m_vec3_cross(&u, &s, &f);
    
    dest->rows[0][0] = s.x;
    dest->rows[0][1] = s.y;
    dest->rows[0][2] = s.z;
    dest->rows[0][3] = -m_vec3_dot(&s, eye);
    
    dest->rows[1][0] = u.x;
    dest->rows[1][1] = u.y;
    dest->rows[1][2] = u.z;
    dest->rows[1][3] = -m_vec3_dot(&u, eye);
    
    dest->rows[2][0] = -f.x;
    dest->rows[2][1] = -f.y;
    dest->rows[2][2] = -f.z;
    dest->rows[2][3] = m_vec3_dot(&f, eye);
    
    dest->rows[3][0] = dest->rows[3][1] = dest->rows[3][2] = 0;
    dest->rows[3][3] = 1;
}

void m_mat4_mul(m_mat4_t* restrict dest, const m_mat4_t* restrict a, const m_mat4_t* restrict b) {
    __m128 b_row0 = _mm_loadu_ps(b->rows[0]);
    __m128 b_row1 = _mm_loadu_ps(b->rows[1]);
    __m128 b_row2 = _mm_loadu_ps(b->rows[2]);
    __m128 b_row3 = _mm_loadu_ps(b->rows[3]);
    
    __m128 row = _mm_mul_ps(b_row0, _mm_set1_ps(a->rows[0][0]));
    row = _mm_add_ps(row, _mm_mul_ps(b_row1, _mm_set1_ps(a->rows[0][1])));
    row = _mm_add_ps(row, _mm_mul_ps(b_row2, _mm_set1_ps(a->rows[0][2])));
    row = _mm_add_ps(row, _mm_mul_ps(b_row3, _mm_set1_ps(a->rows[0][3])));
    _mm_storeu_ps(dest->rows[0], row);
    
    row = _mm_mul_ps(b_row0, _mm_set1_ps(a->rows[1][0]));
    row = _mm_add_ps(row, _mm_mul_ps(b_row1, _mm_set1_ps(a->rows[1][1])));
    row = _mm_add_ps(row, _mm_mul_ps(b_row2, _mm_set1_ps(a->rows[1][2])));
    row = _mm_add_ps(row, _mm_mul_ps(b_row3, _mm_set1_ps(a->rows[1][3])));
    _mm_storeu_ps(dest->rows[1], row);
    
    row = _mm_mul_ps(b_row0, _mm_set1_ps(a->rows[2][0]));
    row = _mm_add_ps(row, _mm_mul_ps(b_row1, _mm_set1_ps(a->rows[2][1])));
    row = _mm_add_ps(row, _mm_mul_ps(b_row2, _mm_set1_ps(a->rows[2][2])));
    row = _mm_add_ps(row, _mm_mul_ps(b_row3, _mm_set1_ps(a->rows[2][3])));
    _mm_storeu_ps(dest->rows[2], row);
    
    row = _mm_mul_ps(b_row0, _mm_set1_ps(a->rows[3][0]));
    row = _mm_add_ps(row, _mm_mul_ps(b_row1, _mm_set1_ps(a->rows[3][1])));
    row = _mm_add_ps(row, _mm_mul_ps(b_row2, _mm_set1_ps(a->rows[3][2])));
    row = _mm_add_ps(row, _mm_mul_ps(b_row3, _mm_set1_ps(a->rows[3][3])));
    _mm_storeu_ps(dest->rows[3], row);
    
    /*for (unsigned int i = 0; i < 4; i++) {
        for (unsigned int j = 0; j < 4; j++) {
            float sum = 0;
            for (unsigned int k = 0; k < 4; k++)
                sum += a->rows[i][k] * b->rows[k][j];
            dest->rows[i][j] = sum;
        }
    }*/
}

void m_mat4_transpose(m_mat4_t* restrict dest, const m_mat4_t* restrict mat) {
    for (unsigned int i = 0; i < 4; i++)
        for (unsigned int j = 0; j < 4; j++)
            dest->rows[i][j] = mat->rows[j][i];
}

float m_mat4_determinant(const m_mat4_t* mat) {
    #define d mat->rows
    return (d[3][0])*d[2][1]*d[1][2]*d[0][3] - (d[2][0])*d[3][1]*d[1][2]*d[0][3] - (d[3][0])*d[1][1]*d[2][2]*d[0][3] + (d[1][0])*d[3][1]*d[2][2]*d[0][3] +
           (d[2][0])*d[1][1]*d[3][2]*d[0][3] - (d[1][0])*d[2][1]*d[3][2]*d[0][3] - (d[3][0])*d[2][1]*d[0][2]*d[1][3] + (d[2][0])*d[3][1]*d[0][2]*d[1][3] +
           (d[3][0])*d[0][1]*d[2][2]*d[1][3] - (d[0][0])*d[3][1]*d[2][2]*d[1][3] - (d[2][0])*d[0][1]*d[3][2]*d[1][3] + (d[0][0])*d[2][1]*d[3][2]*d[1][3] +
           (d[3][0])*d[1][1]*d[0][2]*d[2][3] - (d[1][0])*d[3][1]*d[0][2]*d[2][3] - (d[3][0])*d[0][1]*d[1][2]*d[2][3] + (d[0][0])*d[3][1]*d[1][2]*d[2][3] +
           (d[1][0])*d[0][1]*d[3][2]*d[2][3] - (d[0][0])*d[1][1]*d[3][2]*d[2][3] - (d[2][0])*d[1][1]*d[0][2]*d[3][3] + (d[1][0])*d[2][1]*d[0][2]*d[3][3] +
           (d[2][0])*d[0][1]*d[1][2]*d[3][3] - (d[0][0])*d[2][1]*d[1][2]*d[3][3] - (d[1][0])*d[0][1]*d[2][2]*d[3][3] + (d[0][0])*d[1][1]*d[2][2]*d[3][3];
    #undef d
}

//http://www.cg.info.hiroshima-cu.ac.jp/~miyazaki/knowledge/teche23.html
void m_mat4_inverse(m_mat4_t* restrict dest, const m_mat4_t* restrict mat) {
    #define m mat->rows
    float a11 = m[0][0];
    float a12 = m[0][1];
    float a13 = m[0][2];
    float a14 = m[0][3];
    float a21 = m[1][0];
    float a22 = m[1][1];
    float a23 = m[1][2];
    float a24 = m[1][3];
    float a31 = m[2][0];
    float a32 = m[2][1];
    float a33 = m[2][2];
    float a34 = m[2][3];
    float a41 = m[3][0];
    float a42 = m[3][1];
    float a43 = m[3][2];
    float a44 = m[3][3];
    #undef m
    
    float det = a11*a22*a33*a44 + a11*a23*a34*a42 + a11*a24*a32*a43 +
                a12*a21*a34*a43 + a12*a23*a31*a44 + a12*a24*a33*a41 +
                a13*a21*a32*a44 + a13*a22*a34*a41 + a13*a24*a31*a42 +
                a14*a21*a33*a42 + a14*a22*a31*a43 + a14*a23*a32*a41 -
                a11*a22*a33*a43 - a11*a23*a32*a44 - a11*a24*a33*a42 -
                a12*a21*a34*a44 - a12*a23*a34*a41 - a11*a24*a31*a43 -
                a13*a21*a32*a42 - a13*a22*a31*a44 - a13*a24*a32*a41 -
                a14*a21*a33*a43 - a14*a22*a33*a41 - a14*a23*a31*a42;
    if (abs(det) <= 0.0001f) {
        m_mat4_create_identity(dest);
        return;
    }
    
    dest->rows[0][0] = a22*a33*a44 + a23*a34*a42 + a24*a32*a43 - a22*a34*a43 - a23*a32*a44 - a24*a33*a42;
    dest->rows[0][1] = a12*a34*a43 + a13*a32*a44 + a14*a33*a42 - a12*a33*a44 - a13*a34*a42 - a14*a32*a43;
    dest->rows[0][2] = a12*a23*a44 + a13*a24*a42 + a14*a22*a43 - a12*a24*a43 - a13*a22*a44 - a14*a23*a42;
    dest->rows[0][3] = a12*a24*a33 + a13*a22*a34 + a14*a23*a32 - a12*a23*a34 - a13*a24*a32 - a14*a22*a33;
    dest->rows[1][0] = a21*a34*a43 + a23*a31*a44 + a24*a33*a41 - a21*a33*a44 - a23*a34*a41 - a24*a31*a43;
    dest->rows[1][1] = a11*a33*a44 + a13*a34*a41 + a14*a31*a43 - a11*a34*a43 - a13*a31*a44 - a14*a33*a41;
    dest->rows[1][2] = a11*a24*a43 + a13*a21*a44 + a14*a23*a41 - a11*a23*a44 - a13*a24*a41 - a14*a21*a43;
    dest->rows[1][3] = a11*a23*a34 + a13*a24*a31 + a14*a21*a33 - a11*a24*a33 - a13*a21*a34 - a14*a23*a31;
    dest->rows[2][0] = a21*a32*a44 + a22*a34*a41 + a24*a31*a42 - a21*a34*a42 - a22*a31*a44 - a24*a32*a41;
    
    dest->rows[2][1] = a11*a34*a42 + a12*a31*a44 + a14*a32*a41 - a11*a32*a44 - a12*a34*a41 - a14*a31*a42;
    dest->rows[2][2] = a11*a22*a44 + a12*a24*a41 + a14*a21*a42 - a11*a24*a42 - a12*a21*a44 - a14*a22*a41;
    dest->rows[2][3] = a11*a24*a32 + a12*a21*a34 + a14*a22*a31 - a11*a22*a34 - a12*a24*a31 - a14*a21*a32;
    dest->rows[3][0] = a21*a33*a42 + a22*a31*a43 + a23*a32*a41 - a21*a32*a43 - a22*a33*a41 - a23*a31*a42;
    dest->rows[3][1] = a11*a32*a43 + a12*a33*a41 + a13*a31*a42 - a11*a33*a42 - a12*a31*a43 - a13*a32*a41;
    dest->rows[3][2] = a11*a23*a42 + a12*a21*a43 + a13*a22*a41 - a11*a22*a43 - a12*a23*a41 - a13*a21*a42;
    dest->rows[3][3] = a11*a22*a33 + a12*a23*a31 + a13*a21*a32 - a11*a23*a32 - a12*a21*a33 - a13*a22*a31;
    
    for (unsigned int i = 0; i < 4; i++)
        for (unsigned int j = 0; j < 4; j++)
            dest->rows[i][j] /= det;
    
    return;
}

void m_mat4_mul_vec3(m_vec3_t* restrict res, const m_mat4_t* restrict mat, const m_vec3_t* restrict p) {
    float v4[4];
    v4[0] = mat->rows[0][0]*p->x + mat->rows[0][1]*p->y + mat->rows[0][2]*p->z + mat->rows[0][3];
    v4[1] = mat->rows[1][0]*p->x + mat->rows[1][1]*p->y + mat->rows[1][2]*p->z + mat->rows[1][3];
    v4[2] = mat->rows[2][0]*p->x + mat->rows[2][1]*p->y + mat->rows[2][2]*p->z + mat->rows[2][3];
    v4[3] = mat->rows[3][0]*p->x + mat->rows[3][1]*p->y + mat->rows[3][2]*p->z + mat->rows[3][3];
    res->x = v4[0] / v4[3];
    res->y = v4[1] / v4[3];
    res->z = v4[2] / v4[3];
}

m_plane_t m_plane_create(m_vec3_t normal, float dist) {
    m_plane_t res;
    res.normal = normal;
    res.dist = dist;
    return res;
}

m_plane_t m_plane_create_4f(float nx, float ny, float nz, float d) {
    m_plane_t res;
    res.normal.x = nx;
    res.normal.y = ny;
    res.normal.z = nz;
    float len = m_vec3_length(&res.normal);
    res.normal.x /= len;
    res.normal.y /= len;
    res.normal.z /= len;
    res.dist = d / len;
    return res;
}

m_plane_t m_plane_create_3v(m_vec3_t a, m_vec3_t b, m_vec3_t c) {
    m_vec3_t e0, e1;
    m_vec3_sub(&e0, &b, &a);
    m_vec3_sub(&e1, &c, &a);
    
    m_plane_t res;
    m_vec3_cross(&res.normal, &e0, &e1);
    m_vec3_normalize(&res.normal, &res.normal);
    res.dist = m_vec3_dot(&res.normal, &a);
    return res;
}

float m_plane_dist_to_point(const m_plane_t* plane, const m_vec3_t* p) {
    return m_vec3_dot(&plane->normal, p) + plane->dist;
}

m_frustum_t m_frustum_create_view(const m_mat4_t* mat, float left, float right, float bottom, float top, float near, float far) {
    float leftf = left * far / near;
    float rightf = right * far / near;
    float bottomf = bottom * far / near;
    float topf = top * far / near;
    
    m_frustum_t res;
    m_vec3_t t0 = m_vec3_create(left, bottom, -near);
    m_mat4_mul_vec3(&res.corners[0], mat, &t0);
    
    t0 = m_vec3_create(right, bottom, -near);
    m_mat4_mul_vec3(&res.corners[1], mat, &t0);
    
    t0 = m_vec3_create(right, top, -near);
    m_mat4_mul_vec3(&res.corners[2], mat, &t0);
    
    t0 = m_vec3_create(left, top, -near);
    m_mat4_mul_vec3(&res.corners[3], mat, &t0);
    
    t0 = m_vec3_create(leftf, bottomf, -far);
    m_mat4_mul_vec3(&res.corners[4], mat, &t0);
    
    t0 = m_vec3_create(rightf, bottomf, -far);
    m_mat4_mul_vec3(&res.corners[5], mat, &t0);
    
    t0 = m_vec3_create(rightf, topf, -far);
    m_mat4_mul_vec3(&res.corners[6], mat, &t0);
    
    t0 = m_vec3_create(leftf, topf, -far);
    m_mat4_mul_vec3(&res.corners[7], mat, &t0);
    
    t0 = m_vec3_create(0, 0, 0);
    m_mat4_mul_vec3(&res.origin, mat, &t0);
    
    res.planes[0] = m_plane_create_3v(res.origin, res.corners[3], res.corners[0]);
    res.planes[1] = m_plane_create_3v(res.origin, res.corners[1], res.corners[2]);
    res.planes[2] = m_plane_create_3v(res.origin, res.corners[0], res.corners[1]);
    res.planes[3] = m_plane_create_3v(res.origin, res.corners[2], res.corners[3]);
    res.planes[4] = m_plane_create_3v(res.corners[0], res.corners[1], res.corners[2]);
    res.planes[5] = m_plane_create_3v(res.corners[5], res.corners[4], res.corners[7]);
    
    return res;
}

m_frustum_t m_frustum_create_perspective(const m_mat4_t* mat, float fov, float aspect, float near, float far) {
    float ymax = near * tanf(fov/2.0 / 180 * pi);
    float xmax = ymax * aspect;
    return m_frustum_create_view(mat, -xmax, xmax, -ymax, ymax, near, far);
}

m_frustum_t m_frustum_create_mats(const m_mat4_t* view, const m_mat4_t* proj) {
    m_mat4_t m;
    m_mat4_mul(&m, proj, view);
    
    m_frustum_t res;
    #define d m.rows
    res.planes[0] = m_plane_create_4f(-(d[3][0]+d[0][0]), -(d[3][1]+d[0][1]), -(d[3][2]+d[0][2]), -(d[3][3]+d[0][3]));
    res.planes[1] = m_plane_create_4f(-(d[3][0]-d[0][0]), -(d[3][1]-d[0][1]), -(d[3][2]-d[0][2]), -(d[3][3]-d[0][3]));
    res.planes[2] = m_plane_create_4f(-(d[3][0]+d[1][0]), -(d[3][1]+d[1][1]), -(d[3][2]+d[1][2]), -(d[3][3]+d[1][3]));
    res.planes[3] = m_plane_create_4f(-(d[3][0]-d[1][0]), -(d[3][1]-d[1][1]), -(d[3][2]-d[1][2]), -(d[3][3]-d[1][3]));
    res.planes[4] = m_plane_create_4f(-(d[3][0]+d[2][0]), -(d[3][1]+d[2][1]), -(d[3][2]+d[2][2]), -(d[3][3]+d[2][3]));
    res.planes[5] = m_plane_create_4f(-(d[3][0]-d[2][0]), -(d[3][1]-d[2][1]), -(d[3][2]-d[2][2]), -(d[3][3]-d[2][3]));
    #undef d
    
    m_mat4_t iv;
    m_mat4_inverse(&iv, view);
    m_vec3_t t0 = m_vec3_create(0, 0, 0);
    m_mat4_mul_vec3(&res.origin, &iv, &t0);
    
    m_mat4_t im;
    m_mat4_inverse(&im, &m);
    
    t0 = m_vec3_create(-1, -1, -1);
    m_mat4_mul_vec3(&res.corners[0], &im, &t0);
    
    t0 = m_vec3_create(1, -1, -1);
    m_mat4_mul_vec3(&res.corners[1], &im, &t0);
    
    t0 = m_vec3_create(1, 1, -1);
    m_mat4_mul_vec3(&res.corners[2], &im, &t0);
    
    t0 = m_vec3_create(-1, 1, -1);
    m_mat4_mul_vec3(&res.corners[3], &im, &t0);
    
    t0 = m_vec3_create(-1, -1, 1);
    m_mat4_mul_vec3(&res.corners[4], &im, &t0);
    
    t0 = m_vec3_create(1, -1, 1);
    m_mat4_mul_vec3(&res.corners[5], &im, &t0);
    
    t0 = m_vec3_create(1, 1, 1);
    m_mat4_mul_vec3(&res.corners[6], &im, &t0);
    
    t0 = m_vec3_create(-1, 1, 1);
    m_mat4_mul_vec3(&res.corners[7], &im, &t0);
    
    return res;
}

bool m_frustum_cull_aabb(const m_frustum_t* restrict frustum, const m_aabb_t* restrict aabb) {
    for (unsigned int i = 0; i < 6; i++) {
        const m_vec3_t* n = &frustum->planes[i].normal;
        
        m_vec3_t pos = aabb->min;
        if (n->x <= 0) pos.x = aabb->max.x;
        if (n->y <= 0) pos.y = aabb->max.y;
        if (n->z <= 0) pos.z = aabb->max.z;
        
        if (m_plane_dist_to_point(&frustum->planes[i], &pos) > 0)
            return true;
    }
    
    return false;
}

bool m_frustum_cull_sphere(const m_frustum_t* restrict frustum, const m_sphere_t* restrict sphere) {
    if (m_plane_dist_to_point(&frustum->planes[0], &sphere->pos) > sphere->radius)
        return true;
    if (m_plane_dist_to_point(&frustum->planes[1], &sphere->pos) > sphere->radius)
        return true;
    if (m_plane_dist_to_point(&frustum->planes[2], &sphere->pos) > sphere->radius)
        return true;
    if (m_plane_dist_to_point(&frustum->planes[3], &sphere->pos) > sphere->radius)
        return true;
    if (m_plane_dist_to_point(&frustum->planes[4], &sphere->pos) > sphere->radius)
        return true;
    if (m_plane_dist_to_point(&frustum->planes[5], &sphere->pos) > sphere->radius)
        return true;
    return false;
}

m_aabb_t m_aabb_create(const m_vec3_t* min, const m_vec3_t* max) {
    m_aabb_t res;
    res.min = *min;
    res.max = *max;
    return res;
}

m_aabb_t m_aabb_create_empty() {
    m_aabb_t res;
    res.min = m_vec3_create(INFINITY, INFINITY, INFINITY);
    res.max = m_vec3_create(-INFINITY, -INFINITY, -INFINITY);
    return res;
}

void m_aabb_extend_vec3(m_aabb_t* restrict aabb, const m_vec3_t* restrict other) {
    aabb->min.x = fminf(aabb->min.x, other->x);
    aabb->min.y = fminf(aabb->min.y, other->y);
    aabb->min.z = fminf(aabb->min.z, other->z);
    aabb->max.x = fmaxf(aabb->max.x, other->x);
    aabb->max.y = fmaxf(aabb->max.y, other->y);
    aabb->max.z = fmaxf(aabb->max.z, other->z);
}

void m_aabb_extent_aabb(m_aabb_t* restrict aabb, const m_aabb_t* restrict other) {
    m_aabb_extend_vec3(aabb, &other->min);
    m_aabb_extend_vec3(aabb, &other->max);
}

void m_aabb_transform(m_aabb_t* restrict dest, const m_aabb_t* restrict aabb, const m_mat4_t* mat) {
    for (unsigned int i = 0; i < 3; i++) {
        ((float*)&dest->min)[i] = mat->rows[i][3];
        ((float*)&dest->max)[i] = mat->rows[i][3];
        for (unsigned int j = 0; j < 3; j++) {
            float x = ((const float*)&aabb->min)[j] * mat->rows[i][j];
            float y = ((const float*)&aabb->max)[j] * mat->rows[i][j];
            ((float*)&dest->min)[i] += fminf(x, y);
            ((float*)&dest->max)[i] += fmaxf(x, y);
        }
    }
}

m_quat_t m_quat_create(float x, float y, float z, float w) {
    m_quat_t res;
    res.x = x;
    res.y = y;
    res.z = z;
    res.w = w;
    return res;
}

m_quat_t m_quat_create_axis_angle(const m_vec3_t* axis, float angle) {
    m_quat_t res;
    m_quat_set_angle(&res, angle);
    m_quat_set_x_axis(&res, axis->x);
    m_quat_set_y_axis(&res, axis->y);
    m_quat_set_z_axis(&res, axis->z);
    return res;
}

m_quat_t m_quat_create_euler_angles(float x, float y, float z) {
    m_quat_t res;
    float cx = cos(x / 2);
    float cy = cos(y / 2);
    float cz = cos(z / 2);
    float sx = sin(x / 2);
    float sy = sin(y / 2);
    float sz = sin(z / 2);
    res.x = sx * cy * cz - cx * sy * sz;
    res.y = cx * sy * cz + sx * cy * sz;
    res.z = cx * cy * sz - sx * sy * cz;
    res.w = cx * cy * cz + sx * sy * sz;
    return res;
}

float m_quat_get_angle(const m_quat_t* quat) {
    return acos(quat->w) * 2;
}

float m_quat_get_x_axis(const m_quat_t* quat) {
    return quat->x / sin(acos(quat->w));
}

float m_quat_get_y_axis(const m_quat_t* quat) {
    return quat->y / sin(acos(quat->w));
}

float m_quat_get_z_axis(const m_quat_t* quat) {
    return quat->z / sin(acos(quat->w));
}

void m_quat_set_angle(m_quat_t* quat, float angle) {
    quat->w = cos(angle / 2);
}

void m_quat_set_x_axis(m_quat_t* quat, float xaxis) {
    quat->x = xaxis * sin(acos(quat->w));
}

void m_quat_set_y_axis(m_quat_t* quat, float yaxis) {
    quat->y = yaxis * sin(acos(quat->w));
}

void m_quat_set_z_axis(m_quat_t* quat, float zaxis) {
    quat->z = zaxis * sin(acos(quat->w));
}

void m_quat_get_eulur_angles(const m_quat_t* restrict quat, m_vec3_t* restrict angles) {
    float x = quat->x;
    float y = quat->y;
    float z = quat->z;
    float w = quat->w;
    angles->x = atan2(2.0f * (w*x + y*z), 1.0f-2.0f*(x*x + y*y));
    angles->y = asin(2.0f * (w*y - z*x));
    angles->z = atan2(2.0f * (w*z + x*y), 1.0f-2.0f*(y*y + z*z));
}
