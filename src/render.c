#include "render.h"
#include "debug.h"
#include "scene.h"
#include "utils.h"
#include "utils.h"
#include "rapi.h"

#include <assert.h>

#define GBUFFER_SUBPASS_DRAW_MESHES 0
#define GBUFFER_STENCIL_LIGHTING (1<<0)

#define MAX_DEBUG_LINES_PER_DRAW 16384
#define MAX_DEBUG_TRIS_PER_DRAW 16384

typedef struct image_tracker_t {
    r_image_layout_t layout;
    r_image_access_t access;
} image_tracker_t;

typedef struct framebuffer_image_t {
    image_tracker_t tracker;
    r_image_t* image;
} framebuffer_image_t;

typedef struct posteffect_t {
    uint32_t src_image_count, dest_image_count;
    framebuffer_image_t* src_images[16];
    framebuffer_image_t* dest_images[7];
    
    framebuffer_image_t* stencil_image;
    
    uint32_t src_buffer_count;
    r_buffer_t* src_buffers[12];
    
    r_desc_set_layout_t* desc_set_layout;
    r_desc_set_t* desc_set;
    r_pass_t* pass;
    r_framebuffer_t* framebuffer;
    r_pipeline_t* pipeline;
} posteffect_t;

typedef struct timing_t {
    const char* name; //Statically allocated
    struct timing_t* next;
    r_timestamp_t* begin;
    r_timestamp_t* end;
    struct timing_t* children;
    uint64_t update_count;
    uint64_t nanoseconds[60];
} timing_t;

static SDL_Window* window;
static framebuffer_image_t gbuffer_albedo_image;
static framebuffer_image_t gbuffer_normal_image;
static framebuffer_image_t gbuffer_material_image;
static framebuffer_image_t gbuffer_depth_stencil_image;
static framebuffer_image_t present_image;
static r_pass_t* gbuffer_pass;
static r_framebuffer_t* gbuffer_framebuffer;
static r_cmd_buf_t* cmd_buf;
static bin_t* shader_bin;
static r_shader_t* gbuffer_vertex_shader;
static r_shader_t* posteffect_vertex_shader;
static r_sampler_t* posteffect_sampler;
static r_sampler_t* nearest_sampler;
static r_desc_set_layout_t* skybox_desc_set_layout;
static r_pipeline_t* skybox_pipeline;
static const r_cubemap_t* skybox;
static r_desc_set_t* skybox_desc_set;
static r_pass_t* text_pass;
static r_framebuffer_t* text_framebuffer;
static r_sampler_t* linear_sampler;
static r_buffer_t* lighting_data_buffer;
static posteffect_t lighting_posteffect;
static posteffect_t nonlighting_posteffect;
static r_buffer_t* debug_line_buffer;
static r_pipeline_t* debug_line_pipeline;
static r_buffer_t* debug_tri_buffer;
static r_pipeline_t* debug_tri_pipeline;
static uint32_t cur_width = 0xFFFFFFFF;
static uint32_t cur_height = 0xFFFFFFFF;
static timing_t frame_timing;
static timing_t gbuffer_timing;
static timing_t lighting_timing;
static bool write_timing_queries;
extern size_t d_line_count;
extern d_line_t* d_lines;
extern size_t d_tri_count;
extern d_tri_t* d_tris;

struct r_material_t {
    r_shader_t* vertex_shader;
    r_shader_t* fragment_shader;
    r_buffer_t* uniform_buffer;
    r_desc_set_layout_t* desc_set_layout;
    r_desc_set_t* desc_set;
};

struct r_mesh_t {
    uint32_t index_count;
    r_buffer_t* vertex_buffer;
    r_buffer_t* index_buffer;
    r_material_t* material;
    r_pipeline_t* gbuffer_pipeline;
};

void d_line_clear();
void d_tri_clear();

void r_builtin_font_init(bin_t* shader_bin, r_pass_t* pass, uint32_t subpass_index);
void r_builtin_font_deinit();
void r_builtin_font_render(r_cmd_buf_t* cmd_buf, uint32_t view_width, uint32_t view_height, const char* text, uint32_t left, uint32_t bottom, uint32_t height, m_vec3_t color);

static void timing_create(timing_t* timing, const char* name) {
    timing->name = name;
    timing->next = NULL;
    timing->begin = r_timestamp_create();
    timing->end = r_timestamp_create();
    timing->children = NULL;
    timing->update_count = 0;
    memset(timing->nanoseconds, 0, sizeof(timing->nanoseconds));
}

static void timing_del(timing_t* timing) {
    r_timestamp_del(timing->begin);
    r_timestamp_del(timing->end);
}

static void timing_begin(r_cmd_buf_t* cmd_buf, timing_t* timing) {
    if (write_timing_queries) r_cmd_buf_set_timestamp(cmd_buf, timing->begin);
}

static void timing_end(r_cmd_buf_t* cmd_buf, timing_t* timing) {
    if (write_timing_queries) r_cmd_buf_set_timestamp(cmd_buf, timing->end);
}

static void _update_timing(timing_t* timing) {
    timing->nanoseconds[(timing->update_count++)%60] = r_timestamp_get(timing->end) - r_timestamp_get(timing->begin);
    timing_t* cur = timing->children;
    while (cur) {
        _update_timing(cur);
        cur = cur->next;
    }
}

static void _draw_timing(timing_t* timing, size_t indent, uint32_t* bottom) {
    char str[1024];
    memset(str, ' ', indent*4);
    double ms = 0.0;
    for (uint32_t i = 0; i < 60; i++)
        ms = fmax(timing->nanoseconds[i]/1000000.0, ms);
    snprintf(str+indent*4, sizeof(str)-indent*4, "%s - %f ms", timing->name, ms);
    
    r_builtin_font_render(cmd_buf, cur_width, cur_height, str, 0, *bottom, 16, m_vec3_create(1, 1, 1));
    *bottom -= 16;
    
    timing_t* cur = timing->children;
    while (cur) {
        _draw_timing(cur, indent+1, bottom);
        cur = cur->next;
    }
}

static void draw_timings() {
    uint32_t bottom = cur_height - 16;
    _draw_timing(&frame_timing, 0, &bottom);
}

static void timing_update() {
    write_timing_queries = false;
    
    if (r_timestamp_available(frame_timing.end)) {
        _update_timing(&frame_timing);
        write_timing_queries = true;
    }
}

static void image_barrier(r_cmd_buf_t* cmd_buf, r_image_t* image, image_tracker_t* tracker, r_image_layout_t layout, r_image_access_t access) {
    if (layout!=tracker->layout || access!=tracker->access) {
        r_image_access_t accessa[2] = {tracker->access=access, access};
        r_image_layout_t layouta[2] = {tracker->layout=layout, layout};
        const r_image_access_t* accessp = accessa;
        const r_image_layout_t* layoutp = layouta;
        r_cmd_buf_image_barriers(cmd_buf, 1, &image, &accessp, &layoutp);
    }
}

static void posteffect_create_begin(posteffect_t* res) {
    res->src_image_count = 0;
    res->dest_image_count = 0;
    res->src_buffer_count = 0;
    res->stencil_image = NULL;
}

static void posteffect_create_end(posteffect_t* res, const char* fragment_shader, bool stencil_test,
                                  uint32_t stencil_flags, bool invert_stencil_test_result) {
    r_desc_set_layout_binding_t bindings[16];
    for (uint32_t i = 0; i < res->src_image_count; i++) {
        bindings[i].binding = i;
        bindings[i].type = R_DESC_IMAGE_AND_SAMPLER;
    }
    for (uint32_t i = 0; i < res->src_buffer_count; i++) {
        bindings[i+res->src_image_count].binding = i + res->src_image_count;
        bindings[i+res->src_image_count].type = R_DESC_UNIFORM_BUFFER;
    }
    res->desc_set_layout = r_desc_set_layout_create(res->src_image_count+res->src_buffer_count, bindings);
    
    r_pass_t* pass = r_pass_create();
    if (res->stencil_image)
        r_pass_add_attachment(pass, R_PASS_ATTACHMENT_DEPTHSTENCIL);
    for (uint32_t i = 0; i < res->dest_image_count; i++) r_pass_add_attachment(pass, 0);
    r_subpass_t subpass = {{-1, -1, -1, -1, -1, -1, -1, -1}, 0};
    for (uint32_t i = 0; i < res->dest_image_count; i++) subpass.color_indices[i] = i+1;
    r_pass_add_subpass(pass, &subpass);
    r_pass_finalize(pass);
    res->pass = pass;
    
    r_stencil_state_face_t stencil_state_faces[2];
    for (uint32_t i = 0; i < 2; i++) {
        stencil_state_faces[i].stencil_fail_func = R_STENCIL_KEEP;
        stencil_state_faces[i].stencil_pass_depth_pass_func = R_STENCIL_KEEP;
        stencil_state_faces[i].stencil_pass_depth_fail_func = R_STENCIL_KEEP;
        stencil_state_faces[i].compare_func = invert_stencil_test_result ? R_COMPARE_EQUAL : R_COMPARE_NOT_EQUAL;
        stencil_state_faces[i].compare_mask = stencil_flags;
        stencil_state_faces[i].write_mask = 0xFFFFFFFF;
        stencil_state_faces[i].reference = 0xFFFFFFFF;
    }
    
    r_pipeline_t* pipeline = r_pipeline_create(R_PIPELINE_GRAPHICS);
    r_pipeline_push_constant_info(pipeline, 0, 0);
    r_pipeline_vertex_shader(pipeline, posteffect_vertex_shader);
    b_shader_t* shader = b_find_shader(shader_bin, fragment_shader);
    if (!shader) fatal("Failed to find %s in shaders.bin\n", fragment_shader);
    r_pipeline_fragment_shader(pipeline, b_get_r_shader(shader));
    r_pipeline_topology(pipeline, R_TOPOLOGY_TRIANGLE_STRIP);
    r_pipeline_cull(pipeline, R_CULL_NONE, R_WINDING_CCW);
    r_pipeline_depth_stencil(pipeline, R_PIPELINE_STENCIL_TEST, R_COMPARE_LESS, stencil_state_faces);
    r_pipeline_pass(pipeline, res->pass, 0);
    r_pipeline_desc_set_layout(pipeline, R_STAGE_FRAGMENT, res->desc_set_layout);
    r_pipeline_finalize(pipeline);
    res->pipeline = pipeline;
}

static void posteffect_del(posteffect_t* res) {
    r_pipeline_del(res->pipeline);
    r_pass_del(res->pass);
    r_desc_set_layout_del(res->desc_set_layout);
}

static void posteffect_on_create_images(posteffect_t* posteffect) {
    r_desc_t descs[16];
    for (uint32_t i = 0; i < posteffect->src_image_count; i++) {
        descs[i].image_and_sampler.image = posteffect->src_images[i]->image;
        descs[i].image_and_sampler.sampler = posteffect_sampler;
    }
    for (uint32_t i = 0; i < posteffect->src_buffer_count; i++)
        descs[i+posteffect->src_image_count].uniform_buffer = posteffect->src_buffers[i];
    posteffect->desc_set = r_desc_set_create(posteffect->desc_set_layout, descs);
    
    r_image_t* images[8];
    if (posteffect->stencil_image) images[0] = posteffect->stencil_image->image;
    for (uint32_t i = 0; i < posteffect->dest_image_count; i++)
        images[i+posteffect->stencil_image?1:0] = posteffect->dest_images[i]->image;
    posteffect->framebuffer = r_framebuffer_create(posteffect->pass, images);
}

static void posteffect_on_del_images(posteffect_t* posteffect) {
    r_framebuffer_del(posteffect->framebuffer);
    r_desc_set_del(posteffect->desc_set);
}

static void posteffect_do(r_cmd_buf_t* cmd_buf, const posteffect_t* posteffect) {
    for (uint32_t i = 0; i < posteffect->src_image_count; i++)
        image_barrier(cmd_buf, posteffect->src_images[i]->image, &posteffect->src_images[i]->tracker,
                      R_IMAGE_SHADER_SAMPLE_OPTIMAL, R_IMAGE_SHADER_READ);
    
    for (uint32_t i = 0; i < posteffect->dest_image_count; i++)
        image_barrier(cmd_buf, posteffect->dest_images[i]->image, &posteffect->src_images[i]->tracker,
                      R_IMAGE_COLOR_ATTACHMENT_OPTIMAL, R_IMAGE_COLOR_ATTACHMENT_WRITE);
    
    if (posteffect->stencil_image)
        image_barrier(cmd_buf, posteffect->stencil_image->image, &posteffect->stencil_image->tracker,
                      R_IMAGE_DEPTH_STENCIL_ATTACHMENT_OPTIMAL, R_IMAGE_DEPTH_STENCIL_ATTACHMENT_READ|R_IMAGE_DEPTH_STENCIL_ATTACHMENT_WRITE);
    
    r_clear_value_t clear_vals[8];
    memset(clear_vals, 0, sizeof(clear_vals));
    r_cmd_buf_pass_begin(cmd_buf, posteffect->pass, posteffect->framebuffer, clear_vals);
    r_cmd_buf_bind_pipeline(cmd_buf, posteffect->pipeline);
    r_cmd_buf_bind_desc_set(cmd_buf, R_STAGE_FRAGMENT, posteffect->desc_set);
    r_cmd_buf_draw(cmd_buf, 4, 1, 0, 0);
    r_cmd_buf_pass_end(cmd_buf);
}

static void framebuffer_image_create(framebuffer_image_t* image, uint32_t width, uint32_t height, r_image_format_t format) {
    image->tracker.layout = R_IMAGE_COLOR_ATTACHMENT_OPTIMAL;
    image->tracker.access = R_IMAGE_COLOR_ATTACHMENT_WRITE;
    
    r_image_create_info_t info;
    info.type = R_IMAGE_2D;
    info.format = format;
    info.width = width;
    info.height = height;
    info.mipmaps = 1;
    info.data2d = NULL;
    info.flags = R_IMAGE_SAMPLE | R_IMAGE_COLOR_ATTACHMENT;
    info.initial_access = image->tracker.access;
    info.initial_layout = image->tracker.layout;
    
    image->image = r_image_create(info);
}

static void framebuffer_image_del(framebuffer_image_t* image) {
    r_image_del(image->image);
}

static void framebuffer_image_barrier(r_cmd_buf_t* cmd_buf, framebuffer_image_t* image, r_image_layout_t layout, r_image_access_t access) {
    image_barrier(cmd_buf, image->image, &image->tracker, layout, access);
}

static void create_framebuffers(uint32_t width, uint32_t height) {
    r_init_swapchain(width, height, 0);
    
    framebuffer_image_create(&gbuffer_albedo_image, width, height, R_MAKE_FORMAT(R_IMAGE_U16, 3, 1, 0));
    framebuffer_image_create(&gbuffer_normal_image, width, height, R_MAKE_FORMAT(R_IMAGE_S16, 3, 1, 0));
    framebuffer_image_create(&gbuffer_material_image, width, height, R_MAKE_FORMAT(R_IMAGE_U8, 2, 1, 0));
    framebuffer_image_create(&gbuffer_depth_stencil_image, width, height, R_IMAGE_DEPTH_STENCIL_U24_NORM_U8);
    
    r_image_t* gbuffer_images[] = {gbuffer_albedo_image.image, gbuffer_material_image.image,
                                   gbuffer_normal_image.image, gbuffer_depth_stencil_image.image};
    gbuffer_framebuffer = r_framebuffer_create(gbuffer_pass, gbuffer_images);
    
    present_image.image = r_swapchain_get_color_image();
    present_image.tracker.access = R_IMAGE_MEMORY_READ;
    present_image.tracker.layout = R_IMAGE_PRESENT;
    
    text_framebuffer = r_framebuffer_create(text_pass, &present_image.image);
    
    cur_width = width;
    cur_height = height;
    
    posteffect_on_create_images(&lighting_posteffect);
    posteffect_on_create_images(&nonlighting_posteffect);
}

static void delete_framebuffers() {
    if (cur_width==0xFFFFFFFF || cur_height==0xFFFFFFFF) return;
    posteffect_on_del_images(&lighting_posteffect);
    posteffect_on_del_images(&nonlighting_posteffect);
    framebuffer_image_del(&gbuffer_albedo_image);
    framebuffer_image_del(&gbuffer_normal_image);
    framebuffer_image_del(&gbuffer_material_image);
    framebuffer_image_del(&gbuffer_depth_stencil_image);
    r_framebuffer_del(text_framebuffer);
    r_framebuffer_del(gbuffer_framebuffer);
}

void r_init(renderer_t renderer, SDL_Window* window_) {
    window = window_;
    cur_width = 0xFFFFFFFF;
    cur_height = 0xFFFFFFFF;
    
    r_init_api(R_API_OPENGL, window);
    
    gbuffer_pass = r_pass_create();
    r_pass_add_attachment(gbuffer_pass, 0);
    r_pass_add_attachment(gbuffer_pass, 0);
    r_pass_add_attachment(gbuffer_pass, 0);
    r_pass_add_attachment(gbuffer_pass, R_PASS_ATTACHMENT_CLEAR|R_PASS_ATTACHMENT_DEPTHSTENCIL);
    r_subpass_t gbuffer_subpass = {{0, 1, 2, -1, -1, -1, -1, -1}, 3};
    r_pass_add_subpass(gbuffer_pass, &gbuffer_subpass);
    r_pass_finalize(gbuffer_pass);
    
    cmd_buf = r_cmd_buf_create(R_CMD_BUF_GRAPHICS);
    
    shader_bin = b_read("shaders.bin");
    if (!shader_bin)
        fatal("Failed to read shaders.bin\n");
    
    b_shader_t* shader = b_find_shader(shader_bin, "shaders/gbuffer.vs.glsl");
    if (!shader) fatal("shaders/gbuffer.vs.glsl not found in shaders.bin\n");
    gbuffer_vertex_shader = b_get_r_shader(shader);
    
    shader = b_find_shader(shader_bin, "shaders/posteffect.vs.glsl");
    if (!shader) fatal("shaders/posteffect.vs.glsl not found in shaders.bin\n");
    posteffect_vertex_shader = b_get_r_shader(shader);
    
    posteffect_sampler = r_sampler_create();
    r_sampler_min_filter(posteffect_sampler, R_SAMPLER_NEAREST);
    r_sampler_mag_filter(posteffect_sampler, R_SAMPLER_NEAREST);
    r_sampler_mipmap_filter(posteffect_sampler, R_SAMPLER_NEAREST);
    {
        static const r_sampler_address_mode_t a[] = {R_SAMPLER_CLAMP_TO_EDGE, R_SAMPLER_CLAMP_TO_EDGE, R_SAMPLER_CLAMP_TO_EDGE};
        r_sampler_address_modes(posteffect_sampler, a);
    }
    r_sampler_anisotropy(posteffect_sampler, false, 1);
    r_sampler_min_max_lod(posteffect_sampler, 0.000000, 0.250000);
    r_sampler_finalize(posteffect_sampler);
    
    #define SAMPLER(sampler, mag_filter) do {\
        sampler = r_sampler_create();\
        r_sampler_min_filter(sampler, R_SAMPLER_LINEAR);\
        r_sampler_mag_filter(sampler, mag_filter);\
        r_sampler_mipmap_filter(sampler, R_SAMPLER_LINEAR);\
        r_sampler_address_mode_t addr_modes[] = {R_SAMPLER_REPEAT, R_SAMPLER_REPEAT, R_SAMPLER_REPEAT};\
        r_sampler_address_modes(sampler, addr_modes);\
        r_sampler_anisotropy(sampler, true, 16);\
        r_sampler_min_max_lod(sampler, 0, 999999);\
        r_sampler_finalize(sampler);\
    } while (0)
    
    SAMPLER(nearest_sampler, R_SAMPLER_NEAREST);
    SAMPLER(linear_sampler, R_SAMPLER_LINEAR);
    
    #undef SAMPLER
    
    r_desc_set_layout_binding_t skybox_binding = {.binding=0, .type=R_DESC_IMAGE_AND_SAMPLER};
    skybox_desc_set_layout = r_desc_set_layout_create(1, &skybox_binding);
    
    skybox = NULL;
    
    r_stencil_state_face_t stencil_face_states[2];
    for (size_t i = 0; i < 2; i++) {
        stencil_face_states[i].stencil_fail_func = R_STENCIL_KEEP;
        stencil_face_states[i].stencil_pass_depth_pass_func = R_STENCIL_REPLACE;
        stencil_face_states[i].stencil_pass_depth_fail_func = R_STENCIL_KEEP;
        stencil_face_states[i].compare_func = R_COMPARE_TRUE;
        stencil_face_states[i].compare_mask = 0xFFFFFFFF;
        stencil_face_states[i].write_mask = 0xFFFFFFFF;
        stencil_face_states[i].reference = 0;
    }
    
    skybox_pipeline = r_pipeline_create(R_PIPELINE_GRAPHICS);
    r_pipeline_push_constant_info(skybox_pipeline, 0, 64);
    shader = b_find_shader(shader_bin, "shaders/skybox.vs.glsl");
    if (!shader) fatal("shaders/skybox.vs.glsl not found in shaders.bin\n");
    r_pipeline_vertex_shader(skybox_pipeline, b_get_r_shader(shader));
    shader = b_find_shader(shader_bin, "shaders/skybox.fs.glsl");
    if (!shader) fatal("shaders/skybox.fs.glsl not found in shaders.bin\n");
    r_pipeline_fragment_shader(skybox_pipeline, b_get_r_shader(shader));
    r_pipeline_topology(skybox_pipeline, R_TOPOLOGY_TRIANGLES);
    r_pipeline_cull(skybox_pipeline, R_CULL_NONE, R_WINDING_CCW);
    r_pipeline_depth_stencil(skybox_pipeline, R_PIPELINE_DEPTH_TEST|R_PIPELINE_STENCIL_WRITE, R_COMPARE_LESS_EQUAL, stencil_face_states);
    r_pipeline_pass(skybox_pipeline, gbuffer_pass, GBUFFER_SUBPASS_DRAW_MESHES);
    r_pipeline_desc_set_layout(skybox_pipeline, R_STAGE_FRAGMENT, skybox_desc_set_layout);
    r_pipeline_finalize(skybox_pipeline);

    debug_line_buffer = r_buffer_create(MAX_DEBUG_LINES_PER_DRAW*24*2, NULL, R_BUFFER_VERTEX);
    debug_line_pipeline = r_pipeline_create(R_PIPELINE_GRAPHICS);
    r_pipeline_push_constant_info(debug_line_pipeline, 0, 64);
    shader = b_find_shader(shader_bin, "shaders/debug.vs.glsl");
    if (!shader) fatal("shaders/debug.vs.glsl not found in shaders.bin\n");
    r_pipeline_vertex_shader(debug_line_pipeline, b_get_r_shader(shader));
    shader = b_find_shader(shader_bin, "shaders/debug.fs.glsl");
    if (!shader) fatal("shaders/debug.fs.glsl not found in shaders.bin\n");
    r_pipeline_fragment_shader(debug_line_pipeline, b_get_r_shader(shader));
    r_pipeline_topology(debug_line_pipeline, R_TOPOLOGY_LINES);
    r_pipeline_vertex_attribute(debug_line_pipeline, 0, 24, 0, R_VERTEX_FORMAT_XYZ32F);
    r_pipeline_vertex_attribute(debug_line_pipeline, 1, 24, 12, R_VERTEX_FORMAT_XYZ32F);
    r_pipeline_cull(debug_line_pipeline, R_CULL_NONE, R_WINDING_CCW);
    r_pipeline_depth_stencil(debug_line_pipeline, R_PIPELINE_DEPTH_TEST|R_PIPELINE_STENCIL_WRITE, R_COMPARE_LESS_EQUAL, stencil_face_states);
    r_pipeline_pass(debug_line_pipeline, gbuffer_pass, GBUFFER_SUBPASS_DRAW_MESHES);
    r_pipeline_desc_set_layout(debug_line_pipeline, R_STAGE_FRAGMENT, skybox_desc_set_layout);
    r_pipeline_finalize(debug_line_pipeline);
    
    debug_tri_buffer = r_buffer_create(MAX_DEBUG_TRIS_PER_DRAW*24*3, NULL, R_BUFFER_VERTEX);
    debug_tri_pipeline = r_pipeline_create(R_PIPELINE_GRAPHICS);
    r_pipeline_push_constant_info(debug_tri_pipeline, 0, 64);
    shader = b_find_shader(shader_bin, "shaders/debug.vs.glsl");
    if (!shader) fatal("shaders/debug.vs.glsl not found in shaders.bin\n");
    r_pipeline_vertex_shader(debug_tri_pipeline, b_get_r_shader(shader));
    shader = b_find_shader(shader_bin, "shaders/debug.fs.glsl");
    if (!shader) fatal("shaders/debug.fs.glsl not found in shaders.bin\n");
    r_pipeline_fragment_shader(debug_tri_pipeline, b_get_r_shader(shader));
    r_pipeline_topology(debug_tri_pipeline, R_TOPOLOGY_TRIANGLES);
    r_pipeline_vertex_attribute(debug_tri_pipeline, 0, 24, 0, R_VERTEX_FORMAT_XYZ32F);
    r_pipeline_vertex_attribute(debug_tri_pipeline, 1, 24, 12, R_VERTEX_FORMAT_XYZ32F);
    r_pipeline_cull(debug_tri_pipeline, R_CULL_NONE, R_WINDING_CCW);
    r_pipeline_depth_stencil(debug_tri_pipeline, R_PIPELINE_DEPTH_TEST|R_PIPELINE_STENCIL_WRITE, R_COMPARE_LESS_EQUAL, stencil_face_states);
    r_pipeline_pass(debug_tri_pipeline, gbuffer_pass, GBUFFER_SUBPASS_DRAW_MESHES);
    r_pipeline_desc_set_layout(debug_tri_pipeline, R_STAGE_FRAGMENT, skybox_desc_set_layout);
    r_pipeline_finalize(debug_tri_pipeline);
    
    lighting_data_buffer = r_buffer_create(48, NULL, R_BUFFER_UNIFORM);
    
    posteffect_create_begin(&lighting_posteffect);
    lighting_posteffect.stencil_image = &gbuffer_depth_stencil_image;
    lighting_posteffect.src_images[lighting_posteffect.src_image_count++] = &gbuffer_albedo_image;
    lighting_posteffect.src_images[lighting_posteffect.src_image_count++] = &gbuffer_normal_image;
    lighting_posteffect.src_images[lighting_posteffect.src_image_count++] = &gbuffer_material_image;
    lighting_posteffect.src_images[lighting_posteffect.src_image_count++] = &gbuffer_depth_stencil_image;
    lighting_posteffect.src_buffers[lighting_posteffect.src_buffer_count++] = lighting_data_buffer;
    lighting_posteffect.dest_images[lighting_posteffect.dest_image_count++] = &present_image;
    posteffect_create_end(&lighting_posteffect, "shaders/lighting.fs.glsl", true, GBUFFER_STENCIL_LIGHTING, false);
    
    posteffect_create_begin(&nonlighting_posteffect);
    nonlighting_posteffect.stencil_image = &gbuffer_depth_stencil_image;
    nonlighting_posteffect.src_images[nonlighting_posteffect.src_image_count++] = &gbuffer_albedo_image;
    nonlighting_posteffect.dest_images[nonlighting_posteffect.dest_image_count++] = &present_image;
    posteffect_create_end(&nonlighting_posteffect, "shaders/nonlighting.fs.glsl", true, GBUFFER_STENCIL_LIGHTING, true);
    
    text_pass = r_pass_create();
    r_pass_add_attachment(text_pass, 0);
    r_subpass_t text_subpass = {{0, -1, -1, -1, -1, -1, -1, -1}, -1};
    r_pass_add_subpass(text_pass, &text_subpass);
    r_pass_finalize(text_pass);
    
    r_builtin_font_init(shader_bin, text_pass, 0);
    
    timing_create(&frame_timing, "Frame");
    timing_create(&gbuffer_timing, "G-Buffer");
    timing_create(&lighting_timing, "Lighting");
    frame_timing.children = &gbuffer_timing;
    gbuffer_timing.next = &lighting_timing;
    
    write_timing_queries = true;
}

void r_deinit() {
    free(d_lines);
    
    timing_del(&frame_timing);
    timing_del(&gbuffer_timing);
    timing_del(&lighting_timing);
    r_builtin_font_deinit();
    delete_framebuffers();
    r_pass_del(text_pass);
    posteffect_del(&lighting_posteffect);
    r_buffer_del(lighting_data_buffer);
    if (skybox) r_desc_set_del(skybox_desc_set);
    r_pipeline_del(debug_tri_pipeline);
    r_buffer_del(debug_tri_buffer);
    r_pipeline_del(debug_line_pipeline);
    r_buffer_del(debug_line_buffer);
    r_pipeline_del(skybox_pipeline);
    r_desc_set_layout_del(skybox_desc_set_layout);
    r_sampler_del(linear_sampler);
    r_sampler_del(nearest_sampler);
    r_sampler_del(posteffect_sampler);
    r_cmd_buf_del(cmd_buf);
    r_pass_del(gbuffer_pass);
    r_deinit_api();
}

r_texture_t* r_texture_create(const b_texture_t* texture) {
    r_image_create_info_t info;
    info.type = R_IMAGE_2D;
    info.width = texture->width;
    info.height = texture->height;
    info.mipmaps = texture->mipmaps;
    info.data2d = (const void**)texture->data;
    info.flags = R_IMAGE_SAMPLE;
    info.initial_access = R_IMAGE_SHADER_READ;
    info.initial_layout = R_IMAGE_SHADER_SAMPLE_OPTIMAL;
    
    switch (texture->format) {
    case B_TEX_FMT_RGBA8_UNORM: {info.format = R_MAKE_FORMAT(R_IMAGE_U8, 4, 1, 0); break;}
    case B_TEX_FMT_RGB8_UNORM: {info.format = R_MAKE_FORMAT(R_IMAGE_U8, 3, 1, 0); break;}
    case B_TEX_FMT_SRGBA8_UNORM: {info.format = R_MAKE_FORMAT(R_IMAGE_U8, 4, 1, 1); break;}
    case B_TEX_FMT_SRGB8_UNORM: {info.format = R_MAKE_FORMAT(R_IMAGE_U8, 3, 1, 1); break;}
    default: assert(false);
    }
    
    return (r_texture_t*)r_image_create(info);
}

void r_texture_del(r_texture_t* texture) {
    r_image_del((r_image_t*)texture);
}

r_cubemap_t* r_cubemap_create(const b_cubemap_t* cubemap) {
    r_image_create_info_t info;
    info.type = R_IMAGE_CUBE_MAP;
    info.format = R_MAKE_FORMAT(R_IMAGE_U8, 3, 1, 1);
    info.width = cubemap->width;
    info.height = cubemap->height;
    info.mipmaps = 1;
    for (uint32_t i = 0; i < 6; i++)
        info.data_cube[i] = (const void**)&cubemap->data[i];
    info.flags = R_IMAGE_SAMPLE;
    info.initial_access = R_IMAGE_SHADER_READ;
    info.initial_layout = R_IMAGE_SHADER_SAMPLE_OPTIMAL;
    return (r_cubemap_t*)r_image_create(info);
}

void r_cubemap_del(r_cubemap_t* cubemap) {
    r_image_del((r_image_t*)cubemap);
}

r_material_t* r_material_create(const b_material_t* material) {
    r_material_t* mat = malloc(sizeof(r_material_t));
    mat->vertex_shader = gbuffer_vertex_shader;
    mat->fragment_shader = b_get_r_shader(material->fragment);
    if (!material->frag_data_size)
    mat->uniform_buffer = r_buffer_create(1, NULL, R_BUFFER_UNIFORM);
    else
        mat->uniform_buffer = r_buffer_create(material->frag_data_size, material->frag_data, R_BUFFER_UNIFORM);
    
    uint32_t binding_count = 1;
    for (uint32_t i = 0; i < material->tex_slot_count; i++)
        binding_count += material->tex_slots[i] ? 1 : 0;
    
    r_desc_set_layout_binding_t* bindings = malloc(binding_count*sizeof(r_desc_set_layout_binding_t));
    r_desc_t* descs = malloc(binding_count*sizeof(r_desc_t));
    uint32_t binding_index = 0;
    descs[binding_index].uniform_buffer = mat->uniform_buffer;
    bindings[binding_index].type = R_DESC_UNIFORM_BUFFER;
    bindings[binding_index].binding = binding_index;
    binding_index++;
    for (uint32_t i = 0; i < material->tex_slot_count; i++) {
        if (!material->tex_slots[i]) continue;
        r_texture_t* tex = b_get_r_texture(material->tex_slots[i]);
        descs[binding_index].image_and_sampler.image = (r_image_t*)tex;
        descs[binding_index].image_and_sampler.sampler = material->tex_filters[i] == B_FILTER_LINEAR ?
                                                         linear_sampler : nearest_sampler;
        bindings[binding_index].type = R_DESC_IMAGE_AND_SAMPLER;
        bindings[binding_index].binding = i + 1;
        binding_index++;
    }
    
    mat->desc_set_layout = r_desc_set_layout_create(binding_count, bindings);
    mat->desc_set = r_desc_set_create(mat->desc_set_layout, descs);
    
    free(bindings);
    free(descs);
    
    return mat;
}

void r_material_del(r_material_t* material) {
    r_desc_set_del(material->desc_set);
    r_desc_set_layout_del(material->desc_set_layout);
    r_buffer_del(material->uniform_buffer);
    free(material);
}

r_mesh_t* r_mesh_create(const b_mesh_t* mesh) {
    r_mesh_t* res = malloc(sizeof(r_mesh_t));
    
    res->index_count = mesh->index_count;
    
    if (!mesh->vertex_count) {
        res->vertex_buffer = r_buffer_create(1, NULL, R_BUFFER_VERTEX);
        res->index_buffer = r_buffer_create(1, NULL, R_BUFFER_INDEX);
    } else {
        float* vertex_data = malloc(mesh->vertex_count*44);
        for (uint32_t i = 0; i < mesh->vertex_count; i++) {
            vertex_data[i*11] = mesh->positions[i*3];
            vertex_data[i*11+1] = mesh->positions[i*3+1];
            vertex_data[i*11+2] = mesh->positions[i*3+2];
            vertex_data[i*11+3] = mesh->normals[i*3];
            vertex_data[i*11+4] = mesh->normals[i*3+1];
            vertex_data[i*11+5] = mesh->normals[i*3+2];
            vertex_data[i*11+6] = mesh->tangents[i*3];
            vertex_data[i*11+7] = mesh->tangents[i*3+1];
            vertex_data[i*11+8] = mesh->tangents[i*3+2];
            vertex_data[i*11+9] = mesh->uvs[i*2];
            vertex_data[i*11+10] = mesh->uvs[i*2+1];
        }
        res->vertex_buffer = r_buffer_create(mesh->vertex_count*44, vertex_data, R_BUFFER_VERTEX);
        res->index_buffer = r_buffer_create(mesh->index_count*4, mesh->indices, R_BUFFER_INDEX);
        free(vertex_data);
    }
    
    res->material = b_get_r_material(mesh->material);
    
    r_stencil_state_face_t stencil_state_faces[2];
    for (uint32_t i = 0; i < 2; i++) {
        stencil_state_faces[i].stencil_fail_func = R_STENCIL_REPLACE;
        stencil_state_faces[i].stencil_pass_depth_pass_func = R_STENCIL_REPLACE;
        stencil_state_faces[i].stencil_pass_depth_fail_func = R_STENCIL_KEEP;
        stencil_state_faces[i].compare_func = R_COMPARE_TRUE;
        stencil_state_faces[i].compare_mask = 0xFFFFFFFF;
        stencil_state_faces[i].write_mask = 0xFFFFFFFF;
        stencil_state_faces[i].reference = GBUFFER_STENCIL_LIGHTING;
    }
    
    res->gbuffer_pipeline = r_pipeline_create(R_PIPELINE_GRAPHICS);
    r_pipeline_push_constant_info(res->gbuffer_pipeline, 0, 128);
    r_pipeline_vertex_shader(res->gbuffer_pipeline, res->material->vertex_shader);
    r_pipeline_fragment_shader(res->gbuffer_pipeline, res->material->fragment_shader);
    r_pipeline_vertex_attribute(res->gbuffer_pipeline, 0, 44, 0, R_VERTEX_FORMAT_XYZ32F);
    r_pipeline_vertex_attribute(res->gbuffer_pipeline, 1, 44, 12, R_VERTEX_FORMAT_XYZ32F);
    r_pipeline_vertex_attribute(res->gbuffer_pipeline, 2, 44, 24, R_VERTEX_FORMAT_XYZ32F);
    r_pipeline_vertex_attribute(res->gbuffer_pipeline, 3, 44, 36, R_VERTEX_FORMAT_XY32F);
    r_pipeline_topology(res->gbuffer_pipeline, R_TOPOLOGY_TRIANGLES);
    r_pipeline_cull(res->gbuffer_pipeline, R_CULL_NONE, R_WINDING_CCW); //TODO: Add an option for this?
    r_pipeline_depth_stencil(res->gbuffer_pipeline, R_PIPELINE_DEPTH_TEST|R_PIPELINE_DEPTH_WRITE|R_PIPELINE_STENCIL_WRITE,
                             R_COMPARE_LESS, stencil_state_faces);
    r_pipeline_pass(res->gbuffer_pipeline, gbuffer_pass, GBUFFER_SUBPASS_DRAW_MESHES);
    r_pipeline_desc_set_layout(res->gbuffer_pipeline, R_STAGE_FRAGMENT, res->material->desc_set_layout);
    r_pipeline_finalize(res->gbuffer_pipeline);
    
    return res;
}

void r_mesh_del(r_mesh_t* mesh) {
    r_pipeline_del(mesh->gbuffer_pipeline);
    r_buffer_del(mesh->index_buffer);
    r_buffer_del(mesh->vertex_buffer);
    free(mesh);
}

typedef struct state_t {
    const r_scene_t* scene;
    m_mat4_t view;
    m_mat4_t proj;
    m_mat4_t vp;
    m_mat4_t ivp;
    m_vec3_t cam_pos;
    m_vec3_t sun_dir;
    m_vec3_t sun_color;
} state_t;

static void render_debug_lines(state_t* state) {
    size_t lines_drawn = 0;
    float* buf_data = malloc(MAX_DEBUG_LINES_PER_DRAW*24*2);
    
    r_cmd_buf_bind_pipeline(cmd_buf, debug_line_pipeline);
    r_buffer_t* buffers[] = {debug_line_buffer, debug_line_buffer};
    uint32_t offsets[] = {0, 0};
    r_cmd_buf_bind_vertex_buffers(cmd_buf, 2, buffers, offsets);
    r_cmd_buf_set_push_constants(cmd_buf, 0, 64, &state->vp);
    
    while (lines_drawn != d_line_count) {
        size_t line_count = d_line_count - lines_drawn;
        if (line_count > MAX_DEBUG_LINES_PER_DRAW) line_count = MAX_DEBUG_LINES_PER_DRAW;
        
        for (size_t i = 0; i < line_count; i++) {
            d_line_t line = d_lines[lines_drawn+i];
            buf_data[i*12] = line.a.x;
            buf_data[i*12+1] = line.a.y;
            buf_data[i*12+2] = line.a.z;
            buf_data[i*12+3] = line.color[0];
            buf_data[i*12+4] = line.color[1];
            buf_data[i*12+5] = line.color[2];
            buf_data[i*12+6] = line.b.x;
            buf_data[i*12+7] = line.b.y;
            buf_data[i*12+8] = line.b.z;
            buf_data[i*12+9] = line.color[0];
            buf_data[i*12+10] = line.color[1];
            buf_data[i*12+11] = line.color[2];
        }
        
        r_cmd_buf_set_buffer_data(cmd_buf, debug_line_buffer, 0, line_count*24*2, buf_data);
        r_cmd_buf_draw(cmd_buf, line_count*2, 1, 0, 0);
        
        lines_drawn += line_count;
    }
    
    d_line_clear();
    
    free(buf_data);
}

static void render_debug_tris(state_t* state) {
    size_t tris_drawn = 0;
    float* buf_data = malloc(MAX_DEBUG_TRIS_PER_DRAW*24*3);
    
    r_cmd_buf_bind_pipeline(cmd_buf, debug_tri_pipeline);
    r_buffer_t* buffers[] = {debug_tri_buffer, debug_tri_buffer};
    uint32_t offsets[] = {0, 0};
    r_cmd_buf_bind_vertex_buffers(cmd_buf, 2, buffers, offsets);
    r_cmd_buf_set_push_constants(cmd_buf, 0, 64, &state->vp);
    
    while (tris_drawn != d_tri_count) {
        size_t tri_count = d_tri_count - tris_drawn;
        if (tri_count > MAX_DEBUG_TRIS_PER_DRAW) tri_count = MAX_DEBUG_TRIS_PER_DRAW;
        
        for (size_t i = 0; i < tri_count; i++) {
            d_tri_t tri = d_tris[tris_drawn+i];
            buf_data[i*18] = tri.a.x;
            buf_data[i*18+1] = tri.a.y;
            buf_data[i*18+2] = tri.a.z;
            buf_data[i*18+3] = tri.color[0];
            buf_data[i*18+4] = tri.color[1];
            buf_data[i*18+5] = tri.color[2];
            buf_data[i*18+6] = tri.b.x;
            buf_data[i*18+7] = tri.b.y;
            buf_data[i*18+8] = tri.b.z;
            buf_data[i*18+9] = tri.color[0];
            buf_data[i*18+10] = tri.color[1];
            buf_data[i*18+11] = tri.color[2];
            buf_data[i*18+12] = tri.c.x;
            buf_data[i*18+13] = tri.c.y;
            buf_data[i*18+14] = tri.c.z;
            buf_data[i*18+15] = tri.color[0];
            buf_data[i*18+16] = tri.color[1];
            buf_data[i*18+17] = tri.color[2];
        }
        
        r_cmd_buf_set_buffer_data(cmd_buf, debug_tri_buffer, 0, tri_count*24*3, buf_data);
        r_cmd_buf_draw(cmd_buf, tri_count*3, 1, 0, 0);
        
        tris_drawn += tri_count;
    }
    
    d_tri_clear();
    
    free(buf_data);
}

static void render_debug(state_t* state) {
    render_debug_lines(state);
    render_debug_tris(state);
}

static void render_gbuffer(state_t* state) {
    timing_begin(cmd_buf, &gbuffer_timing);
    
    r_cmd_buf_set_viewport(cmd_buf, 0, 0, cur_width, cur_height);
    
    framebuffer_image_barrier(cmd_buf, &gbuffer_albedo_image, R_IMAGE_COLOR_ATTACHMENT_OPTIMAL, R_IMAGE_COLOR_ATTACHMENT_WRITE);
    framebuffer_image_barrier(cmd_buf, &gbuffer_normal_image, R_IMAGE_COLOR_ATTACHMENT_OPTIMAL, R_IMAGE_COLOR_ATTACHMENT_WRITE);
    framebuffer_image_barrier(cmd_buf, &gbuffer_material_image, R_IMAGE_COLOR_ATTACHMENT_OPTIMAL, R_IMAGE_COLOR_ATTACHMENT_WRITE);
    framebuffer_image_barrier(cmd_buf, &gbuffer_depth_stencil_image, R_IMAGE_COLOR_ATTACHMENT_OPTIMAL, R_IMAGE_COLOR_ATTACHMENT_WRITE);
    
    r_clear_value_t clear_vals[8];
    memset(clear_vals, 0, sizeof(clear_vals));
    clear_vals[3].depthstencil.depth = 1;
    clear_vals[3].depthstencil.stencil = 0;
    r_cmd_buf_pass_begin(cmd_buf, gbuffer_pass, gbuffer_framebuffer, clear_vals);
    
    for (uint32_t i = 0; i < state->scene->obj_count; i++) {
        const r_obj_t* obj = &state->scene->objs[i];
        const r_mesh_t* mesh = obj->mesh;
        
        m_mat4_t mats[2];
        m_mat4_mul(&mats[0], &state->vp, &obj->mat);
        
        m_mat4_t temp_mat;
        m_mat4_inverse(&temp_mat, &obj->mat);
        m_mat4_transpose(&mats[1], &temp_mat);
        
        r_cmd_buf_bind_pipeline(cmd_buf, mesh->gbuffer_pipeline);
        r_buffer_t* vertex_buffers[] = {mesh->vertex_buffer, mesh->vertex_buffer, mesh->vertex_buffer, mesh->vertex_buffer};
        uint32_t vertex_buffer_offsets[] = {0, 0, 0, 0};
        r_cmd_buf_bind_vertex_buffers(cmd_buf, 4, vertex_buffers, vertex_buffer_offsets);
        r_cmd_buf_bind_index_buffer(cmd_buf, mesh->index_buffer, 0, R_INDEX_TYPE_UINT32);
        r_cmd_buf_bind_desc_set(cmd_buf, R_STAGE_FRAGMENT, mesh->material->desc_set);
        r_cmd_buf_set_push_constants(cmd_buf, 0, 128, mats);
        r_cmd_buf_draw_indexed(cmd_buf, mesh->index_count, 1, 0, 0, 0);
    }
    
    m_mat4_t view2 = state->view;
    for (uint32_t i = 0; i < 4; i++) view2.rows[3][i] = 0;
    for (uint32_t i = 0; i < 4; i++) view2.rows[i][3] = 0;
    view2.rows[3][3] = 1;
    m_mat4_t vp2;
    m_mat4_mul(&vp2, &state->proj, &view2);
    
    r_cmd_buf_bind_pipeline(cmd_buf, skybox_pipeline);
    r_cmd_buf_bind_desc_set(cmd_buf, R_STAGE_FRAGMENT, skybox_desc_set);
    r_cmd_buf_set_push_constants(cmd_buf, 0, 64, &vp2);
    r_cmd_buf_draw(cmd_buf, 36, 1, 0, 0);
    
    render_debug(state);
    
    r_cmd_buf_pass_end(cmd_buf);
    
    timing_end(cmd_buf, &gbuffer_timing);
}

static void render_lighting(state_t* state) {
    timing_begin(cmd_buf, &lighting_timing);
    
    r_cmd_buf_set_viewport(cmd_buf, 0, 0, cur_width, cur_height);
    
    float lighting_data[12];
    lighting_data[0] = state->cam_pos.x;
    lighting_data[1] = state->cam_pos.y;
    lighting_data[2] = state->cam_pos.z;
    lighting_data[4] = state->sun_dir.x;
    lighting_data[5] = state->sun_dir.y;
    lighting_data[6] = state->sun_dir.z;
    lighting_data[8] = state->sun_color.x;
    lighting_data[9] = state->sun_color.y;
    lighting_data[10] = state->sun_color.z;
    r_cmd_buf_set_buffer_data(cmd_buf, lighting_data_buffer, 0, sizeof(lighting_data), lighting_data);
    
    float lighting_pushconstants[16];
    memcpy(lighting_pushconstants, &state->vp.rows, 64); //TODO: m_mat4_inverse does not work so it is done in the shader
    r_cmd_buf_set_push_constants(cmd_buf, 0, 64, lighting_pushconstants);
    posteffect_do(cmd_buf, &lighting_posteffect);
    
    posteffect_do(cmd_buf, &nonlighting_posteffect);
    
    timing_end(cmd_buf, &lighting_timing);
}

void r_frame(const r_scene_t* scene) {
    if (scene->skybox != skybox) {
        skybox = scene->skybox;
        r_desc_t skybox_desc;
        skybox_desc.image_and_sampler.image = (r_image_t*)skybox;
        skybox_desc.image_and_sampler.sampler = linear_sampler;
        skybox_desc_set = r_desc_set_create(skybox_desc_set_layout, &skybox_desc);
    }
    
    int width, height;
    SDL_GL_GetDrawableSize(window, &width, &height);
    
    if (width!=cur_width || height!=cur_height) {
        delete_framebuffers();
        create_framebuffers(width, height);
    }
    
    state_t state;
    
    state.scene = scene;
    
    m_mat4_create_perspective(&state.proj, 45, width/(float)height, 0.1, 100);
    m_mat4_create_lookat(&state.view, &scene->cam_pos, &scene->cam_dir, &scene->cam_up);
    m_mat4_mul(&state.vp, &state.proj, &state.view);
    m_mat4_inverse(&state.ivp, &state.vp);
    
    state.cam_pos = scene->cam_pos;
    
    m_mat4_t sun_dir_mat;
    m_mat4_create_rotate(&sun_dir_mat, scene->sun->orientation.x, scene->sun->orientation.y,
                         scene->sun->orientation.z, scene->sun->orientation.w);
    m_vec3_t t0 = m_vec3_create(0, -1, 0);
    m_mat4_mul_vec3(&state.sun_dir, &sun_dir_mat, &t0);
    m_vec3_normalize(&state.sun_dir, &state.sun_dir);
    
    state.sun_color = m_vec3_create(scene->sun->sun.color[0] * scene->sun->sun.intensity,
                                    scene->sun->sun.color[1] * scene->sun->sun.intensity,
                                    scene->sun->sun.color[2] * scene->sun->sun.intensity);
    
    r_cmd_buf_begin(cmd_buf);
    timing_begin(cmd_buf, &frame_timing);
    
    render_gbuffer(&state);
    render_lighting(&state);
    
    r_clear_value_t clear_vals[8];
    memset(clear_vals, 0, sizeof(clear_vals));
    r_cmd_buf_pass_begin(cmd_buf, text_pass, text_framebuffer, clear_vals);
    draw_timings();
    r_cmd_buf_pass_end(cmd_buf);
    
    timing_end(cmd_buf, &frame_timing);
    r_cmd_buf_end(cmd_buf);
    r_cmd_buf_submit(1, &cmd_buf);
    
    r_frame_submit();
    
    timing_update();
}
