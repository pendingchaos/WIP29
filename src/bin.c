#define _DEFAULT_SOURCE
#include "physics.h"
#include "render.h"
#include "utils.h"
#include "bin.h"

#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <endian.h>
#include <stdio.h>
#include <zlib.h>

void b_del(bin_t* bin) {
    if (!bin) return;
    
    for (size_t i = 0; i < bin->scene_count; i++) {
        b_scene_t* scene = &bin->scenes[i];
        free(scene->name);
        
        for (size_t i = 0; i < scene->object_count; i++)
            free(scene->objects[i].name);
        free(scene->objects);
    }
    
    for (size_t i = 0; i < bin->mesh_count; i++) {
        b_mesh_t* mesh = &bin->meshes[i];
        free(mesh->name);
        free(mesh->positions);
        free(mesh->normals);
        free(mesh->tangents);
        free(mesh->uvs);
        free(mesh->indices);
        if (mesh->_render_mesh) r_mesh_del(mesh->_render_mesh);
    }
    
    for (size_t i = 0; i < bin->physical_shape_count; i++) {
        b_physical_shape_t* shape = &bin->physical_shapes[i];
        free(shape->name);
        if (shape->type == B_SHAPE_COMPOUND) {
            free(shape->shapes);
            free(shape->positions);
            free(shape->orientations);
        }
        if (shape->_physics_shape)
            p_shape_del(shape->_physics_shape);
    }
    
    for (size_t i = 0; i < bin->material_count; i++) {
        b_material_t* mat = &bin->materials[i];
        free(mat->name);
        free(mat->tex_filters);
        free(mat->tex_slots);
        free(mat->frag_data);
        if (mat->_render_material)
            r_material_del(mat->_render_material);
    }
    
    for (size_t i = 0; i < bin->shader_count; i++) {
        b_shader_t* shdr = &bin->shaders[i];
        free(shdr->name);
        free(shdr->spirv);
        free(shdr->glsl_source);
        if (shdr->_render_shader)
            r_shader_del(shdr->_render_shader);
    }
    
    for (size_t i = 0; i < bin->cubemap_count; i++) {
        b_cubemap_t* cubemap = &bin->cubemaps[i];
        free(cubemap->name);
        for (size_t face = 0; face < 6; face++)
            free(cubemap->data[face]);
        if (cubemap->_render_cubemap)
            r_cubemap_del(cubemap->_render_cubemap);
    }
    
    for (size_t i = 0; i < bin->texture_count; i++) {
        b_texture_t* tex = &bin->textures[i];
        free(tex->name);
        for (uint32_t i = 0; i < tex->mipmaps; i++) free(tex->data[i]);
        free(tex->data);
        if (tex->_render_texture)
            r_texture_del(tex->_render_texture);
    }
    
    free(bin->scenes);
    free(bin->meshes);
    free(bin->physical_shapes);
    free(bin->materials);
    free(bin->shaders);
    free(bin->cubemaps);
    free(bin->textures);
    free(bin);
}

typedef struct object_t {
    uint32_t size;
    uint32_t offset;
    void* data;
} object_t;

static size_t min_size_t(size_t a, size_t b) {
    return a < b ? a : b;
}

static void* read_data(FILE* stream, uint32_t amount) {
    void* data = NULL;
    size_t cur_data_size = 0;
    uint8_t buf[4096];
    size_t buf_used;
    
    while ((buf_used=fread(buf, 1, min_size_t(amount-cur_data_size, 4096), stream))) {
        u_arr_append(cur_data_size, (void**)&data, buf_used, buf);
        cur_data_size += buf_used;
    }
    
    if (ferror(stream) || cur_data_size!=amount) {
        free(data);
        return NULL;
    }
    
    return data;
}

static object_t read_object(FILE* stream, bool* success) {
    *success = true;
    
    uint8_t mode;
    if (!fread(&mode, 1, 1, stream)) goto error;
    
    uint32_t size;
    if (!fread(&size, 4, 1, stream)) goto error;
    size = le32toh(size);
    
    switch (mode) {
    case 0: {
        return (object_t){size, 0, read_data(stream, size)};
    }
    case 1: {
        uint32_t uncompressed_size;
        if (!fread(&uncompressed_size, 4, 1, stream)) goto error;
        uncompressed_size = le32toh(uncompressed_size);
        
        if (size > uncompressed_size) goto error;
        
        void* compressed = read_data(stream, size);
        void* uncompressed = malloc(uncompressed_size);
        
        uLongf res_size = uncompressed_size;
        if ((uncompress(uncompressed, &res_size, compressed, size)!=Z_OK) ||
            (res_size!=uncompressed_size)) {
            free(compressed);
            free(uncompressed);
            goto error;
        }
        
        free(compressed);
        
        return (object_t){uncompressed_size, 0, uncompressed};
    }
    default: {
        goto error;
    }
    }
    
    error:
        *success = false;
        return (object_t){0, 0, NULL};
}

static bool obj_read4(object_t* object, void* res) {
    if ((object->size-object->offset) < 4) return false;
    *(uint32_t*)res = *(uint32_t*)((uint8_t*)object->data+object->offset);
    *(uint32_t*)res = le32toh(*(uint32_t*)res);
    object->offset += 4;
    return true;
}

static bool obj_read1(object_t* object, void* res) {
    if ((object->size-object->offset) < 1) return false;
    *(uint8_t*)res = ((uint8_t*)object->data)[object->offset];
    object->offset++;
    return true;
}

static bool obj_read_str(object_t* object, char** res) {
    uint32_t len;
    if (!obj_read4(object, &len)) return false;
    
    if ((object->size-object->offset) < len) return false;
    
    *res = malloc(len+1);
    memcpy(*res, ((uint8_t*)object->data)+object->offset, len);
    (*res)[len] = 0;
    
    object->offset += len;
    
    return true;
}

static bool obj_read_data(object_t* object, uint32_t amount, void** res) {
    if ((object->size-object->offset) < amount) return false;
    
    *res = malloc(amount);
    memcpy(*res, ((uint8_t*)object->data)+object->offset, amount);
    
    object->offset += amount;
    
    return true;
}

bin_t* b_read(const char* filename) {
    FILE* stream = fopen(filename, "rb");
    if (!stream) return NULL;
    
    object_t obj;
    obj.data = NULL;
    
    bin_t* bin = calloc(1, sizeof(bin_t));
    if (!bin) goto error;
    
    if (!fread(&bin->texture_count, 4, 1, stream)) goto error;
    if (!fread(&bin->cubemap_count, 4, 1, stream)) goto error;
    if (!fread(&bin->shader_count, 4, 1, stream)) goto error;
    if (!fread(&bin->material_count, 4, 1, stream)) goto error;
    if (!fread(&bin->mesh_count, 4, 1, stream)) goto error;
    if (!fread(&bin->physical_shape_count, 4, 1, stream)) goto error;
    if (!fread(&bin->scene_count, 4, 1, stream)) goto error;
    bin->texture_count = le32toh(bin->texture_count);
    bin->cubemap_count = le32toh(bin->cubemap_count);
    bin->shader_count = le32toh(bin->shader_count);
    bin->material_count = le32toh(bin->material_count);
    bin->mesh_count = le32toh(bin->mesh_count);
    bin->physical_shape_count = le32toh(bin->physical_shape_count);
    bin->scene_count = le32toh(bin->scene_count);
    
    bin->scenes = calloc(bin->scene_count, sizeof(b_scene_t));
    bin->meshes = calloc(bin->mesh_count, sizeof(b_mesh_t));
    bin->physical_shapes = calloc(bin->physical_shape_count, sizeof(b_physical_shape_t));
    bin->materials = calloc(bin->material_count, sizeof(b_material_t));
    bin->shaders = calloc(bin->shader_count, sizeof(b_shader_t));
    bin->cubemaps = calloc(bin->cubemap_count, sizeof(b_cubemap_t));
    bin->textures = calloc(bin->texture_count, sizeof(b_texture_t));
    if (!bin->scenes) goto error;
    if (!bin->meshes) goto error;
    if (!bin->materials) goto error;
    if (!bin->shaders) goto error;
    if (!bin->cubemaps) goto error;
    if (!bin->textures) goto error;
    
    for (size_t i = 0; i < bin->texture_count; i++) {
        b_texture_t* tex = &bin->textures[i];
        
        bool success;
        obj = read_object(stream, &success);
        if (!success) goto error;
        
        if (!obj_read_str(&obj, &tex->name)) goto error;
        if (!obj_read4(&obj, &tex->width)) goto error;
        if (!obj_read4(&obj, &tex->height)) goto error;
        if (!obj_read4(&obj, &tex->mipmaps)) goto error;
        
        uint8_t fmt;
        if (!obj_read1(&obj, &fmt)) goto error;
        
        size_t fmt_size;
        switch (fmt) {
        case 0: tex->format = B_TEX_FMT_RGBA8_UNORM; fmt_size = 4; break;
        case 1: tex->format = B_TEX_FMT_RGB8_UNORM; fmt_size = 3; break;
        case 2: tex->format = B_TEX_FMT_SRGBA8_UNORM; fmt_size = 4; break;
        case 3: tex->format = B_TEX_FMT_SRGB8_UNORM; fmt_size = 3; break;
        default: goto error;
        }
        
        uint32_t w = tex->width;
        uint32_t h = tex->height;
        
        tex->data = calloc(tex->mipmaps, sizeof(void*));
        for (uint32_t j = 0; j < tex->mipmaps; j++) {
            size_t amount = w * h * fmt_size;
            if (!obj_read_data(&obj, amount, &tex->data[j])) goto error;
            if (w > 1) w /= 2;
            if (h > 1) h /= 2;
        }
        
        free(obj.data);
        obj.data = NULL;
    }
    
    for (size_t i = 0; i < bin->cubemap_count; i++) {
        b_cubemap_t* cubemap = &bin->cubemaps[i];
        
        bool success;
        obj = read_object(stream, &success);
        if (!success) goto error;
        
        if (!obj_read_str(&obj, &cubemap->name)) goto error;
        if (!obj_read4(&obj, &cubemap->width)) goto error;
        if (!obj_read4(&obj, &cubemap->height)) goto error;
        
        size_t amount = cubemap->width * cubemap->height * 3;
        for (size_t face = 0; face < 6; face++)
            if (!obj_read_data(&obj, amount, (void**)&cubemap->data[face])) goto error;
        
        free(obj.data);
        obj.data = NULL;
    }
    
    for (size_t i = 0; i < bin->shader_count; i++) {
        b_shader_t* shader = &bin->shaders[i];
        
        bool success;
        obj = read_object(stream, &success);
        if (!success) goto error;
        
        if (!obj_read_str(&obj, &shader->name)) goto error;
        
        uint8_t type;
        if (!obj_read1(&obj, &type)) goto error;
        switch (type) {
        case 0: shader->type = B_SHDR_TYPE_VERTEX; break;
        case 1: shader->type = B_SHDR_TYPE_FRAGMENT; break;
        case 2: shader->type = B_SHDR_TYPE_COMPUTE; break;
        default: goto error;
        }
        
        if (!obj_read_str(&obj, &shader->glsl_source)) goto error;
        
        if (!obj_read4(&obj, &shader->spirv_count)) goto error;
        shader->spirv_count = le32toh(shader->spirv_count) / 4;
        if (!obj_read_data(&obj, shader->spirv_count*4, (void**)&shader->spirv)) goto error;
        
        bool swap_endian = false;
        if (((uint8_t*)shader->spirv)[0] == 0x03) swap_endian = BYTE_ORDER == BIG_ENDIAN;
        else swap_endian = BYTE_ORDER == LITTLE_ENDIAN;
        
        for (size_t j = 0; j < swap_endian?shader->spirv_count:0; j++)
            #if BYTE_ORDER == BIG_ENDIAN
            shader->spirv[j] = le32toh(shader->spirv[j]);
            #else
            shader->spirv[j] = be32toh(shader->spirv[j]);
            #endif
        
        free(obj.data);
        obj.data = NULL;
    }
    
    for (size_t i = 0; i < bin->material_count; i++) {
        b_material_t* mat = &bin->materials[i];
        
        bool success;
        obj = read_object(stream, &success);
        if (!success) goto error;
        
        if (!obj_read_str(&obj, &mat->name)) goto error;
        
        uint32_t shader_index;
        if (!obj_read4(&obj, &shader_index)) goto error;
        if (shader_index >= bin->shader_count) goto error;
        
        mat->fragment = &bin->shaders[shader_index];
        
        if (!obj_read4(&obj, &mat->frag_data_size)) goto error;
        if (!obj_read_data(&obj, mat->frag_data_size, &mat->frag_data)) goto error;
        
        if (!obj_read4(&obj, &mat->tex_slot_count)) goto error;
        
        mat->tex_slots = malloc(mat->tex_slot_count*sizeof(b_texture_t*));
        mat->tex_filters = malloc(mat->tex_slot_count*sizeof(filter_t));
        for (size_t i = 0; i < mat->tex_slot_count; i++) {
            int32_t index;
            if (!obj_read4(&obj, &index)) goto error;
            if ((int64_t)index >= (int64_t)bin->texture_count) goto error;
            mat->tex_slots[i] = index<0 ? NULL : &bin->textures[index];
            
            uint8_t filter;
            if (!obj_read1(&obj, &filter)) goto error;
            switch (filter) {
            case 0: mat->tex_filters[i] = B_FILTER_NEAREST; break;
            case 1: mat->tex_filters[i] = B_FILTER_LINEAR; break;
            default: goto error;
            }
        }
        
        free(obj.data);
        obj.data = NULL;
    }
    
    for (size_t i = 0; i < bin->mesh_count; i++) {
        b_mesh_t* mesh = &bin->meshes[i];
        
        bool success;
        obj = read_object(stream, &success);
        if (!success) goto error;
        
        if (!obj_read_str(&obj, &mesh->name)) goto error;
        if (!obj_read4(&obj, &mesh->vertex_count)) goto error;
        if (!obj_read4(&obj, &mesh->index_count)) goto error;
        
        if (!obj_read_data(&obj, mesh->vertex_count*12, (void**)&mesh->positions)) goto error;
        if (!obj_read_data(&obj, mesh->vertex_count*12, (void**)&mesh->normals)) goto error;
        if (!obj_read_data(&obj, mesh->vertex_count*12, (void**)&mesh->tangents)) goto error;
        if (!obj_read_data(&obj, mesh->vertex_count*8, (void**)&mesh->uvs)) goto error;
        if (!obj_read_data(&obj, mesh->index_count*4, (void**)&mesh->indices)) goto error;
        
        for (size_t j = 0; j < mesh->index_count; j++)
            mesh->indices[j] = le32toh(mesh->indices[j]);
        
        uint32_t material_index;
        if (!obj_read4(&obj, &material_index)) goto error;
        if (material_index >= bin->material_count) goto error;
        mesh->material = &bin->materials[material_index];
        
        free(obj.data);
        obj.data = NULL;
    }
    
    for (size_t i = 0; i < bin->physical_shape_count; i++) {
        b_physical_shape_t* shape = &bin->physical_shapes[i];
        
        bool success;
        obj = read_object(stream, &success);
        if (!success) goto error;
        
        if (!obj_read_str(&obj, &shape->name)) goto error;
        
        uint8_t type;
        if (!obj_read1(&obj, &type)) goto error;
        switch (type) {
        case 0: shape->type = B_SHAPE_SPHERE; break;
        case 1: shape->type = B_SHAPE_BOX; break;
        case 2: shape->type = B_SHAPE_CYLINDER_X; break;
        case 3: shape->type = B_SHAPE_CYLINDER_Y; break;
        case 4: shape->type = B_SHAPE_CYLINDER_Z; break;
        case 5: shape->type = B_SHAPE_CAPSULE_X; break;
        case 6: shape->type = B_SHAPE_CAPSULE_Y; break;
        case 7: shape->type = B_SHAPE_CAPSULE_Z; break;
        case 8: shape->type = B_SHAPE_CONE_X; break;
        case 9: shape->type = B_SHAPE_CONE_Y; break;
        case 10: shape->type = B_SHAPE_CONE_Z; break;
        case 11: shape->type = B_SHAPE_HULL; break;
        case 12: shape->type = B_SHAPE_TRIANGLE_MESH; break;
        case 13: shape->type = B_SHAPE_BVH_TRIANGLE_MESH; break;
        case 14: shape->type = B_SHAPE_PLANE; break;
        case 15: shape->type = B_SHAPE_COMPOUND; break;
        }
        
        switch (shape->type) {
        case B_SHAPE_SPHERE:
            if (!obj_read4(&obj, &shape->radius)) goto error;
            break;
        case B_SHAPE_BOX:
            if (!obj_read4(&obj, &shape->half_extents.x)) goto error;
            if (!obj_read4(&obj, &shape->half_extents.y)) goto error;
            if (!obj_read4(&obj, &shape->half_extents.z)) goto error;
            break;
        case B_SHAPE_CYLINDER_X:
        case B_SHAPE_CYLINDER_Y:
        case B_SHAPE_CYLINDER_Z:
        case B_SHAPE_CAPSULE_X:
        case B_SHAPE_CAPSULE_Y:
        case B_SHAPE_CAPSULE_Z:
        case B_SHAPE_CONE_X:
        case B_SHAPE_CONE_Y:
        case B_SHAPE_CONE_Z:
            if (!obj_read4(&obj, &shape->radius)) goto error;
            if (!obj_read4(&obj, &shape->half_height)) goto error;
            break;
        case B_SHAPE_HULL:
        case B_SHAPE_TRIANGLE_MESH:
        case B_SHAPE_BVH_TRIANGLE_MESH: {
            uint32_t mesh_index;
            if (!obj_read4(&obj, &mesh_index)) goto error;
            if (mesh_index >= bin->mesh_count) goto error;
            shape->mesh = &bin->meshes[mesh_index];
            break;
        }
        case B_SHAPE_PLANE:
            if (!obj_read4(&obj, &shape->dist)) goto error;
            if (!obj_read4(&obj, &shape->normal.x)) goto error;
            if (!obj_read4(&obj, &shape->normal.y)) goto error;
            if (!obj_read4(&obj, &shape->normal.z)) goto error;
            break;
        case B_SHAPE_COMPOUND: { //TODO: Check for circular dependencies
            if (!obj_read4(&obj, &shape->count)) goto error;
            shape->shapes = malloc(shape->count*sizeof(b_physical_shape_t*));
            shape->positions = malloc(shape->count*sizeof(m_vec3_t));
            shape->orientations = malloc(shape->count*sizeof(m_quat_t));
            for (uint32_t j = 0; j < shape->count; j++) {
                uint32_t shape_index;
                if (!obj_read4(&obj, &shape_index)) goto error;
                if (shape_index >= bin->physical_shape_count) goto error;
                shape->shapes[j] = &bin->physical_shapes[shape_index];
                if (!obj_read4(&obj, &shape->positions[j].x)) goto error;
                if (!obj_read4(&obj, &shape->positions[j].y)) goto error;
                if (!obj_read4(&obj, &shape->positions[j].z)) goto error;
                if (!obj_read4(&obj, &shape->orientations[j].x)) goto error;
                if (!obj_read4(&obj, &shape->orientations[j].y)) goto error;
                if (!obj_read4(&obj, &shape->orientations[j].z)) goto error;
                if (!obj_read4(&obj, &shape->orientations[j].w)) goto error;
            }
            break;
        }
        }
        
        free(obj.data);
        obj.data = NULL;
    }
    
    for (size_t i = 0; i < bin->scene_count; i++) {
        b_scene_t* scene = &bin->scenes[i];
        
        bool success;
        obj = read_object(stream, &success);
        if (!success) goto error;
        
        if (!obj_read_str(&obj, &scene->name)) goto error;
        if (!obj_read4(&obj, &scene->object_count)) goto error;
        
        scene->objects = calloc(scene->object_count, sizeof(b_object_t));
        
        for (size_t j = 0; j < scene->object_count; j++) {
            b_object_t* s_obj = &scene->objects[j];
            
            if (!obj_read_str(&obj, &s_obj->name)) goto error;
            
            int32_t parent_index;
            if (!obj_read4(&obj, &parent_index)) goto error;
            
            if ((int64_t)parent_index >= (int64_t)scene->object_count)
                goto error;
            if (parent_index >= 0)
                s_obj->parent = &scene->objects[parent_index];
            
            if (!obj_read4(&obj, &s_obj->pos.x)) goto error;
            if (!obj_read4(&obj, &s_obj->pos.y)) goto error;
            if (!obj_read4(&obj, &s_obj->pos.z)) goto error;
            if (!obj_read4(&obj, &s_obj->orientation.x)) goto error;
            if (!obj_read4(&obj, &s_obj->orientation.y)) goto error;
            if (!obj_read4(&obj, &s_obj->orientation.z)) goto error;
            if (!obj_read4(&obj, &s_obj->orientation.w)) goto error;
            if (!obj_read4(&obj, &s_obj->scale.x)) goto error;
            if (!obj_read4(&obj, &s_obj->scale.y)) goto error;
            if (!obj_read4(&obj, &s_obj->scale.z)) goto error;
            
            uint8_t type;
            if (!obj_read1(&obj, &type)) goto error;
            
            switch (type) {
            case 0: { //TODO: Handle cycles
                s_obj->type = B_OBJ_SCENE;
                uint32_t scene_index;
                if (!obj_read4(&obj, &scene_index)) goto error;
                if (scene_index >= bin->scene_count) goto error;
                s_obj->scene = &bin->scenes[scene_index];
                break;
            }
            case 1: {
                s_obj->type = B_OBJ_COMPONENTS;
                
                int32_t mesh_index;
                if (!obj_read4(&obj, &mesh_index)) goto error;
                if ((int64_t)mesh_index >= (int64_t)bin->mesh_count) goto error;
                s_obj->components.visible_mesh = mesh_index < 0 ? NULL : &bin->meshes[mesh_index];
                
                uint8_t rigid_body_type;
                if (!obj_read1(&obj, &rigid_body_type)) goto error;
                switch (rigid_body_type) {
                case 0: s_obj->components.rigid_body_type = B_BODY_NONE; break;
                case 1: s_obj->components.rigid_body_type = B_BODY_STATIC; break;
                case 2: s_obj->components.rigid_body_type = B_BODY_DYNAMIC; break;
                default: goto error;
                }
                
                if (s_obj->components.rigid_body_type != B_BODY_NONE) {
                    uint32_t shape_index;
                    if (!obj_read4(&obj, &shape_index)) goto error;
                    s_obj->components.rigid_body_shape = &bin->physical_shapes[shape_index];
                    if (!obj_read4(&obj, &s_obj->components.rigid_body_mass)) goto error;
                }
                
                uint8_t has_sun_component;
                if (!obj_read1(&obj, &has_sun_component)) goto error;
                
                s_obj->components.has_sun_component = has_sun_component;
                
                if (s_obj->components.has_sun_component) {
                    if (!obj_read4(&obj, &s_obj->components.sun_color[0])) goto error;
                    if (!obj_read4(&obj, &s_obj->components.sun_color[1])) goto error;
                    if (!obj_read4(&obj, &s_obj->components.sun_color[2])) goto error;
                    if (!obj_read4(&obj, &s_obj->components.sun_intensity)) goto error;
                }
                
                int32_t skybox_index;
                if (!obj_read4(&obj, &skybox_index)) goto error;
                if ((int64_t)skybox_index >= (int64_t)bin->cubemap_count) goto error;
                s_obj->components.skybox = skybox_index < 0 ? NULL : &bin->cubemaps[skybox_index];
                break;
            }
            }
        }
        
        free(obj.data);
        obj.data = NULL;
    }
    
    fclose(stream);
    return bin;
    
    error:
        free(obj.data);
        fclose(stream);
        b_del(bin);
        return NULL;
}

static void write_obj_head(FILE* stream, uint32_t size) {
    uint8_t mode = 0;
    fwrite(&mode, 1, 1, stream);
    size = htole32(size);
    fwrite(&size, 4, 1, stream);
}

bool b_write(const char* filename, bin_t* bin) {
    FILE* stream = fopen(filename, "wb");
    if (!stream) return false;
    
    uint32_t texture_count = htole32(bin->texture_count);
    uint32_t cubemap_count = htole32(bin->cubemap_count);
    uint32_t shader_count = htole32(bin->shader_count);
    uint32_t material_count = htole32(bin->material_count);
    uint32_t physical_shape_count = htole32(bin->physical_shape_count);
    uint32_t mesh_count = htole32(bin->mesh_count);
    uint32_t scene_count = htole32(bin->scene_count);
    if (!fwrite(&texture_count, 4, 1, stream)) goto error;
    if (!fwrite(&cubemap_count, 4, 1, stream)) goto error;
    if (!fwrite(&shader_count, 4, 1, stream)) goto error;
    if (!fwrite(&material_count, 4, 1, stream)) goto error;
    if (!fwrite(&physical_shape_count, 4, 1, stream)) goto error;
    if (!fwrite(&mesh_count, 4, 1, stream)) goto error;
    if (!fwrite(&scene_count, 4, 1, stream)) goto error;
    
    for (size_t i = 0; i < bin->texture_count; i++) {
        b_texture_t* tex = &bin->textures[i];
        
        uint8_t fmt;
        size_t fmt_size = 0;
        switch (tex->format) {
        case B_TEX_FMT_RGBA8_UNORM: fmt = 0; fmt_size = 4; break;
        case B_TEX_FMT_RGB8_UNORM: fmt = 1; fmt_size = 3; break;
        case B_TEX_FMT_SRGBA8_UNORM: fmt = 2; fmt_size = 4; break;
        case B_TEX_FMT_SRGB8_UNORM: fmt = 3; fmt_size = 3; break;
        }
        
        uint32_t texels = 0;
        uint32_t w = tex->width;
        uint32_t h = tex->height;
        for (uint32_t j = 0; j < tex->mipmaps; j++) {
            texels += w * h;
            w /= 2;
            h /= 2;
            if (!w) w = 1;
            if (!h) h = 1;
        }
        
        write_obj_head(stream, texels*fmt_size+13+strlen(tex->name));
        
        uint32_t len = htole32(strlen(tex->name));
        if (!fwrite(&len, 4, 1, stream)) goto error;
        if (!fwrite(tex->name, len, 1, stream)) goto error;
        
        uint32_t width = htole32(tex->width);
        uint32_t height = htole32(tex->height);
        if (!fwrite(&width, 4, 1, stream)) goto error;
        if (!fwrite(&height, 4, 1, stream)) goto error;
        
        if (!fwrite(&fmt, 1, 1, stream)) goto error;
        
        w = tex->width;
        h = tex->height;
        for (uint32_t j = 0; j < tex->mipmaps; j++) {
            if (!fwrite(tex->data[j], w*h*fmt_size, 1, stream)) goto error;
            w /= 2;
            h /= 2;
            if (!w) w = 1;
            if (!h) h = 1;
        }
    }
    
    for (size_t i = 0; i < bin->cubemap_count; i++) {
        b_cubemap_t* cubemap = &bin->cubemaps[i];
        
        write_obj_head(stream, strlen(cubemap->name)+12+cubemap->width*cubemap->height*18);
        
        uint32_t len = htole32(strlen(cubemap->name));
        if (!fwrite(&len, 4, 1, stream)) goto error;
        if (!fwrite(cubemap->name, len, 1, stream)) goto error;
        
        uint32_t width = htole32(cubemap->width);
        uint32_t height = htole32(cubemap->height);
        if (!fwrite(&width, 4, 1, stream)) goto error;
        if (!fwrite(&height, 4, 1, stream)) goto error;
        
        size_t amount = cubemap->width * cubemap->height * 3;
        for (size_t face = 0; face < 6; face++)
            if (!fwrite(cubemap->data[face], amount, 1, stream) && amount)
                goto error;
    }
    
    for (size_t i = 0; i < bin->shader_count; i++) {
        b_shader_t* shader = &bin->shaders[i];
        
        write_obj_head(stream, strlen(shader->name)+strlen(shader->glsl_source)+shader->spirv_count*4+13);
        
        uint32_t len = htole32(strlen(shader->name));
        if (!fwrite(&len, 4, 1, stream)) goto error;
        if (!fwrite(shader->name, len, 1, stream)) goto error;
        
        uint8_t type;
        switch (shader->type) {
        case B_SHDR_TYPE_VERTEX: type = 0; break;
        case B_SHDR_TYPE_FRAGMENT: type = 1; break;
        case B_SHDR_TYPE_COMPUTE: type = 2; break;
        }
        if (!fwrite(&type, 1, 1, stream)) goto error;
        
        len = htole32(strlen(shader->glsl_source));
        if (!fwrite(&len, 4, 1, stream)) goto error;
        if (!fwrite(shader->glsl_source, len, 1, stream)) goto error;
        
        uint32_t spirv_size = htole32(shader->spirv_count * 4);
        if (!fwrite(&spirv_size, 4, 1, stream)) goto error;
        
        if (!fwrite(shader->spirv, spirv_size, 1, stream) && spirv_size) goto error;
    }
    
    for (size_t i = 0; i < bin->material_count; i++) {
        b_material_t* mat = &bin->materials[i];
        
        write_obj_head(stream, strlen(mat->name)+mat->frag_data_size+mat->tex_slot_count*5+12);
        
        uint32_t len = htole32(strlen(mat->name));
        if (!fwrite(&len, 4, 1, stream)) goto error;
        if (!fwrite(mat->name, len, 1, stream)) goto error;
        
        uint32_t shader_index = htole32(mat->fragment - bin->shaders);
        if (!fwrite(&shader_index, 4, 1, stream)) goto error;
        
        uint32_t frag_data_size = htole32(mat->frag_data_size);
        if (!fwrite(&frag_data_size, 4, 1, stream)) goto error;
        
        if (!fwrite(mat->frag_data, mat->frag_data_size, 1, stream) &&
            mat->frag_data_size) goto error;
        
        uint32_t tex_slot_count = htole32(mat->tex_slot_count);
        if (!fwrite(&tex_slot_count, 4, 1, stream)) goto error;
        
        for (size_t i = 0; i < mat->tex_slot_count; i++) {
            int32_t index = htole32(mat->tex_slots[i] ? mat->tex_slots[i]-bin->textures : -1);
            if (!fwrite(&index, 4, 1, stream)) goto error;
            
            uint8_t filter;
            switch (mat->tex_filters[i]) {
            case B_FILTER_NEAREST: filter = 0; break;
            case B_FILTER_LINEAR: filter = 1; break;
            }
            if (!fwrite(&filter, 1, 1, stream)) goto error;
        }
    }
    
    for (size_t i = 0; i < bin->mesh_count; i++) {
        b_mesh_t* mesh = &bin->meshes[i];
        
        write_obj_head(stream, strlen(mesh->name)+16+mesh->vertex_count*32+mesh->index_count*4);
        
        uint32_t len = htole32(strlen(mesh->name));
        if (!fwrite(&len, 4, 1, stream)) goto error;
        if (!fwrite(mesh->name, len, 1, stream)) goto error;
        
        uint32_t vertex_count = htole32(mesh->vertex_count);
        if (!fwrite(&vertex_count, 4, 1, stream)) goto error;
        uint32_t index_count = htole32(mesh->index_count);
        if (!fwrite(&index_count, 4, 1, stream)) goto error;
        
        if (!fwrite(mesh->positions, mesh->vertex_count*12, 1, stream))
            goto error;
        if (!fwrite(mesh->normals, mesh->vertex_count*12, 1, stream))
            goto error;
        if (!fwrite(mesh->uvs, mesh->vertex_count*8, 1, stream))
            goto error;
        
        for (uint32_t j = 0; j < mesh->index_count; j++) {
            uint32_t index = htole32(mesh->indices[j]);
            if (!fwrite(&index, 4, 1, stream))
                goto error;
        }
        
        uint32_t material_index = htole32(mesh->material - bin->materials);
        if (!fwrite(&material_index, 4, 1, stream)) goto error;
    }
    
    for (size_t i = 0; i < bin->physical_shape_count; i++) {
        b_physical_shape_t* shape = &bin->physical_shapes[i];
        
        uint32_t size = strlen(shape->name) + 5;
        switch (shape->type) {
        case B_SHAPE_SPHERE:
        case B_SHAPE_HULL:
        case B_SHAPE_TRIANGLE_MESH:
        case B_SHAPE_BVH_TRIANGLE_MESH: size += 4; break;
        case B_SHAPE_CYLINDER_X:
        case B_SHAPE_CYLINDER_Y:
        case B_SHAPE_CYLINDER_Z:
        case B_SHAPE_CAPSULE_X:
        case B_SHAPE_CAPSULE_Y:
        case B_SHAPE_CAPSULE_Z:
        case B_SHAPE_CONE_X:
        case B_SHAPE_CONE_Y:
        case B_SHAPE_CONE_Z: size += 8; break;
        case B_SHAPE_BOX: size += 12; break;
        case B_SHAPE_PLANE: size += 16; break;
        case B_SHAPE_COMPOUND: size += 4 + shape->count*32; break;
        }
        
        write_obj_head(stream, size);
        
        uint32_t len = htole32(strlen(shape->name));
        if (!fwrite(&len, 4, 1, stream)) goto error;
        if (!fwrite(shape->name, len, 1, stream)) goto error;
        
        uint8_t type;
        switch (shape->type) {
        case B_SHAPE_SPHERE: type = 0; break;
        case B_SHAPE_BOX: type = 1; break;
        case B_SHAPE_CYLINDER_X: type = 2; break;
        case B_SHAPE_CYLINDER_Y: type = 3; break;
        case B_SHAPE_CYLINDER_Z: type = 4; break;
        case B_SHAPE_CAPSULE_X: type = 5; break;
        case B_SHAPE_CAPSULE_Y: type = 6; break;
        case B_SHAPE_CAPSULE_Z: type = 7; break;
        case B_SHAPE_CONE_X: type = 8; break;
        case B_SHAPE_CONE_Y: type = 9; break;
        case B_SHAPE_CONE_Z: type = 10; break;
        case B_SHAPE_HULL: type = 11; break;
        case B_SHAPE_TRIANGLE_MESH: type = 12; break;
        case B_SHAPE_BVH_TRIANGLE_MESH: type = 13; break;
        case B_SHAPE_PLANE: type = 14; break;
        case B_SHAPE_COMPOUND: type = 15; break;
        }
        if (!fwrite(&type, 1, 1, stream)) goto error;
        
        switch (shape->type) {
        case B_SHAPE_SPHERE:
            if (!fwrite(&shape->radius, 4, 1, stream)) goto error;
            break;
        case B_SHAPE_BOX:
            if (!fwrite(&shape->half_extents.x, 4, 1, stream)) goto error;
            if (!fwrite(&shape->half_extents.y, 4, 1, stream)) goto error;
            if (!fwrite(&shape->half_extents.z, 4, 1, stream)) goto error;
            break;
        case B_SHAPE_CYLINDER_X:
        case B_SHAPE_CYLINDER_Y:
        case B_SHAPE_CYLINDER_Z:
        case B_SHAPE_CAPSULE_X:
        case B_SHAPE_CAPSULE_Y:
        case B_SHAPE_CAPSULE_Z:
        case B_SHAPE_CONE_X:
        case B_SHAPE_CONE_Y:
        case B_SHAPE_CONE_Z:
            if (!fwrite(&shape->radius, 4, 1, stream)) goto error;
            if (!fwrite(&shape->half_height, 4, 1, stream)) goto error;
            break;
        case B_SHAPE_HULL:
        case B_SHAPE_TRIANGLE_MESH:
        case B_SHAPE_BVH_TRIANGLE_MESH: {
            uint32_t index = htole32(shape->mesh - bin->meshes);
            if (!fwrite(&index, 4, 1, stream)) goto error;
            break;
        }
        case B_SHAPE_PLANE:
            if (!fwrite(&shape->dist, 4, 1, stream)) goto error;
            if (!fwrite(&shape->normal.x, 4, 1, stream)) goto error;
            if (!fwrite(&shape->normal.y, 4, 1, stream)) goto error;
            if (!fwrite(&shape->normal.z, 4, 1, stream)) goto error;
            break;
        case B_SHAPE_COMPOUND: {
            uint32_t count = htole32(shape->count);
            if (!fwrite(&count, 4, 1, stream)) goto error;
            for (uint32_t j = 0; j < count; j++) {
                uint32_t index = htole32(shape->shapes[j] - bin->physical_shapes);
                if (!fwrite(&index, 4, 1, stream)) goto error;
                if (!fwrite(&shape->positions[j].x, 4, 1, stream)) goto error;
                if (!fwrite(&shape->positions[j].y, 4, 1, stream)) goto error;
                if (!fwrite(&shape->positions[j].z, 4, 1, stream)) goto error;
                if (!fwrite(&shape->orientations[j].x, 4, 1, stream)) goto error;
                if (!fwrite(&shape->orientations[j].y, 4, 1, stream)) goto error;
                if (!fwrite(&shape->orientations[j].z, 4, 1, stream)) goto error;
                if (!fwrite(&shape->orientations[j].w, 4, 1, stream)) goto error;
            }
            break;
        }
        }
    }
    
    for (size_t i = 0; i < bin->scene_count; i++) {
        b_scene_t* scene = &bin->scenes[i];
        
        uint32_t size = strlen(scene->name) + 8;
        for (size_t j = 0; j < scene->object_count; j++) {
            b_object_t* obj = &scene->objects[j];
            size += strlen(obj->name) + 49;
            switch (obj->type) {
            case B_OBJ_COMPONENTS:
                size += 5;
                size += obj->components.rigid_body_type!=B_BODY_NONE ? 8 : 0;
                size += 1;
                size += obj->components.has_sun_component ? 16 : 0;
                size += 4;
                break;
            case B_OBJ_SCENE:
                size += 4;
                break;
            }
        }
        
        write_obj_head(stream, size);
        
        uint32_t len = htole32(strlen(scene->name));
        if (!fwrite(&len, 4, 1, stream)) goto error;
        if (!fwrite(scene->name, len, 1, stream)) goto error;
        
        uint32_t object_count = htole32(scene->object_count);
        if (!fwrite(&object_count, 4, 1, stream)) goto error;
        
        for (size_t j = 0; j < scene->object_count; j++) {
            b_object_t* obj = &scene->objects[j];
            
            uint32_t len = htole32(strlen(obj->name));
            if (!fwrite(&len, 4, 1, stream)) goto error;
            if (!fwrite(obj->name, len, 1, stream)) goto error;
            
            int32_t parent_index = htole32(obj->parent ? obj->parent-scene->objects : -1);
            if (!fwrite(&parent_index, 4, 1, stream)) goto error;
            
            if (!fwrite(&obj->pos, 12, 1, stream)) goto error;
            if (!fwrite(&obj->orientation, 16, 1, stream)) goto error;
            if (!fwrite(&obj->scale, 12, 1, stream)) goto error;
            
            uint8_t type;
            switch (obj->type) {
            case B_OBJ_SCENE: type = 0; break;
            case B_OBJ_COMPONENTS: type = 1; break;
            }
            if (!fwrite(&type, 1, 1, stream)) goto error;
            
            switch (obj->type) {
            case B_OBJ_SCENE: {
                uint32_t scene_index = htole32(obj->scene - bin->scenes);
                if (!fwrite(&scene_index, 4, 1, stream)) goto error;
                break;
            }
            case B_OBJ_COMPONENTS: {
                int32_t mesh_index = htole32(obj->components.visible_mesh ?
                                             obj->components.visible_mesh - bin->meshes : -1);
                if (!fwrite(&mesh_index, 4, 1, stream)) goto error;
                
                uint8_t rigid_body_type;
                switch (obj->components.rigid_body_type) {
                case B_BODY_NONE: rigid_body_type = 0; break;
                case B_BODY_STATIC: rigid_body_type = 1; break;
                case B_BODY_DYNAMIC: rigid_body_type = 2; break;
                }
                if (!fwrite(&rigid_body_type, 1, 1, stream)) goto error;
                
                if (obj->components.rigid_body_type != B_BODY_NONE) {
                    uint32_t shape_index = htole32(obj->components.rigid_body_shape - bin->physical_shapes);
                    if (!fwrite(&shape_index, 4, 1, stream)) goto error;
                    if (!fwrite(&obj->components.rigid_body_mass, 4, 1, stream)) goto error;
                }
                
                uint8_t has_sun_component = obj->components.has_sun_component ? 1 : 0;
                if (!fwrite(&has_sun_component, 1, 1, stream)) goto error;
                if (has_sun_component) {
                    if (!fwrite(&obj->components.sun_color, 12, 1, stream)) goto error;
                    if (!fwrite(&obj->components.sun_intensity, 4, 1, stream)) goto error;
                }
                
                int32_t skybox_index = htole32(obj->components.skybox ?
                                               obj->components.skybox - bin->cubemaps : -1);
                if (!fwrite(&skybox_index, 4, 1, stream)) goto error;
                break;
            }
            }
        }
    }
    
    fclose(stream);
    return true;
    
    error:
        fclose(stream);
        return false;
}

b_texture_t* b_find_texture(bin_t* bin, const char* name) {
    for (uint32_t i = 0; i < bin->texture_count; i++)
        if (!strcmp(bin->textures[i].name, name))
            return &bin->textures[i];
    return NULL;
}

b_cubemap_t* b_find_cubemap(bin_t* bin, const char* name) {
    for (uint32_t i = 0; i < bin->cubemap_count; i++)
        if (!strcmp(bin->cubemaps[i].name, name))
            return &bin->cubemaps[i];
    return NULL;
}

b_shader_t* b_find_shader(bin_t* bin, const char* name) {
    for (uint32_t i = 0; i < bin->shader_count; i++)
        if (!strcmp(bin->shaders[i].name, name))
            return &bin->shaders[i];
    return NULL;
}

b_material_t* b_find_material(bin_t* bin, const char* name) {
    for (uint32_t i = 0; i < bin->material_count; i++)
        if (!strcmp(bin->materials[i].name, name))
            return &bin->materials[i];
    return NULL;
}

b_physical_shape_t* b_find_physical_shape(bin_t* bin, const char* name) {
    for (uint32_t i = 0; i < bin->physical_shape_count; i++)
        if (!strcmp(bin->physical_shapes[i].name, name))
            return &bin->physical_shapes[i];
    return NULL;
}

b_mesh_t* b_find_mesh(bin_t* bin, const char* name) {
    for (uint32_t i = 0; i < bin->mesh_count; i++)
        if (!strcmp(bin->meshes[i].name, name))
            return &bin->meshes[i];
    return NULL;
}

b_scene_t* b_find_scene(bin_t* bin, const char* name) {
    for (uint32_t i = 0; i < bin->scene_count; i++)
        if (!strcmp(bin->scenes[i].name, name))
            return &bin->scenes[i];
    return NULL;
}

r_texture_t* b_get_r_texture(b_texture_t* texture) {
    if (texture->_render_texture) return texture->_render_texture;
    return texture->_render_texture = r_texture_create(texture);
}

r_cubemap_t* b_get_r_cubemap(b_cubemap_t* cubemap) {
    if (cubemap->_render_cubemap) return cubemap->_render_cubemap;
    return cubemap->_render_cubemap = r_cubemap_create(cubemap);
}

r_shader_t* b_get_r_shader(b_shader_t* shader) {
    if (shader->_render_shader) return shader->_render_shader;
    return shader->_render_shader = r_shader_create(shader);
}

p_shape_t* b_get_p_shape(b_physical_shape_t* shape) {
    if (shape->_physics_shape) return shape->_physics_shape;
    switch (shape->type) {
    case B_SHAPE_SPHERE:
        shape->_physics_shape = p_shape_create_sphere(shape->radius);
        break;
    case B_SHAPE_BOX:
        shape->_physics_shape = p_shape_create_box(shape->half_extents);
        break;
    case B_SHAPE_CYLINDER_X:
        shape->_physics_shape = p_shape_create_cylinder_x(shape->radius, shape->half_height);
        break;
    case B_SHAPE_CYLINDER_Y:
        shape->_physics_shape = p_shape_create_cylinder_y(shape->radius, shape->half_height);
        break;
    case B_SHAPE_CYLINDER_Z:
        shape->_physics_shape = p_shape_create_cylinder_z(shape->radius, shape->half_height);
        break;
    case B_SHAPE_CAPSULE_X:
        shape->_physics_shape = p_shape_create_capsule_x(shape->radius, shape->half_height);
        break;
    case B_SHAPE_CAPSULE_Y:
        shape->_physics_shape = p_shape_create_capsule_y(shape->radius, shape->half_height);
        break;
    case B_SHAPE_CAPSULE_Z:
        shape->_physics_shape = p_shape_create_capsule_z(shape->radius, shape->half_height);
        break;
    case B_SHAPE_CONE_X:
        shape->_physics_shape = p_shape_create_cone_x(shape->radius, shape->half_height);
        break;
    case B_SHAPE_CONE_Y:
        shape->_physics_shape = p_shape_create_cone_y(shape->radius, shape->half_height);
        break;
    case B_SHAPE_CONE_Z:
        shape->_physics_shape = p_shape_create_cone_z(shape->radius, shape->half_height);
        break;
    case B_SHAPE_HULL:
        shape->_physics_shape = p_shape_create_hull(shape->mesh->vertex_count,
                                                    (m_vec3_t*)shape->mesh->positions);
        break;
    case B_SHAPE_TRIANGLE_MESH: {
        m_vec3_t* positions = malloc(sizeof(m_vec3_t)*shape->mesh->vertex_count);
        for (uint32_t i = 0; i < shape->mesh->vertex_count; i++) {
            positions[i].x = shape->mesh->positions[i*3];
            positions[i].y = shape->mesh->positions[i*3+1];
            positions[i].z = shape->mesh->positions[i*3+2];
        }
        shape->_physics_shape = p_shape_create_triangle_mesh(shape->mesh->index_count/3,
                                                             shape->mesh->indices,
                                                             (m_vec3_t*)shape->mesh->positions);
        free(positions);
        break;
    }
    case B_SHAPE_BVH_TRIANGLE_MESH: {
        m_vec3_t* positions = malloc(sizeof(m_vec3_t)*shape->mesh->vertex_count);
        for (uint32_t i = 0; i < shape->mesh->vertex_count; i++) {
            positions[i].x = shape->mesh->positions[i*3];
            positions[i].y = shape->mesh->positions[i*3+1];
            positions[i].z = shape->mesh->positions[i*3+2];
        }
        shape->_physics_shape = p_shape_create_bvh_triangle_mesh(shape->mesh->index_count/3,
                                                                 shape->mesh->indices,
                                                                 (m_vec3_t*)shape->mesh->positions);
        free(positions);
        break;
    }
    case B_SHAPE_PLANE:
        shape->_physics_shape = p_shape_create_plane(shape->dist, shape->normal);
        break;
    case B_SHAPE_COMPOUND: {
        p_shape_t** shapes = malloc(shape->count*sizeof(p_shape_t*));
        for (uint32_t i = 0; i < shape->count; i++) shapes[i] = b_get_p_shape(shape->shapes[i]);
        shape->_physics_shape = p_shape_create_compound(shape->count,
                                                        (const p_shape_t**)shapes,
                                                        shape->positions,
                                                        shape->orientations);
        free(shapes);
        break;
    }
    }
    
    return shape->_physics_shape;
}

r_material_t* b_get_r_material(b_material_t* material) {
    if (material->_render_material) return material->_render_material;
    return material->_render_material = r_material_create(material);
}

r_mesh_t* b_get_r_mesh(b_mesh_t* mesh) {
    if (mesh->_render_mesh) return mesh->_render_mesh;
    return mesh->_render_mesh = r_mesh_create(mesh);
}
