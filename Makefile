CFLAGS = -pedantic -Wall -std=c11 -g -Og `sdl2-config --cflags` -Isrc -DGLFL_USE_SDL2 `pkg-config lua --cflags` `pkg-config zlib --cflags` `pkg-config bullet --cflags`
LDFLAGS = -lm `sdl2-config --libs` -lvulkan -ldl -rdynamic `pkg-config lua --libs` `pkg-config zlib --libs` `pkg-config bullet --libs`
RUN_OPT = runbin crytek_sponza.bin CrytekSponza

TARGET = $@
PREREQ1 = $<

src_c = $(wildcard src/*.c) $(wildcard src/gl/*.c)
src_cpp = $(wildcard src/*.cpp) $(wildcard src/gl/*.cpp)
src = $(src_c) $(src_cpp)
obj = $(join $(dir $(src_c)), $(addprefix ., $(notdir $(src_c:.c=.o))))\
	  $(join $(dir $(src_cpp)), $(addprefix ., $(notdir $(src_cpp:.cpp=.o))))
dep = $(obj:.o=.d)

shdr_src = $(wildcard shaders/*.glsl)
shdr = $(join $(dir $(shdr_src)), $(addprefix ., $(notdir $(shdr_src:.glsl=.bin))))

.PHONY: all
all: wip29

wip29: $(obj)
	$(CXX) $(obj) $(LDFLAGS) -o $(TARGET)

-include $(dep)

.%.d: %.c
	@# -M:  generate Makefile rule
	@# -MT: specify the rule's target
	$(CPP) $(CFLAGS) $(PREREQ1) -M -MT $(@:.d=.o) >$(TARGET)

.%.d: %.cpp
	@# -M:  generate Makefile rule
	@# -MT: specify the rule's target
	$(CPP) $(CFLAGS) $(PREREQ1) -M -MT $(@:.d=.o) >$(TARGET)

.%.o: %.c Makefile
	$(CC) -c $(PREREQ1) $(CFLAGS) -o $(TARGET)

.%.o: %.cpp Makefile
	$(CXX) -c $(PREREQ1) $(CFLAGS) -o $(TARGET)

.%.vs.bin: %.vs.glsl Makefile
	python converter/shader_compiler.py $(PREREQ1) $(PREREQ1) $(TARGET)

.%.fs.bin: %.fs.glsl Makefile
	python converter/shader_compiler.py $(PREREQ1) $(PREREQ1) $(TARGET)

shaders.bin: $(shdr)
	./wip29 combinebin shaders.bin $(shdr)
	./wip29 compressbin .shaders.bin.compressed shaders.bin
	rm shaders.bin
	mv .shaders.bin.compressed shaders.bin

%.cubemap.bin: %/posx.jpg %/negx.jpg %/posy.jpg %/negy.jpg %/posz.jpg %/negz.jpg
	python converter/convert_cubemap.py $(subst /,, $(dir $(PREREQ1))) $(TARGET) $(dir $(PREREQ1))posx.jpg $(dir $(PREREQ1))negx.jpg $(dir $(PREREQ1))posy.jpg $(dir $(PREREQ1))negy.jpg $(dir $(PREREQ1))posz.jpg $(dir $(PREREQ1))negz.jpg
	./wip29 compressbin .$(TARGET).compressed $(TARGET)
	rm $(TARGET)
	mv .$(TARGET).compressed $(TARGET)

crytek_sponza.bin: crytek_sponza/sponza.obj.cached
	python converter/convert_obj.py crytek_sponza/sponza.obj.cached .crytek_sponza.bin CrytekSponza 1 bvh_tri_mesh
	./wip29 compressbin crytek_sponza.bin .crytek_sponza.bin
	rm .crytek_sponza.bin

.PHONY: clean
clean:
	rm -f $(dep) $(obj) $(shdr) shaders.bin Yokohama.cubemap.bin wip29

.PHONY: valgrind
valgrind: wip29 Yokohama.cubemap.bin shaders.bin crytek_sponza.bin
	valgrind --track-origins=yes --leak-check=full --num-callers=500 ./wip29 $(RUN_OPT)

.PHONY: valgrind
callgrind: wip29 Yokohama.cubemap.bin shaders.bin crytek_sponza.bin
	valgrind --tool=callgrind ./wip29 $(RUN_OPT)

.PHONY: run
run: wip29 Yokohama.cubemap.bin shaders.bin crytek_sponza.bin
	./wip29 $(RUN_OPT)

.PHONY: run-opengl
run-opengl: wip29 Yokohama.cubemap.bin shaders.bin crytek_sponza.bin
	./wip29 $(RUN_OPT) --renderer gl

.PHONY: run-vulkan
run-vulkan: wip29 Yokohama.cubemap.bin shaders.bin crytek_sponza.bin
	./wip29 $(RUN_OPT) --renderer vk

.PHONY: apitrace
apitrace: wip29 Yokohama.cubemap.bin shaders.bin crytek_sponza.bin
	apitrace trace -o wip29.trace ./wip29 $(RUN_OPT)
	qapitrace wip29.trace
	rm -f wip29.trace
