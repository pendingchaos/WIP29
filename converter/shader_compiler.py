import sys, tempfile, os
from struct import pack, unpack

def compile(type, source):
    shader_src = '#version 430 core\n'
    if type == 'fragment':
        shader_src += '#define FRAGMENT\n'
    elif type == 'vertex':
        shader_src += '#define VERTEX\n'
    shader_src += '''#ifdef VULKAN
#ifdef VERTEX\n'''
    shader_src += ''.join('#define BINDING%d set=0,binding=%d\n' % (i, i) for i in range(16))
    shader_src += '#elif defined(FRAGMENT)\n'
    shader_src += ''.join('#define BINDING%d set=0,binding=%d\n' % (i, i) for i in range(16))
    shader_src += '#endif\n'
    shader_src += '''#else
#ifdef VERTEX\n'''
    shader_src += ''.join('#define BINDING%d binding=%d\n' % (i, i+16) for i in range(16))
    shader_src += '#elif defined(FRAGMENT)\n'
    shader_src += ''.join('#define BINDING%d binding=%d\n' % (i, i+16) for i in range(16))
    shader_src += '''#endif\n
#endif\n'''
    shader_src += '#line 1\n'
    shader_src += source

    if type == 'fragment':
        h, vulkan_glsl_file = tempfile.mkstemp('.frag')
    elif type == 'vertex':
        h, vulkan_glsl_file = tempfile.mkstemp('.vert')
    os.close(h)
    h, spirv_file = tempfile.mkstemp()
    os.close(h)
    with open(vulkan_glsl_file, 'w') as f: f.write(shader_src)
    os.unlink(spirv_file)
    os.system('/home/rugrats/Downloads/glslang/build/install/bin/glslangValidator -s -V %s -o %s' % (vulkan_glsl_file, spirv_file))
    
    if not os.path.exists(spirv_file):
        # Run again except this time not in silent mode
        os.system('/home/rugrats/Downloads/glslang/build/install/bin/glslangValidator -V %s -o %s' % (vulkan_glsl_file, spirv_file))
    
    os.unlink(vulkan_glsl_file)
    
    spirv = open(spirv_file, 'rb').read()
    os.unlink(spirv_file)
    if spirv[0] == '\x03':
        spirv = list(unpack('<%dL'%(len(spirv)/4), spirv))
    else:
        spirv = list(unpack('>%dL'%(len(spirv)/4), spirv))
    
    spirv = ''.join([pack('<L', v) for v in spirv])
    
    return shader_src, spirv

if __name__ == '__main__':
    if sys.argv[2][::-1].startswith('.fs.glsl'[::-1]):
        type = 'fragment'
    elif sys.argv[2][::-1].startswith('.vs.glsl'[::-1]):
        type = 'vertex'
    
    shader_src, spirv = compile(type, open(sys.argv[2]).read())
    
    output = open(sys.argv[3], 'wb')
    output.write(pack('<LLLLLLL', 0, 0, 1, 0, 0, 0, 0))
    
    data = pack('<L', len(sys.argv[1])) + sys.argv[1]
    if type == 'vertex':
        data += pack('<B', 0)
    elif type == 'fragment':
        data += pack('<B', 1)
    data += pack('<L', len(shader_src))
    data += shader_src
    data += pack('<L', len(spirv))
    data += spirv
    output.write(pack('<BL', 0, len(data)))
    output.write(data)
