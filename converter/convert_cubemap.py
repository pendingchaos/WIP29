import sys
from struct import pack, Struct
from PIL import Image

output = open(sys.argv[2], 'wb')

output.write(pack('<LLLLLLL', 0, 1, 0, 0, 0, 0, 0))

faces = [Image.open(f) for f in sys.argv[3:9]]

max_width = max([face.width for face in faces])
max_height = max([face.height for face in faces])

faces = [face.resize((max_width, max_height), Image.BICUBIC) for face in faces]

data = pack('<L', len(sys.argv[1])) + sys.argv[1]
data += pack('<LL', max_width, max_height)

pixel = Struct('<BBB')
for face in faces:
    data += ''.join([pixel.pack(*v) for v in face.getdata()])

output.write(pack('<BL', 0, len(data)))
output.write(data)
