import sys
import os.path
import cPickle as pickle
from parse import *
from struct import pack
import itertools
import shader_compiler
from PIL import Image

if sys.argv[1].endswith('.cached'):
    objects, materials, positions, texcoords, normals = pickle.load(open(sys.argv[1], 'rb'))
else:
    lines = open(sys.argv[1], 'r').read().split('\n')
    
    positions = []
    texcoords = []
    normals = []
    objects = {'_': {'name': '_', 'faces': []}}
    materials = {}
    cur_obj = '_'
    cur_material = None

    for line in lines:
        line = line.lstrip().rstrip().replace('  ', ' ')
        if len(line) == 0: continue
        if line.startswith('#'):
            pass
        elif line.split(' ')[0] == 'o':
            cur_obj = line[2:]
            objects[line[2:]] = {'name': cur_obj, 'faces': []}
            print 'Parsing', cur_obj
        elif line.split(' ')[0] == 'v':
            v = parse('v {:g} {:g} {:g}', line)
            if v:
                positions.append(v)
            else:
                v = tuple(parse('v {:g} {:g} {:g} {:g}', line))[:-1]
                positions.append(v)
        elif line.split(' ')[0] == 'vt':
            v = parse('vt {:g} {:g}', line)
            if v:
                texcoords.append(tuple(v))
            else:
                texcoords.append(tuple(parse('vt {:g} {:g} {:g}', line))[:-1])
        elif line.split(' ')[0] == 'vn':
            normals.append(tuple(parse('vn {:g} {:g} {:g}', line)))
        elif line.split(' ')[0] == 'f': # face vertex = (position, texcoord, normal)
                                        # face = (material, vertex0, vertex1, ...)
            vertices = []
            for v in line.split(' ')[1:]:            
                f = parse('{:d}', v)
                if f:
                    vertices.append((f[0], None, None))
                    continue
                f = parse('{:d}/{:d}', v)
                if f:
                    vertices.append((f[0], f[2], None))
                    continue
                f = parse('{:d}/{:d}/{:d}', v)
                if f:
                    vertices.append((f[0], f[1], f[2]))
                    continue
                f = parse('f {:d}//{:d}', v)
                if f:
                    vertices.append((f[0], None, f[1]))
                    continue
            objects[cur_obj]['faces'].append(tuple([cur_material]+vertices))
        elif line.split(' ')[0] == 'mtllib':
            fname = line[7:]
            fname = os.path.normpath(os.path.join(os.path.dirname(sys.argv[1]), fname))
            mtl_lines = open(fname, 'r').read().split('\n')
            cur_mat = None
            for line in mtl_lines:
                line = line.lstrip().rstrip()
                if len(line) == 0: continue
                if line.startswith('#'):
                    pass
                elif line.split(' ')[0] == 'newmtl':
                    cur_mat = {}
                    materials[line[7:]] = cur_mat
                    print 'Parsing', line[7:]
                elif line.split(' ')[0] == 'Ka': # TODO: Hack
                    cur_mat['metallic'] = 0 #parse('Ka {:g} {:g} {:g}', line)[0]
                elif line.split(' ')[0] == 'Kd':
                    cur_mat['albedo'] = tuple(parse('Kd {:g} {:g} {:g}', line))
                elif line.split(' ')[0] == 'Ks': # TODO: Hack
                    cur_mat['roughness'] = 1 #parse('Ks {:g} {:g} {:g}', line)[0]
                elif line.split(' ')[0] == 'map_Ka':
                    cur_mat['metallic_texture'] = line[7:]
                elif line.split(' ')[0] == 'map_Kd':
                    cur_mat['albedo_texture'] = line[7:]
                elif line.split(' ')[0] == 'map_Ns':
                    cur_mat['roughness_texture'] = line[7:]
                elif line.split(' ')[0] == 'map_bump':
                    cur_mat['normal_texture'] = line[9:]
                elif line.split(' ')[0] == 'bump':
                    cur_mat['normal_texture'] = line[5:]
        elif line.split(' ')[0] == 'usemtl':
            cur_material = materials[line[7:]]
    
    pickle.dump((objects, materials, positions, texcoords, normals), open(sys.argv[1]+'.cached', 'wb'))

meshes = []
for name, obj in objects.iteritems():
    print 'Getting meshes from', name
    cur_mesh = {'name': obj['name'], 'positions': [], 'normals': [], 'tangents': [], 'texcoords': [], 'material': None}
    for face in obj['faces']:
        if face[0] != cur_mesh['material'] and len(cur_mesh['positions']):
            meshes.append(cur_mesh)
            cur_mesh = {'name': obj['name']+str(id(face[0])), 'positions': [], 'normals': [], 'tangents': [], 'texcoords': [], 'material': face[0]}
        elif face[0] != cur_mesh['material']:
            cur_mesh['material'] = face[0]
        
        for i in xrange(len(face[1:])-2): # TODO: This does not seem to handle quads
            vertices = [face[i+1], face[i+2], face[i+3]]
            pos = [positions[vertices[j][0]-1] for j in xrange(3)]
            uv = [texcoords[vertices[j][1]-1] for j in xrange(3)]
            delta_pos1 = tuple([pos[1][j]-pos[0][j] for j in xrange(3)])
            delta_pos2 = tuple([pos[2][j]-pos[0][j] for j in xrange(3)])
            delta_uv1 = tuple([uv[1][j]-uv[0][j] for j in xrange(2)])
            delta_uv2 = tuple([uv[2][j]-uv[0][j] for j in xrange(2)])
            r = (delta_uv1[0]*delta_uv2[1] - delta_uv1[1]*delta_uv2[0])
            if r == 0: r = 0.0001
            tangent = ((delta_pos1[0]*delta_uv2[1] - delta_pos2[0]*delta_uv1[1]) / r,
                       (delta_pos1[1]*delta_uv2[1] - delta_pos2[1]*delta_uv1[1]) / r,
                       (delta_pos1[2]*delta_uv2[1] - delta_pos2[2]*delta_uv1[1]) / r)
            for p, t, v in zip(pos, uv, vertices):
                cur_mesh['positions'].append(p)
                cur_mesh['normals'].append(normals[v[2]-1])
                cur_mesh['tangents'].append(tangent)
                cur_mesh['texcoords'].append(t)
    if len(cur_mesh['positions']): meshes.append(cur_mesh)

for mesh in meshes:
    print 'Indexing', mesh['name']
    vert_to_index = {}
    positions = mesh['positions']
    normals = mesh['normals']
    tangents = mesh['tangents']
    texcoords = mesh['texcoords']
    mesh['positions'] = []
    mesh['normals'] = []
    mesh['tangents'] = []
    mesh['texcoords'] = []
    mesh['indices'] = []
    for i in range(len(positions)):
        vert = (positions[i], normals[i], texcoords[i])
        if vert in vert_to_index:
            index = vert_to_index[vert]
            mesh['indices'].append(index)
            tangent = mesh['tangents'][index]
            mesh['tangents'][index] = (tangents[0]+tangents[i][0],
                                       tangents[1]+tangents[i][1],
                                       tangents[2]+tangents[i][2])
        else:
            index = len(mesh['positions'])
            vert_to_index[index] = vert
            mesh['positions'].append(positions[i])
            mesh['normals'].append(normals[i])
            mesh['tangents'].append(tangents[i])
            mesh['texcoords'].append(texcoords[i])
            mesh['indices'].append(index)

textures = set()
for mat in materials.values():
    if 'metallic_texture' in mat:
        textures.add((mat['metallic_texture'], False))
    if 'roughness_texture' in mat:
        textures.add((mat['roughness_texture'], False))
    if 'normal_texture' in mat:
        textures.add((mat['normal_texture'], False))
    if 'albedo_texture' in mat:
        textures.add((mat['albedo_texture'], True))

scene_name = sys.argv[3]
mass = float(sys.argv[4])
shape_type = sys.argv[5]

output = open(sys.argv[2], 'wb')

if shape_type in ['hull', 'tri_mesh', 'bvh_tri_mesh']: shape_count = len(meshes)
else: shape_count = 1
output.write(pack('<LLLLLLL', len(textures), 0, len(materials.keys()), len(materials.keys()), len(meshes), shape_count, 1))

texture_to_index = {}
next_texture_index = 0
for texture in textures:
    print 'Writing', texture[0]
    
    texture_to_index[texture] = next_texture_index
    next_texture_index += 1
    
    name = scene_name + '::' + texture[0] + '::srgb' if texture[1] else '::linear'
    
    data = pack('<L', len(name)) + name
    
    image = Image.open(os.path.normpath(os.path.join(os.path.dirname(sys.argv[1]), texture[0])))
    image.load()
    if image.mode not in ['RGB', 'RGBA']:
        image = image.convert('RGBA')
    
    mipmaps = []
    w = image.width
    h = image.height
    while (w, h) != (1, 1):
        mipmaps.append(image.resize((w, h), Image.LANCZOS)) # TODO: Handle sRGB images
        if w > 1: w /= 2
        if h > 1: h /= 2
    
    data += pack('<LLL', image.width, image.height, len(mipmaps))
    
    if image.mode == 'RGB' and texture[1]:
        data += pack('<B', 3)
    elif image.mode == 'RGB':
        data += pack('<B', 1)
    elif image.mode == 'RGBA' and texture[1]:
        data += pack('<B', 2)
    elif image.mode == 'RGBA':
        data += pack('<B', 0)
    
    w = image.width
    h = image.height
    for mipmap in mipmaps:
        pixels = mipmap.tobytes() #TODO: This could work incorrectly for some image formats (or does image.load() fix that problem?)
        if image.mode == 'RGB': assert len(pixels) == w*h*3
        elif image.mode == 'RGBA': assert len(pixels) == w*h*4
        #pixels = list(itertools.chain.from_iterable(image.getdata()))
        #pixels = ''.join([chr(c) for c in pixels])
        if w > 1: w /= 2
        if h > 1: h /= 2
        data += pixels
    
    output.write(pack('<BL', 0, len(data)))
    output.write(data)

material_to_index = {}

next_material_index = 0
for name, mat in materials.iteritems():
    source = '''in vec3 vfNormal;
in vec3 vfTangent;
in vec2 vfUv;

layout(location=0) out vec3 oColor;
layout(location=1) out vec2 oMaterial;
layout(location=2) out vec3 oNormal;

%s
%s
%s
%s

vec3 getNormal(in vec3 normal, in vec3 tangent, in vec3 normalTS) {
    tangent = normalize(tangent - dot(tangent, normal)*normal);
    normal = mat3(tangent,
                  cross(tangent, normal),
                  normal) * normalTS;
    return normalize(normal);
}

void main() {
    vec4 albedo = %s * %s;
    if (albedo.a < 0.5) discard;
    oColor = albedo.rgb;
    oMaterial.r = %s * %s;
    oMaterial.g = %s * %s;
    oNormal = %snormalize(vfNormal)%s;
}
''' % ('layout(BINDING1) uniform sampler2D uMetallicTexture;' if 'metallic_texture' in mat else '',
       'layout(BINDING2) uniform sampler2D uRoughnessTexture;' if 'roughness_texture' in mat else '',
       'layout(BINDING3) uniform sampler2D uNormalTexture;' if 'normal_texture' in mat else '',
       'layout(BINDING4) uniform sampler2D uAlbedoTexture;' if 'albedo_texture' in mat else '',
       'vec4(%f, %f, %f, 1.0)' % mat.get('albedo', (1, 1, 1)),
       'texture(uAlbedoTexture, vfUv)' if 'albedo_texture' in mat else 'vec3(1.0)',
       '%f' % (mat.get('roughness', 1)),
       'texture(uRoughnessTexture, vfUv).r' if 'roughness_texture' in mat else '1.0',
       '%f' % (mat.get('metallic', 0)),
       'texture(uMetallicTexture, vfUv).r' if 'metallic_texture' in mat else '1.0',
       'getNormal(' if 'normal_texture' in mat else '',
       ', normalize(vfTangent), normalize(texture(uNormalTexture, vfUv).xyz*2-1))' if 'normal_texture' in mat else '')
    
    shader_src, spirv = shader_compiler.compile('fragment', source)
    
    data = pack('<L', len(scene_name+'::'+name+'::shader'))
    data += scene_name+'::'+name+'::shader'
    data += pack('<B', 1)
    data += pack('<L', len(shader_src))
    data += shader_src
    data += pack('<L', len(spirv))
    data += spirv
    output.write(pack('<BL', 0, len(data)))
    output.write(data)
    
    material_to_index[str(mat)] = next_material_index
    next_material_index += 1

i = 0
for name, mat in materials.iteritems():
    data = pack('<L', len(scene_name+'::'+name+'_material'))
    data += scene_name+'::'+name+'_material'
    data += pack('<LLL', i, 0, 4)
    
    if 'metallic_texture' in mat:
        data += pack('<lB', texture_to_index[(mat['metallic_texture'], False)], 1)
    else:
        data += pack('<lB', -1, 1)
    
    if 'roughness_texture' in mat:
        data += pack('<lB', texture_to_index[(mat['roughness_texture'], False)], 1)
    else:
        data += pack('<lB', -1, 1)
    
    if 'normal_texture' in mat:
        data += pack('<lB', texture_to_index[(mat['normal_texture'], False)], 1)
    else:
        data += pack('<lB', -1, 1)
    
    if 'albedo_texture' in mat:
        data += pack('<lB', texture_to_index[(mat['albedo_texture'], True)], 1)
    else:
        data += pack('<lB', -1, 1)
    
    output.write(pack('<BL', 0, len(data)))
    output.write(data)
    
    i += 1

for mesh in meshes:
    data = pack('<L', len(scene_name+'::'+mesh['name']+'::mesh'))
    data += scene_name+'::'+mesh['name']+'::mesh'
    data += pack('<L', len(mesh['positions']))
    data += pack('<L', len(mesh['indices']))
    data += ''.join([pack('<3f', *p) for p in mesh['positions']])
    data += ''.join([pack('<3f', *n) for n in mesh['normals']])
    data += ''.join([pack('<3f', *t) for t in mesh['tangents']])
    data += ''.join([pack('<2f', t[0], 1-t[1]) for t in mesh['texcoords']])
    data += ''.join([pack('<L', i) for i in mesh['indices']])
    data += pack('<L', material_to_index[str(mesh['material'])])
    
    output.write(pack('<BL', 0, len(data)))
    output.write(data)

if shape_type in ['hull', 'tri_mesh', 'bvh_tri_mesh']:
    for i, mesh in zip(range(len(meshes)), meshes):
        data = pack('<L', len(scene_name+'::'+mesh['name']+'::shape'))
        data += scene_name+'::'+mesh['name']+'::shape'
        data += pack('<B', {'hull':11, 'tri_mesh':12, 'bvh_tri_mesh':13}[shape_type])
        data += pack('<L', i)
        
        output.write(pack('<BL', 0, len(data)))
        output.write(data)
else:
    data = pack('<L', len(scene_name+'::shape'))
    data += scene_name+'::shape'
    if shape_type == 'sphere':
        data += pack('<B', 0)
        data += pack('<f', float(sys.argv[6]))
    elif shape_type == 'sphere':
        data += pack('<B', 1)
        data += pack('<f', float(sys.argv[6]))
        data += pack('<f', float(sys.argv[7]))
        data += pack('<f', float(sys.argv[8]))
    elif shape_type in ['cylinder_x', 'cylinder_y', 'cylinder_z',
                        'capsule_x', 'capsule_y', 'capsule_z',
                        'cone_x', 'cone_y', 'cone_z']:
        data += pack('<B', {'cylinder_x':2, 'cylinder_y':3, 'cylinder_z':4,
                            'capsule_x':5, 'capsule_y':6, 'capsule_z':7,
                            'cone_x':8, 'cone_y':9, 'cone_z':10}[shape_type])
        data += pack('<f', float(sys.argv[6]))
        data += pack('<f', float(sys.argv[7]))
    elif shape_type == 'plane':
        data += pack('<B', 14)
        data += pack('<f', float(sys.argv[6]))
        data += pack('<f', float(sys.argv[7]))
        data += pack('<f', float(sys.argv[8]))
        data += pack('<f', float(sys.argv[9]))
    
    output.write(pack('<BL', 0, len(data)))
    output.write(data)

data = pack('<L', len(scene_name))
data += scene_name
data += pack('<L', len(meshes))
for i in xrange(len(meshes)):
    data += pack('<L', len('obj%d' % i))
    data += 'obj%d' % i
    data += pack('<l', -1)
    data += pack('<3f', 0, 0, 0)
    data += pack('<4f', 0, 0, 0, 1)
    data += pack('<3f', 1, 1, 1)
    
    data += pack('<B', 1)
    
    data += pack('<l', i)
    
    data += pack('<B', 1)
    if shape_type in ['hull', 'tri_mesh', 'bvh_tri_mesh']:
        data += pack('<L', i)
    else:
        data += pack('<L', 0)
    data += pack('<f', mass)
    
    data += pack('<B', 0)
    data += pack('<l', -1)

output.write(pack('<BL', 0, len(data)))
output.write(data)
