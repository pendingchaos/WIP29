out gl_PerVertex {
    vec4 gl_Position;
};

layout(location=0) in vec3 aPosition;
layout(location=1) in vec3 aNormal;
layout(location=2) in vec3 aTangent;
layout(location=3) in vec2 aUv;

#ifdef VULKAN
layout(push_constant) uniform PushConstants {
    layout(row_major) mat4 MVPMatrix;
    layout(row_major) mat4 NormalWorldMatrix;
} u;
#define uMVPMatrix u.MVPMatrix
#define uNormalWorldMatrix u.NormalWorldMatrix
#else
layout(location=0) uniform float uPC[32];
#define uMVPMatrix mat4(uPC[0], uPC[4], uPC[8], uPC[12], uPC[1], uPC[5], uPC[9], uPC[13], uPC[2], uPC[6], uPC[10], uPC[14], uPC[3], uPC[7], uPC[11], uPC[15])
#define uNormalWorldMatrix mat4(uPC[16], uPC[20], uPC[24], uPC[28], uPC[17], uPC[21], uPC[25], uPC[29], uPC[18], uPC[22], uPC[26], uPC[30], uPC[19], uPC[23], uPC[27], uPC[31])
#endif

out vec3 vfNormal;
out vec3 vfTangent;
out vec2 vfUv;

void main() {
    gl_Position = uMVPMatrix * vec4(aPosition, 1.0);
    #ifdef VULKAN
    gl_Position.y *= -1;
    #endif
    vfNormal = mat3(uNormalWorldMatrix) * normalize(aNormal); //TODO: Is normalize() needed?
    vfTangent = mat3(uNormalWorldMatrix) * normalize(aTangent); //TODO: Is normalize() needed?
    vfUv = aUv;
}
