in vec2 vfUv;

layout(location=0) out vec4 oColor;

layout(BINDING0) uniform sampler2D uColorTexture;

#ifndef VULKAN
layout(location=0) uniform float uPC[32];
#endif

void main() {
    oColor.rgb = texture(uColorTexture, vfUv).rgb;
    oColor.a = 1.0;
}
