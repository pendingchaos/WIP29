in vec2 vfUv;

layout(location=0) out vec4 oColor;

layout(BINDING0) uniform sampler2D uFontTexture;

#ifdef VULKAN
layout(push_constant) uniform PushConstants {
    float Left;
    float Bottom;
    float Height;
    float Width;
    float TexcoordLeft;
    float Red;
    float Green;
    float Blue;
} u;
#define uColor vec3(u.Red, u.Green, u.Blue)
#else
layout(location=0) uniform float uPC[32];
#define uColor vec3(uPC[5], uPC[6], uPC[7])
#endif

void main() {
    if (texture(uFontTexture, vfUv).r < 0.5) discard;
    oColor = vec4(uColor, 1);
}
