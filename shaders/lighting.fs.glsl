in vec2 vfUv;

layout(location=0) out vec3 oColor;

layout(BINDING0) uniform sampler2D uAlbedoTexture;
layout(BINDING1) uniform sampler2D uNormalTexture;
layout(BINDING2) uniform sampler2D uMaterialTexture;
layout(BINDING3) uniform sampler2D uDepthTexture;
/*layout(binding=4) uniform sampler2DArray uSunShadowmap;
layout(location=9) uniform mat4 uSunShadowmapMatrix;*/
#ifdef VULKAN
layout(push_constant) uniform PushConstants {
    layout(row_major) mat4 InvViewProj;
    vec3 EyePos;
    vec3 SunDir;
    vec3 SunColor;
} u;
#define uInvViewProj u.InvViewProj
#else
layout(location=0) uniform float uPC[32];
#define uInvViewProj mat4(uPC[0], uPC[4], uPC[8], uPC[12], uPC[1], uPC[5], uPC[9], uPC[13], uPC[2], uPC[6], uPC[10], uPC[14], uPC[3], uPC[7], uPC[11], uPC[15])
#endif

layout(std140, BINDING4) uniform Uniforms {
    vec3 uEyePos;
    vec3 uSunDir;
    vec3 uSunColor;
};

vec3 mul(in mat4 mat, in vec3 v) {
    vec4 v4 = mat * vec4(v, 1);
    return v4.xyz / v4.w;
}

vec3 F_shlick(in vec3 specColor, in float vh) {
    return specColor + (1-specColor)*pow(1.0 - vh, 5.0);
}

float F_shlick(in float specColor, in float vh) {
    return specColor + (1-specColor)*pow(1.0 - vh, 5.0);
}

float D_ggx(in float nh, in float roughness) {
    float a2 = roughness * roughness;
    return a2 / (3.14159 * pow(nh*nh*(a2-1)+1, 2.0));
}

float G_Schlick(in float a, in float nv, in float nl) {
    return nv * nl;
    float a2 = a * a;
    return ((nv) / (nv + sqrt(a2+(1-a2)*nv*nv))) *
           ((nl) / (nl + sqrt(a2+(1-a2)*nl*nl)));
}

vec3 directSpecular(in float nl, in float nv, in float roughness, in float nh, in float lh, in vec3 specularColor) {
    float a2 = roughness * roughness;
    
    nv = max(nv, 0.0001);
    
    /*Unoptimized:
    float G = G_Schlick(roughness, nv, nl);
    float D = D_ggx(nh, roughness);
    vec3 F = F_shlick(specularColor, lh);
    return (D * G * F) / (4*nl*nv);*/
    
    /*Has artifacts but probably faster
    The artifacts have something to do with nv?
    Caused by moving `a2+(1-a2)` in G_Schlick to a variable
    float v = (a2-1)*nh*nh + 1;
    return -0.0795775387622191*(pow(-lh+1,5)*(specularColor-1)-specularColor)*a2/(v*v*nl*nv*4);*/
    
    float v0 = a2 - 1;
    float v1 = -v0;
    return -0.0795775387622191*(pow(-lh+1, 5)*(specularColor-1)-specularColor)*a2/(pow(v0*nh*nh+1, 2)*(nl+sqrt(v1*nl*nl+a2))*(nv+sqrt(v1*nv*nv+a2)));
}

/*float getShadowFactor(in vec3 positionSS, in vec2 offset) {
    positionSS.xy += offset;
    vec4 depths = textureGather(uSunShadowmap, vec3(floor(positionSS.xy)/vec2(1023.0), 0), 0);
    vec4 factors = step(depths, positionSS.zzzz);
    vec2 mf = fract(positionSS.xy);
    return mix(mix(factors.w, factors.z, mf.x),
               mix(factors.x, factors.y, mf.x), mf.y);
}*/

struct LightingShared {
    float nv;
    float roughness;
    float specularFactor;
    vec3 diffuseColor;
    vec3 specularColor;
    vec3 viewDir;
    vec3 normal;
    vec3 position;
};

void initLighting(out LightingShared lshared, in vec3 position, in vec3 viewDir, in vec3 albedo, in float metallic, in float linearRoughness, in vec3 normal) {
    lshared.nv = max(dot(normal, viewDir), 0.0);
    lshared.roughness = max(linearRoughness*linearRoughness, 0.01);
    lshared.specularFactor = mix(0.04, 1.0, metallic);
    lshared.diffuseColor = mix(albedo, vec3(0.0), lshared.specularFactor);
    lshared.specularColor = mix(vec3(0.04), albedo, lshared.specularFactor);
    lshared.viewDir = viewDir;
    lshared.normal = normal;
    lshared.position = position;
}

vec3 directLighting(in LightingShared lshared, in vec3 lightDir) {
    float nl = max(dot(lshared.normal, lightDir), 0.0);
    vec3 halfDir = normalize(lshared.viewDir + lightDir);
    float nh = max(dot(lshared.normal, halfDir), 0.0);
    float lh = max(dot(lightDir, halfDir), 0.0);
    float vn = max(dot(lshared.viewDir, lshared.normal), 0.0);
    
    vec3 diffuse = lshared.diffuseColor;
    vec3 specular = directSpecular(nl, lshared.nv, lshared.roughness, nh, lh, lshared.specularColor);
    
    /*vec3 positionSS = mul(uSunShadowmapMatrix, lshared.position);
    positionSS.z -= 0.005;
    positionSS.xy *= 1023;
    float shadow = 0.0;
    const int radius = 5;
    for (int x = -radius; x <= radius; x++)
        for (int y = -radius; y <= radius; y++)
            shadow += getShadowFactor(positionSS, vec2(x, y));
    shadow /= (radius*2+1)*(radius*2+1);*/
    
    //TODO: How correct is this?
    return (diffuse+specular) * nl;
    //return mix(diffuse, specular, F_shlick(lshared.specularFactor, lh)) * nl/* * min(nl, 1 - shadow)*/;
}

void main() {
    vec3 albedo = texture(uAlbedoTexture, vfUv).rgb;
    float depth = texture(uDepthTexture, vfUv).x;
    vec2 mat = texture(uMaterialTexture, vfUv).rg;
    float linearRoughness = mat.r;
    float metallic = mat.g;
    vec3 normal = normalize(texture(uNormalTexture, vfUv).xyz);
    
    vec3 position = mul(inverse(uInvViewProj), vec3(vfUv, depth)*2 - 1);
    vec3 viewDir = normalize(uEyePos - position);
    
    LightingShared lshared;
    initLighting(lshared, position, viewDir, albedo, metallic, linearRoughness, normal);
    
    oColor = directLighting(lshared, -uSunDir) * uSunColor;
}
