out gl_PerVertex {
    vec4 gl_Position;
};

layout(location=0) in vec3 aPosition;

#ifdef VULKAN
layout(push_constant) uniform PushConstants {
    layout(row_major) mat4 MVPMatrix;
} u;
#define uMVPMatrix u.MVPMatrix
#else
layout(location=0) uniform mat4 uMVPMatrix;
#endif

void main() {
    gl_Position = uMVPMatrix * vec4(aPosition, 1.0);
    #ifdef VULKAN
    gl_Position.y *= -1;
    #endif
}
