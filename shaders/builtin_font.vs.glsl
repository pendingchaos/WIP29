out vec2 vfUv;

#ifdef VULKAN
layout(push_constant) uniform PushConstants {
    float Left;
    float Bottom;
    float Height;
    float Width;
    float TexcoordLeft;
    float Red;
    float Green;
    float Blue;
} u;
#define uLeft u.Left
#define uBottom u.Bottom
#define uHeight u.Height
#define uWidth u.Width
#define uTexcoordLeft u.TexcoordLeft
#else
layout(location=0) uniform float uPC[32];
#define uLeft uPC[0]
#define uBottom uPC[1]
#define uHeight uPC[2]
#define uWidth uPC[3]
#define uTexcoordLeft uPC[4]
#endif

void main() {
    #ifdef VULKAN
    uint index = gl_VertexIndex;
    #else
    uint index = gl_VertexID;
    #endif
    vec2 corner = vec2[](vec2(0.0, 1.0), vec2(0.0, 0.0), vec2(1.0, 1.0), vec2(1.0, 0.0))[index];
    gl_Position = vec4(corner.x*uWidth+uLeft, corner.y*uHeight+uBottom, 0.0, 1.0);
    vfUv = vec2(uTexcoordLeft+corner.x*(8.0/1024.0), 1.0-corner.y);
}
