out gl_PerVertex {
    vec4 gl_Position;
};

#ifdef VULKAN
layout(push_constant) uniform PushConstants {
    layout(row_major) mat4 VPMatrix;
} u;
#define uVPMatrix u.VPMatrix
#else
layout(location=0) uniform float uPC[32];
#define uVPMatrix mat4(uPC[0], uPC[4], uPC[8], uPC[12], uPC[1], uPC[5], uPC[9], uPC[13], uPC[2], uPC[6], uPC[10], uPC[14], uPC[3], uPC[7], uPC[11], uPC[15])
#endif

out vec3 vfUv;

void main() {
    #ifdef VULKAN
    uint index = gl_VertexIndex;
    #else
    uint index = gl_VertexID;
    #endif
    vec3 position = vec3[](vec3(-1.0,  1.0, -1.0),
                           vec3(-1.0, -1.0, -1.0),
                           vec3( 1.0, -1.0, -1.0),
                           vec3( 1.0, -1.0, -1.0),
                           vec3( 1.0,  1.0, -1.0),
                           vec3(-1.0,  1.0, -1.0),
                           vec3(-1.0, -1.0,  1.0),
                           vec3(-1.0, -1.0, -1.0),
                           vec3(-1.0,  1.0, -1.0),
                           vec3(-1.0,  1.0, -1.0),
                           vec3(-1.0,  1.0,  1.0),
                           vec3(-1.0, -1.0,  1.0),
                           vec3( 1.0, -1.0, -1.0),
                           vec3( 1.0, -1.0,  1.0),
                           vec3( 1.0,  1.0,  1.0),
                           vec3( 1.0,  1.0,  1.0),
                           vec3( 1.0,  1.0, -1.0),
                           vec3( 1.0, -1.0, -1.0),
                           vec3(-1.0, -1.0,  1.0),
                           vec3(-1.0,  1.0,  1.0),
                           vec3( 1.0,  1.0,  1.0),
                           vec3( 1.0,  1.0,  1.0),
                           vec3( 1.0, -1.0,  1.0),
                           vec3(-1.0, -1.0,  1.0),
                           vec3(-1.0,  1.0, -1.0),
                           vec3( 1.0,  1.0, -1.0),
                           vec3( 1.0,  1.0,  1.0),
                           vec3( 1.0,  1.0,  1.0),
                           vec3(-1.0,  1.0,  1.0),
                           vec3(-1.0,  1.0, -1.0),
                           vec3(-1.0, -1.0, -1.0),
                           vec3(-1.0, -1.0,  1.0),
                           vec3( 1.0, -1.0, -1.0),
                           vec3( 1.0, -1.0, -1.0),
                           vec3(-1.0, -1.0,  1.0),
                           vec3( 1.0, -1.0,  1.0))[index];
    
    gl_Position = (uVPMatrix * vec4(position, 1.0)).xyww;
    #ifdef VULKAN
    gl_Position.y *= -1;
    #endif
    vfUv = normalize(position);
}
