in vec3 vfColor;

layout(location=0) out vec4 oColor;

#ifndef VULKAN
layout(location=0) uniform float uPC[32];
#endif

void main() {
    oColor.rgb = vfColor;
    oColor.a = 1.0;
}
