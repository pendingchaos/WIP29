out vec2 vfUv;

void main() {
    #ifdef VULKAN
    uint index = gl_VertexIndex;
    #else
    uint index = gl_VertexID;
    #endif
    gl_Position = vec4(vec2[](vec2(-1.0, 1.0),
                              vec2(-1.0, -1.0),
                              vec2(1.0, 1.0),
                              vec2(1.0, -1.0))[index], 0.0, 1.0);
    vfUv = gl_Position.xy * 0.5 + 0.5;
}
