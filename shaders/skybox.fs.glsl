in vec3 vfUv;

layout(location=0) out vec4 oColor;

layout(BINDING0) uniform samplerCube uSkyboxTexture;

#ifndef VULKAN
layout(location=0) uniform float uPC[32];
#endif

void main() {
    oColor.rgb = texture(uSkyboxTexture, vfUv).rgb;
    oColor.a = 1.0;
}
